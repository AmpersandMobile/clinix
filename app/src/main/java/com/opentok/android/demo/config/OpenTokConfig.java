package com.opentok.android.demo.config;

public class OpenTokConfig {

    // *** Fill the following variables using your own Project info from the OpenTok dashboard  ***
    // ***                      https://dashboard.tokbox.com/projects                           ***
    // Replace with a generated Session ID

    //NOTE, these get overwritten in networking class when the call to set up the call is done, GAZ.
    public static String SESSION_ID = "1_MX40NTI1MzQ5Mn5-MTQzNDEwNDA1NzUwNH5hZXNXeFQ5cHY2aTZNeWFZNHhKb0tnYjd-fg";
    // Replace with a generated token (from the dashboard or using an OpenTok server SDK)
    public static String TOKEN = "T1==cGFydG5lcl9pZD00NTI1MzQ5MiZzaWc9OWVmMDk0N2ZiMDkzMWNmNmFhYWI3OGRlZWNjN2U5ZmE3MDI3MjgwNzpyb2xlPXB1Ymxpc2hlciZzZXNzaW9uX2lkPTFfTVg0ME5USTFNelE1TW41LU1UUXpOREV3TkRBMU56VXdOSDVoWlhOWGVGUTVjSFkyYVRaTmVXRlpOSGhLYjB0bllqZC1mZyZjcmVhdGVfdGltZT0xNDM0MTA0MDg5Jm5vbmNlPTAuODk5MDIxMzUxMTY0MjIwOCZleHBpcmVfdGltZT0xNDM2Njk1NDU0JmNvbm5lY3Rpb25fZGF0YT0=";
    // Replace with your OpenTok API key
    public static final String API_KEY = "45226982";

    // Subscribe to a stream published by this client. Set to false to subscribe
    // to other clients' streams only.
    public static final boolean SUBSCRIBE_TO_SELF = true;


}
