/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.common.Networking;
import com.clinix.clinicare.clinixv2.common.Tools;
import com.clinix.clinicare.clinixv2.dataobjs.SaveKeys;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

// a simple register activity class
public class FirstTimeRegistrationAppInstructionsActivity extends Activity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    Context c;
    Activity a;

    Button btOK;
    TextView tvIAccept;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        _("FirstTimeRegistrationAppInstructionsActivity Born!////////////////////////////////");
        c = this;
        a = this;
        final Tools t = new Tools(c);
        t.setupPrefs(c);

        setContentView(R.layout.firsttimeinstructions);

        btOK      = (Button)    findViewById(R.id.btOK);
        btOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("btOK hit!");
                // this is exactly as if they had hit "change NHS surgery" inside surgery details
                Intent intent = new Intent(c, NearbyNHSSurgeriesActivity.class);
                c.startActivity(intent);
            }
        });

        //Make the texiews clickable too
        TextView tvA= (TextView)    findViewById(R.id.tvA);
        TextView tvB= (TextView)    findViewById(R.id.tvB);
        TextView tvC= (TextView)    findViewById(R.id.tvC);

        //each button goes to a standard text screen
        ImageButton ibButton1 = (ImageButton)    findViewById(R.id.ibButton1);
        ibButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ibButton1 hit!");
                GenericTextScreenActivity.textState = GenericTextScreenActivity.INTRO_SCREEN_A;
                Intent intent = new Intent(c, GenericTextScreenActivity.class);
                c.startActivity(intent);
            }
        });
        tvA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("tvA hit!");
                GenericTextScreenActivity.textState = GenericTextScreenActivity.INTRO_SCREEN_A;
                Intent intent = new Intent(c, GenericTextScreenActivity.class);
                c.startActivity(intent);
            }
        });

        ImageButton ibButton2 = (ImageButton)    findViewById(R.id.ibButton1);
        ibButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ibButton2 hit!");
                GenericTextScreenActivity.textState = GenericTextScreenActivity.INTRO_SCREEN_B;
                Intent intent = new Intent(c, GenericTextScreenActivity.class);
                c.startActivity(intent);
            }
        });
        tvB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("tvB hit!");
                GenericTextScreenActivity.textState = GenericTextScreenActivity.INTRO_SCREEN_B;
                Intent intent = new Intent(c, GenericTextScreenActivity.class);
                c.startActivity(intent);
            }
        });


        ImageButton ibButton3 = (ImageButton)    findViewById(R.id.ibButton1);
        ibButton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ibButton3 hit!");
                GenericTextScreenActivity.textState = GenericTextScreenActivity.INTRO_SCREEN_C;
                Intent intent = new Intent(c, GenericTextScreenActivity.class);
                c.startActivity(intent);
            }
        });
        tvC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("tvC hit!");
                GenericTextScreenActivity.textState = GenericTextScreenActivity.INTRO_SCREEN_C;
                Intent intent = new Intent(c, GenericTextScreenActivity.class);
                c.startActivity(intent);
            }
        });


        //for getting location if they wish to search for surgeries
        buildGoogleApiClient();

   }

    @Override
    public void onResume() {
        super.onResume();
        _("///////////////onResume/////////////");
       // Tools.sendAnalytics_ScreenView(this, "RegisterActivity");//do a screenview for analytics
    }


    //here we need to get location because as soon as they hit ok we want nto show them a list of local surgeries
    //get the users location on this screen so that its ready if they click change nhs surgery, and we dont have to wait

    /**
     * Builds a GoogleApiClient. Uses the addApi() method to request the LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }


    GoogleApiClient mGoogleApiClient;
    /**
     * Represents a geographical location.
     */
    protected Location mLastLocation;
    /**
     * Runs when a GoogleApiClient object successfully connects.
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        // Provides a simple way of getting a device's location and is well suited for
        // applications that do not require a fine-grained location and that do not need location
        // updates. Gets the best and most recent location currently available, which may be null
        // in rare cases when a location is not available.
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            double lat = mLastLocation.getLatitude();
            double lng = mLastLocation.getLongitude();
            NearbyNHSSurgeriesActivity.latS=""+lat;//we store them in here so theyre ready anyway
            NearbyNHSSurgeriesActivity.lngS=""+lng;
            _("Location found: LAT"+lat+", LNG"+lng);
        } else {
            Toast.makeText(this, "Can't detect location! Is your GPS turned on?", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        _("Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }


    @Override
    public void onConnectionSuspended(int cause) {
        // The connection to Google Play services was lost for some reason. We call connect() to
        // attempt to re-establish the connection.
        _("Connection suspended");
        mGoogleApiClient.connect();
    }



    //a wrapper so we dont have to type this every time
    private void _(String s)
    {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", "FirstTimeRegistrationAppInstructionsActivity" + "#######" + s);
    }
}
