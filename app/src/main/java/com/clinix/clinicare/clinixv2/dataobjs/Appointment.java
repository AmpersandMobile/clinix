package com.clinix.clinicare.clinixv2.dataobjs;

import android.util.Log;

import com.clinix.clinicare.clinixv2.common.Tools;

/**
 * Created by USER on 27/07/2015.
 */
public class Appointment
{
    public String dateFriendly, dateFriendlyAlt;
    public String personWith;
    public String type;
 //   public String id;


    //practitioner stuff
    public String data_id; // we use this as id
    public String data_practitioner_id;
    public String data_user_id;
    public String data_child_id;
    public String data_start;
    public String data_mode;
    public String data_problem;
    public String data_image;
    public String data_status;
    public String data_session_id;
    public String data_user_token;
    public String data_practitioner_token;


    public Appointment(
             String dateFriendly_, String dateFriendlyAlt_, String type_, String practionerName,
            String data_id_,String data_practitioner_id_,String data_user_id_,String data_child_id_,String data_start_,String data_mode_,String data_problem_,String data_image_,String data_status_,String data_session_id_,String data_user_token_,String data_practitioner_token_)//String date_, String type_,  String personWith_, String id_)
    {

        dateFriendly        = dateFriendly_;
        dateFriendlyAlt        = dateFriendlyAlt_;
        personWith  = practionerName;
        type        = type_;
    //    id          = id_;


        data_id=data_id_;
        data_practitioner_id=data_practitioner_id_;
        data_user_id=data_user_id_;
        data_child_id=data_child_id_;
        data_start=data_start_;
        data_mode=data_mode_;
        data_problem=data_problem_;
        data_image=data_image_;
        data_status=data_status_;
        data_session_id=data_session_id_;
        data_user_token=data_user_token_;
        data_practitioner_token=data_practitioner_token_;



        _("Appointment made! [id is "+data_id+"] dateFriendly:"+dateFriendly+", dateFriendlyAlt:"+dateFriendlyAlt+", "+personWith+", "+type);
    }

    //a wrapper so we dont have to type this every time
    private void _(String s)
    {
        if (!Tools.DEBUG){return;}
        Log.d("MyApp", "Appointment" + "#######" + s);
    }
}
