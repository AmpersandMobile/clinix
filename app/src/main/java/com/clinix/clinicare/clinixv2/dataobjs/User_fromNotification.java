package com.clinix.clinicare.clinixv2.dataobjs;

import android.util.Log;

import com.clinix.clinicare.clinixv2.common.Tools;

/**
 * Created by USER on 11/16/2015.
 */
public class User_fromNotification
{

    public String id                       ;
    public String title                    ;
    public String forename                 ;
    public String surname                  ;
    public String suffix                   ;
    public String email                    ;
    public String register_date            ;
    public String dob                      ;
    public String nhs_number               ;
    public String practitioner_id          ;
    public String surgery_id               ;
    public String custom_practitioner_id   ;
    public String custom_practitioner      ;
    public String custom_surgery_id        ;
    public String custom_surgery           ;
    public String telephone                ;
    public String street                   ;
    public String address_2                ;
    public String town                     ;
    public String county                   ;
    public String postcode                 ;
    public String country                  ;
    public String gender                   ;
    public String height_type              ;
    public String height                   ;
    public String weight_type              ;
    public String weight                   ;
    public String smoker                   ;
    public String image                    ;
    public String bmi                      ;
    public String blood                    ;
    public String type                     ;
    public String status                   ;

   //////IMAGE IS NOW USED//public String pathToPic; // we keep this purely for the user profile,
    public String password; // we keep this here also it doesnt seem to come from the profile but we will svae it locally for now, TODO: WORK OUT IF WE DO OR NOT.

    public User_fromNotification(
            String id_                       ,
            String title_                    ,
            String forename_                 ,
            String surname_                  ,
            String suffix_                   ,
            String email_                    ,
            String register_date_            ,
            String dob_                      ,
            String nhs_number_               ,
            String practitioner_id_          ,
            String surgery_id_               ,
            String custom_practitioner_id_   ,
            String custom_practitioner_      ,
            String custom_surgery_id_        ,
            String custom_surgery_           ,
            String telephone_                ,
            String street_                   ,
            String address_2_                ,
            String town_                     ,
            String county_                   ,
            String postcode_                 ,
            String country_                  ,
            String gender_                   ,
            String height_type_              ,
            String height_                   ,
            String weight_type_              ,
            String weight_                   ,
            String smoker_                   ,
            String image_                    ,
            String bmi_                      ,
            String blood_                    ,
            String type_                     ,
            String status_
    )
    {
        id                       = id_;
        title                    = title_;
        forename                 = forename_;
        surname                  = surname_;
        suffix                   = suffix_;
        email                    = email_;
        register_date            = register_date_;
        dob                      = dob_;
        nhs_number               = nhs_number_;
        practitioner_id          = practitioner_id_;
        surgery_id               = surgery_id_;
        custom_practitioner_id   = custom_practitioner_id_;
        custom_practitioner      = custom_practitioner_;
        custom_surgery_id        = custom_surgery_id_;
        custom_surgery           = custom_surgery_;
        telephone                = telephone_;
        street                   = street_;
        address_2                = address_2_;
        town                     = town_;
        county                   = county_;
        postcode                 = postcode_;
        country                  = country_;
        gender                   = gender_;
        height_type              = height_type_;
        height                   = height_;
        weight_type              = weight_type_;
        weight                   = weight_;
        smoker                   = smoker_;
        image                    = image_;
        bmi                      = bmi_;
        blood                    = blood_;
        type                     = type_;
        status                   = status_;
        _("User_fromNotification made!");
    }

    //a wrapper so we dont have to type this every time
    private void _(String s)
    {
        if (!Tools.DEBUG){return;}
        Log.d("MyApp", "User_fromNotification" + "#######" + s);
    }
}
