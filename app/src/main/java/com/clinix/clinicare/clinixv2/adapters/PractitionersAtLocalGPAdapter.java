/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.adapters;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.activities.ListOfGPsAtLocalSurgeryActivity;
import com.clinix.clinicare.clinixv2.activities.PractitionerProfilePageActivity;
import com.clinix.clinicare.clinixv2.activities.SurgeryDetailsActivity;
import com.clinix.clinicare.clinixv2.common.Tools;
import com.clinix.clinicare.clinixv2.dataobjs.Practitioner_fromNotification;

import java.util.ArrayList;

public class PractitionersAtLocalGPAdapter extends BaseAdapter {//} implements AdapterView.OnItemClickListener {

    public ArrayList<Practitioner_fromNotification> listOfItems;
    private LayoutInflater inflator;
    Context c;

    public PractitionersAtLocalGPAdapter(Context c_, ArrayList<Practitioner_fromNotification> theList){
        _("Adapter made.");
        listOfItems =theList;
        c=c_;
        inflator = LayoutInflater.from(c);

    }

    @Override
    public int getCount() {
        return listOfItems.size();
    }


    public ArrayList<Practitioner_fromNotification> getListOfItems()
    {
        _("getListOfItems is returning "+listOfItems.size()+" listOfItems.");
        return listOfItems;
    }

    @Override
    public Object getItem(int arg0) {
        return null;
    }

    @Override
    public long getItemId(int arg0) {
        return 0;
    }

    /*
    public void flagThisItemAsDefault(int pos, Activity a)
    {
        //ask them if they want to make this the default pharmacy
        Appointment p = listOfItems.get(pos);
        Tools.showDialog(a,
                "Pharmacy",
                "Are you sure you want to set "+p.name+" as your default pharmacy?",
                "Yes",
                "Cancel",
                Tools.DIALOG_SET_DEFAULT_PHARMACY);

        //set none to default first
        for (int i=0; i<listOfItems.size();i++)
        {
            p = listOfItems.get(i);
            p.isDefault=false;
        }
        //and flag the one we hit
        p = listOfItems.get(pos);
        _("->"+p.name);
        p.isDefault=true;
    }
*/
    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        //_("getView");
        //map to row_pharmacy layout
        LinearLayout rowLinearLayout    = (LinearLayout) inflator.inflate(R.layout.row_gpsatlocalsurgery, parent, false);
      //  final TextView toptitle         = (TextView)(rowLinearLayout).findViewById(R.id.toptitle);
        final TextView infobelow        = (TextView)(rowLinearLayout).findViewById(R.id.infobelow);
        final TextView tvDistance        = (TextView)(rowLinearLayout).findViewById(R.id.tvDistance);

        //get row_pharmacy using position
        final Practitioner_fromNotification currentObj = listOfItems.get(position);
        _("currentObj: name: " + currentObj.forename);
        //toptitle.setText(currentObj.suffix);
        infobelow.setText(currentObj.forename+" "+currentObj.surname);

        ImageView ivLittleI        = (ImageView)(rowLinearLayout).findViewById(R.id.ivLittleI);
        ivLittleI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ivLittleI tapped");

                //the little i shows the profilepage for the practioner
                Practitioner_fromNotification p = ListOfGPsAtLocalSurgeryActivity.itemListGPs.get(position);
                _("the practioner clicked on was: "+p.forename);

                Intent intent = new Intent(c, PractitionerProfilePageActivity.class);
                c.startActivity(intent);

            }
        });
        ImageView ibArrowIcon        = (ImageView)(rowLinearLayout).findViewById(R.id.ibArrowIcon);
        ibArrowIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ibArrowIcon tapped");
                //this takes us to surgery details//
                Intent intent = new Intent(c, SurgeryDetailsActivity.class);
                c.startActivity(intent);
                //
            }
        });


       // tvDistance.setText(currentObj.distance);

        /*CheckBox cbDefault = (CheckBox)(rowLinearLayout).findViewById(R.id.cbDefault);
        if (currentObj.isDefault)
        {
            _("this one is default "+currentObj.name);
            //show the tick on our default pharmacy
            cbDefault.setVisibility(View.VISIBLE);
            cbDefault.setChecked(true);
        }
        else
        {
            _("not default");
            cbDefault.setVisibility(View.INVISIBLE);
            cbDefault.setChecked(false);
        }*/

        return (rowLinearLayout);
    }

    //a wrapper so we dont have to type this every time
    private void _(String s)
    {
        if (!Tools.DEBUG){return;}
        Log.d("MyApp", "PractitionAdapter" + "#######" + s);
    }

   /* @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        _("onItemClick");
        Pharmacy p = listOfItems.get(position);
        _("->"+p.name);
        p.isDefault=true;
        ((PharmacyActivity)c).listView.invalidateViews();//refresh it

    }*/
}