/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.adapters;
import android.app.Notification;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.common.Tools;
import com.clinix.clinicare.clinixv2.dataobjs.Notification_;

import java.util.ArrayList;
import java.util.Date;

public class NotificationAdapter extends BaseAdapter implements AdapterView.OnItemLongClickListener {

    private ArrayList<Notification_> listOfItems;
    private LayoutInflater inflator;
    Context c;

    public NotificationAdapter(Context c_, ArrayList<Notification_> theList){
        _("Adapter made.");
        listOfItems =theList;
        c=c_;
        inflator =LayoutInflater.from(c);
    }

    @Override
    public int getCount() {
        return listOfItems.size();
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long arg3){
        _("onItemLongClick");
       // tmpImageView = (ImageView) view.findViewById(R.id.YOUR_IMAGEVIEW_ID);
        //tmpImageView.setVisibility(View.VISIBLE);
        return true;
    }

    public ArrayList<Notification_> getListOfItems()
    {
        _("getListOfItems is returning "+listOfItems.size()+" listOfItems.");
        return listOfItems;
    }

    @Override
    public Object getItem(int arg0) {
        return null;
    }

    @Override
    public long getItemId(int arg0) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        _("getView");
        LinearLayout rowLinearLayout = (LinearLayout) inflator.inflate(R.layout.row_notification, parent, false);

        final TextView toptitle = (TextView)(rowLinearLayout).findViewById(R.id.toptitle);
        final TextView infobelow = (TextView)(rowLinearLayout).findViewById(R.id.infobelow);

        final Notification_ currentObj = listOfItems.get(position);
        toptitle.setText(currentObj.message);
        infobelow.setText(currentObj.message);

        //work out date string and set
        TextView tvDate = (TextView)(rowLinearLayout).findViewById(R.id.tvDate);
        tvDate.setText(currentObj.dateArrived);/////////currentObj.getDateArrivedFriendlyString());     PUT HTIS BACK IF WE NEED NICE STIRNG BUT I THINK IT WAS A LONG NOW ITS NOT? CHECK OLD CODE?

        if (!currentObj.beenRead)
        {
            toptitle.setTextColor(0xffffffff);
            tvDate.setTextColor(0xffffffff);
        }
        else
        {
            toptitle.setTextColor(0xff8AEDF1);
            tvDate.setTextColor(0xff8AEDF1);
        }
        return (rowLinearLayout);
    }

    //a wrapper so we dont have to type this every time
    private void _(String s)
    {
        if (!Tools.DEBUG){return;}
        Log.d("MyApp", "NotificationAdapter" + "#######" + s);
    }
}