/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.common;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.opengl.Visibility;
import android.os.Build;
import android.os.Debug;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
/*
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
*/
import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.activities.ProfileActivity;
import com.clinix.clinicare.clinixv2.activities.SurgeryDetailsActivity;
import com.clinix.clinicare.clinixv2.common.cameratools.AlbumStorageDirFactory;
import com.clinix.clinicare.clinixv2.common.cameratools.BaseAlbumDirFactory;
import com.clinix.clinicare.clinixv2.common.cameratools.FroyoAlbumDirFactory;
import com.clinix.clinicare.clinixv2.common.views.DatePickerFragment;
import com.clinix.clinicare.clinixv2.dataobjs.SaveKeys;
import com.clinix.clinicare.clinixv2.dataobjs.UserProfile;
import com.pkmmte.view.CircularImageView;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Vector;

//Lots of common code that we want to conveniently reuse around the place. I keep it all neat and tidy in here to avoid code duplication etc.
public class Tools
{

    //POSTCODE GU15 1PA, CAMBERLY HEALTH CENTRE,
    public static final String VERSION="0.4.4";                         //19/05/2015 CAME BACK TO THIS ON 12 APRIL 2016, UDPATED TO 0.4.4
    public static boolean DEBUG = true;                                 //turn this off for release
    public static boolean HIDE_NETWORKING_RESULT_FOR_BREVITY=false;      //turn this to false if you wish to see raw networking response from server

    public static boolean USE_ANALYTICS=true;

    public static boolean SHOW_DEBUG_TOASTS                     = DEBUG; //turn this off for release (shows useful info about app for testing/debugging)

    // The following line should be changed to include the correct property id.
    public static final String GOOGLE_ANALYTICS_PROPERTY_ID = "UA-59889546-1";

    public static boolean PRETEND_TO_AT_CLIENTS_SURGERY_LOCATION=false;//good if youre not in uk for looking up local surgeries TURN OFF FOR RELEASE.
    //we assign this just before we need it
    public static Context c;

    //ARGB
    public static final int CLINIX_GREEN = 0xff1993a0;
    public static final int CLINIX_WHITE = 0xffffffff;


    //DIALOG STATES
    public static final int DIALOG_NHS_NUMBER               =       0;
    public static final int DIALOG_TERMS_AND_CONDITIONS     =       1;
    public static final int DIALOG_SET_DEFAULT_PHARMACY     =       2;
    public static final int DIALOG_SET_NEW_SURGERY_AND_GP     =       3;

    //MEDIA PLAYER -----------------------------------

    public static int getDurationInSeconds(MediaPlayer mp)
    {
        return mp.getDuration() / 1000;
    }

    public static int getCurrentPositionInSeconds(MediaPlayer mp)
    {
        return mp.getCurrentPosition() / 1000;
    }

    public Tools(Context c_)
    {
        c=c_;
    }

    //NETWORKING -----------------------

    public static StringBuffer executeNetworking_HTTPGET(String url)
    {
        _("executeNetworking_HTTPGET -> "+url);

        HttpURLConnection urlConnection=null;
        try {
            HttpClient client = new DefaultHttpClient();
            String getURL = url;
            HttpGet get = new HttpGet(getURL);
            get.addHeader("X-Clinix-API-Key", Networking.X_CLINIX_API_KEY_ANDROID);//
            get.addHeader("X-Clinix-Auth-Token", Networking.X_CLINIX_AUTH_TOKEN_ANDROID);
            _("X-Clinix-Auth-Token:"+Networking.X_CLINIX_AUTH_TOKEN_ANDROID);


           /////// get.addHeader("PartySession", getPartySession());

            HttpResponse responseGet = client.execute(get);
            HttpEntity resEntityGet = responseGet.getEntity();

            String response = "";
            if (resEntityGet != null) { //do something with the response
                response = EntityUtils.toString(resEntityGet);
                _("RESPONSE: : from calling "+url);
                _(""+response);
            }

            StringBuffer stringBuffer = new StringBuffer(response);
            return stringBuffer;
            // call this to decide it, same as others but we call explicitly here
           //// decodeResultIntoJson(response,state);
        }
        catch(IOException e)
        {
            if( urlConnection!=null)
            {
                _("ERROR---- HTTP EXCEPTION "+e.getMessage()+" --> "+urlConnection.getErrorStream());
            }
            else
            {
                _("MAJOR ERROR THE URL CONNECTION IS NULL!!!! "+e.getMessage());
            }
            e.printStackTrace();
        }
        _("Whoops ERROR occured returning null from executeNetworking_HTTPGET");
        return null;
    }

    //our standard method for executing posts
    public static StringBuffer executeNetworking_HTTPPOST(HttpPost request, List<NameValuePair> postParameters, HttpClient httpClient)
    {
        _("executeNetworking_HTTPPOST");
        BufferedReader bufferedReader = null;
        StringBuffer stringBuffer = new StringBuffer("");
        try {
            UrlEncodedFormEntity entity = new UrlEncodedFormEntity(postParameters);
            request.setEntity(entity);
            HttpResponse response = httpClient.execute(request);
            bufferedReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            String line = "";
            String LineSeparator = System.getProperty("line.separator");
            while ((line = bufferedReader.readLine()) != null) {
                stringBuffer.append(line + LineSeparator);
            }
            bufferedReader.close();
        } catch (Exception e) {
            _("error doing networking!! "+e.getMessage());
            Tools.toast("error doing networking!! "+e.getMessage(),c);
            e.printStackTrace();
        }
        if (HIDE_NETWORKING_RESULT_FOR_BREVITY)
        {
            _("NETWORK RESULT HIDDEN FOR BREVITY.");
        }
        else
        {
            _("result:"+stringBuffer);
        }
        return stringBuffer;
    }

    public static String convertStringLongIntoFriendlyDateString(String data_start)
    {
        String format = "dd-MM-yy HH:mm";
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.UK);
        long dateLong = Long.parseLong(data_start);
        Date d = new Date(dateLong * 1000L);//to get it out of 1970 you need to multiply the Unix timestamp by 1000

        String data_startFriendlyFormat = sdf.format(d);
        //_("data_startFriendlyFormat: "+data_startFriendlyFormat);
        return data_startFriendlyFormat;
    }

    public static String convertStringLongIntoFriendlyTimeString(String data_start)
    {
        String format = "HH:mm";
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.UK);
        long dateLong = Long.parseLong(data_start);
        Date d = new Date(dateLong * 1000L);//to get it out of 1970 you need to multiply the Unix timestamp by 1000

        String data_startFriendlyFormat = sdf.format(d);
        //_("data_startFriendlyFormat: "+data_startFriendlyFormat);
        return data_startFriendlyFormat;
    }

    public static String convertStringLongIntoFriendlyDateStringALT(String data_start)
    {
        String format = "EE dd MMMM yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.UK);
        long dateLong = Long.parseLong(data_start);
        Date d = new Date(dateLong * 1000L);//to get it out of 1970 you need to multiply the Unix timestamp by 1000

        String data_startFriendlyFormat = sdf.format(d);
        //_("data_startFriendlyFormat: "+data_startFriendlyFormat);
        return data_startFriendlyFormat;
    }
    //simple way to move on to where is appropriate after some networking, based on network state
    /*public static void moveOnToNextActivity(final int state, Context c_)
    {
        c=c_;
        //avoid ui thread nonsense
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(
                new Runnable()
                {
                    @Override
                    public void run()
                    {
                        switch (state)
                        {
                            case Networking.NETWORK_STATE_LOGIN:

                                _("NETWORK_STATE_LOGIN is now going to MusicPlayerActivity");
                                Intent intent = new Intent(c, PharmacyActivity.class);
                                c.startActivity(intent);
                                break;
                            case Networking.NETWORK_STATE_REGISTER:
                                _("NETWORK_STATE_REGISTER is now going to PharmacyActivity");
                                intent = new Intent(c, PharmacyActivity.class);//go straight into app not loginscreen
                                c.startActivity(intent);
                                break;

                            default:
                                _("//////////////Warning unknown state!////////////////");
                        }
                    }
                }
        );
    }*/

    /*
    //SLIDING MENU ------------------------------------
    //methods of convenience for the hamburger facebook style menu
    //so we dont have to have duplicated code around

    //attaches a hamburger menu to the passed in activity.
    public static SlidingMenu makeSlidingMenu(Activity a)
    {
        _("makeSlidingMenu");

        //we need the width of the imagebutton in dp, so we can slide menu out to left and only leave
        //enough room for menu button
        //  int x = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 65, a.getResources().getDisplayMetrics());
        //
        int screenWidth = a.getWindowManager().getDefaultDisplay().getWidth();
        int distanceMenuShouldSlideAlong = screenWidth-(screenWidth/6); // I am assuming the menu button takes up about a xth of width here

        // configure the SlidingMenu
        SlidingMenu sm = new SlidingMenu(a);
        sm.setMode(SlidingMenu.RIGHT);
        sm.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        sm.setShadowWidthRes(R.dimen.shadow_width);
        sm.setShadowDrawable(R.drawable.shadow);
        sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        sm.setFadeDegree(0.35f);
        sm.setBehindWidth(distanceMenuShouldSlideAlong);//a.getWindowManager().getDefaultDisplay().getWidth()/2);//how far the menu will slide out
        sm.attachToActivity(a, SlidingMenu.SLIDING_CONTENT);
        sm.setMenu(R.layout.slidey_menu_right);
        return sm;
    }
*/
    //for reacting to all hits on the hamburger menu:

    //this is used from multiple places to add stuff to the sliding menu without the need to duplicate code
    //we pass listOfSongsSyncedWithServer so we can use the debug functions for easy testing (ie. mimmickUserTappingAddOnAllSongsOnSiteDEBUG() )
    //or just pass null for it (ie release)
/*
    //pass in an activity with a sliding menu attached, and this will activate all the listeners to react to menu touch events
    public static void addListenersToSlidingMenu(final View view, final ArrayList<Song> listOfSongsSyncedWithServer, final Activity a)
    {
        //SLIDING MENU DETECT HITS///////////////////////////
        //detect hits on the menu///////////////////////////

        //EDIT SONG SELECTION-----------------------
        TextView tvEditSongSelection = (TextView)view.findViewById(R.id.tvEditSongSelection);
        tvEditSongSelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("tvEditSongSelection hit.");
               // Intent intent = new Intent(c, PharmacyActivity.class);
               // c.startActivity(intent);

                if (Tools.USE_ANALYTICS)
                {
                    Tools.sendAnalytics_ButtonPress(a,"tvEditSongSelection");
                }

                //this is not always MusicPlayerActivity, if it fails its the other one
                //ie class cast exception is caught
                try
                {
                    //AWESOME way to call an activity method!!
                    ((MusicPlayerActivity)c).backPressed();//do it like this so it cancels the right thread etc.
                }
                catch(ClassCastException e)
                {
                    _("class cast exception, "+e.getMessage());
                    //this means it the PharmacyActivity  and not MusicPlayerActivity, but I dont think we have to do anyhting here
                    //since we are already on this screen
                }

            }
        });

        //HELP SCREEN-----------------------
        TextView tvHelp = (TextView)view.findViewById(R.id.tvHelp);
        tvHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("tvHelp hit.");

                if (Tools.USE_ANALYTICS)
                {
                    Tools.sendAnalytics_ButtonPress(a,"tvHelp");
                }

                //- SEND THEM TO THE HELP SCREEN
                Intent intent = new Intent(c, HelpActivity.class);
                c.startActivity(intent);
            }
        });

        //LOG OUT---------------------------------
        TextView tvLogOut = (TextView)view.findViewById(R.id.tvLogOut);
        tvLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("tvLogOut hit.");

                if (Tools.USE_ANALYTICS)
                {
                    Tools.sendAnalytics_ButtonPress(a,"tvLogOut");
                }
                //LOG OUT SHOULD:
                //- WIPE WHAT WE KNOW ABOUT THEM (or it would auto log them back in immediately)

                //0.2.0 actually lets not wipe anything, if they have ticked remember me then itll work, if they havent then it wont
                //Tools t = new Tools();
                //t.setupPrefs(c);
                //t.wipeEveryThingWeKnowAboutUser();

                //kill any current party
                logoutHit(a);




            }
        });

        //FEEDBACK SCREEN-----------------------
        TextView tvFeedback = (TextView)view.findViewById(R.id.tvFeedback);
        tvFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("tvFeedback hit.");

                if (Tools.USE_ANALYTICS)
                {
                    Tools.sendAnalytics_ButtonPress(a,"tvFeedback");
                }

                openEmailClientToSendUsFeedback();
            }
        });

        //DEBUG ONES---------------------------
        boolean ADD_DEBUG_MENU_ITEMS = SHOW_DEBUG_SECRET_MENU_ITEMS; //turn this off for release (turn it on for easy testing :))

        // TOGGLE DEBUG ---------------------------
        TextView tvToggleDebug = (TextView)view.findViewById(R.id.tvToggleDebug);
        tvToggleDebug.setText("Toggle Debug (DEBUG)");
        tvToggleDebug.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("Toggle Debug");

                if (Tools.USE_ANALYTICS)
                {
                    Tools.sendAnalytics_ButtonPress(a,"tvToggleDebug");
                }

                //simply hide the purple debug boxes
                LinearLayout toggleDebugHolder = (LinearLayout)view.findViewById(R.id.debugHolder);//toggleDebugHolder);
                if (toggleDebugHolder.getVisibility()==View.GONE)
                {
                    toggleDebugHolder.setVisibility(View.VISIBLE);
                    Tools.SHOW_DEBUG_TOASTS=true;
                    Tools.toast("Showing debug player info and toasts",c);
                }
                else
                {
                    toggleDebugHolder.setVisibility(View.GONE);
                    Tools.SHOW_DEBUG_TOASTS=false;
                    Tools.toast("Hiding debug player info and toasts",c);
                }

                //legacy, this used to be a call to end party, that happens automatically now when we go back to music selection screen
                //but if we ever need an explicit call on the debug menu we can add it back here.


            }
        });
        if (!ADD_DEBUG_MENU_ITEMS)
        {
            //we want to hide the whole thing so we grab the linear that holds it and hide that
            LinearLayout toggleDebugHolder = (LinearLayout)view.findViewById(R.id.toggleDebugHolder);
            toggleDebugHolder.setVisibility(View.GONE);
        }

        //CRASH ME----------------------------
        TextView tvCrashMe = (TextView)view.findViewById(R.id.tvCrashMe);
        tvCrashMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("/////////////tvCrashMe hit/////////");

                if (Tools.USE_ANALYTICS)
                {
                    Tools.sendAnalytics_ButtonPress(a,"tvCrashMe");
                }

                //CRASH ON PURPOSE FOR DEBUGGING (it grabs logcat so when anyone finds an issue if they
                //hit crash me on purpose they can send me the logcat for diagnosis, VERY useful way to debug in the field so to speak
                Tools.toast("Good bye cruel world!",c);
                Networking n = null;
                n.getLivePlayList();

            }
        });
        if (!ADD_DEBUG_MENU_ITEMS)
        {
            //we want to hide the whole thing so we grab the linear that holds it and hide that
            LinearLayout crashMeOnPurposeHolder = (LinearLayout)view.findViewById(R.id.crashMeOnPurposeHolder);
            crashMeOnPurposeHolder.setVisibility(View.GONE);
        }

        //RACK UP ALL TUNES ON SITE----------------------------
        TextView tvMimicUserSelectingAll = (TextView)view.findViewById(R.id.tvMimicUserSelectingAll);
        tvMimicUserSelectingAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("/////////////tvMimicUserSelectingAll hit/////////");
                _("/////////////tvMimicUserSelectingAll hit/////////");
                _("/////////////tvMimicUserSelectingAll hit/////////");
                _("/////////////tvMimicUserSelectingAll hit/////////");
                _("/////////////tvMimicUserSelectingAll hit/////////");

                if (Tools.USE_ANALYTICS)
                {
                    Tools.sendAnalytics_ButtonPress(a,"tvMimicUserSelectingAll");
                }

                //instead of us manually going to our playlist on the website and laboriously tapping add on each one
                //this method will go through all the songs and do it for us, speeds up testing insanely.
                mimmickUserTappingAddOnAllSongsOnSiteDEBUG(false,listOfSongsSyncedWithServer);//NO AUTO SKIP,

            }
        });
        if (!ADD_DEBUG_MENU_ITEMS)
        {
            //we want to hide the whole thing so we grab the linear that holds it and hide that
            LinearLayout rackupAllTunesOnSiteHolder = (LinearLayout)view.findViewById(R.id.rackupAllTunesOnSiteHolder);
            rackupAllTunesOnSiteHolder.setVisibility(View.GONE);
        }

        //RACK UP ALL TUNES ON SITE (AND AUTO SKIP)----------------------------
        TextView tvMimicUserSelectingAllAndSkip = (TextView)view.findViewById(R.id.tvMimicUserSelectingAllAndSkip);
        tvMimicUserSelectingAllAndSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("/////////////tvMimicUserSelectingAll hit/////////");
                _("/////////////tvMimicUserSelectingAll hit/////////");
                _("/////////////tvMimicUserSelectingAll hit/////////");
                _("/////////////tvMimicUserSelectingAll hit/////////");
                _("/////////////tvMimicUserSelectingAll hit/////////");

                if (Tools.USE_ANALYTICS)
                {
                    Tools.sendAnalytics_ButtonPress(a,"tvMimicUserSelectingAllAndSkip");
                }

                //same as above, for racking up all songs, but also will auto slip each track to speed up testing
                //so we dont have to sit through the whole song, the auto skipping logic is explained elsewhere
                //but basically it skips on each 10th poll to server.

                //this ends up in a big loop calling networking etc, we do it in its own thread
                new Thread(new Runnable() {
                    public void run(){
                        mimmickUserTappingAddOnAllSongsOnSiteDEBUG(true,listOfSongsSyncedWithServer);// AUTO SKIP ON...!
                    }
                }).start();


            }
        });
        if (!ADD_DEBUG_MENU_ITEMS)
        {
            //we want to hide the whole thing so we grab the linear that holds it and hide that
            LinearLayout rackupAllTunesOnSiteAndSkipHolder = (LinearLayout)view.findViewById(R.id.rackupAllTunesOnSiteAndSkipHolder);
            rackupAllTunesOnSiteAndSkipHolder.setVisibility(View.GONE);
        }

        //SET NEW POLLING INTERVAL----------------------------
        final EditText etPollingInterval = (EditText)view.findViewById(R.id.etPollingInterval);
        Button btSetPollingInterval = (Button)view.findViewById(R.id.btSetPollingInterval);
        btSetPollingInterval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("/////////////btSetPollingInterval hit/////////");

                //WHATS THIS FOR ?
                // we didnt use this much but the purpose is so that you can check how different polling values work
                //lets say we want to use let power then we want to poll less often, in music selection screen if we adjust this
                //and hit SET once we go to the player screen the polling interval will be what we set
                //THIS WONT WORK IF DONE FROM PLAYER SCREEN, MUST BE DONE ON MUSIC SELECTION SCREEN
                //why? because the threads and all that are set in motion as soon as we go into player screen so its too late to change
                //the interval by then
                //WE havent really tested any longer than 8 seconds, but I think it will work if we increase the interval, but the player screen
                //will update slower of course, for example if we add a song, it wont show until we next get the live play list through polling
                //if this poll doesnt happen for 60 secs then it wont show up for 60 sec, 8 secs seems a good value for things pretty much appearing
                //instantly
                //other use case if when we remove a song because its finished, with a fast polling time it will sort of vanish as we want it to (since we start removing
                //around 11 secs before end) but a long polling time it might stay on the list for longer than we want until the new live play list comes in
                //without this track on.
                if (Tools.USE_ANALYTICS)
                {
                    Tools.sendAnalytics_ButtonPress(a,"btSetPollingInterval");
                }

                if (etPollingInterval.getText().length()>0)
                {
                    _("POLLING INTERVAL WILL NOW BE SET TO "+etPollingInterval.getText());
                    int newPollingInterval = Integer.parseInt(etPollingInterval.getText()+"");
                    MusicPlayerActivity.SECONDS_BETWEEN_POLLING = newPollingInterval;
                    Tools.toast("POLLING INTERVAL IS NOW "+MusicPlayerActivity.SECONDS_BETWEEN_POLLING,c);
                }
                else
                {
                    Tools.toast("Enter a number bru!",c);
                }
            }
        });
        if (!ADD_DEBUG_MENU_ITEMS)
        {
            //we want to hide the whole thing so we grab the linear that holds it and hide that
            LinearLayout pollingIntervalHolder = (LinearLayout)view.findViewById(R.id.pollingIntervalHolder);
            pollingIntervalHolder.setVisibility(View.GONE);
        }
    }*/


    public static void showDialog(final Activity a, String title, String message, final String button1, final String button2, final int state)
    {
        _("/////////////////////////////////////showDialog for " + title);

        final Dialog dialog = new Dialog(a);
        dialog.setContentView(R.layout.dialog);
        dialog.setTitle(title);

        TextView text = (TextView) dialog.findViewById(R.id.text);
        text.setText(message);
        text.setTextColor(0xff000000);

        Button btYES = (Button) dialog.findViewById(R.id.btYes);
        btYES.setText(button1);
        btYES.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("HIT -> "+button1);
//TODO IMPLEMENT STATES IN HERE FOR EACH DIALOG!
                switch (state)
                {
                   case DIALOG_NHS_NUMBER:
                       _("DIALOG_NHS_NUMBER");
                       break;
                    case DIALOG_SET_NEW_SURGERY_AND_GP:
                        _("DIALOG_SET_NEW_SURGERY_AND_GP");
                        //now we send them back to the the surgery details screen, where it will update and show the surgery info we just set
                        Intent intent = new Intent(c, SurgeryDetailsActivity.class);
                        c.startActivity(intent);
                        break;
                    default:_("UNKNOWN STATE!!!!!!!!!!!!!!!!!");
                }

                if (Tools.USE_ANALYTICS)
                {
                    //// Tools.sendAnalytics_ButtonPress(a,"btYES");
                }

                dialog.dismiss();


                //   _("END PREVIOUS PARTY AND LOG OUT ========================");
                //IVE TURNED OFF THE PROGRESS BAR FOR THIS SO IT DOESNT CONFUSE THINGS, TODO: REINSTATE IT ?
                //
              /*  Tools.c = c; //set this up so we can toast if there's an error
                String messageForProgressBar = "Ending previous party";
                _(""+messageForProgressBar);
                Networking n = null;
                String url = Networking.URL_END_PARTY;
                url = url.replace("{venueid}",Tools.getValueStringFromInternal("id"));
                int nState = Networking.NETWORK_STATE_END_PARTY;
                n = new Networking(messageForProgressBar,c,false);// show this happening or not (I think ultimately no?)
                n.execute(url,nState);
*/
                //- SEND THEM TO THE MAIN SCREEN
                // Intent intent = new Intent(c, SlidingIntroductionImagePagerActivity.class);
                // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);//clear the back stack of activites so they cant simply hit back key to find them again
                // c.startActivity(intent);
            }
        });
        Button btNo = (Button) dialog.findViewById(R.id.btNo);
        btNo.setText(button2);
        if (button2==null)
        {
            btNo.setVisibility(View.GONE);
            LinearLayout middlelin = (LinearLayout) dialog.findViewById(R.id.middlelin);
            middlelin.setVisibility(View.GONE);
        }
        btNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("HIT -> "+button2);

                switch (state)
                {
                    case DIALOG_NHS_NUMBER:
                        _("DIALOG_NHS_NUMBER");
                        break;
                }

                if (Tools.USE_ANALYTICS) {
                    //    Tools.sendAnalytics_ButtonPress(a, "btNo");
                }
                dialog.dismiss();

            }
        });
        dialog.show();
    }


    //here we keep all the logic of what different dialogs should do, this is so that we can reuse the showDialog method and not repeat it all over.
    private static void proceedFromDialog()
    {

    }


    public static void hideKeyboard(Context c, View view) {

        _("hidekeyboard");
        // Check if no view has focus:
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public static Date dateJustPicked;
    public static void popupDateDialog(final TextView dateButton, final TextView nextFocus, final Activity a, final boolean showTimePickerToo)
    {
        _("popupDateDialog");
        DialogFragment datePickerFragment = new DatePickerFragment() {
            @Override
            public void onDateSet(DatePicker view, final int year, final int month, final int day) {
                _("onDateSet");
                final Calendar c = Calendar.getInstance();
                int hour         = c.get(Calendar.HOUR_OF_DAY);// set current time
                int min          = c.get(Calendar.MINUTE);
                final int sec    = 0;
                c.set(year, month, day, hour, min ,sec);

                //set text on button to what they just picked
                String myFormat = "yyyy-MM-dd";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.UK);
                dateButton.setText(sdf.format(c.getTime()));

                if (showTimePickerToo)
                {
                    // now show the time picker//
                    // make the listener to pass in.
                     TimePickerDialog.OnTimeSetListener mTimeSetListener =
                            new TimePickerDialog.OnTimeSetListener() {
                                public void onTimeSet(TimePicker view, int hour_, int min_) {

                                    c.set(year, month, day, hour_, min_, sec);

                                    //pad it out if needed
                                    String hourToPrint = ""+hour_;
                                    if (hourToPrint.length()==1)
                                    {
                                        hourToPrint="0"+hourToPrint;
                                    }
                                    String minuteToPrint = ""+min_;
                                    if (minuteToPrint.length()==1)
                                    {
                                        minuteToPrint="0"+minuteToPrint;
                                    }
                                    _("Time set to: " + hourToPrint + " : " + minuteToPrint);
                                    dateButton.setText(dateButton.getText()+", "+hourToPrint+":"+minuteToPrint );

                                    //we store what we just picked in this variable, so we can get at it.
                                    //we need to divide it to get it out of millis..

                                    //we add a minute to it so its in the future or the server complains
                                    long oneMinuteInMS = 60000;
                                    dateJustPicked = new Date( ((c.getTimeInMillis()+oneMinuteInMS)/1000*1000L)  );//new Date((c.getTimeInMillis()/1000) *1000L);//and get it out the 1970s with 1000L
                                    _("-->which is " + dateJustPicked.toString());

                                }
                            };
                    // make it 8am
                    hour = 8;
                    min  = 0;
                    TimePickerDialog tpd = new TimePickerDialog(a, mTimeSetListener, hour, min, false);
                    tpd.show();
                    /////////////////////////
                }

                //moves the focus to something else after dialog is closed
                nextFocus.requestFocus();
            }
        };

        datePickerFragment.show(a.getFragmentManager(), "datePicker");
    }

    static TextToSpeech t2s;
    public static void makeTextToSpeechEngine()
    {
        if (c==null)
        {
            _("Warning context is null");
        }
        t2s = new TextToSpeech(c, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status)
            {
                _("TextToSpeech initiated. ");
            }
        }
        );
    }

    public static void _S(String s)
    {
        if (t2s==null)
        {
            makeTextToSpeechEngine();
        }
        _("%%WARING%% SPEAKING ->> "+s);
        t2s.speak(s, TextToSpeech.QUEUE_FLUSH, null);
    }

    //height defaults
    public static String feet = "5";
    public static String inches = "8";

    public static String meters = "1";
    public static String centimetres = "70";

    //weight defaults
    public static String stones = "10";
    public static String pounds = "3";

    public static String kilos = "85";
    public static String grams = "500";

    //Top level states either height or weight
    public static final int BODY_HEIGHT =100;
        public static final int HEIGHT_FEET_AND_INCHES  = 0;
        public static final int HEIGHT_CENTIMETRES      = 1;
    public static final int BODY_WEIGHT =200;
        public static final int WEIGHT_STONES_AND_POUNDS= 0;
        public static final int WEIGHT_KILOS            = 1;

    //pops a window that can be used for asking user for
    //  -height  - ft andinches or M and CM
    //  -weight - stone and lbs or Kg and g
    //superstate is height or weight, substate if more specific units
    public static void popupBiometricDialog(final Context c, final TextView b, final int superstate, final int substate)
    {
        _("popupBiometricDialog");

        final Dialog dialog = new Dialog(c);
        dialog.setContentView(R.layout.dialog_heightweight_picker);

        //set initial values
        final NumberPicker left_Pick = (NumberPicker) dialog.findViewById(R.id.feetpicker);//This will be feet AND Meters (also stone and Kgs)
        final NumberPicker right_Pick = (NumberPicker) dialog.findViewById(R.id.inchpicker);//This will be inches and CM (also pounds and gs)

        final TextView tvLeftUnit   = (TextView) dialog.findViewById(R.id.tvFeet);
        final TextView tvRightUnit = (TextView) dialog.findViewById(R.id.tvInches);
        int stateIfToggled = -1;
        String textForSwapButton = "";
        String title="";

        switch (superstate)
        {
            case BODY_HEIGHT:
                _("BODY_HEIGHT");
                title = "Select height";

                ////
                switch(substate)
                {
                    case HEIGHT_FEET_AND_INCHES:
                        _(" HEIGHT_FEET_AND_INCHES");
                        left_Pick.setMaxValue(8);
                        left_Pick.setMinValue(1);
                        left_Pick.setValue(Integer.parseInt(feet));

                        right_Pick.setMaxValue(11);
                        right_Pick.setMinValue(0);
                        right_Pick.setValue(Integer.parseInt(inches));

                        tvLeftUnit.setText(" ' ");
                        tvRightUnit.setText(" \" ");
                        stateIfToggled = HEIGHT_CENTIMETRES;//for if they change units
                        textForSwapButton = "Swap to Metres";
                        break;
                    case HEIGHT_CENTIMETRES:
                        _(" HEIGHT_CENTIMETRES");
                        left_Pick.setMaxValue(2);
                        left_Pick.setMinValue(1);
                        left_Pick.setValue(Integer.parseInt(meters));

                        right_Pick.setMaxValue(100);
                        right_Pick.setMinValue(0);
                        right_Pick.setValue(Integer.parseInt(centimetres));

                        tvLeftUnit.setText(" M ");
                        tvRightUnit.setText(" CM ");
                        stateIfToggled = HEIGHT_FEET_AND_INCHES;//for if they change units
                        textForSwapButton = "Swap to feet & inches";
                        break;
                    default:
                        _S("Unknown height substate! "+superstate);
                        break;
                }
                ////

                break;
            case BODY_WEIGHT:
                _("BODY_WEIGHT");
                title = "Select weight";
                ////
                switch(substate)
                {
                    case WEIGHT_STONES_AND_POUNDS:
                        _(" WEIGHT_STONES_AND_POUNDS");
                        left_Pick.setMaxValue(35);
                        left_Pick.setMinValue(0);
                        left_Pick.setValue(Integer.parseInt(stones));

                        right_Pick.setMaxValue(14);
                        right_Pick.setMinValue(0);
                        right_Pick.setValue(Integer.parseInt(pounds));

                        tvLeftUnit.setText(" st ");
                        tvRightUnit.setText(" lbs ");
                        stateIfToggled = WEIGHT_KILOS;//for if they change units
                        textForSwapButton = "Swap to Kilos";
                        break;
                    case WEIGHT_KILOS:
                        _(" WEIGHT_KILOS");
                        left_Pick.setMaxValue(300);
                        left_Pick.setMinValue(0);
                        left_Pick.setValue(Integer.parseInt(kilos));

                        right_Pick.setMaxValue(1000);
                        right_Pick.setMinValue(0);
                        right_Pick.setValue(Integer.parseInt(grams));

                        tvLeftUnit.setText(" Kg ");
                        tvRightUnit.setText(" g ");
                        stateIfToggled = WEIGHT_STONES_AND_POUNDS;//for if they change units
                        textForSwapButton = "Swap to Stones";
                        break;
                    default:
                        _S("Unknown weight substate! "+substate);
                        break;
                }
                ////

                break;
            default:
                _S("Unknown super state! "+superstate);
                break;
        }

        left_Pick.setWrapSelectorWheel(false);
        right_Pick.setWrapSelectorWheel(false);

        Button btYES = (Button) dialog.findViewById(R.id.btYes);
        btYES.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("OK HIT");
                //sets text of their height in either state
                b.setText(left_Pick.getValue() + "" + tvLeftUnit.getText() + " " + right_Pick.getValue() + "" + tvRightUnit.getText());
                dialog.dismiss();
                //nextFocus.requestFocus(); //moves the focus to something else after dialog is closed
            }
        });
        Button btNo = (Button) dialog.findViewById(R.id.btNo);
        btNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("CANCEL HIT");
                dialog.dismiss();
            }
        });

        final int stateIfToggled_ = stateIfToggled;

        Button btSwapUnits = (Button) dialog.findViewById(R.id.btSwapUnits);
        btSwapUnits.setText(textForSwapButton);
        btSwapUnits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("btSwapUnits HIT");

                //dismiss this pop up new one
                dialog.dismiss();

                popupBiometricDialog(c, b, superstate, stateIfToggled_);
            }
        });


        //change colors of header
        //LINE
       /* int dividerId = dialog.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
        View divider = dialog.findViewById(dividerId);
        divider.setBackgroundColor(0xff00ffff);
        //TEXT
        String chars = title;
        SpannableString str = new SpannableString(chars);
        str.setSpan(new ForegroundColorSpan(Color.WHITE), 0, chars.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        dialog.setTitle(chars);
*/
        //
        dialog.setTitle(title);


        dialog.show();
    }

    public static void toggleActionBarMenu(LinearLayout fakeActionBarButtonHolder)
    {
        if (fakeActionBarButtonHolder.getVisibility()==View.VISIBLE)
        {
            fakeActionBarButtonHolder.setVisibility(View.GONE);
        }
        else
        {
            fakeActionBarButtonHolder.setVisibility(View.VISIBLE);
        }
    }
    public static void showChangePasswordDialog()
    {
        final Dialog dialog = new Dialog(c);
        dialog.setContentView(R.layout.dialog_heightweight_picker);

    }
/*
    private static void logoutHit(final Activity a)
    {

            _("logoutHit");
            final Dialog dialog = new Dialog(c);
            dialog.setContentView(R.layout.end_party_dialog);
            dialog.setTitle("Log Out?");

            final CheckBox cbEndParty = (CheckBox) dialog.findViewById(R.id.cbEndParty);
            cbEndParty.setVisibility(View.GONE);
            //I did have a checkbox allowing them to optionally not end the party, but this causes major headaches all round
            //so for now im hiding this, and just making sure that it wont end/go back unless they confirm.

            Button btYES = (Button) dialog.findViewById(R.id.btYes);
            btYES.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    _("LOG OUTCONFIRMED LETS GO BACK TO MAIN SCREEN");

                    if (Tools.USE_ANALYTICS)
                    {
                        Tools.sendAnalytics_ButtonPress(a,"btYES");
                    }

                    dialog.dismiss();


                    _("END PREVIOUS PARTY AND LOG OUT ========================");
                    //IVE TURNED OFF THE PROGRESS BAR FOR THIS SO IT DOESNT CONFUSE THINGS, TODO: REINSTATE IT ?
                    //
                    Tools.c = c; //set this up so we can toast if there's an error
                    String messageForProgressBar = "Ending previous party";
                    _(""+messageForProgressBar);
                    Networking n = null;
                    String url = Networking.URL_END_PARTY;
                    url = url.replace("{venueid}",Tools.getValueStringFromInternal("id"));
                    int nState = Networking.NETWORK_STATE_END_PARTY;
                    n = new Networking(messageForProgressBar,c,false);// show this happening or not (I think ultimately no?)
                    n.execute(url,nState);

                    //- SEND THEM TO THE MAIN SCREEN
                    Intent intent = new Intent(c, SlidingIntroductionImagePagerActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);//clear the back stack of activites so they cant simply hit back key to find them again
                    c.startActivity(intent);
                }
            });
            Button btNo = (Button) dialog.findViewById(R.id.btNo);
            btNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    _("NO LETS NOT GO BACK");
                    if (Tools.USE_ANALYTICS)
                    {
                        Tools.sendAnalytics_ButtonPress(a,"btNo");
                    }
                    dialog.dismiss();

                }
            });
            dialog.show();
    }
*/
    //mimicks as if we went to our own jukebox site and clicked add on every single song
    //can also auto skip every 10th poll so u can have it automatedly be testing for you all you got
    //to do is listen. (auto skip basically means it will pretend a user hit the skip button on each 10th poll to server)
    //these are PURELY for debugging/testing and not intended for release..
/*
    private static void mimmickUserTappingAddOnAllSongsOnSiteDEBUG(boolean autoSkip, ArrayList<Song> listOfSongsSyncedWithServer)
    {
        _("mimmickUserTappingAddOnAllSongsOnSiteDEBUG");
        if (!Tools.DEBUG)
        {
            toast("This should not be available in release mode :-O",c);// you wont EVER see this basically, but just in case ;)
        }

        MusicPlayerActivity.AUTO_SKIP_EVERY_TRACK=autoSkip; //WILL AUTO SKIP THEM TOO..

        // FAKE THE USER GOING TO SITE AND HITTING ADD ON ALL OF THEM!
        //takes forever to do this manually so this is awesome for speeding up testing
        Networking[] nARRAY = new Networking[listOfSongsSyncedWithServer.size()];
        String url = Networking.URL_USER_CLICKED_ADD_ON_SITE;
        // for (int i=0; i<listOfSongsSyncedWithServer.size(); i++)

        int amountOfSongsToSync=10; //this inhibits getting the playlist if we do too many, cant seem tofix that so jus treduced this for now so it ends in good time to get anoyhrt play list
        if (amountOfSongsToSync>listOfSongsSyncedWithServer.size()-1)
        {
            amountOfSongsToSync = listOfSongsSyncedWithServer.size()-1;
        }
        //just put this at 15 for now. butdont just pick the first few, pick them randomly
        for (int i=amountOfSongsToSync; i>=0; i--)//do it in reverse so the order feels right when viewed on site
        {
            Song s = listOfSongsSyncedWithServer.get(getRand(0,listOfSongsSyncedWithServer.size()-1));//i);
            url = Networking.URL_USER_CLICKED_ADD_ON_SITE;
            //error checking here, s.getSongID() can return null somehow
            if (s.getSongID()==null)
            {
                _("////////////////////////////////////////////WARNING s.getSongID() WAS NULL SO WE SKIPPED THIS TRACK.(" + s.getTitle() + " by " + s.getArtist() + ")///////////////////////////");
                if (Tools.SHOW_DEBUG_TOASTS)
                {
                    toast("Picking alternative to " + s.getTitle()+" since songID is null",c);//remove this for release it can be transparent to the user
                }
                //add one back onto i, so it picks another one! this seems correct
                i++;
            }
            else
            {
                url = url.replace("{songid}",s.getSongID());
                url = url.replace("{username}",Tools.getValueStringFromInternal("slug"));

                //_("musicSrv.currentSong_songID: "+musicSrv.currentSong_songID);
                _(i+"///////// AUTO FAKE USER CLICKING ADD ON SITE FOR SONG "+s.getSongID() +" ===> "+s.getTitle()+"   ARTIST:"+s.getArtist());
                int nState = Networking.NETWORK_STATE_USER_CLICKED_ADD_ON_SITE;
                nARRAY[i] = new Networking("Faking user hitting add on website for all synced songs (autoskip:"+MusicPlayerActivity.AUTO_SKIP_EVERY_TRACK+")",c,false);//SongID:"+s.getSongID(),c,true);
                Tools.toastS("DEBUG Queuing up on site: "+s.getTitle(),c);
                nARRAY[i].execute(url,nState);


            }

        }
    }*/

    //this method is similar to the one above (that one is for debugging this one isnt), client wanted X songs to rack up randomly if play list is empty, seems the best
    //way to do this is by simply racking up songs randomly on the site, THUS, the parameter here amountOfTracksToRandomlySelect, depicts how many it will choose
    // ie this is being done not for debug but due to the fact the user has no songs on the live play list and we want to avoid silence (its a feature not debug).
    //this also gets called when their playlist is running low to top it up and avoid silence
/*
    public static void addXTracksToLivePlayListViaWebsite(ArrayList<Song> listOfSongsSyncedWithServer, int amountOfTracksToRandomlySelect)
    {
        _("addXTracksToLivePlayListViaWebsite ---> "+amountOfTracksToRandomlySelect);
        //use the logic from the method above
        //FAKE THE USER GOING TO SITE AND HITTING ADD ON X SONGS
        //to rack up tunes when its not got any left, or just started and no one racked any up yet
        Networking[] nARRAY = new Networking[listOfSongsSyncedWithServer.size()];
        String url = "";
        if (listOfSongsSyncedWithServer.size()<3)//force them to rack up 3 tracks, otherwise how can we randomly rack up ZERO songs on site, this kills our getRand
        {
            Tools.toast("Please sync at least 3 tracks if you want the app to randomly pick 3 for you when empty.",c);
            return;
        }
        for (int i=0; i<amountOfTracksToRandomlySelect; i++)
        {
            int rand = getRand(0,listOfSongsSyncedWithServer.size()-1);
            Song s = listOfSongsSyncedWithServer.get( rand ); //here we randomly get one from the list TODO: IS -1 RIGHT or do we miss last track?
            url = Networking.URL_USER_CLICKED_ADD_ON_SITE;
            if (s.getSongID()==null)
            {
                _("////////////////////////////////////////////WARNING s.getSongID() WAS NULL SO WE SKIPPED THIS TRACK.("+s.getTitle()+" by "+s.getArtist()+")///////////////////////////");
                toast("Skipping " + s.getTitle()+" since songID is null",c);
                //REMOVE one from i, so it picks another one! TODO: Verify if this is right
                i--;
            }
            else
            {
                url = url.replace("{songid}",s.getSongID());
                url = url.replace("{username}",Tools.getValueStringFromInternal("slug"));

                //_("musicSrv.currentSong_songID: "+musicSrv.currentSong_songID);
                _(i+"///////// AUTO FAKE USER CLICKING ADD ON SITE FOR SONG "+s.getSongID() +" ===> "+s.getTitle());
                int nState = Networking.NETWORK_STATE_USER_CLICKED_ADD_ON_SITE;
                nARRAY[i] = new Networking("Faking user hitting add on website for all synced songs (autoskip:"+MusicPlayerActivity.AUTO_SKIP_EVERY_TRACK+")",c,false);//SongID:"+s.getSongID(),c,true);
                Tools.toastS("Random Queue up on site: "+s.getTitle(),c);
                nARRAY[i].execute(url,nState);
            }
        }
    }
*/

    //IMAGE & CAMERA -------------------------------------


    //loads user pic path (and makes bitmap and puts it into the circular imageview), and returns true if this works.
    public String loadPictureAndSetImage(ImageView ibMyPic, String path)
    {
        _("loadPictureAndSetImage path:"+path);
        if (path==null)
        {
            _("WARNING path IS NULL WE CANNOT LOAD THE PROFILE PIC!!");
            return "";
        }
        Tools t = new Tools(c);
        t.setupPrefs(c);
        //String path2profilepic = ProfileActivity.///t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SaveKeys.SAVE_KEY_PICTURE, "", c);//read name

        if (path!=null && path.length()>0)//path2profilepic!=null && path2profilepic.length()>0 )
        {
           //////////// ProfileActivity.myProfile.image=path2profilepic;
            _("pic found ! now loading  " + path);
            //change to real an url
            Uri photoUri = null;
            //if its a file we treat it different to if its a url
            if (path.contains("http"))
            {
                _("its a url");
                photoUri = Uri.parse( path );
            }
            else
            {
                _("its a normal path");
                photoUri = Uri.fromFile(new File(path));
            }
            ;;/////Uri.fromFile(new File(ProfileActivity.myProfile.image));
            try {
                //Bitmap bitmap = MediaStore.Images.Media.getBitmap(c.getContentResolver(), photoUri);
                _("picasso load pic into imageview");
                if (ibMyPic==null)
                {
                    _("WARNING ibMyPic IS NULL..........");
                }

                Picasso.with(c).load(photoUri).into(ibMyPic);
                return path;
            } catch (Exception e) {
                _("Problems opening pic @ "+path);
                e.printStackTrace();
            }
        }
        else
        {
            _("No profile photo is stored for this user.");
        }
        return "error";
    }

    //make a smaller version of a bitmap
    //note you dont get back exactly what you ask for, just close, but this should save lots of mem.
    public static Bitmap decodeSampledBitmapFromFile(String path, int reqWidth, int reqHeight) {
        //RFR//_("decodeSampledBitmapFromFile - you asked for " + reqWidth + " x " + reqHeight);
        // BEST QUALITY MATCH

        //First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize, Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        int inSampleSize = 1;

        if (height > reqHeight) {
            inSampleSize = Math.round((float) height / (float) reqHeight);
        }
        int expectedWidth = width / inSampleSize;

        if (expectedWidth > reqWidth) {
            //if(Math.round((float)width / (float)reqWidth) > inSampleSize) // If bigger SampSize..
            inSampleSize = Math.round((float) width / (float) reqWidth);
        }

        //RFR//_("Original image was " + width + " x " + height + "");
        options.inSampleSize = inSampleSize;

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, options);
    }

    //it seems that some android cams allow portrait phototaking fine, others force landscape so
    //the pic ends up the wrong way around, we need to detect this and rotate to fix if needed
    public static Bitmap rotateIfNeeded(String imagePath, Context c) {
        try {
            //RFR//_("rotateIfNeeded ---> " + imagePath);
            File f = new File(imagePath);
            ExifInterface exif = null;
            try {
                exif = new ExifInterface(f.getPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            int angle = 0;
            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                angle = 90;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                angle = 180;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                angle = 270;
            }
            //RFR//_("angle:" + angle);

            Matrix mat = new Matrix();
            mat.postRotate(angle);
            BitmapFactory.Options options = new BitmapFactory.Options();
            //options.inSampleSize = 4;////2;  4 uses less memory [ for really low mem devices like nexus one!]
            //RFR////RFR//_("decode bitmap");
            Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(f), null, options);
            //RFR////RFR//_("bitmap decided");
            if (bmp == null) {
                //RFR//_("warning!!!!!!!!!!!!!!!! bmp is still null");
            }
            if (angle == 0) {
                //RFR//_("NO NEED TO ROTATE");
                return null;//so they know we dont need it
            }

            Bitmap bitmap = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), mat, true);
            ByteArrayOutputStream outstudentstreamOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outstudentstreamOutputStream);
            //imageView.setImageBitmap(bitmap);
            return bitmap;

        } catch (IOException e) {
            //RFR//_("-- Error in setting image " + e.getMessage());
            e.printStackTrace();
        } catch (OutOfMemoryError oom) {
            //RFR//_("-- OOM Error in setting image " + oom.getMessage());
            Tools.toast("Out of memory", c);
            oom.printStackTrace();
        }
        return null;
    }

    //makes a string filename from a passed in Uri
    public static String getFileNameByUri(Context context, Uri uri) {
        _("getFileNameByUri");
        String fileName = "unknown";//default fileName
        Uri filePathUri = uri;

        if (uri.getScheme()==null)
        {
            _("Warning uri.getScheme() is null!!");
        }
        if (uri.getScheme().toString().compareTo("content") == 0) {
            Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
            if (cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);//Instead of "MediaStore.Images.Media.DATA" can be used "_data"
                filePathUri = Uri.parse(cursor.getString(column_index));

                fileName = filePathUri.getLastPathSegment().toString();
            }
        } else if (uri.getScheme().compareTo("file") == 0) {
            fileName = filePathUri.getLastPathSegment().toString();
        } else {
            fileName = fileName + "_" + filePathUri.getLastPathSegment();
        }
        //RFR//_("full path: " + filePathUri);
        return filePathUri + "";//fileName;
    }



    //INTERNAL STORAGE ------------------------------------

    //methods of convenience for storing and reading from internal memory easily

    //wrappers for saving to internal memory
    SharedPreferences mPrefs;
    public static final int INTERNAL_MEMORY_SAVE_TYPE_INT       =   0;
    public static final int INTERNAL_MEMORY_SAVE_TYPE_STRING    =   1;

    //error handling
    public static final int     ERRORi=-99;
    public static final String  ERRORs=null;
    public static final String MY_APP_PREFS_NAME="ClinixAppPrefs";

    //call this before anything ones below it to set stuff up
    public void setupPrefs(Context c_) {
        c=c_;
        //_("setupPrefs for "+MY_APP_PREFS_NAME);
        mPrefs = c.getSharedPreferences(MY_APP_PREFS_NAME, 0); //0 = mode private. only this app can read these preferences
    }

    //SIMPLEST WAY TO GET A VALUE BACK IS CALL THIS ONE
    public static String getValueStringFromInternal(String valToGet)
    {
        //get username stored.
        Tools t = new Tools(c);
        t.setupPrefs(c);

        String returnme = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, valToGet,null,c);
        if ( returnme == Tools.ERRORs)
        {
            _("*****************WARNING*******************");
            _("*****************WARNING******************* "+valToGet+" was not found!");
            _("*****************WARNING*******************");
        }
        else
        {
            //_("INTERNAL MEMORY - "+valToGet+" was found as: "+returnme);
        }
        return returnme;
    }

    //if u are saving a string pass -1 for int, if you are saving an int pass null for string
    public void setValue(int type, String key, int valueI, String valueS) {
        SharedPreferences.Editor edit = mPrefs.edit();
        switch(type)
        {
            case INTERNAL_MEMORY_SAVE_TYPE_INT:         edit.putInt(    key, valueI);       _("WRITING TO INTERNAL ["+key+"]-> "+valueI);    break;
            case INTERNAL_MEMORY_SAVE_TYPE_STRING:      edit.putString( key, valueS);       _("WRITING TO INTERNAL ["+key+"]-> "+valueS);    break;
            default: _("CANNOT SAVE, UNKNOWN TYPE! "+type);
        }
        edit.commit();
    }

    // separate ones for each type
    public int getValueI(int type, String key, int defaultvalueI, String defaultvalueS, Context c_) {
        if (type!=INTERNAL_MEMORY_SAVE_TYPE_INT)
        {
            _("You asked for an int but you passed wrong type, returning ERROR !");
            return ERRORi;
        }
        if (mPrefs==null || c==null)
        {
            setupPrefs(c_);
        }
        return mPrefs.getInt(key, defaultvalueI);
    }

    public String getValueS(int type, String key, String defaultvalueS, Context c_) {
        if (type!=INTERNAL_MEMORY_SAVE_TYPE_STRING)
        {
            _("You asked for an String but you passed wrong type, returning ERROR !");
            return ERRORs;
        }
        if (mPrefs==null || c==null)
        {
            setupPrefs(c_);
        }
        String result = mPrefs.getString(key, defaultvalueS);
        _("LOADING FOR "+key+", WHAT WE HAVE: "+result);
        return result;
    }

    public void wipeEveryThingWeKnowAboutUser()
    {
        _("wipeEveryThingWeKnowAboutUser");
        _("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
        _("%%%%%%%%%%% WIPE ALL PREFS %%%%%%%%%%%%%%");
        _("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
        mPrefs = c.getSharedPreferences(MY_APP_PREFS_NAME, 0);
        mPrefs.edit().clear().commit();

        /* Removing single preference:
        * SharedPreferences settings = context.getSharedPreferences("PreferencesName", Context.MODE_PRIVATE);
          settings.edit().remove("KeyName").commit();
        * */
    }

    //MUSIC SELECTION LIST -----------------------------------------------------
    //some general helpers for the music selection list functionality
/*
    //this will go through adding a letter before each set of things starting with that letter
    public static final String[] letters = new String[] {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
    public static ArrayList<Song> addLetterSeperators( ArrayList<Song> list )
    {
        ArrayList songListWithSeperators = new ArrayList<Song>();
        boolean foundLETTER=false;
        int letterIndex = 0;
        boolean reachedEndOfAlphabet=false;

        for (int i=0; i<list.size(); i++)
        {
            Song song = list.get(i);
            ////////Add seperator like "A"
            if (!foundLETTER)
            {
                if (letterIndex>=letters.length)
                {
                    _("we have reached the end of the alphabet, we are done.");
                    reachedEndOfAlphabet=true;
                    break;
                }
                else
                {
                    //_("======> looking for "+letters[letterIndex]);
                    //_(""+song.getTitle()+" STARTS WITH "+letters[letterIndex]+"?");
                    //ie: A and a
                    if (song.getTitle().startsWith(letters[letterIndex]) || song.getTitle().startsWith(letters[letterIndex].toLowerCase()))
                    {
                        //_("YES");
                        foundLETTER = true;                   /// eg "A"
                        songListWithSeperators.add(new Song(-99,letters[letterIndex],"ARTIST","NONE://","DURATION","ALBUMID"));///ALBUM ID is just for cover art
                    }
                    else
                    {
                        //_("NO");
                    }
                }
            }
            if (foundLETTER && song.getTitle().startsWith(letters[letterIndex]))
            {
                songListWithSeperators.add(song);
            }
            //check if its got to the end without finding one at all,
            if (i==list.size()-1)
            {
                //_("LOOP");
                i=-1;//YES this should be -1, I thought 0 for ages and I was losing the first track!
                letterIndex++;
                foundLETTER=false;
            }
        }
        _("FINISHED");
        if (reachedEndOfAlphabet)
        {
            _("reached end of alphabet no errors detected.");
        }
        _("List size is "+songListWithSeperators.size());
        return songListWithSeperators;
    }
*/

    //FEEDBACK -----------------------------------------

    public static void openEmailClientToSendUsFeedback(String emailaddress, String message)
    {
        /* Create the Intent */
        final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

        /* Fill it with Data */
        emailIntent.setType("plain/text");
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{emailaddress});
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Message from Android app");
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, message);

        /* Send it off to the Activity-Chooser */
        c.startActivity(Intent.createChooser(emailIntent, "Send mail..."));
    }

    //MISC HELPERS----------------------------------------------------

    //easily get the venue id from internal memory,
    public static String venueid;
    public static String getVenueId()
    {
        if (venueid==null)
        {
            _("venueid is not known yet, lets grab it from the internal memory now.");
            Tools t = new Tools(c);
            t.setupPrefs(c);

            venueid         = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, "id",null,c); //yes we pass id here!
            if ( venueid == Tools.ERRORs)
            {
                _("WARNING venueid was not found (NULL), we cannot sync without it!");
                Tools.toast("VenueID is null, error.",c);
                return null;
            }
            else
            {
                //_("INTERNAL MEMORY - venueid was found as: "+venueid);
                //_("This one is for username: "+username);
            }
        }
        else
        {
            //_("We already know venueid, its: "+venueid);
        }
        return venueid;
    }

    //gets a random number for us between min and max
    //we are slightly more smart here because we dont want the same random number back really for atleast
    //3 attempts, so we track the last ones and only return new ones, this is so that we dont ask the server to rack up the
    //same song, we want them unique, also if it runs out of them 3 and picks another 3, we dont really want them being the
    //same songs either, but the worry is if they only have say 3 songs lined up then this random generator could go into
    //and infinate loop.. For this reason we clear lastRandomVals on every 10th call to getRand, this should mean its unlikely
    //we get the same values, and also that an infinate loop cant happen, i think :) TODO: Verify if this is silly or not.

    static Vector lastRandomVals = new Vector();
    static int timesGetRandCalled = 0;
    private static int getRand(int min, int max)
    {
        if (min==max)
        {
            Tools.toast("getRand received error values "+max+" VS "+min,c);
            return 0;
        }

        //why 20 ? so if they let it play 3 songs over and over and it keeps randomly getting them, you should usually get around 6 or 7 runs before it repeats
        if (timesGetRandCalled++>20)//so we dont get stuck in a loop generating random values, this could only happen on very small music collections but its still possible.
        {
            _("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
            _("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
            _("CLEARED THE LAST RANDOM VALS LIST.");
            _("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
            _("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
            lastRandomVals = new Vector();
            timesGetRandCalled=0;
        }

        int randomValue = new Random().nextInt((max - min) + 1) + min;
        _("=======================================================> RANDOM VALUE IS "+randomValue);

        ///////CHECK IF WE HAVE SEEN THIS TOO RECENTLY AS WE DONT WANT USERS GETTTING SAME SONGS BACK TOO OFTEN!//////
        //if its on our stack of past ints regenerate it
        Enumeration e = lastRandomVals.elements();
        while (e.hasMoreElements())
        {
            Integer x = (Integer) e.nextElement();

            if (randomValue == x)
            {
                //made this blatant so i spot any peculiarities!
                _("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                _("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                _("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                _("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                _("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                _("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                _("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                 _("WEVE SEEN THIS RANDOM NUMBER TOO RECENTLY ("+x+"), REGENERATE IT.    timesGetRandCalled:"+timesGetRandCalled);
                _("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                _("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                _("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                _("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                _("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                _("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                _("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                randomValue = getRand(min,max);
                break;
            }
        }

        //if not, add it so it will be on our recent list
        lastRandomVals.add(randomValue);
        //////////////////////////////////////
        return randomValue;
    }

    //check how much memory is in use, debug method, for testing on crap devices that run out of memory
    //no device has done that in this app.
    public static void reportMem(String calledFrom)
    {
        int usedMegs = (int)(Debug.getNativeHeapAllocatedSize() / 1048576L);
        String usedMegsString = String.format(" - Memory Used: %d MB", usedMegs);
        //return usedMegsString;
        _("-----------------------------------" + calledFrom + " MEMORY REPORT: " + usedMegsString);
    }
/*
    static Tracker t;
    //convenient way to send a "screenview" to analytics (ie when someone went into a screen in the app)
    public static void sendAnalytics_ScreenView(Activity a, String screenName)
    {
        // Get tracker.
        //Tracker t = ((MyApplication) a.getApplication()).getTracker(MyApplication.TrackerName.APP_TRACKER);

        GoogleAnalytics analytics = GoogleAnalytics.getInstance(a);
        if (t==null)
        {
            t = analytics.newTracker(GOOGLE_ANALYTICS_PROPERTY_ID); // Send hits to tracker id UA-XXXX-Y
        }

        // Set screen name.
        t.setScreenName(screenName);
        _("---------> GOOGLE ANALYTICS SCREENVIEW: " + screenName);
        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());
    }

    //convenient way to send a "ButtonPress" to analytics (ie when someone presses any button in app)
    public static void sendAnalytics_ButtonPress(Activity a, String buttonName)
    {
        // Get tracker.
        //Tracker t = ((MyApplication) (a).getApplication()).getTracker(MyApplication.TrackerName.APP_TRACKER);
        GoogleAnalytics analytics = GoogleAnalytics.getInstance(a);
        if (t==null)
        {
            t = analytics.newTracker(GOOGLE_ANALYTICS_PROPERTY_ID); // Send hits to tracker id UA-XXXX-Y
        }


        // here I just made up buttonPress, should work still ?
        //t.set("AndroidApp", "buttonPress. " + buttonName);//NOTE: I made this one myself, presumably we can track it fine from the site!
        _("---------> GOOGLE ANALYTICS BUTTONPRESS: " + buttonName);
        // Send a screen view.
        //t.send(new HitBuilders.EventBuilder().build());//I used an event builder for button presses TODO: confirm this is correct!

        t.send(new HitBuilders.EventBuilder()
                .setCategory("UX")
                        //    .setAction("click")
                .setAction("click ->" + buttonName) //hmm i couldntfinds label in analytics so ive addedit to "click" !!
                .setLabel(buttonName)
                .build());
    }
*/

    /*
    example:
        Spinner spTitle  = (Spinner)    findViewById(R.id.spTitle);
        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add("Master");
        spinnerArray.add("Miss");
        spinnerArray.add("Mr");
        spinnerArray.add("Mrs");
        spinnerArray.add("Ms");
        spinnerArray.add("Other");
        Tools.populateSpinner(spTitle, spinnerArray);
    * */
    public static void populateSpinner(Spinner spinner,List<String> spinnerArray)
    {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(c, R.layout.spiiner_list_item, spinnerArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        if (spinner==null)
        {
            _("WARNING SPINNER IS NULL WILL NOT POPULATE IT!!!");
            return;
        }
        spinner.setAdapter(adapter);
    }

    public static boolean isNetworkAvailable(Context c) {
        ConnectivityManager connectivityManager = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    //we make toasting easy, using these convenience methods
    //toast wrapper
    public static void toast(final String message, final Context context ) {
        //_("****TOAST: " + message);

        if (context==null)
        {
            _("CANT TOAST WITH NULL CONTEXT!");
            return;
        }
        if (message==null)
        {
            _("CANT TOAST WITH NULL message!");
        }

        //avoid ui thread nonsense
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(
                new Runnable()
                {
                    @Override
                    public void run()
                    {
                        _("TOASTIEEEEE ------------>"+message);
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();//MADE THEM ALL SHORT FOR NOW
                    }
                }
        );
    }

    //toast wrapper
    public static void toastS(final String message, final Context context ) {
        //_("****TOAST: " + message);

        if (context==null)
        {
            _("CANT TOAST WITH NULL CONTEXT!");
            return;
        }
        if (message==null)
        {
            _("CANT TOAST WITH NULL message!");
        }
        //avoid ui thread nonsense
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(
                new Runnable()
                {
                    @Override
                    public void run()
                    {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }

    //this is for debug so we can compare what we sent to server and what it sends back easily without the hassles of too large systemouts etc
    public static void writeStringToSD(String s, String name, Context c)
    {

        try {
            File myFile = new File(Environment.getExternalStorageDirectory()+"/"+name+".txt");
            myFile.createNewFile();
            FileOutputStream fOut = new FileOutputStream(myFile);
            OutputStreamWriter myOutWriter =
                    new OutputStreamWriter(fOut);
            myOutWriter.append(s);
            myOutWriter.close();
            fOut.close();
            toast("WRITTEN "+name+".txt",c);
        } catch (Exception e) {
            _("EXCEPTION WRITING THIS FILE!!!!!!!!!!!!!!!!!!!!!!");
        }
    }


    public static void logLargeString(String str) {

        /////if(str.length() > 3000) {

        int lengthToPrint = 3000;
        if (str.length()<lengthToPrint)
        {
            lengthToPrint=str.length();
        }

        if (lengthToPrint<=0)
        {
            _("NOTHING LEFT TO PRINT");
            return;
        }
        _(str.substring(0, lengthToPrint));
        logLargeString(str.substring(lengthToPrint));
        //// } else {
        ////    _("BLARGESTRING//"+str);
        //// }
    }

    /**
     * Mutates and applies a filter that converts the given drawable to a Gray
     * image. This method may be used to simulate the color of disable icons in
     * Honeycomb's ActionBar.
     *
     * @return a mutated version of the given drawable with a color filter applied.
     */
    public static Drawable convertDrawableToGrayScale(Drawable drawable) {

        if (drawable == null)
            return null;

        Drawable res = drawable.mutate();
        res.setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
        return res;
    }



    /////////////////////////////////////////////////////////
    ////SELECT PHOTO CODE////////////////////////////////////
    /////////////////////////////////////////////////////////
    //this stuff has been used in various projects over the years, so it is tuned and has some funny comments etc, but its all good :)

    static AlbumStorageDirFactory mAlbumStorageDirFactory = null;
    static AlertDialog dialog;
    Bitmap bitmap;

    private static final int ACTION_PHOTO_FROM_CAMERA   = 1;
    private static final int ACTION_PHOTO_FROM_GALLERY  = 2;

    //for the old devices that are killing me right now
    public static final boolean USE_EXTREMELY_LOW_RAM=true;
    private static String mCurrentPhotoPath;

    public static void buildPhotoDialog(final Context c, final Activity a)
    {
        _("buildPhotoDialog");
        final String[] items = new String[] { "Take from camera","Select from gallery" };
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(c, android.R.layout.select_dialog_item, items);
        AlertDialog.Builder builder = new AlertDialog.Builder(c);
        builder.setTitle("Select Image");

        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                Tools._S("//////onClick " + item);
                if (item == 0) {
                    Tools._S("-FROM CAMERA");
                    dispatchTakePictureIntent(ACTION_PHOTO_FROM_CAMERA,a);
                } else {
                    Tools._S("-FROM FILE");
                    // pick from file
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    a.startActivityForResult(Intent.createChooser(intent, "Choose a Photo"), ACTION_PHOTO_FROM_GALLERY);
                }
            }
        });

        dialog = builder.create();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
            mAlbumStorageDirFactory = new FroyoAlbumDirFactory();
        } else {
            mAlbumStorageDirFactory = new BaseAlbumDirFactory();
        }
        dialog.show();
    }

    private static File setUpPhotoFile() throws IOException {
        Tools._S("setUpPhotoFile");
        File f = createImageFile();
        mCurrentPhotoPath = f.getAbsolutePath();
        Tools._S("mCurrentPhotoPath becomes " + mCurrentPhotoPath);
        return f;
    }
    private static void dispatchTakePictureIntent(int actionCode, Activity a) {
        Tools._S("dispatchTakePictureIntent");
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        switch(actionCode) {
            case ACTION_PHOTO_FROM_CAMERA:
                _("ACTION_PHOTO_FROM_CAMERA");
                File f = null;
                try {
                    f = setUpPhotoFile();
                    mCurrentPhotoPath = f.getAbsolutePath();
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                } catch (IOException e) {
                    e.printStackTrace();
                    f = null;
                    mCurrentPhotoPath = null;
                }
                break;
            default:
                Tools._S("uknown action code "+actionCode);
                break;
        }
        a.startActivityForResult(takePictureIntent, actionCode);
    }

    private static final String JPEG_FILE_PREFIX = "Clinix_";
    private static final String JPEG_FILE_SUFFIX = ".jpg";

    private static File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = JPEG_FILE_PREFIX + timeStamp + "_";
        File albumF = getAlbumDir();
        File imageF = File.createTempFile(imageFileName, JPEG_FILE_SUFFIX, albumF);
        return imageF;
    }

    private static String getAlbumName()
    {
        return ("clinix");
    }

    private static File getAlbumDir() {
        File storageDir = null;
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            storageDir = mAlbumStorageDirFactory.getAlbumStorageDir(getAlbumName());
            _("storageDir: "+storageDir);
            if (storageDir != null) {
                if (! storageDir.mkdirs()) {
                    if (! storageDir.exists()){
                        _("failed to create directory");
                        return null;
                    }
                }
            }
        } else {
            _("External storage is not mounted READ/WRITE.");
        }
        return storageDir;
    }
    ////////////////////////////////////////////////////



    //a wrapper so we dont have to type this every time
    private static void _(String s)
    {
        if (!Tools.DEBUG)
        {
            return;
        }
        if (s.length()>3000) {
            _("BIG STRING COMING UP");
            logLargeString(s);

        }
        else
        {
            Log.d("MyApp", "Tools" + "###################################### " + s);
        }
    }
}
