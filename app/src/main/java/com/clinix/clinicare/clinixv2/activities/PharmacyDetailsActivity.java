/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.common.Networking;
import com.clinix.clinicare.clinixv2.common.Tools;
import com.clinix.clinicare.clinixv2.dataobjs.Pharmacy;

public class PharmacyDetailsActivity extends Activity {

    Context c;
    Activity a;
    Tools t;
    public static Pharmacy myPharmacy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _("PharmacyDetailsActivity Born!////////////////////////////////");
        c = this;
        a = this;
        setContentView(R.layout.pharmacydetails);
    }

    @Override
    public void onResume() {
        super.onResume();
        _("///////////////onResume/////////////");

        t = new Tools(c);
        t.setupPrefs(c);
        setupGUI();
        doNetworkingToGetMyPharmacyDetails();
        //COME FROM NETWORK NOW I THINKL 0.3.1 loadGPDetails();
    }

    private void setupGUI()
    {

        //Save hit
       /* Button btSave = (Button) findViewById(R.id.btSave);
        btSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("btSave HIT");
                saveGPDetails();
            }
        });
*/
        setupFakeActionBar();
    }

    public void populateGUI()
    {
        if (myPharmacy==null)
        {
            _("WARNING myPharmacy IS NULL SO WE CANNOT POPULATE THE GUI!..");
            return;
        }

        EditText etPharmacyName = (EditText)findViewById( R.id.etPharmacyName );
      ///  EditText etSurgeryName = (EditText)findViewById( R.id.etSurgeryName );
        EditText etAddressA = (EditText)findViewById( R.id.etAddressA );
        EditText etAddressB = (EditText)findViewById( R.id.etAddressB );
        EditText etAddressC = (EditText)findViewById( R.id.etAddressC );
        EditText etAddressD = (EditText)findViewById( R.id.etAddressD );
        EditText etTelephone = (EditText)findViewById( R.id.etTelephone );
        EditText etEmail = (EditText)findViewById( R.id.etEmail );
       // EditText btSave = (Button)findViewById( R.id.btSave );


        etPharmacyName.setText(myPharmacy.name);////.title +" "+myGP.forename+" "+myGP.surname);
        //etSurgeryName.setText("How to get?");//myGP.suffix);
        etAddressA.setText(myPharmacy.street);
        etAddressB.setText(myPharmacy.address_2);
        etAddressC.setText(myPharmacy.town);
        etAddressD.setText(myPharmacy.county);
        etTelephone.setText(myPharmacy.telephone);
        etEmail.setText(myPharmacy.email);
    }

//DOES THIS COME FROM MEMORY OR FROM SERVER EACH TIME ????
    private void doNetworkingToGetMyPharmacyDetails()
    {
        _("doNetworkingToGetMyPharmacyDetails=========================================");
        if (!Tools.isNetworkAvailable(this) )
        {
            Tools.toast("This app needs an active Internet connection to work.",c);
            return;
        }

        //Do networking!
        String messageForProgressBar = "Getting pharmacy details";
        String url = Networking.URL_GET_MY_PATIENT_PHARMACY;
        int nState = Networking.NETWORK_STATE_GET_MY_PATIENT_PHARMACY;
        Networking n = new Networking(messageForProgressBar, c, true);

        Tools t = new Tools(c);
        t.setupPrefs(c);
        ///String type = "patient"; // this can be admin / practitioner / patient
       // String id = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, "id",null,c);
       // n.setHistoryInfo(id);// we call this specific method for this state so right vars are set up.
        n.execute(url, nState);
    }


    //we use our own actionbar to avoid the crap real one
    private void setupFakeActionBar()
    {
        //fake actionbar menu button
        //we use this for:
        //-change password
        //-save
        //
        final LinearLayout fakeActionBarButtonHolder = (LinearLayout) findViewById(R.id.fakeActionBarButtonHolder);
        //hide it right away
        fakeActionBarButtonHolder.setVisibility(View.GONE);

        ImageButton ibMenu = (ImageButton)    findViewById(R.id.ibMenu);
        ibMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ibMenu hit.");
                Tools.toggleActionBarMenu(fakeActionBarButtonHolder);
            }
        });
        //and its buttons:
        Button btA = (Button)    findViewById(R.id.btA);
        btA.setText(" Save ");
        btA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("SAVE btA hit.");
               // savePharmacyProfile();
                doNetworkingToSavePharmacyProfile();
                Tools.toggleActionBarMenu(fakeActionBarButtonHolder);
            }
        });
        Button btB = (Button)    findViewById(R.id.btB);
        btB.setVisibility(View.GONE);
    }

    private void doNetworkingToSavePharmacyProfile()
    {
        _("doNetworkingToSaveProfile=========================================");
        if (!Tools.isNetworkAvailable(this) )
        {
            Tools.toast("This app needs an active Internet connection to work.",c);
            return;
        }

        //Do networking!
        String messageForProgressBar = "Saving pharmacy";
        String url = Networking.URL_SET_MY_PATIENT_PHARMACY_PROFILE;
        url = url.replace("{id}",myPharmacy.id);

        int nState = Networking.NETWORK_STATE_MY_PATIENT_PHARMACY_PROFILE;
        Networking n = new Networking(messageForProgressBar, c, true);

        Tools t = new Tools(c);
        t.setupPrefs(c);
        n.setUserPharmacyInfo(myPharmacy);// we call this specific method for this state so right vars are set up.
        n.execute(url, nState);
    }


    //a wrapper so we dont have to type this every time
    private void _(String s)
    {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", "PharmacyDetailsActivity" + "#######" + s);
    }
}
