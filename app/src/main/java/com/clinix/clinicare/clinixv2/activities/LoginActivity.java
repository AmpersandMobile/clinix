/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.common.Networking;
import com.clinix.clinicare.clinixv2.common.Tools;
import com.clinix.clinicare.clinixv2.dataobjs.SaveKeys;
import com.opentok.android.demo.opentoksamples.OpenTokSamples;

// a simple register activity class
public class LoginActivity extends Activity {

    Context c;
    Activity a;
    EditText etEmailAddress, etPassword;
    Button btLogin;
    TextView tvIAccept;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        _("LoginActivity Born!////////////////////////////////");
        c = this;
        a = this;
        final Tools t = new Tools(c);
        t.setupPrefs(c);

        setContentView(R.layout.login);

        etEmailAddress      = (EditText)    findViewById(R.id.etEmailAddress);
        etPassword          = (EditText)    findViewById(R.id.etPassword);

        //populate
        //for testing
        //etFullname.setText("Gareth Murfin");
        //etPassword.setText("MyPassword");
        //etEmailAddress.setText("gaz@garethmurfin.co.uk");
        //etUsername.setText("gmtest2");

        // populate form memory
        String username     = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SaveKeys.STORAGE_KEY_EMAIL_ADDRESS,null,this);
        String password     = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SaveKeys.STORAGE_KEY_PASSWORD,null,this);
        if (username!=null && username.length()>0)
        {
            etEmailAddress.setText(username);
            etPassword.setText(password);
        }
        //

        Button btLogin  = (Button) findViewById(R.id.btLogin);
        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("btLogin hit!");

                String emailAddress = etEmailAddress.getText() + "";
                String password = etPassword.getText() + "";
                _("emailAddress........:" + emailAddress);
                _("password............:" + password);

                if (    emailAddress.length()   == 0        ||
                        password.length()       == 0

                    ) {
                    Tools.toast("Please fill in all the info", c);
                    return;
                }

                if (Tools.USE_ANALYTICS) {
                    //Tools.sendAnalytics_ButtonPress(a, "ibSignUp");
                }

                //save what they have entered here so we can repopulate next time
                t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING,SaveKeys.STORAGE_KEY_EMAIL_ADDRESS,-1, emailAddress);
                t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING,SaveKeys.STORAGE_KEY_PASSWORD,-1, password);
                //

                if (!Tools.isNetworkAvailable(c) )
                {
                    Tools.toast("This app needs an active Internet connection to work.",c);
                    return;
                }


                //Do networking!
                String messageForProgressBar = "Logging in";
                String url = Networking.URL_LOGIN;
                int nState = Networking.NETWORK_STATE_LOGIN;

                Networking n = new Networking(messageForProgressBar, c, true);
                n.setLoginInfo(emailAddress, password);// we call this specific method for this state so right vars are set up.
                n.execute(url, nState);
            }
        });

        //so they can click on t&c at bottom
        TextView tvAccept1  = (TextView) findViewById(R.id.tvAccept1);
        tvAccept1.setText(Html.fromHtml("By logging in I accept Clinix <U>Terms and<BR>Conditions</U> and Informed Consent"));
        tvAccept1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("tvIAccept1 onClick");
                Intent intent = new Intent(c, TermsAndConditionsActivity.class);
                c.startActivity(intent);
                /*Tools.showDialog(a,
                        "Terms and Conditions",
                        "Do you accept Clinix's Terms and Conditions and Informed Consent?",
                        "Yes, I accept",
                        "View Terms",
                        Tools.DIALOG_TERMS_AND_CONDITIONS);*/
            }
        });

        TextView tvAccept2  = (TextView) findViewById(R.id.tvAccept2);
        tvAccept2.setText(Html.fromHtml("<U>Conditions</U> and Informed Consent"));
        tvAccept2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("tvAccept2 onClick");
                Intent intent = new Intent(c, TermsAndConditionsActivity.class);
                c.startActivity(intent);
                /*Tools.showDialog(a,
                        "Terms and Conditions",
                        "Do you accept Clinix's Terms and Conditions and Informed Consent?",
                        "Yes, I accept",
                        "View Terms",
                        Tools.DIALOG_TERMS_AND_CONDITIONS);*/
            }
        });

        TextView tvForgotPassword = (TextView) findViewById(R.id.tvForgotPassword);
        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("tvForgotPassword onClick");
                Intent intent = new Intent(c, ForgotPasswordActivity.class);
                c.startActivity(intent);
            }
        });

        TextView tvNotRegistered = (TextView) findViewById(R.id.tvNotRegistered);
        tvNotRegistered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("tvNotRegistered onClick");
                Intent intent = new Intent(c, RegisterActivity.class);
                c.startActivity(intent);

            }
        });

        //DEBUG LAUNCH OpenTokSamples on logo hit
        ImageView ivClinix  = (ImageView) findViewById(R.id.ivClinix);
        ivClinix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ivClinix onClick");

                //now move to main menu
               // Intent intent = new Intent(c, OpenTokSamples.class);
              //  c.startActivity(intent);
            }
        });
   }

    @Override
    public void onResume() {
        super.onResume();
        _("///////////////onResume/////////////");
       // Tools.sendAnalytics_ScreenView(this, "RegisterActivity");//do a screenview for analytics
    }





    //a wrapper so we dont have to type this every time
    private void _(String s)
    {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", "LoginActivity" + "#######" + s);
    }
}
