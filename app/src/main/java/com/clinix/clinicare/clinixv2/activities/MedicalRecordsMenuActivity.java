/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.common.Tools;

public class MedicalRecordsMenuActivity extends Activity {

    Context c;
    Activity a;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        _("MedicalRecordsMenuActivity Born!////////////////////////////////");
        c = this;
        a = this;

        setContentView(R.layout.medicalrecordsmenu);

        ImageButton ibAppointments = (ImageButton)findViewById(R.id.ibAppointments);
        ibAppointments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ibAppointments hit.");
                //now move to main menu
                Intent intent = new Intent(c, AppointmentsActivity.class);
                c.startActivity(intent);
            }
        });

        //grey out 2 buttons
        ImageButton ibTestResults = (ImageButton)    findViewById(R.id.ibTestResults);
        ibTestResults.setClickable(false);
        ibTestResults.setBackgroundColor(Color.parseColor("#808080"));

        ImageButton ibHealthMonitoring = (ImageButton)    findViewById(R.id.ibHealthMonitoring);
        ibHealthMonitoring.setClickable(false);
        ibHealthMonitoring.setBackgroundColor(Color.parseColor("#808080"));

        TextView tvTestResults = (TextView)    findViewById(R.id.tvTestResults);
        TextView tvHealthMonitoring = (TextView)    findViewById(R.id.tvHealthMonitoring);
        tvTestResults.setTextColor(Color.parseColor("#808080"));
        tvHealthMonitoring.setTextColor(Color.parseColor("#808080"));

        ImageButton ibMedications = (ImageButton)    findViewById(R.id.ibMedications);
        ibMedications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ibMedications hit.");
                Intent intent = new Intent(c, MedicationsActivity.class);
                c.startActivity(intent);
            }
        });



        ImageButton ibHistory = (ImageButton)    findViewById(R.id.ibHistory);
        ibHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ibHistory hit.");
                Intent intent = new Intent(c, HistoryActivity.class);
                c.startActivity(intent);
            }
        });


        ImageButton ibAllergies = (ImageButton)    findViewById(R.id.ibAllergies);
        ibAllergies.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ibAllergies hit.");
                Intent intent = new Intent(c, AllergiesActivity.class);
                c.startActivity(intent);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        _("///////////////onResume/////////////");
       // Tools.sendAnalytics_ScreenView(this, "RegisterActivity");//do a screenview for analytics
    }

    //a wrapper so we dont have to type this every time
    private void _(String s)
    {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", "MedicalRecordsMenuActivity" + "#######" + s);
    }
}
