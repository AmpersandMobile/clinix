package com.clinix.clinicare.clinixv2.dataobjs;

/**
 * Created by USER on 28/10/2015.
 */
public class Allergy
{

    public String id;
    public String user_id;
    public String consultation_id;
    public String allergy;
    public String reaction;
    public String type;
    public String image;
    public String date;

    public Allergy(String id_, String user_id_ , String consultation_id_,String allergy_,String reaction_,String type_,String image_,String date_)
    {
        id=id_;
        user_id=user_id_;
        consultation_id=consultation_id_;
        allergy=allergy_;
        reaction=reaction_;
        type=type_;
        image=image_;
        date=date_;

    }
}
