/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.common.Networking;
import com.clinix.clinicare.clinixv2.common.Tools;

public class AboutUsMenuActivity extends Activity {

    Context c;
    Activity a;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _("AboutUsMenuActivity Born!////////////////////////////////");
        c = this;
        a = this;
        setContentView(R.layout.aboutusmenu);

        //this was removed
        /*
        ImageButton idAboutClinix = (ImageButton)    findViewById(R.id.idAboutClinix);
        idAboutClinix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("idAboutClinix hit.");
                Intent intent = new Intent(c, AboutUsActivity.class);
                c.startActivity(intent);
            }
        });
*/
        ImageButton ibTandC = (ImageButton)    findViewById(R.id.ibTandC);
        ibTandC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ibTandC hit.");
                Intent intent = new Intent(c, TermsAndConditionsActivity.class);
                c.startActivity(intent);
            }
        });

        ImageButton ibContactUs = (ImageButton)    findViewById(R.id.ibContactUs);
        ibContactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ibContactUs hit.");
                Intent intent = new Intent(c, ContactUsActivity.class);
                c.startActivity(intent);
            }
        });


        ImageButton ibPrivacy = (ImageButton)    findViewById(R.id.ibPrivacy);
        ibPrivacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ibPrivacy hit.");
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.clinix.co.uk/privacy"));
                startActivity(i);
            }
        });

        ImageButton ibHelp = (ImageButton)    findViewById(R.id.ibHelp);
        ibHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ibHelp hit.");
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.clinix.co.uk/help"));
                startActivity(i);
            }
        });


        //hidden button to test tok system

      /*  ImageButton ibDebugTok = (ImageButton)    findViewById(R.id.ibDebugTok);
        ibDebugTok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ibDebugTok hit.");
            }
        });*/
    }

    //networking to start a call//
    private void doNetworkingToStartCall()
    {
        _("doNetworkingToStartCall=========================================");
        if (!Tools.isNetworkAvailable(this) )
        {
            Tools.toast("This app needs an active Internet connection to work.",c);
            return;
        }

        //Do networking!
        String messageForProgressBar = "Starting call";
        String url = Networking.URL_START_CALL_TO_BEGIN_APPOINTMENT;//+"?all=1";
        int nState = Networking.NETWORK_STATE_START_CALL_TO_BEGIN_APPOINTMENT;

        Networking n = new Networking(messageForProgressBar, c, true);
        //n.setCallInfo();// we call this specific method for this state so right vars are set up.
        n.execute(url, nState);
    }

    @Override
    public void onResume() {
        super.onResume();
        _("///////////////onResume/////////////");
       // Tools.sendAnalytics_ScreenView(this, "RegisterActivity");//do a screenview for analytics
    }

    //a wrapper so we dont have to type this every time
    private void _(String s)
    {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", "AboutUsMenuActivity" + "#######" + s);
    }
}
