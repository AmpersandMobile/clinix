/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.common.Networking;
import com.clinix.clinicare.clinixv2.common.Tools;
import com.clinix.clinicare.clinixv2.dataobjs.SaveKeys;

// a simple register activity class
public class RegisterActivity extends Activity {

    Context c;
    Activity a;

    EditText etFirstName, etLastName, etEmailAddress, etPassword, etNHSNumber ;
    TextView tvDOB;
    Button btRegister;
    TextView tvIAccept;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        _("RegisterActivity Born!////////////////////////////////");

        c = this;
        a = this;
        final Tools t = new Tools(c);
        t.setupPrefs(c);

        setContentView(R.layout.register);

        etFirstName         = (EditText)    findViewById(R.id.etFirstName);
        etLastName          = (EditText)    findViewById(R.id.etLastName);
        etEmailAddress      = (EditText)    findViewById(R.id.etEmailAddress);
        etPassword          = (EditText)    findViewById(R.id.etPassword);
        tvDOB               = (TextView)    findViewById(R.id.tvDOB);
        etNHSNumber         = (EditText)    findViewById(R.id.etNHSNumber);

        //for testing
        //etFullname.setText("Gareth Murfin");
        //etPassword.setText("MyPassword");
        //etEmailAddress.setText("gaz@garethmurfin.co.uk");
        //etUsername.setText("gmtest2");
        //
        //populate form memory - do we want this on the reg screen? maybe not
        String firstName    = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SaveKeys.STORAGE_KEY_FIRST_NAME       ,null,this);
        String secondName   = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SaveKeys.STORAGE_KEY_LAST_NAME        ,null,this);
        String emailAddress = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SaveKeys.STORAGE_KEY_EMAIL_ADDRESS    ,null,this);
        String password     = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SaveKeys.STORAGE_KEY_PASSWORD         ,null,this);
        String dob          = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SaveKeys.STORAGE_KEY_DOB              ,null,this);
        String nhsNumer     = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SaveKeys.STORAGE_KEY_NHS_NUMBER       ,null,this);
        if (firstName!=null     && firstName.length()>0     ) { etFirstName.setText(firstName);         }
        if (secondName!=null    && secondName.length()>0    ) { etLastName.setText(secondName);         }
        if (emailAddress!=null  && emailAddress.length()>0  ) { etEmailAddress.setText(emailAddress);   }
        if (password!=null      && password.length()>0      ) { etPassword.setText(password);           }
        if (dob!=null           && dob.length()>0           ) { tvDOB.setText(dob);                     }
        if (nhsNumer!=null      && nhsNumer.length()>0      ) { etNHSNumber.setText(nhsNumer);          }

        Button btRegister  = (Button) findViewById(R.id.btRegister);
        btRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("btRegister hit!");

                String firstName = etFirstName.getText() + "";
                String lastName = etLastName.getText() + "";
                String emailAddress = etEmailAddress.getText() + "";
                String password = etPassword.getText() + "";
                String dob = tvDOB.getText() + "";
                String nhsNumber = etNHSNumber.getText() + "";
                String surgery_id = etNHSNumber.getText() + "";//FIX ME ONCE IN

                _("firstName...........:" + firstName);
                _("lastName............:" + lastName);
                _("emailAddress........:" + emailAddress);
                _("password............:" + password);
                _("dob.................:" + dob);
                _("nhsNumber...........:" + nhsNumber);
                _("surgery_id...........:"+ surgery_id);

                if (

                        firstName.length()      == 0        ||
                        lastName.length()       == 0        ||
                        emailAddress.length()   == 0        ||
                        password.length()       == 0        ||
                        dob.length()            == 0
                     //not compulsary:   nhsNumber.length()      == 0
                     //not compulsary:   surgery_id.length()      == 0
                    ) {
                    Tools.toast("Please fill in all the info", c);
                    return;
                }

                //save what they have entered here so we can repopulate next time
                t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SaveKeys.STORAGE_KEY_FIRST_NAME,     -1, firstName);
                t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SaveKeys.STORAGE_KEY_LAST_NAME,      -1, lastName);
                t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SaveKeys.STORAGE_KEY_EMAIL_ADDRESS,  -1, emailAddress);
                t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SaveKeys.STORAGE_KEY_PASSWORD,       -1, password);
                t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SaveKeys.STORAGE_KEY_DOB,            -1, dob);
                t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SaveKeys.STORAGE_KEY_NHS_NUMBER,     -1, nhsNumber);
                t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SaveKeys.STORAGE_KEY_SURGERY_ID,     -1, surgery_id);

                if (Tools.USE_ANALYTICS) {
                    //Tools.sendAnalytics_ButtonPress(a, "ibSignUp");
                }

                doNetworkingToRegister(firstName, lastName, emailAddress, password, dob, nhsNumber, surgery_id);


            }
        });

        /*
        ibBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("back hit");

                if (Tools.USE_ANALYTICS)
                {
                    Tools.sendAnalytics_ButtonPress(a, "ibBack");
                }

                finish();
            }
        });
*/


        //special tap on date to pop up date picker
        tvDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tools.popupDateDialog(tvDOB,etNHSNumber,a,false);
            }
        });


        ImageButton ibInfo = (ImageButton) findViewById(R.id.ibInfo);
        ibInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("info tapped");
                Tools.showDialog(a,
                        "NHS Number",
                        "Please enter your NHS number to register as an NHS patient to access live video consultations with your GP",
                        "OK",
                        "View Terms",
                        Tools.DIALOG_NHS_NUMBER);
            }
        });




        //so they can click on t&c at bottom
        TextView tvIAccept  = (TextView) findViewById(R.id.tvIAccept);
        tvIAccept.setText(Html.fromHtml("I accept Clinix's <U>Terms and Conditions</U>"));
        tvIAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("tvIAccept onClick");
                Intent intent = new Intent(c, TermsAndConditionsActivity.class);
                c.startActivity(intent);
                /*Tools.showDialog(a,
                        "Terms and Conditions",
                        "Do you accept Clinix's Terms and Conditions and Informed Consent?",
                        "Yes, I accept",
                        "View Terms",
                        Tools.DIALOG_TERMS_AND_CONDITIONS);*/
            }
        });


        //DEBUG LAUNCH OpenTokSamples on logo hit
        ImageView ivClinix  = (ImageView) findViewById(R.id.ivClinix);
        ivClinix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ivClinix onClick");

                //now move to main menu
             //   Intent intent = new Intent(c, OpenTokSamples.class);
              //  c.startActivity(intent);
            }
        });

   }

    private void doNetworkingToRegister(String firstName, String lastName, String emailAddress, String password, String dob, String nhsNumber, String surgery_id)
    {
        if (!Tools.isNetworkAvailable(this) )
        {
            Tools.toast("This app needs an active Internet connection to work.",c);
            return;
        }

        //Do networking!
        String messageForProgressBar = "Registering";
        String url = Networking.URL_REGISTER;
        int nState = Networking.NETWORK_STATE_REGISTER;

        Networking n = new Networking(messageForProgressBar, c, true);
        n.setRegisterInfo(firstName,lastName, emailAddress,password,dob,nhsNumber,surgery_id);// we call this specific method for this state so right vars are set up.
        n.execute(url, nState);
    }

    @Override
    public void onResume() {
        super.onResume();
        _("///////////////onResume/////////////");
       // Tools.sendAnalytics_ScreenView(this, "RegisterActivity");//do a screenview for analytics
    }





    //a wrapper so we dont have to type this every time
    private void _(String s)
    {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", "RegisterActivity" + "#######" + s);
    }
}
