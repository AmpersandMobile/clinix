/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.adapters;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.activities.PharmacyActivity;
import com.clinix.clinicare.clinixv2.common.Tools;
import com.clinix.clinicare.clinixv2.dataobjs.Pharmacy;

import java.util.ArrayList;

public class PharmacyAdapter extends BaseAdapter {//} implements AdapterView.OnItemClickListener {

    public ArrayList<Pharmacy> listOfItems;
    private LayoutInflater inflator;
    Context c;

    public PharmacyAdapter(Context c_, ArrayList<Pharmacy> theList){
        _("Adapter made.");
        listOfItems =theList;
        c=c_;
        inflator =LayoutInflater.from(c);

    }

    @Override
    public int getCount() {
        return listOfItems.size();
    }


    public ArrayList<Pharmacy> getListOfItems()
    {
        _("getListOfItems is returning "+listOfItems.size()+" listOfItems.");
        return listOfItems;
    }

    @Override
    public Object getItem(int arg0) {
        return null;
    }

    @Override
    public long getItemId(int arg0) {
        return 0;
    }

    public void flagThisItemAsDefault(int pos, Activity a)
    {
        /*
        //ask them if they want to make this the default pharmacy
        Pharmacy p = listOfItems.get(pos);
        Tools.showDialog(a,
                "Pharmacy",
                "Are you sure you want to set "+p.name+" as your default pharmacy?",
                "Yes",
                "Cancel",
                Tools.DIALOG_SET_DEFAULT_PHARMACY);

        //set none to default first
        for (int i=0; i<listOfItems.size();i++)
        {
            p = listOfItems.get(i);
            p.isDefault=false;
        }
        //and flag the one we hit
        p = listOfItems.get(pos);
        _("->"+p.name);
        p.isDefault=true;*/
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        _("getView");
        //map to row_pharmacy layout
        LinearLayout rowLinearLayout    = (LinearLayout) inflator.inflate(R.layout.row_pharmacy, parent, false);
        final TextView toptitle         = (TextView)(rowLinearLayout).findViewById(R.id.toptitle);
        final TextView infobelow        = (TextView)(rowLinearLayout).findViewById(R.id.infobelow);

        //get row_pharmacy using position
        final Pharmacy currentObj = listOfItems.get(position);
        toptitle.setText(currentObj.name);
        infobelow.setText(currentObj.address_2);

        CheckBox cbDefault = (CheckBox)(rowLinearLayout).findViewById(R.id.cbDefault);
        if (currentObj.isDefault)
        {
            _("this one is default "+currentObj.name);
            //show the tick on our default pharmacy
            cbDefault.setVisibility(View.VISIBLE);
            cbDefault.setChecked(true);
        }
        else
        {
            _("not default");
            cbDefault.setVisibility(View.INVISIBLE);
            cbDefault.setChecked(false);
        }

        return (rowLinearLayout);
    }

    //a wrapper so we dont have to type this every time
    private void _(String s)
    {
        if (!Tools.DEBUG){return;}
        Log.d("MyApp", "PharmacyAdapter" + "#######" + s);
    }

   /* @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        _("onItemClick");
        Pharmacy p = listOfItems.get(position);
        _("->"+p.name);
        p.isDefault=true;
        ((PharmacyActivity)c).listView.invalidateViews();//refresh it

    }*/
}