package com.clinix.clinicare.clinixv2.dataobjs;

import android.util.Log;

import com.clinix.clinicare.clinixv2.common.Tools;

import java.util.Date;

/**
 * Created by USER on 10/07/2015.
 */
public class Notification_ {

    public String subject;
    public String message;
    public String dateArrived;
    public boolean beenRead;

    //for making the friendly string
    public static final long ONE_SECOND  = 1000;
    public static final long ONE_MINUTE  = 60*(ONE_SECOND);
    public static final long ONE_HOUR    = 60*(ONE_MINUTE);
    public static final long ONE_DAY     = 24*(ONE_HOUR);
    public static final long ONE_MONTH   = 30*(ONE_DAY);
    public static final long ONE_YEAR    = 12*(ONE_MONTH);

    public static  Date rightNow;//we only need one
    String myFriendlyDateString;//we populate this to get the "15 mins ago" string

    public Notification_(String subject_, String message_, String dateArrived_, boolean beenRead_)
    {
        subject     = subject_;
        message     = message_;
        dateArrived = dateArrived_;
        beenRead    = beenRead_;
    }


    //LOOOKS THEY NOW SEND US A DATE STRING SOD LETS FOR ALL THIS FOR NOW
    /*
    public String getDateArrivedFriendlyString()
    {
        _("getDateArrived");
        String returnme="";
        if (myFriendlyDateString!=null)
        {
            _("returned cached string");
            returnme=myFriendlyDateString;
        }
        else
        {
            _("making new date from time {"+dateArrived+"}");
            if (dateArrived==0)
            {
                _("////////WARNING DATE ARRIVED NOT SET//////////////");
            }

            //we need to make a friendly string to say how long ago it was
            //ie, 15 mins ago
            //    2 hours ago
            //    1 day ago

            //lets assume right now doesnt need to be made each time, probably we should null this every time we come in
            //so it updates, but of course not for each item which is needless
            //if (rightNow==null)
            {
                rightNow = new Date( System.currentTimeMillis() );
            }
            long difference = rightNow.getTime() - dateArrived;

            //DEBUG/////
            _("ONE_SECOND   :"+ONE_SECOND);
            _("ONE_MINUTE   :"+ONE_MINUTE);
            _("ONE_HOUR     :"+ONE_HOUR);
            _("ONE_DAY      :"+ONE_DAY);
            _("ONE_MONTH    :"+ONE_MONTH);
            _("ONE_YEAR     :"+ONE_YEAR);

            _("TIME NOW, "+rightNow.getTime());
            _("TIME NOTIFICATION ARRIVED: "+dateArrived);
            _("difference is: "+difference);

            //work out how large the difference is and generate a string based on it..

            //TODO: technically we could optimise out a fair few divides here later if needed
            //is it minutes?
            if (difference>ONE_HOUR)
            {
                if (difference>ONE_DAY)
                {
                    //its days or maybe even months
                    if (difference>ONE_MONTH)
                    {
                        if (difference>ONE_YEAR)
                        {
                            returnme = (difference/ONE_YEAR)+" years ago.";////////////YEARS, will be rough, like 2 rather than 2.6
                            if ((difference/ONE_YEAR)==1)//need to remove S if its only 1
                            {
                                returnme = (difference/ONE_YEAR)+" year ago.";
                            }
                        }
                        else
                        {
                            returnme =  (difference/ONE_MONTH)+" months ago.";////////////MONTHS, will be rough, like 2 rather than 2.6
                            if ((difference/ONE_MONTH)==1)//need to remove S if its only 1
                            {
                                returnme = (difference/ONE_MONTH)+" month ago.";
                            }
                        }
                    }
                    else
                    {
                        returnme =  (difference/ONE_DAY)+" days ago.";////////////DAYS
                        if ((difference/ONE_DAY)==1)//need to remove S if its only 1
                        {
                            returnme = (difference/ONE_DAY)+" day ago.";
                        }
                    }
                }
                else
                {
                    returnme =  (difference/ONE_HOUR)+" hours ago.";////////////HOURS
                    if ((difference/ONE_HOUR)==1)//need to remove S if its only 1
                    {
                        returnme = (difference/ONE_HOUR)+" hour ago.";
                    }
                }
            }
            else
            {
                //yes its minutes, or maybe its seconds?
                if (difference<ONE_MINUTE)
                {
                    returnme =  (difference/ONE_SECOND)+" seconds ago.";////////////SECONDS
                    if ((difference/ONE_SECOND)==1)//need to remove S if its only 1
                    {
                        returnme = (difference/ONE_SECOND)+" second ago.";
                    }
                }
                else
                {
                    returnme =  (difference/ONE_MINUTE)+" mins ago.";///////////MINUTES
                    if ((difference/ONE_MINUTE)==1)//need to remove S if its only 1
                    {
                        returnme = (difference/ONE_MINUTE)+" mins ago.";
                    }
                }

            }

        }
        return returnme ;
    }

*/
    //a wrapper so we dont have to type this every time
    private void _(String s)
    {
        if (!Tools.DEBUG){return;}
        Log.d("MyApp", "Notification_" + "#######" + s);
    }
}
