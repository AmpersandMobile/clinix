/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.adapters.HistoryAdapter;
import com.clinix.clinicare.clinixv2.adapters.MedicationsAdapter;
import com.clinix.clinicare.clinixv2.common.Networking;
import com.clinix.clinicare.clinixv2.common.Tools;
import com.clinix.clinicare.clinixv2.dataobjs.History;
import com.clinix.clinicare.clinixv2.dataobjs.Medication;

import java.util.ArrayList;

public class HistoryActivity extends FragmentActivity {

    Context c;
    public static ArrayList<History> itemListHistory;
    public ListView listView;
    HistoryAdapter adapter;
    Activity a;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.history);
        c = this;
        a = this;

        doNetworkingToGetHistory();
        setupFakeActionBar();
    }

    private void doNetworkingToGetHistory()
    {
        _("doNetworkingToGetHistory=========================================");
        if (!Tools.isNetworkAvailable(this) )
        {
            Tools.toast("This app needs an active Internet connection to work.",c);
            return;
        }

        //Do networking!
        String messageForProgressBar = "Getting history";
        String url = Networking.URL_GET_MY_PATIENT_HISTORY;
        int nState = Networking.NETWORK_STATE_GET_MY_PATIENT_HISTORY;
        Networking n = new Networking(messageForProgressBar, c, true);

        Tools t = new Tools(c);
        t.setupPrefs(c);
        String id = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, "id",null,c);
        n.setHistoryInfo(id);// we call this specific method for this state so right vars are set up.
        n.execute(url, nState);
    }

    public void fillUpList()
    {
        listView = (ListView)findViewById(R.id.mylist);
        _("fillUpSongList----");

        //make adapter and set it, this updates the view
        adapter = new HistoryAdapter(this, itemListHistory);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                _("////////////////////////clicked on list/////////////////////");
                //since only one can be the default we need to make sure all others are flagged as false now
                ///// adapter.flagThisItemAsDefault(position,a);
                History a = itemListHistory.get(position);
                _("" + a.diagnosis);
                _("id to pass over:"+a.id);
               Intent intent = new Intent(c, HistoryDetailsActivity.class);
                intent.putExtra("id",a.id);
               c.startActivity(intent);
                //   listView.invalidateViews();//refresh it
            }
        });

    }


    //we use our own actionbar to avoid the crap real one
    private void setupFakeActionBar()
    {
        //fake actionbar menu button
        //we use this for:
        //-change password
        //-save
        //
        //This is basically a fake menu that pops up
        final LinearLayout fakeActionBarButtonHolder = (LinearLayout) findViewById(R.id.fakeActionBarButtonHolder);
        //hide it right away
        fakeActionBarButtonHolder.setVisibility(View.GONE);

        ImageButton ibMenu = (ImageButton)    findViewById(R.id.ibMenu);
        ibMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ibMenu hit.");
                Tools.toggleActionBarMenu(fakeActionBarButtonHolder);
            }
        });
        //and its buttons:
        Button btA = (Button)    findViewById(R.id.btA);
        btA.setText(" Add History ");
        btA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("Add History btA hit.");
                Intent intent = new Intent(c, HistoryDetailsActivity.class);
                 c.startActivity(intent);
                //Tools.toast("Add History. [NOT IMPLEMENTED]", c);
                //Tools.toggleActionBarMenu(fakeActionBarButtonHolder);
            }
        });
        Button btB = (Button)    findViewById(R.id.btB);
        btB.setVisibility(View.GONE);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        _("///////////////onResume/////////////");
    }

    //a wrapper so we dont have to type this every time
    private void _(String s) {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", "HistoryActivity" + "#######" + s);
    }
}
