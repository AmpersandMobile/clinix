/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2; /**
 * Created by user on 07/03/2015.
 */
import android.app.Application;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import org.acra.*;
import org.acra.annotation.*;
import org.json.JSONObject;

import com.clinix.clinicare.clinixv2.activities.LoginActivity;
import com.clinix.clinicare.clinixv2.common.Tools;
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.ParseUser;
import com.parse.PushService;
import com.parse.SaveCallback;

import java.util.List;

import static com.parse.ParseACL.*;

//THIS CLASS IS FOR SENDING THE EMAIL DEBUG REPORTS..
@ReportsCrashes(
        //formKey = "", // will not be used
        mailTo = "gareth.murfin@gmail.com",
        customReportContent = { ReportField.APP_VERSION_CODE, ReportField.APP_VERSION_NAME, ReportField.ANDROID_VERSION, ReportField.PHONE_MODEL, ReportField.CUSTOM_DATA, ReportField.STACK_TRACE, ReportField.LOGCAT },
        mode = ReportingInteractionMode.TOAST,
        logcatArguments = { "-t", "100", "-v", "time", "MyApp:D", "*:S" },//only show MyApp ones, comment this line out for regular logcat,
        resToastText = R.string.crash_toast_text)
public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        _("////////////////////////////////////////////////////////////");
        _("APPLICATION BORN. " + Tools.VERSION);
        _("////////////////////////////////////////////////////////////");

        Tools t = new Tools(this);

        // The following line triggers the initialization of ACRA
        ACRA.init(this);
        getTracker();//had to adjust analytics, this should be called starytAnalytics, if it works
        _("ACRA INITIALISED--------------------------------------------- ");


        //PARSE SDK, for receiving push
        // Enable Local Datastore.
        ////Parse.enableLocalDatastore(this);

        //                      app id                                      // client key
        Parse.initialize(this, getResources().getString(R.string.parse_app_id), getResources().getString(R.string.parse_client_key));


        //WE DONT SUBSCRIBE UNTIL WE LOGIN, SO WE ALWAYS HAVE A USER ID TO USE,
        //therefore this has been moved to networking.

/*
        String myUserId = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, "id", "-1", this);
        if (myUserId.equals("-1"))
        {
            Tools.toast("warning: cant subscribe to parse with this id of -1.",this);
        }
        else
        {
            //we need to subscribe to a specific channel for this user, in order to receive the push messages from parse, here is how dom describes the push:
            //
            //[4:20:26 PM] Dominic Talbot: Ok so, Push with Parse. When certain endpoints are hit on the server i.e. /appointments/book it triggers a few events:
            // email; web notification and push notification. The push notification is then sent to parse as a simple cURL request.
            // Parse then distributes it down to the devices which the user has logged in on
            //[4:20:48 PM] Dominic Talbot: so the channel works like “user-{id}”
            //[4:21:09 PM] Dominic Talbot: So upon sign in, you want to register onto the parse servers on that channel
            //[4:21:15 PM] Dominic Talbot: the device will then receive that users push notifications
            ///
                _("now subscribing on parse channel: " + "user-" + myUserId);
                //ParsePush.unsubscribeInBackground("user-");//dont subscribe to this one
                //ParsePush.subscribeInBackground("user-" + myUserId);
                //ParsePush.subscribeInBackground("broadcast");

                ParsePush.subscribeInBackground("user-" + myUserId, new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e == null) {
                            _("successfully subscribed to the broadcast channel.");
                        } else {
                            _("failed to subscribe for push "+ e.getMessage());
                        }
                    }
                });
        }

        ParsePush.subscribeInBackground("broadcast", new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    _("successfully subscribed to the broadcast channel.");
                } else {
                    _("failed to subscribe for push"+ e.getMessage());
                }
            }
        });

        ParseInstallation.getCurrentInstallation().saveInBackground();

*/
        _("PARSE INITIALISED--------------------------------------------- ");

        ///
        //ParseUser.enableAutomaticUser();
        //ParseACL defaultACL = new ParseACL();
        // Optionally enable public read access.
        // defaultACL.setPublicReadAccess(true);
        //setDefaultACL(defaultACL, true);
/*
        _("my channels : ");
        List<String> subscribedChannels = ParseInstallation.getCurrentInstallation().getList("channels");
        for (int i=0; i<subscribedChannels.size(); i++)
        {
            _(i+"// "+subscribedChannels.get(i));
        }
*/
        //from their sample app:///
/*
        // Enable Local Datastore.
        Parse.enableLocalDatastore(this);

        // Add your initialization code here
        Parse.initialize(this);

        ParseUser.enableAutomaticUser();
        ParseACL defaultACL = new ParseACL();
        // Optionally enable public read access.
        // defaultACL.setPublicReadAccess(true);
        setDefaultACL(defaultACL, true);
        */
        //////
    }

    //GOOGLE ANALYTICS////////////////////////////////////
    //////////////////////////////////////////////////////
    //WE DONT NEED IN THIS APP YET

    public static int GENERAL_TRACKER = 0;
    public enum TrackerName {
        APP_TRACKER, // Tracker used only in this app.
    }

  //  HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

    public synchronized void /*Tracker*/ getTracker(){//TrackerName trackerId) {
        //if (!mTrackers.containsKey(trackerId)) {
/*
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            analytics.getLogger().setLogLevel(Logger.LogLevel.VERBOSE);
            analytics.setLocalDispatchPeriod(1800);
            //Tracker t = (trackerId == TrackerName.APP_TRACKER) ? analytics.newTracker(PROPERTY_ID);

            //if((trackerId == TrackerName.APP_TRACKER)) {
                //_("GOOGLE ANALYTICS STARTING UP WITH TRACKID " + trackerId+" AND PROPERY_ID "+Tools.GOOGLE_ANALYTICS_PROPERTY_ID);

                _("//// Start analytics for "+Tools.GOOGLE_ANALYTICS_PROPERTY_ID);
                Tracker t  = analytics.newTracker(Tools.GOOGLE_ANALYTICS_PROPERTY_ID);
                t.enableAdvertisingIdCollection(true); //do we want this ? or ones below?
                t.enableExceptionReporting(true);
                t.enableAutoActivityTracking(true);
             //   mTrackers.put(trackerId, t);
            //}
            //else
            //{
            //    _("GOOGLE ANALYTICS WILL NOT BE USED. WARNING! WILL NOT DO TRACKING WITH THIS ID " + trackerId);
             //   if (Tools.SHOW_DEBUG_TOASTS)
             //   {
//
             //       Tools.toast("warning analytics will not track",this.getApplicationContext());
             //   }

           // }
           // Tracker t = (trackerId == TrackerName.APP_TRACKER) ? analytics.newTracker(PROPERTY_ID)
           //         : (trackerId == TrackerName.GLOBAL_TRACKER) ? analytics.newTracker(R.xml.global_tracker)
           //         : analytics.newTracker(R.xml.ecommerce_tracker);

       // }
        //return mTrackers.get(trackerId);*/
    }
    ///////////////////////////


    //protected void onPushReceive(Context context, Intent intent) {
       // super.onPushReceive(context,intent);
      //  _("onPushReceive");
        /*JSONObject data = getDataFromIntent(intent);
        // Do something with the data. To create a notification do:

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setContentTitle("Title");
        builder.setContentText("Text");
        builder.setSmallIcon(R.drawable.notification_template_icon_bg);
        builder.setAutoCancel(true);

        // OPTIONAL create soundUri and set sound:
        //builder.setSound(soundUri);

        notificationManager.notify("MyTag", 0, builder.build());
*/
 //   }

    //a wrapper so we dont have to type this every time
    private void _(String s)
    {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", "MyApplication" + "#######" + s);
    }
}
