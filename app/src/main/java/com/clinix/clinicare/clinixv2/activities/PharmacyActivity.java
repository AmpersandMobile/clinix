/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.common.Tools;
import com.clinix.clinicare.clinixv2.adapters.PharmacyAdapter;
import com.clinix.clinicare.clinixv2.dataobjs.Pharmacy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class PharmacyActivity extends FragmentActivity {

    Context c;
    private ArrayList<Pharmacy> itemList;
    public ListView listView;
    PharmacyAdapter adapter;
    Activity a;
    LinearLayout fakeActionBarButtonHolder;//this holds the buttons in the fake action bar

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.pharamcies);
        c = this;
        a = this;
        fillUpList();
       // setupFakeActionBar();
    }



    private void fillUpList()
    {
        listView = (ListView)findViewById(R.id.mylist);
        _("fillUpSongList----");
        itemList = new ArrayList<Pharmacy>();

       /* Pharmacy p = new Pharmacy();
        p.name="Lloyds Pharmacy";
        p.address="5 High Street, Town Name, ABC 123";

        itemList.add(p);

        p = new Pharmacy();
        p.name="Boots the Chemist";
        p.address="23 Church Lane, Another Town, XYZ, 321";
        itemList.add(p);

        p = new Pharmacy();
        p.name="Family Pharmacy";
        p.address="Example Street, Fictiontopia, SW1. 3PE";
        itemList.add(p);

        //sort them by distance
        Collections.sort(itemList, new Comparator<Pharmacy>() {
            public int compare(Pharmacy a, Pharmacy b) {
                return a.name.compareTo(b.name);//    return a.getRealpath().compareTo(b.getRealpath()); // to order by filename!
            }
        });

        //make adapter and set it, this updates the view
        adapter = new PharmacyAdapter(this, itemList);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                _("clicked");
                //since only one can be the default we need to make sure all others are flagged as false now
                adapter.flagThisItemAsDefault(position,a);
                listView.invalidateViews();//refresh it
            }
        });*/
    }

    @Override
    public void onResume()
    {
        super.onResume();
        _("///////////////onResume/////////////");
    }

    //a wrapper so we dont have to type this every time
    private void _(String s) {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", "PharmacyActivity" + "#######" + s);
    }
}
