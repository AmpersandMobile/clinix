package com.clinix.clinicare.clinixv2.activities;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageButton;
import android.widget.TextView;
import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.common.Tools;

public class MedicalRecordsActivity extends Activity {

    Context c;
    Activity a;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        _("MedicalRecordsActivity Born!////////////////////////////////");
        c = this;
        a = this;

        setContentView(R.layout.medicalrecordsmenu);

        //grey out 2 buttons
        ImageButton ibTestResults = (ImageButton)    findViewById(R.id.ibTestResults);
        ibTestResults.setClickable(false);
        ibTestResults.setBackgroundColor(Color.parseColor("#808080"));

        ImageButton ibHealthMonitoring = (ImageButton)    findViewById(R.id.ibHealthMonitoring);
        ibHealthMonitoring.setClickable(false);
        ibHealthMonitoring.setBackgroundColor(Color.parseColor("#808080"));

        TextView tvTestResults = (TextView)    findViewById(R.id.tvTestResults);
        TextView tvHealthMonitoring = (TextView)    findViewById(R.id.tvHealthMonitoring);
        tvTestResults.setTextColor(Color.parseColor("#808080"));
        tvHealthMonitoring.setTextColor(Color.parseColor("#808080"));
    }

    @Override
    public void onResume() {
        super.onResume();
        _("///////////////onResume/////////////");
       // Tools.sendAnalytics_ScreenView(this, "RegisterActivity");//do a screenview for analytics
    }

    //a wrapper so we dont have to type this every time
    private void _(String s)
    {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", "MedicalRecordsActivity" + "#######" + s);
    }
}
