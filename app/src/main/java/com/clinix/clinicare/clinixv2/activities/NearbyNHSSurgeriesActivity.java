/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.adapters.MedicationsAdapter;
import com.clinix.clinicare.clinixv2.adapters.SurgeriesAdapter;
import com.clinix.clinicare.clinixv2.common.Networking;
import com.clinix.clinicare.clinixv2.common.Tools;
import com.clinix.clinicare.clinixv2.dataobjs.Medication;
import com.clinix.clinicare.clinixv2.dataobjs.Surgery;

import java.util.ArrayList;

public class NearbyNHSSurgeriesActivity extends FragmentActivity {

    Context c;
    public static ArrayList<Surgery> itemListSurgeries;
    public ListView listView;
    SurgeriesAdapter adapter;
    Activity a;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.localsurgerys);
        c = this;
        a = this;

        doNetworkingToFindNearbySurgeries();
        setupFakeActionBar();
    }

    public static String lngS = "";
    public static String latS = "";
    private void doNetworkingToFindNearbySurgeries()
    {
        _("doNetworkingToFindNearbySurgeries=========================================");
        if (!Tools.isNetworkAvailable(this) )
        {
            Tools.toast("This app needs an active Internet connection to work.",c);
            return;
        }

        //Do networking!
        String messageForProgressBar = "Getting local surgeries";
        String url = Networking.URL_GET_LOCAL_SURGERIES;
        int nState = Networking.NETWORK_STATE_GET_LOCAL_SURGERIES;
        Networking n = new Networking(messageForProgressBar, c, true);

        Tools t = new Tools(c);
        t.setupPrefs(c);
        ///String type = "patient"; // this can be admin / practitioner / patient
        // String id = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, "id",null,c);
        // n.setHistoryInfo(id);// we call this specific method for this state so right vars are set up.

        String postcode; //we can pass lng, lat OR a postcode.

        //to test its working this is where a known surgery is
        if (Tools.PRETEND_TO_AT_CLIENTS_SURGERY_LOCATION)
        {
            _("warning PRETEND_TO_AT_CLIENTS_SURGERY_LOCATION is ON, do you really want this???");
            latS="51.3244376";
            lngS="-0.7563062";
        }

        n.setMyLocationInfo(lngS,latS);
        n.execute(url, nState);
    }

    public void fillUpList()
    {
        listView = (ListView)findViewById(R.id.mylist);
        _("fillUpList----");
      /*  itemListMedication = new ArrayList<Medication>();
        Medication a = new Medication("28/10/2015, 5:49","Amlodipine 5mg Tablets");

        itemListMedication.add(a);
        a = new Medication("28/10/2015, 5:49","Amlodipine 5mg Tablets");
        itemListMedication.add(a);
        a = new Medication("28/10/2015, 5:49","Amlodipine 5mg Tablets");
        itemListMedication.add(a);
        a = new Medication("28/10/2015, 5:49","Amlodipine 5mg Tablets");
        itemListMedication.add(a);
*/
        //buttons at top for filtering
     /*   Button btScheduled = (Button)findViewById(R.id.btScheduled);
        btScheduled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("//////onClick-btScheduled/////");
            }
        });

        Button btPrevious = (Button)findViewById(R.id.btPrevious);
        btPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("//////onClick-btPrevious/////");
            }
        });
*/
        //sort them by distance
        //Collections.sort(itemListScheduled, new Comparator<Appointment>() {
        //    public int compare(Appointment a, Appointment b) {
        //        return a.date.compareTo(b.date);//    return a.getRealpath().compareTo(b.getRealpath()); // to order by filename!
        //    }
        //});



        //make adapter and set it, this updates the view
        adapter = new SurgeriesAdapter(this, itemListSurgeries);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                _("////////////////////////clicked on list/////////////////////");
                //since only one can be the default we need to make sure all others are flagged as false now
                ///// adapter.flagThisItemAsDefault(position,a);
                Surgery s = itemListSurgeries.get(position);
                _("" + s.name+" Id:"+s.id);

                Intent intent = new Intent(c, ListOfGPsAtLocalSurgeryActivity.class);
                intent.putExtra("id", s.id);
                intent.putExtra("address", s.name+", "+s.street+", "+s.address_2+". "+s.postcode);
                c.startActivity(intent);
                //   listView.invalidateViews();//refresh it
            }
        });

        //set the button colours
        //shcedule starts selected, so we want it to be white with green font
      //  btScheduled.setBackgroundResource(R.drawable.buttonborder_whitebackground);
      //  btScheduled.setTextColor(Tools.CLINIX_GREEN);
        //and the previous button, green with white text
      //  btPrevious.setBackgroundResource(R.drawable.buttonborder_greenbackground);
      //  btPrevious.setTextColor(Tools.CLINIX_WHITE);

      ////  doNetworkingToGetAppointments();
    }



    //we use our own actionbar to avoid the crap real one
    private void setupFakeActionBar()
    {
        //fake actionbar menu button
        //we use this for:
        //-change password
        //-save
        //
        //This is basically a fake menu that pops up
        final LinearLayout fakeActionBarButtonHolder = (LinearLayout) findViewById(R.id.fakeActionBarButtonHolder);
        //hide it right away
        fakeActionBarButtonHolder.setVisibility(View.GONE);

        ImageButton ibMenu = (ImageButton)    findViewById(R.id.ibMenu);
        ibMenu.setVisibility(View.GONE);
        //and its buttons:
        Button btA = (Button)    findViewById(R.id.btA);
        btA.setVisibility(View.GONE);
        Button btB = (Button)    findViewById(R.id.btB);
        btB.setVisibility(View.GONE);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        _("///////////////onResume/////////////");


        if (itemListSurgeries==null)
        {
            _("no surgeries found so adding dummy onw to click on");
            itemListSurgeries = new  ArrayList<Surgery>();
            itemListSurgeries.add(new Surgery("My Surgery is not listed"));
            fillUpList();
        }
    }

    //a wrapper so we dont have to type this every time
    private void _(String s) {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", "NearbyNHSSurgeriesActivity" + "#######" + s);
    }
}
