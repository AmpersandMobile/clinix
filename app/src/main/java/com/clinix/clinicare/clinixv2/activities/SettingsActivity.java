/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.common.Tools;

public class SettingsActivity extends Activity {

    Context c;
    Activity a;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        _("SettingsActivity Born!////////////////////////////////");
        c = this;
        a = this;

        setContentView(R.layout.settingsmenu);

        ImageButton ibProfile = (ImageButton)    findViewById(R.id.ibProfile);
        ibProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(c, ProfileActivity.class);
                c.startActivity(intent);
            }
        });

        ImageButton ibMyGP = (ImageButton)    findViewById(R.id.ibMyGP);
        ibMyGP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(c, SurgeryDetailsActivity.class);
                c.startActivity(intent);
            }
        });

        ImageButton ibPharmacy = (ImageButton)    findViewById(R.id.ibPharmacy);
        ibPharmacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               //DONT THIN KWE GO TO THE LIST OF PAHRAMCIES HERE ANYMORE WE GO TO PHARMACY DETAILS GRABBED FROM SERVER?
                Intent intent = new Intent(c, PharmacyDetailsActivity.class);///OLD LIST ONE ->>>>>>>PharmacyActivity.class);
                c.startActivity(intent);
            }
        });

        ImageButton ibMyAccount = (ImageButton)    findViewById(R.id.ibMyAccount);
        ibMyAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(c, MyAccountActivity.class);
                c.startActivity(intent);
            }
        });




    }

    @Override
    public void onResume() {
        super.onResume();
        _("///////////////onResume/////////////");
       // Tools.sendAnalytics_ScreenView(this, "RegisterActivity");//do a screenview for analytics
    }

    //a wrapper so we dont have to type this every time
    private void _(String s)
    {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", "SettingsActivity" + "#######" + s);
    }
}
