package com.clinix.clinicare.clinixv2.dataobjs;

/**
 * Created by USER on 28/10/2015.
 */
public class History
{

    public String id;
    public String user_id;
    public String diagnosis;
    public String notes;
    public String practitioner_id;
    public String image;
    public String date;

    public History(String id_, String user_id_, String date_, String diagnosis_, String notes_)
    {
        date=date_;
        diagnosis=diagnosis_;
        id = id_;
        notes = notes_;
        user_id = user_id_;
    }

}
