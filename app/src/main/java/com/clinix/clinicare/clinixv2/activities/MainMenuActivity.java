/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.common.Networking;
import com.clinix.clinicare.clinixv2.common.Tools;
import com.opentok.android.demo.opentoksamples.OpenTokSamples;
import com.pkmmte.view.CircularImageView;

public class MainMenuActivity extends Activity {

    Context c;
    Activity a;
    Tools t;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        _("MainMenuActivity Born!////////////////////////////////");
        c = this;
        a = this;
        t = new Tools(c);
        t.setupPrefs(c);

        setContentView(R.layout.mainmenu);

        ImageButton ibBookAppointment = (ImageButton)findViewById(R.id.ibBookAppointment);
        ibBookAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ibBookAppointment hit.");
                //now move to main menu
                Intent intent = new Intent(c, BookAppointmentActivity_Page1.class);
                c.startActivity(intent);
            }
        });

        ImageButton ibSettings = (ImageButton)findViewById(R.id.ibSettings);
        ibSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("Settings hit.");
                //now move to main menu
                Intent intent = new Intent(c, SettingsActivity.class);
                c.startActivity(intent);
            }
        });

        ImageButton ibAbout = (ImageButton)    findViewById(R.id.ibAbout);
        ibAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("About hit.");
                Intent intent = new Intent(c, AboutUsMenuActivity.class);
                c.startActivity(intent);
            }
        });

        ImageButton ibMedicalRecords = (ImageButton)    findViewById(R.id.ibMedicalRecords);
        ibMedicalRecords.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ibMedicalRecords hit.");
                Intent intent = new Intent(c, MedicalRecordsMenuActivity.class);
                c.startActivity(intent);
            }
        });

        ImageButton ibNotification = (ImageButton)    findViewById(R.id.ibNotification);
        ibNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ibNotification hit.");
                Intent intent = new Intent(c, NotificationActivity.class);
                c.startActivity(intent);
            }
        });

        setupFakeActionBar();


        //FOR NOW I DO NETWORK CALLS HERE TO MAKE SURE THEY WORK//

    doNetworkingToGetMyProfile();////to update the pic live but fix this so it reads from memory again on this specific page and only get live on on the profile page ?

//AND ME APRIL 12

        if (Networking.USER_JUST_REGISTERED)
        {
            Networking.USER_JUST_REGISTERED=false;
            _("USER_JUST_REGISTERED show first time stuff.");

            //step one instructions about app//
            Intent intent = new Intent(c, FirstTimeRegistrationAppInstructionsActivity.class);
            c.startActivity(intent);

            //step two auto surgery selection app//
            //this happens in FirstTimeRegistrationAppInstructionsActivity when you hit ok
        }
    }

//COPIED THIS FROM THE PROFILE PAGE ITS PROB BEST TO NOTUSE THIS AND KEEP THE PATH OF THE PIC AND USE THAT INSYEAD, DO THIS :)
    private void doNetworkingToGetMyProfile()
    {
        _("doNetworkingToGetMyProfile=========================================");
        if (!Tools.isNetworkAvailable(this) )
        {
            Tools.toast("This app needs an active Internet connection to work.",c);
            return;
        }

        //Do networking!
        String messageForProgressBar = "Getting Profile details";
        String url = Networking.URL_GET_MY_PATIENT_PROFILE;
        int nState = Networking.NETWORK_STATE_GET_MY_PATIENT_PROFILE;
        Networking n = new Networking(messageForProgressBar, c, false);

        Tools t = new Tools(c);
        t.setupPrefs(c);
        ///String type = "patient"; // this can be admin / practitioner / patient
        // String id = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, "id",null,c);
        // n.setHistoryInfo(id);// we call this specific method for this state so right vars are set up.
        n.execute(url, nState);
    }



    public void updateProfilePic()
    {
        _("updateProfilePic");
        CircularImageView ibMyPic = (CircularImageView)findViewById(R.id.ibChosenByPic);      //we use a special circular imageview
        if (ProfileActivity.myProfile==null)
        {
            _("WARNING ProfileActivity.myProfile IS NULL!!! BAIL OUT");
            return;
        }
        //we call this in onResume in case they have changed it in profile, so it updates when they come back
        ProfileActivity.myProfile.image = t.loadPictureAndSetImage(ibMyPic,ProfileActivity.myProfile.image);


        //make it so u can tap the imageview with the pic in it

        ibMyPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("profile pic hit");
                Intent intent = new Intent(c, ProfileActivity.class);
                c.startActivity(intent);
            }
        });


        TextView tvName = (TextView) findViewById(R.id.tvName);
        if (ProfileActivity.myProfile.forename.length()>0 && ProfileActivity.myProfile.surname.length()>0)
        {
            tvName.setText(ProfileActivity.myProfile.forename+" "+ProfileActivity.myProfile.surname);
        }
        else if (ProfileActivity.myProfile.forename.length()>0)
        {
            tvName.setText(ProfileActivity.myProfile.forename);
        }
        else
        {
            tvName.setText("Welcome!");
        }

     /*   //set circular image
        CircularImageView ibChosenByPic = (CircularImageView)findViewById(R.id.ibChosenByPic);  //we use a special circular imageview
        Picasso.with(c).load(R.drawable.defaultavatar).into(ibChosenByPic);                     //set dummy pic
        t.loadPictureAndSetImage(ibChosenByPic);    //load their profile pic with picasso, if it exists

        //set their name if we have it
        Tools t = new Tools();
        t.setupPrefs(c);
        String firstName    = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SaveKeys.SAVE_KEY_FIRSTNAME, "", c);  //read name
        String secondName   = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SaveKeys.SAVE_KEY_LASTNAME, "", c);

        TextView tvName = (TextView) findViewById(R.id.tvName);
        if (firstName.length()>0 && secondName.length()>0)
        {
            tvName.setText(firstName+" "+secondName);
        }
        else if (firstName.length()>0)
        {
            tvName.setText(firstName);
        }
        else
        {
            tvName.setText("Welcome!");
        }*/
    }

    //we use our own actionbar to avoid the crap real one
    private void setupFakeActionBar()
    {
        //fake actionbar menu button
        //we use this for:
        //-change password
        //-save
        //
        final LinearLayout fakeActionBarButtonHolder = (LinearLayout) findViewById(R.id.fakeActionBarButtonHolder);
        //hide it right away
        fakeActionBarButtonHolder.setVisibility(View.GONE);

        ImageButton ibMenu = (ImageButton)    findViewById(R.id.ibMenu);
        ibMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ibMenu hit.");
                Tools.toggleActionBarMenu(fakeActionBarButtonHolder);
            }
        });
        //and its buttons:
        Button btA = (Button)    findViewById(R.id.btA);
        btA.setText("  Contact Us   ");
        btA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("Contact us btA hit.");
                Tools.toggleActionBarMenu(fakeActionBarButtonHolder);
                Intent intent = new Intent(c, ContactUsActivity.class);
                c.startActivity(intent);
            }
        });
        Button btB = (Button)    findViewById(R.id.btB);
        btB.setText("  Log out     ");
        btB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("btLogout hit.");
                Tools.toggleActionBarMenu(fakeActionBarButtonHolder);
                //wipe what we know about them now
                t.wipeEveryThingWeKnowAboutUser();
                //
                Intent intent = new Intent(c, LoginActivity.class);
                c.startActivity(intent);
            }
        });
    }

    //we must stop this timer when not on this screen MAYBE? DO WE REALLY WANT THIS,
    @Override
    public void onPause()
    {
        super.onPause();

        //no we dont want this we want them to be taken to the call screen from anywhere they are in the app when the time is right..
        //CONSIDER WHAT HAPPENS IF THEY LEAVE THE APP IT GETS KILLED THEN THEY RETURN TO IT BUT THEY ARENT ON MAIN MENU, THIS TIMER
        //WONT START, PERHAPS WE NEED TO DO THIS IN THE APPLICATION CLASS?
        /*if (timer!=null)
        {
            _("Cancel the appointment polling timer.");
            timer.cancel();
        }
        else
        {
            _("warning timer was null, cant cancel.. why is this ?");
        }*/
    }

    @Override
    public void onResume() {
        super.onResume();
        _("///////////////onResume/////////////");
        // Tools.sendAnalytics_ScreenView(this, "RegisterActivity");//do a screenview for analytics
        updateProfilePic();

        //Poll server to see if we have an appointment coming up (assuming notifications is turned off.)
        //note Android WILL NOT let us check if notifications are on or off, its write only apparently.
        //http://stackoverflow.com/questions/14225171/android-determine-if-notifications-are-turned-off-by-the-user
        //ONLY NEED THIS IF WE ARE CATERING FOR PEOPLE TURNING OFF NOTIFICATIONS IN SETTINGS,
        //FOR NOW LETS NOT CONSIDER THAT HENCE THIS IS OFF.
        //we need to know if there is an appointment coming up soon, we should receive a notification
        //so we can handle it then, but perhaps we need to consider if users have turned off notifications (probably
        //not but on ios they do, but on iOS there is a requestor allowing you to block notifications easily, in android
        //you need to do this in settings which seems less likely, especially considering we need them, so consider not
        // bothering with instances when people turn this off, perhaps detect and warn them?)
        // OK WE DO NEED THIS SINCE NOTIFICTIONS ARE UNRELIABLE!
        boolean showDebugOutput = true;

        //so we can display debug messages about how long until the appointment
        tvDebug = (TextView)findViewById(R.id.tvDebug);
        if (!showDebugOutput)
        {
            tvDebug.setVisibility(View.INVISIBLE);

        }
        else
        {
            tvDebug.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    _("DEBUG TAPPED GO TO THE CALL SCREEN");
                    timer.cancel(); // We cancel this here because we assume the call will set up and work, but if it doesnt then we dont keep looking
                    //CONSIDER continually polling until the networking class informs us we definitely got a token (as we dont get one until everything is right,
                    //ie, a real appointment is set up and appointment has an id on server

                    doNetworkingToGetTokBoxDetailsForCall();
                    //Intent intent = new Intent(c, OpenTokSamples.class);
                    //c.startActivity(intent);
                }
            });

        }

        //do one call then go into a loop which calls it every 59 secs:
        doNetworkingToGetNextAppointment();

        if (timer!=null)
        {
            _("cancel old timer");
            timer.cancel();
        }
        else
        {
            _("starting timer for first time");
        }
        timer = new CountDownTimer(59000,10000) {
            @Override
            public void onTick(long millisUntilFinished) {
                _("tick. (every one minute this will check for next appointments in case one is imminnent)");

                String detectMe = "in 1 minute";//"1 minute";

                //Count down for last 10 mins?
                if ( (tvDebug.getText()+"").contains("in 10 minutes") ) { Tools.toast(""+tvDebug.getText()+"",c); }
                if ( (tvDebug.getText()+"").contains("in 9 minutes") ) { Tools.toast(""+tvDebug.getText()+"",c); }
                if ( (tvDebug.getText()+"").contains("in 8 minutes") ) { Tools.toast(""+tvDebug.getText()+"",c); }
                if ( (tvDebug.getText()+"").contains("in 7 minutes") ) { Tools.toast(""+tvDebug.getText()+"",c); }
                if ( (tvDebug.getText()+"").contains("in 6 minutes") ) { Tools.toast(""+tvDebug.getText()+"",c); }
                if ( (tvDebug.getText()+"").contains("in 5 minutes") ) { Tools.toast(""+tvDebug.getText()+"",c); }
                if ( (tvDebug.getText()+"").contains("in 4 minutes") ) { Tools.toast(""+tvDebug.getText()+"",c); }
                if ( (tvDebug.getText()+"").contains("in 3 minutes") ) { Tools.toast(""+tvDebug.getText()+"",c); }
                if ( (tvDebug.getText()+"").contains("in 2 minutes") ) { Tools.toast(""+tvDebug.getText()+"",c); }
                ///if ( (tvDebug.getText()+"").contains("1 minute") ) { Tools.toast("Call begins "+tvDebug.getText()+"",c); }
                //

                //When there is 1 minute to go, we begin setting up the call by asking the server what the tokbox call details
                //are, once we get them, we go and sit on the tokbox call screen and wait for the practitioner to call us.
                if ( (tvDebug.getText()+"").contains(detectMe) )
                {
                    _("call starts in " + detectMe + ", cancel timer and grab tokbox details to set up call.");
                    timer.cancel();

                    //we need a session id and a token from the server to set up the tokbox call, so we get them here
                    //and when this networking is done, from there we go into the tokbox call screen
                    doNetworkingToGetTokBoxDetailsForCall();
                    //Intent intent = new Intent(c, OpenTokSamples.class);
                    //c.startActivity(intent);

                }
            }
            @Override
            public void onFinish() {
                try{
                    _("-DO POLL SINCE ONE MINUTE IS UP-");
                    doNetworkingToGetNextAppointment(); //need to poll this and if theres an appointment soon keep polling until the call is going to happen then go to call screen
                    timer.start();
                }catch(Exception e){
                    _("Error: " + e.toString());
                }
            }
        }.start();


        //TEST MAKING A CALL
    }
    public static TextView tvDebug;
    CountDownTimer timer;

    private void doNetworkingToGetTokBoxDetailsForCall()
    {
        _("doNetworkingToGetTokBoxDetailsForCall=========================================");
        if (!Tools.isNetworkAvailable(this) )
        {
            Tools.toast("This app needs an active Internet connection to work.",c);
            return;
        }

        //Do networking!
        String messageForProgressBar = "Call begins in 1 minute. Setting up call...";
        String url = Networking.URL_START_CALL_TO_BEGIN_APPOINTMENT;
        int nState = Networking.NETWORK_STATE_START_CALL_TO_BEGIN_APPOINTMENT;

        Networking n = new Networking(messageForProgressBar, c, false);
        n.setUserProfileInfo(ProfileActivity.myProfile);//so we have the info we need to pass over to get the tokbox call details (ie our username)
        n.execute(url, nState);
    }

    //check when the next appointment is,

    private void doNetworkingToGetNextAppointment()
    {
        _("doNetworkingToGetNextAppointment=========================================");
        if (!Tools.isNetworkAvailable(this) )
        {
            Tools.toast("This app needs an active Internet connection to work.",c);
            return;
        }

        //Do networking!
        String messageForProgressBar = "Getting next appointment";
        String url = Networking.URL_GET_MY_NEXT_APPOINTMENT;
        int nState = Networking.NETWORK_STATE_GET_MY_NEXT_APPOINTMENT;

        Networking n = new Networking(messageForProgressBar, c, false);
        n.execute(url, nState);

    }


    //a wrapper so we dont have to type this every time
    private void _(String s)
    {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", "MainMenuActivity" + "#######" + s);
    }
}
