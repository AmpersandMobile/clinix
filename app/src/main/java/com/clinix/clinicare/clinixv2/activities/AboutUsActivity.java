/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.common.Tools;

public class AboutUsActivity extends Activity {

    Context c;
    Activity a;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //_("AboutUsMenuActivity Born!////////////////////////////////");
        c = this;
        a = this;
        setContentView(R.layout.aboutusdetails);

        TextView tvAboutUs = (TextView)findViewById( R.id.tvAboutUs );

        String info = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas tincidunt nec risus sed hendrerit. Fusce convallis, ex vitae aliquet consequat, massa urna scelerisque arcu, ac rutrum tellus diam quis est. Maecenas sit amet ipsum iaculis, luctus eros eget, tristique orci. Praesent lacinia neque rutrum augue ornare pretium. Aenean auctor eleifend nibh, nec ullamcorper quam dignissim nec. Maecenas eu diam a neque consectetur fringilla eu sit amet eros. Integer risus ligula, efficitur vel metus a, bibendum ullamcorper eros. Sed neque sem, ullamcorper vitae efficitur sed, consequat eu sem. Cras dapibus augue ac auctor cursus. Pellentesque ipsum ipsum, laoreet vel euismod bibendum, eleifend nec arcu. Duis sollicitudin enim quam, a scelerisque ex convallis nec. Vestibulum accumsan, orci nec semper dapibus, tellus nunc auctor risus, ac feugiat eros augue quis nibh.\n" +
                "\n" +
                "Nullam egestas dictum neque, non lobortis elit iaculis vel. Aliquam nisl nibh, lacinia at facilisis nec, rhoncus sit amet lorem. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed finibus leo at tortor tincidunt mollis. In mattis nisi non tincidunt luctus. Nullam imperdiet non ante blandit malesuada. Phasellus fringilla ex nisi, sed volutpat leo sollicitudin nec. Ut mattis, ex sit amet pellentesque pulvinar, odio felis consectetur nunc, eu efficitur augue nibh non turpis.\n" +
                "\n" +
                "Pellentesque egestas vitae neque quis auctor. Cras eu leo lacinia, lacinia mauris in, dapibus nulla. Donec vel quam et sapien tincidunt rhoncus. Fusce vulputate quis sapien nec tempus. Mauris ornare, neque vitae fringilla tincidunt, ipsum quam lacinia turpis, vel consequat tortor mi non ligula. Integer euismod turpis ac nulla ornare lacinia. Aenean gravida, tellus eget varius rutrum, purus leo accumsan libero, vel tempus magna felis sed odio. Etiam vestibulum, odio eget lacinia ullamcorper, tellus purus sagittis ipsum, a feugiat ante metus eu ipsum. Proin maximus erat sed risus scelerisque tempus. Mauris porttitor nisi non quam consequat imperdiet. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Suspendisse ut leo dignissim, vulputate dui sit amet, euismod enim.\n" +
                "\n" +
                "Vivamus pretium tempor dignissim. Nullam auctor ante ut laoreet sollicitudin. Maecenas ultricies ligula sit amet dui lacinia consectetur. Fusce volutpat nibh massa, id congue eros tincidunt sit amet. Suspendisse posuere tempor libero, et accumsan metus. Vivamus sapien dolor, finibus ac pretium et, molestie a orci. Duis imperdiet luctus tempus. Nulla molestie lobortis risus, id sollicitudin lorem. Sed ac elementum erat, et tristique neque. Proin ut laoreet dolor. Sed ac nulla id justo rutrum sodales ut vitae nibh. Nullam et auctor neque.\n" +
                "\n" +
                "Proin non neque sit amet justo bibendum euismod. Phasellus tincidunt congue est a mollis. Ut massa quam, accumsan id velit elementum, viverra vehicula magna. Aliquam erat volutpat. Praesent convallis nisl velit, id ornare felis volutpat quis. Quisque in cursus ipsum, non congue erat. Etiam pharetra justo dolor, quis sagittis dui tempus condimentum.";
        tvAboutUs.setText(info);
    }

    @Override
    public void onResume() {
        super.onResume();
        //_("///////////////onResume/////////////");
       // Tools.sendAnalytics_ScreenView(this, "RegisterActivity");//do a screenview for analytics
    }

    //a wrapper so we dont have to type this every time
    private void _(String s)
    {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", "AboutUsMenuActivity" + "#######" + s);
    }
}
