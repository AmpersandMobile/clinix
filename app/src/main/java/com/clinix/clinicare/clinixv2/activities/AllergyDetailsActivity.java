/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.adapters.HistoryAdapter;
import com.clinix.clinicare.clinixv2.common.BuildPhotoDialog;
import com.clinix.clinicare.clinixv2.common.Networking;
import com.clinix.clinicare.clinixv2.common.Tools;
import com.clinix.clinicare.clinixv2.dataobjs.Allergy;
import com.clinix.clinicare.clinixv2.dataobjs.History;

import java.util.ArrayList;

public class AllergyDetailsActivity extends FragmentActivity {

    Context c;
    Activity a;

    Allergy myAllergy;
    String idOfMyAllergy;

    EditText tvAllergy, tvReaction;
    TextView tvDate, tvAllergyType;
    ImageView ivMyPic;
    public static String image; // this is the path to any image they specify
    BuildPhotoDialog buildPhotoObject;
    AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.allergies_details);
        c = this;
        a = this;
        idOfMyAllergy = getIntent().getStringExtra("id");
        _("id passed into activity: " + idOfMyAllergy);
        if (idOfMyAllergy==null)
        {
            _("since idOfMyAllergy is null you must be adding a new allergy");
            TextView tvTitle = (TextView)    findViewById(R.id.tvTitle);
            tvTitle.setText("Add an Allergy");
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        _("///////////////onResume/////////////");
        assignMyAllergyObject();
        populateGUI();
        setupFakeActionBar();

        if (image!=null && image.length()>0)
        {
            _("Picture for this appointment is "+image);
            ivMyPic = (ImageView)    findViewById(R.id.ivMyPic);
            image = new Tools(c).loadPictureAndSetImage(ivMyPic,image);

        }
        else
        {
            _("this allergy has no picture yet");

        }
    }

    private void assignMyAllergyObject()
    {
        //need to go through each list
        for (int i=0; i<AllergiesActivity.itemListFood.size(); i++)
        {
            Allergy a = AllergiesActivity.itemListFood.get(i);
            //_("Looking for ID "+idOfMyAllergy+". IS THIS IT? "+a.id);
            if (a.id.equals(idOfMyAllergy))
            {
                _("Allergy object assigned");
                myAllergy = a;
            }
        }
        for (int i=0; i<AllergiesActivity.itemListDrugs.size(); i++)
        {
            Allergy a = AllergiesActivity.itemListDrugs.get(i);
            //_("Looking for ID "+idOfMyAllergy+". IS THIS IT? "+a.id);
            if (a.id.equals(idOfMyAllergy))
            {
                _("Allergy object assigned");
                myAllergy = a;
            }
        }
        for (int i=0; i<AllergiesActivity.itemListOthers.size(); i++)
        {
            Allergy a = AllergiesActivity.itemListOthers.get(i);
            //_("Looking for ID "+idOfMyAllergy+". IS THIS IT? "+a.id);
            if (a.id.equals(idOfMyAllergy))
            {
                _("Allergy object assigned");
                myAllergy = a;
            }
        }
        if (myAllergy==null)
        {
            _("WARNING myAllergy was not assigned, could not find id: "+idOfMyAllergy);
        }
    }


    private void populateGUI()
    {
        tvDate          = (TextView) findViewById(R.id.tvDate);
        tvAllergy       = (EditText) findViewById(R.id.tvAllergy);
        tvReaction      = (EditText) findViewById(R.id.tvReaction);
        tvAllergyType   = (TextView) findViewById(R.id.tvAllergyType);
        ivMyPic         = (ImageView)findViewById(R.id.ivMyPic);

        Button ibAttachPhoto = (Button)    findViewById(R.id.ibAttachPhoto);
        //Profile picture - we need user to be able to tap this and select a photo
        ibAttachPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("user tapped attach pic");
                //start activity
                buildPhotoObject = new BuildPhotoDialog(ivMyPic, a, c, new Tools(c), BuildPhotoDialog.MODE_ADD_PIC_TO_ALLERGY);

            }
        });

        tvDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tools.popupDateDialog(tvDate, tvAllergy, a, false);
            }
        });


        tvAllergyType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                _("TAPPED tvAllergyType");
                final String[] items = new String[] { "Food","Drug","Other" };
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(c, android.R.layout.select_dialog_item, items);
                AlertDialog.Builder builder = new AlertDialog.Builder(c);
                builder.setTitle("Select Image");
                builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        _("//////onClick " + item);
                        switch (item)
                        {
                            case 0:
                                _("FOOD");
                                tvAllergyType.setText("Food");
                                break;
                            case 1:
                                _("DRUG");
                                tvAllergyType.setText("Drug");
                                break;
                            case 2:
                                _("OTHER");
                                tvAllergyType.setText("Other");
                                break;
                            default:
                                _("Unknown selection!");
                        }

                    }
                });

                dialog = builder.create();
                dialog.show();
            }
        });






        if (myAllergy==null)
        {
            _("warning myAllergy is still null! bailing.");
            return;
        }

        tvDate.setText(""+myAllergy.date);
        tvAllergy.setText(""+myAllergy.allergy);
        tvReaction.setText(""+myAllergy.reaction);
        tvAllergyType.setText(""+myAllergy.type);
    }

    //we use our own actionbar to avoid the crap real one
    private void setupFakeActionBar()
    {
        //fake actionbar menu button
        //we use this for:
        //-change password
        //-save
        //
        //This is basically a fake menu that pops up
        final LinearLayout fakeActionBarButtonHolder = (LinearLayout) findViewById(R.id.fakeActionBarButtonHolder);
        //hide it right away
        fakeActionBarButtonHolder.setVisibility(View.GONE);

        ImageButton ibMenu = (ImageButton)    findViewById(R.id.ibMenu);
        ibMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ibMenu hit.");
                Tools.toggleActionBarMenu(fakeActionBarButtonHolder);
            }
        });
        //and its buttons:
        Button btA = (Button)    findViewById(R.id.btA);
        btA.setText(" Save ");
        btA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("Save Allergy btA hit.");
                //ADD/SAVE ALLERGY ON SERVER
                doNetworkingToAddAnAllergy();
                Tools.toggleActionBarMenu(fakeActionBarButtonHolder);
            }
        });
        Button btB = (Button)    findViewById(R.id.btB);
        btB.setVisibility(View.GONE);
    }

    private void doNetworkingToAddAnAllergy()
    {
        _("doNetworkingToAddAnAllergy=========================================");
        if (!Tools.isNetworkAvailable(this) )
        {
            Tools.toast("This app needs an active Internet connection to work.",c);
            return;
        }

        //Do networking!
        String messageForProgressBar = "Adding Allergy";
        String url = Networking.URL_CREATE_AN_ALLERGY_FOR_MY_PATIENT;
        int nState = Networking.NETWORK_STATE_CREATE_AN_ALLERGY_FOR_MY_PATIENT;
        Networking n = new Networking(messageForProgressBar, c, true);

        Tools t = new Tools(c);
        t.setupPrefs(c);

        //make a new allergy with whatever info we know, leave stuff we dont know blank.
        String id_              = "";
        String user_id          = ProfileActivity.myProfile.id;//YEH?
        String consultation_id_ = "";
        String allergy_         = ""+tvAllergy.getText();
        String reaction_        = ""+tvReaction.getText();
        String type_            = ""+tvAllergyType.getText();
        String image_           = "";
        String date_            = tvDate.toString();

        Allergy a = new Allergy(id_,user_id,consultation_id_,allergy_,reaction_,type_,image_,date_);
        n.setNewAllergyDetails(a);// we call this specific method for this state so right vars are set up.
        n.execute(url, nState);
    }

    ////////////////SELECT PHOTO CODE///////////////
    //the work is done in BuildPhotoDialog object which wraps up all the implementation into a reusable form.
    //for when we get back the photo from the intent
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        _("onActivityResult///////////////////////////////////");
        buildPhotoObject.dealWithResultFromActivity( requestCode,  resultCode,  data);
    }

    //a wrapper so we dont have to type this every time
    private void _(String s) {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", "AllergyDetailsActivity" + "#######" + s);
    }
}
