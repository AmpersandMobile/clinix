/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.adapters.AppointmentsAdapter;
import com.clinix.clinicare.clinixv2.common.Networking;
import com.clinix.clinicare.clinixv2.common.Tools;
import com.clinix.clinicare.clinixv2.dataobjs.Appointment;
import com.clinix.clinicare.clinixv2.dataobjs.AvailableAppointment;

import java.util.ArrayList;
import java.util.Date;

public class BookAppointmentActivity_Page2 extends FragmentActivity {

    Context c;
    public static ArrayList<AvailableAppointment> itemListAvailableAppointments;
    public ListView listView;
    AppointmentsAdapter adapter;
    Activity a;

    public static final String CALL_TYPE_PHONE = "phone";
    public static final String CALL_TYPE_VIDEO = "video";

    AvailableAppointment mySelectedAppointment;
    public static String detailsOfTheProblem = "noneEntered.";
    String preferredCallType = "";

    public String dateofAppointmentRequired;
    public String typeOfSpecialistRequired;

    //a button for each appointment and an image button to overlay the tick
    ImageButton ibtA;
    ImageButton ibtB;
    ImageButton ibtC;

    ImageButton ibtD;
    ImageButton ibtE;
    ImageButton ibtF;

    ImageButton ibtG;
    ImageButton ibtH;
    ImageButton ibtI;

    //
    Button btA;
    Button btB;
    Button btC;

    Button btD;
    Button btE;
    Button btF;

    Button btG;
    Button btH;
    Button btI;


    ImageButton ibPhone;
    ImageButton ibVideo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.bookappointment2);
        c = this;
        a = this;

        dateofAppointmentRequired = getIntent().getStringExtra("dateofAppointmentRequired");
        _("dateofAppointmentRequired passed into activity: " + dateofAppointmentRequired);

        typeOfSpecialistRequired = getIntent().getStringExtra("typeOfSpecialistRequired");
        _("typeOfSpecialistRequired passed into activity: " + typeOfSpecialistRequired);
/*
        final Button btBookAppointment   = (Button)    findViewById(R.id.btBookAppointment);
        btBookAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("btBookAppointment clicked.");
                Tools.showDialog(a,
                        "Appointment",
                        "Who is this appointment for ?",
                        "Me",
                        "OTHER OPTIONS GO HERE?",
                        Tools.DIALOG_TERMS_AND_CONDITIONS);
            }
        });
*/
        /*
        * buttons are laid out in a grid of nine like this:
        *
        * A B C
        * D E F
        * G H I
        *
        * each button has 2 buttons. because there needs to be an (unclickable) image button with the tick on it to overlay
        *
        * */

        //these are the imagebuttons we use to overlay the tick
        ibtA   = (ImageButton)    findViewById(R.id.ibtA);
        ibtB   = (ImageButton)    findViewById(R.id.ibtB);
        ibtC   = (ImageButton)    findViewById(R.id.ibtC);

        ibtD   = (ImageButton)    findViewById(R.id.ibtD);
        ibtE   = (ImageButton)    findViewById(R.id.ibtE);
        ibtF   = (ImageButton)    findViewById(R.id.ibtF);

        ibtG   = (ImageButton)    findViewById(R.id.ibtG);
        ibtH   = (ImageButton)    findViewById(R.id.ibtH);
        ibtI   = (ImageButton)    findViewById(R.id.ibtI);

        //turn off all ticks until theyre tapped//
        //we also tag them so we know the tick if hidden
        ibtA.setImageResource(R.drawable.buttontickoverlay_transpare); ibtA.setTag("tickoff");
        ibtB.setImageResource(R.drawable.buttontickoverlay_transpare); ibtB.setTag("tickoff");
        ibtC.setImageResource(R.drawable.buttontickoverlay_transpare); ibtC.setTag("tickoff");

        ibtD.setImageResource(R.drawable.buttontickoverlay_transpare); ibtD.setTag("tickoff");
        ibtE.setImageResource(R.drawable.buttontickoverlay_transpare); ibtE.setTag("tickoff");
        ibtF.setImageResource(R.drawable.buttontickoverlay_transpare); ibtF.setTag("tickoff");

        ibtG.setImageResource(R.drawable.buttontickoverlay_transpare); ibtG.setTag("tickoff");
        ibtH.setImageResource(R.drawable.buttontickoverlay_transpare); ibtH.setTag("tickoff");
        ibtI.setImageResource(R.drawable.buttontickoverlay_transpare); ibtI.setTag("tickoff");

        //normal buttons that we can hit//

        btA   = (Button)    findViewById(R.id.btA);
        btA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("btA clicked.");
                untickAllButtons();
                toggleButtonState(ibtA,0);
            }
        });
        btB   = (Button)    findViewById(R.id.btB);
        btB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("btB clicked.");
                untickAllButtons();
                toggleButtonState(ibtB,1);
            }
        });
        btC   = (Button)    findViewById(R.id.btC);
        btC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("btC clicked.");
                untickAllButtons();
                toggleButtonState(ibtC,2);
            }
        });

        btD   = (Button)    findViewById(R.id.btD);
        btD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("btD clicked.");
                untickAllButtons();
                toggleButtonState(ibtD,3);
            }
        });
        btE   = (Button)    findViewById(R.id.btE);
        btE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("btE clicked.");
                untickAllButtons();
                toggleButtonState(ibtE,4);
            }
        });
        btF   = (Button)    findViewById(R.id.btF);
        btF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("btF clicked.");
                untickAllButtons();
                toggleButtonState(ibtF,5);
            }
        });

        btG   = (Button)    findViewById(R.id.btG);
        btG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("btG clicked.");
                untickAllButtons();
                toggleButtonState(ibtG,6);
            }
        });
        btH   = (Button)    findViewById(R.id.btH);
        btH.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("btH clicked.");
                untickAllButtons();
                toggleButtonState(ibtH,7);
            }
        });
        btI   = (Button)    findViewById(R.id.btI);
        btI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("btI clicked.");
                untickAllButtons();
                toggleButtonState(ibtI,8);
            }
        });

        doNetworkingToGetAvailableAppointments();

        //updateAvailableAppointmentsButtons();


        //add details//
        final Button btAddDetails   = (Button)    findViewById(R.id.btAddDetails);
        btAddDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("btAddDetails clicked.");
                Intent intent = new Intent(c, BookAppointment_NotesActivity.class);
                c.startActivity(intent);
            }
        });

        //call type
        ibPhone   = (ImageButton)    findViewById(R.id.ibPhone);
        ibPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                phoneClicked();
            }
        });
        ibVideo   = (ImageButton)    findViewById(R.id.ibVideo);
        ibVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoClicked();
            }
        });

        //make video call clicked by defaukt
        videoClicked();

        //book appointment bbutton//
        final Button btBookAppointment   = (Button)    findViewById(R.id.btBookAppointment);
        btBookAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("btBookAppointment clicked.");


                Intent intent = new Intent(c, BookAppointmentActivity_Page3.class);

                if (mySelectedAppointment==null)
                {
                    Tools.toast("Please select an available appointment.",c);
                    return;
                }




                String mySelectedAppointmentTime = mySelectedAppointment.start;
                _("mySelectedAppointmentTime to pass over:" + mySelectedAppointmentTime);
                intent.putExtra("mySelectedAppointmentTime", mySelectedAppointmentTime);

                _("typeOfSpecialistRequired to pass over:" + typeOfSpecialistRequired);
                intent.putExtra("typeOfSpecialistRequired", typeOfSpecialistRequired);

                //dont let them proceed without adding a problem to the details section
                if (detailsOfTheProblem==null || detailsOfTheProblem.length()==0)
                {
                    _("detailsOfTheProblem was empty");
                    Tools.toast("Please enter a problem.", c);
                    return;
                    //_("detailsOfTheProblem was empty put dummy values in.");
                    //detailsOfTheProblem="...";
                }
                _("detailsOfTheProblem to pass over:"+detailsOfTheProblem);
                intent.putExtra("detailsOfTheProblem", detailsOfTheProblem);

                _("preferredCallType to pass over:" + preferredCallType+"");
                intent.putExtra("preferredCallType", preferredCallType+"");

                c.startActivity(intent);
            }
        });

        AvailableAppointment mySelectedAppointment;
        String detailsOfTheProblem = "";
        String preferredCallType = "";
    }

    private void phoneClicked()
    {
        _("ibPhone clicked.");
        preferredCallType = CALL_TYPE_PHONE;
        ibPhone.setImageResource(R.drawable.circleiconphone2xticked);
        ibVideo.setImageResource(R.drawable.circleiconvideosmall2x);
    }

    private void videoClicked()
    {
        _("ibVideo clicked.");
        preferredCallType = CALL_TYPE_VIDEO;
        ibVideo.setImageResource(R.drawable.circleiconvideosmall2xticke);
        ibPhone.setImageResource(R.drawable.circleiconphone2x);
    }


    public void updateAvailableAppointmentsButtons()
    {
        _("updateAvailableAppointmentsButtons");

        if (itemListAvailableAppointments==null)
        {
            _("warning itemListAvailableAppointments is null, bailing.");
            return;
        }

        //becomes true when we tick the first one available
        boolean firstAppointmentTicked=false;

        //now we grey out ones that arent available, and tick the first one that is//
        for (int i=0; i<itemListAvailableAppointments.size(); i++)
        {
            AvailableAppointment aa = itemListAvailableAppointments.get(i);
            _("Looking at appointment "+i);
             /*
            * buttons are laid out in a grid of nine like this:
            * A B C
            * D E F
            * G H I
            * */

            //set the right time on the button
            switch(i)
            {
                case 0: btA.setText(aa.getFriendlyDateString()); break;
                case 1: btB.setText(aa.getFriendlyDateString()); break;
                case 2: btC.setText(aa.getFriendlyDateString()); break;

                case 3: btD.setText(aa.getFriendlyDateString()); break;
                case 4: btE.setText(aa.getFriendlyDateString()); break;
                case 5: btF.setText(aa.getFriendlyDateString()); break;

                case 6: btG.setText(aa.getFriendlyDateString()); break;
                case 7: btH.setText(aa.getFriendlyDateString()); break;
                case 8: btI.setText(aa.getFriendlyDateString()); break;

                default:
                    _("unknown case skipping it.. its out of bounds of the 9 buttons anyway");
            }
            //

            //Button A
            if (aa.available.equals("true"))
            {
                if (firstAppointmentTicked)
                {

                }
                else
                {
                    _("ticked first appointment! @ "+i);
                    firstAppointmentTicked=true;
                    mySelectedAppointment = aa;

                    switch(i)
                    {
                        case 0: toggleButtonState(ibtA,-1); break;
                        case 1: toggleButtonState(ibtB,-1); break;
                        case 2: toggleButtonState(ibtC,-1); break;

                        case 3: toggleButtonState(ibtD,-1); break;
                        case 4: toggleButtonState(ibtE,-1); break;
                        case 5: toggleButtonState(ibtF,-1); break;

                        case 6: toggleButtonState(ibtG,-1); break;
                        case 7: toggleButtonState(ibtH,-1); break;
                        case 8: toggleButtonState(ibtI,-1); break;

                        default:
                            _("unknown case skipping it.. its out of bounds of the 9 buttons anyway");
                    }
                }
            }
            else
            {
                switch(i)
                {
                    case 0: btA.setBackgroundResource(R.drawable.buttonborder_greybackground); btA.setTextColor(0xff7FA3A4); break;
                    case 1: btB.setBackgroundResource(R.drawable.buttonborder_greybackground); btB.setTextColor(0xff7FA3A4); break;
                    case 2: btC.setBackgroundResource(R.drawable.buttonborder_greybackground); btC.setTextColor(0xff7FA3A4); break;

                    case 3: btD.setBackgroundResource(R.drawable.buttonborder_greybackground); btD.setTextColor(0xff7FA3A4); break;
                    case 4: btE.setBackgroundResource(R.drawable.buttonborder_greybackground); btE.setTextColor(0xff7FA3A4); break;
                    case 5: btF.setBackgroundResource(R.drawable.buttonborder_greybackground); btF.setTextColor(0xff7FA3A4); break;

                    case 6: btG.setBackgroundResource(R.drawable.buttonborder_greybackground); btG.setTextColor(0xff7FA3A4); break;
                    case 7: btH.setBackgroundResource(R.drawable.buttonborder_greybackground); btH.setTextColor(0xff7FA3A4); break;
                    case 8: btI.setBackgroundResource(R.drawable.buttonborder_greybackground); btI.setTextColor(0xff7FA3A4); break;

                    default:
                        _("unknown case skipping it.. its out of bounds of the 9 buttons anyway");
                }
            }

        }
    }


    private void untickAllButtons()
    {
        //set all buttons to have no ticks first so any previous presses are forgotten//
        ibtA.setTag("tickon");toggleButtonState(ibtA,-1);
        ibtB.setTag("tickon");toggleButtonState(ibtB,-1);
        ibtC.setTag("tickon");toggleButtonState(ibtC,-1);
        ibtD.setTag("tickon");toggleButtonState(ibtD,-1);
        ibtE.setTag("tickon");toggleButtonState(ibtE,-1);
        ibtF.setTag("tickon");toggleButtonState(ibtF,-1);
        ibtG.setTag("tickon");toggleButtonState(ibtG,-1);
        ibtH.setTag("tickon");toggleButtonState(ibtH,-1);
        ibtI.setTag("tickon");toggleButtonState(ibtI,-1);

        //
    }

    private void toggleButtonState(ImageButton ib, int indexOfThisAppointment)
    {


        if (ib.getTag().equals("tickoff"))
        {
            ib.setImageResource(R.drawable.buttontickoverlay);
            ib.setTag("tickon");

            if (indexOfThisAppointment==-1)
            {
                //we dont assign it if -1 is passed in because its just toggling the state and not the user selecting it.
            }
            else
            {
                AvailableAppointment aa = itemListAvailableAppointments.get(indexOfThisAppointment);
                mySelectedAppointment   = aa;
            }

        }
        else
        {
            ib.setImageResource(R.drawable.buttontickoverlay_transpare);
            ib.setTag("tickoff");
        }
    }

    private void doNetworkingToGetAvailableAppointments()
    {
        _("doNetworkingToGetAvailableAppointments=========================================");
        if (!Tools.isNetworkAvailable(this) )
        {
            Tools.toast("This app needs an active Internet connection to work.",c);
            return;
        }

        //Do networking!
        String messageForProgressBar = "Getting available appointments";
        String url = Networking.URL_GET_APPOINTMENTS_AVAILABLE;
        int nState = Networking.NETWORK_STATE_GET_APPOINTMENTS_AVAILABLE;

        Networking n = new Networking(messageForProgressBar, c, true);
        n.setAvailableAppointmentInfo(dateofAppointmentRequired,typeOfSpecialistRequired);// we call this specific method for this state so right vars are set up.
        n.execute(url, nState);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        _("///////////////onResume/////////////");
    }

    //a wrapper so we dont have to type this every time
    private void _(String s) {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", "BookAppointmentActivity_Page2" + "#######" + s);
    }
}
