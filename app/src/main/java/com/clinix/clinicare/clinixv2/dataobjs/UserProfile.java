package com.clinix.clinicare.clinixv2.dataobjs;

/**
 * Created by USER on 26/06/2015.
 */
public class UserProfile
{
///THIS IS DEFUNCT IN FAVOUR OF parseUserObject I THINK, ONLY DIFFERENCE IS PATHTOPICK WHICH WE ADDED TO parseUserObject ERGO DELETE ME
    public String pathToPic;
    public String firstName;
    public String lastName;
    public String nhsNumber;
    public String email;
    public String mobile;
    public String Address_1;
    public String Address_2;
    public String Address_3;
    public String Address_4;
    public String gender;
    public String dob = "";
    public String height;
    public String weight;
    public String smoker;
    public String password;

    public UserProfile
            (
                    String pathToPic_,
                    String firstName_,
                    String lastName_,
                    String nhsNumber_,
                    String email_,
                    String mobile_,
                    String Address_1_,
                    String Address_2_,
                    String Address_3_,
                    String Address_4_,
                    String gender_,
                    String dob_,
                    String height_,
                    String weight_,
                    String smoker_,
                    String password_
            )
    {
        pathToPic=pathToPic_;
        firstName=firstName_;
        lastName=lastName_;
        nhsNumber=nhsNumber_;
        email=email_;
        mobile=mobile_;
        Address_1=Address_1_;
        Address_2=Address_2_;
        Address_3=Address_3_;
        Address_4=Address_4_;
        gender=gender_;
        dob=dob_;
        height=height_;
        weight=weight_;
        smoker=smoker_;
        password=password_;
    }


}
