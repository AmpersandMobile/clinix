package com.clinix.clinicare.clinixv2.dataobjs;

/**
 * Created by USER on 07/07/2015.
 */
public class Pharmacy {
    public String id ;
    public String user_id ;
    public String name ;
    public String street ;
    public String address_2 ;
    public String town ;
    public String county ;
    public String postcode ;
    public String country ;
    public String email ;
    public String website ;
    public String telephone ;
    public String fax ;

    public Pharmacy
            (
                    String id_,
                    String user_id_,
                    String name_,
                    String street_,
                    String address_2_,
                    String town_,
                    String county_,
                    String postcode_,
                    String country_,
                    String email_,
                    String website_,
                    String telephone_,
                    String fax_
            )
    {
        id=id_;
        user_id=user_id_;
        name=name_;
        street=street_;
        address_2=address_2_;
        town=town_;
        county=county_;
        postcode=postcode_;
        country=country_;
        email=email_;
        website=website_;
        telephone=telephone_;
        fax=fax_;
    }

    public boolean isDefault;//this is changed when the user taps it to make it default.

}
