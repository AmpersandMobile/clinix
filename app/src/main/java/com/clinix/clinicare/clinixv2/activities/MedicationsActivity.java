/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;


import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.adapters.MedicationsAdapter;
import com.clinix.clinicare.clinixv2.common.Networking;
import com.clinix.clinicare.clinixv2.common.Tools;
import com.clinix.clinicare.clinixv2.dataobjs.Medication;

import java.util.ArrayList;

public class MedicationsActivity extends FragmentActivity {

    Context c;
    public static ArrayList<Medication> itemListMedication;
    public ListView listView;
    MedicationsAdapter adapter;
    Activity a;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.medications);
        c = this;
        a = this;

        doNetworkingToGetMedications();
        setupFakeActionBar();
    }

    private void doNetworkingToGetMedications()
    {
        _("doNetworkingToGetMedications=========================================");
        if (!Tools.isNetworkAvailable(this) )
        {
            Tools.toast("This app needs an active Internet connection to work.",c);
            return;
        }

        //Do networking!
        String messageForProgressBar = "Getting medications";
        String url = Networking.URL_GET_MY_PATIENT_MEDICATIONS;
        int nState = Networking.NETWORK_STATE_GET_MY_PATIENT_MEDICATIONS;
        Networking n = new Networking(messageForProgressBar, c, true);

        Tools t = new Tools(c);
        t.setupPrefs(c);
        ///String type = "patient"; // this can be admin / practitioner / patient
       // String id = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, "id",null,c);
       // n.setHistoryInfo(id);// we call this specific method for this state so right vars are set up.
        n.execute(url, nState);
    }

    public void fillUpList()
    {
        listView = (ListView)findViewById(R.id.mylist);
        _("fillUpSongList----");
      /*  itemListMedication = new ArrayList<Medication>();
        Medication a = new Medication("28/10/2015, 5:49","Amlodipine 5mg Tablets");

        itemListMedication.add(a);
        a = new Medication("28/10/2015, 5:49","Amlodipine 5mg Tablets");
        itemListMedication.add(a);
        a = new Medication("28/10/2015, 5:49","Amlodipine 5mg Tablets");
        itemListMedication.add(a);
        a = new Medication("28/10/2015, 5:49","Amlodipine 5mg Tablets");
        itemListMedication.add(a);
*/
        //buttons at top for filtering
     /*   Button btScheduled = (Button)findViewById(R.id.btScheduled);
        btScheduled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("//////onClick-btScheduled/////");
            }
        });

        Button btPrevious = (Button)findViewById(R.id.btPrevious);
        btPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("//////onClick-btPrevious/////");
            }
        });
*/
        //sort them by distance
        //Collections.sort(itemListScheduled, new Comparator<Appointment>() {
        //    public int compare(Appointment a, Appointment b) {
        //        return a.date.compareTo(b.date);//    return a.getRealpath().compareTo(b.getRealpath()); // to order by filename!
        //    }
        //});

        //make adapter and set it, this updates the view
        adapter = new MedicationsAdapter(this, itemListMedication);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                _("////////////////////////clicked on list/////////////////////");
                //since only one can be the default we need to make sure all others are flagged as false now
                ///// adapter.flagThisItemAsDefault(position,a);
                Medication a = itemListMedication.get(position);
                _("" + a.drug_name+" drugId:"+a.id);
                Intent intent = new Intent(c, MedicationsDetailsActivity.class);
                intent.putExtra("id", a.id);//pass over the id so we can get it from the list
                c.startActivity(intent);
                //   listView.invalidateViews();//refresh it
            }
        });

        //set the button colours
        //shcedule starts selected, so we want it to be white with green font
      //  btScheduled.setBackgroundResource(R.drawable.buttonborder_whitebackground);
      //  btScheduled.setTextColor(Tools.CLINIX_GREEN);
        //and the previous button, green with white text
      //  btPrevious.setBackgroundResource(R.drawable.buttonborder_greenbackground);
      //  btPrevious.setTextColor(Tools.CLINIX_WHITE);

      ////  doNetworkingToGetAppointments();
    }


    private void doNetworkingToGetAppointments()//String firstName, String lastName, String emailAddress, String password, String dob, String nhsNumber, String surgery_id)
    {
        _("doNetworkingToGetAppointments=========================================");
        if (!Tools.isNetworkAvailable(this) )
        {
            Tools.toast("This app needs an active Internet connection to work.",c);
            return;
        }

        //Do networking!
        String messageForProgressBar = "Getting medications appointments";
        String url = Networking.URL_GET_MY_PATIENT_MEDICATIONS;//+"?all=1";
        int nState = Networking.NETWORK_STATE_GET_MY_PATIENT_MEDICATIONS;

        Networking n = new Networking(messageForProgressBar, c, true);
       // n.setRegisterInfo(firstName,lastName, emailAddress,password,dob,nhsNumber,surgery_id);// we call this specific method for this state so right vars are set up.
        n.execute(url, nState);
    }

    //we use our own actionbar to avoid the crap real one
    private void setupFakeActionBar()
    {
        //fake actionbar menu button
        //we use this for:
        //-change password
        //-save
        //
        //This is basically a fake menu that pops up
        final LinearLayout fakeActionBarButtonHolder = (LinearLayout) findViewById(R.id.fakeActionBarButtonHolder);
        //hide it right away
        fakeActionBarButtonHolder.setVisibility(View.GONE);

        ImageButton ibMenu = (ImageButton)    findViewById(R.id.ibMenu);
        ibMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ibMenu hit.");
                Tools.toggleActionBarMenu(fakeActionBarButtonHolder);
            }
        });
        //and its buttons:
        Button btA = (Button)    findViewById(R.id.btA);
        btA.setText(" Add Medication ");
        btA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("Add Appointment btA hit.");
               // Tools.toast("Add Appointment. [NOT IMPLEMENTED]", c);
                //Tools.toggleActionBarMenu(fakeActionBarButtonHolder);
                Intent intent = new Intent(c, MedicationsDetailsActivity.class);
                c.startActivity(intent);
            }
        });
        Button btB = (Button)    findViewById(R.id.btB);
        btB.setVisibility(View.GONE);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        _("///////////////onResume/////////////");
    }

    //a wrapper so we dont have to type this every time
    private void _(String s) {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", "AppointmentsActivity" + "#######" + s);
    }
}
