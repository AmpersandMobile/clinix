package com.clinix.clinicare.clinixv2.dataobjs;

import com.clinix.clinicare.clinixv2.common.Tools;

/**
 * Created by USER on 24/07/2015.
 */
public class SaveKeys
{
    //this is just a convenient place to store the constants we use to access the preferences

    //REGISTRATION & LOGIN INFORMATION////////
    public static final String STORAGE_KEY_FIRST_NAME       = "firstName"   ;
    public static final String STORAGE_KEY_LAST_NAME        = "lastName"    ;
    public static final String STORAGE_KEY_EMAIL_ADDRESS    = "emailAddress";
    public static final String STORAGE_KEY_PASSWORD         = "password"    ;
    public static final String STORAGE_KEY_DOB              = "dob"         ;
    public static final String STORAGE_KEY_NHS_NUMBER       = "nhsNumber"   ;
    public static final String STORAGE_KEY_SURGERY_ID       = "surgeryId"   ;

    //PROFILE INFORMATION///////////
    //For loading and saving.
    public static final String SAVE_KEY_PICTURE         =   "SAVE_KEY_PICTURE";
    public static final String SAVE_KEY_FIRSTNAME       =   "SAVE_KEY_FIRSTNAME";
    public static final String SAVE_KEY_LASTNAME        =   "SAVE_KEY_LASTNAME";
    public static final String SAVE_KEY_NHSNUMBER       =   "SAVE_KEY_NHSNUMBER";
    public static final String SAVE_KEY_EMAIL           =   "SAVE_KEY_EMAIL";
    public static final String SAVE_KEY_MOBILE          =   "SAVE_KEY_MOBILE";
    public static final String SAVE_KEY_ADDRESS1        =   "SAVE_KEY_ADDRESS1";
    public static final String SAVE_KEY_ADDRESS2        =   "SAVE_KEY_ADDRESS2";
    public static final String SAVE_KEY_ADDRESS3        =   "SAVE_KEY_ADDRESS3";
    public static final String SAVE_KEY_ADDRESS4        =   "SAVE_KEY_ADDRESS4";
    public static final String SAVE_KEY_GENDER          =   "SAVE_KEY_GENDER";
    public static final String SAVE_KEY_DOB             =   "SAVE_KEY_DOB";
    public static final String SAVE_KEY_HEIGHT          =   "SAVE_KEY_HEIGHT";
    public static final String SAVE_KEY_WEIGHT          =   "SAVE_KEY_WEIGHT";
    public static final String SAVE_KEY_SMOKER          =   "SAVE_KEY_SMOKER";
    public static final String SAVE_KEY_PASSWORD        =   "SAVE_KEY_PASSWORD";
}
