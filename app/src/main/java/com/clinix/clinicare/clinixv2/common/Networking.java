/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.common;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.text.format.DateUtils;
import android.util.Log;

import com.clinix.clinicare.clinixv2.activities.AllergiesActivity;
import com.clinix.clinicare.clinixv2.activities.AppointmentsActivity;
import com.clinix.clinicare.clinixv2.activities.BookAppointmentActivity_Page2;
import com.clinix.clinicare.clinixv2.activities.BookAppointmentActivity_Page3;
import com.clinix.clinicare.clinixv2.activities.ListOfGPsAtLocalSurgeryActivity;
import com.clinix.clinicare.clinixv2.activities.NearbyNHSSurgeriesActivity;
import com.clinix.clinicare.clinixv2.activities.SurgeryDetailsActivity;
import com.clinix.clinicare.clinixv2.activities.HistoryActivity;
import com.clinix.clinicare.clinixv2.activities.LoginActivity;
import com.clinix.clinicare.clinixv2.activities.MainMenuActivity;
import com.clinix.clinicare.clinixv2.activities.MedicationsActivity;
import com.clinix.clinicare.clinixv2.activities.MedicationsDetailsActivity;
import com.clinix.clinicare.clinixv2.activities.NotificationActivity;
import com.clinix.clinicare.clinixv2.activities.PharmacyDetailsActivity;
import com.clinix.clinicare.clinixv2.activities.ProfileActivity;
import com.clinix.clinicare.clinixv2.dataobjs.Admin_fromNotification;
import com.clinix.clinicare.clinixv2.dataobjs.Allergy;
import com.clinix.clinicare.clinixv2.dataobjs.Appointment;
import com.clinix.clinicare.clinixv2.dataobjs.AvailableAppointment;
import com.clinix.clinicare.clinixv2.dataobjs.GPDetails;
import com.clinix.clinicare.clinixv2.dataobjs.History;
import com.clinix.clinicare.clinixv2.dataobjs.Medication;
import com.clinix.clinicare.clinixv2.dataobjs.Notification_;
import com.clinix.clinicare.clinixv2.dataobjs.Pharmacy;
import com.clinix.clinicare.clinixv2.dataobjs.Practitioner_fromNotification;
import com.clinix.clinicare.clinixv2.dataobjs.Surgery;
import com.clinix.clinicare.clinixv2.dataobjs.User_fromNotification;
import com.opentok.android.demo.config.OpenTokConfig;
import com.opentok.android.demo.opentoksamples.OpenTokSamples;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.SaveCallback;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//All of the networking stuff is handled in this class, we use a network state to know what to do
public class Networking extends AsyncTask
{
    Tools t;
    public static final String  X_CLINIX_API_KEY_ANDROID     = "245ca07f-4de6-4ad2-99dc-6e6bad7a78d5";
    public static String        X_CLINIX_AUTH_TOKEN_ANDROID  = "{getMeFromLoginResponse}";//this comes in from logging in and the token remains valid for 20 mins.

    //This class does all the networking depending on the network state
    //URLS NEEDED, one for each state, things in curly braces are substituted for real vals.
    public static String URL_DEVELOPMENT                            = "http://clinicareapi.royand.co.uk";
    public static String URL_PRODUCTION                             = "https://android.clinix.co.uk";

    public static final String URL_BASE                             = URL_PRODUCTION;//use production for release
                                                                                                         // Http request type:
    // LOGIN STUFF
    public static final String URL_REGISTER                         = URL_BASE+"/register"                      ; //POST
    public static final String URL_LOGIN                            = URL_BASE+"/login"                         ; //POST
    public static final String URL_PASSWORD_RESET_FORGOT            = URL_BASE+"/password/forgot"               ; //POST
    public static final String URL_PASSWORD_RESET_RESET             = URL_BASE+"/password/reset/{token}"        ; //GET
    public static final String URL_PASSWORD_RESET_CHANGE            = URL_BASE+"/password/change"               ; //POST
    // MISC
    public static final String URL_GET_SURGERIES                    = URL_BASE+"/surgeries"                     ; //GET
    public static final String URL_GET_ENQUIRIES                    = URL_BASE+"/enquiries"                     ; //GET
    public static final String URL_GET_NOTIFICATIONS                = URL_BASE+"/notifications"                 ; //GET
    public static final String URL_GET_S3_ACCESS                    = URL_BASE+"/s3-access"                     ; //GET
    // APPOINTMENT RELATED
    public static final String URL_GET_APPOINTMENTS_SCHEDULED       = URL_BASE+"/appointments/scheduled"        ; //GET
    public static final String URL_GET_APPOINTMENTS_PREVIOUS        = URL_BASE+"/appointments/previous"         ; //GET
    public static final String URL_GET_APPOINTMENTS_AVAILABLE       = URL_BASE+"/appointments/available"        ; //GET
    public static final String URL_BOOK_AN_APPOINTMENT              = URL_BASE+"/appointments/book"             ; //POST
    public static final String URL_GET_MY_NEXT_APPOINTMENT          = URL_BASE+"/appointments/next-appointment" ; //GET
    public static final String URL_CANCEL_AN_APPOINTMENT            = URL_BASE+"/appointments/cancel"           ; //POST
    public static final String URL_START_CALL_TO_BEGIN_APPOINTMENT  = URL_BASE+"/appointments/call"           ; //POST
    public static final String URL_END_CALL_TO_END_APPOINTMENT      = URL_BASE+"/appointments/end"           ; //POST
    // PATIENT RELATED
    public static final String URL_GET_MY_PATIENT_DETAILS           = URL_BASE+"/patient"                       ; //POST
    public static final String URL_GET_MY_PATIENT_PROFILE           = URL_BASE+"/patient/profile"               ; //POST
    public static final String URL_SET_MY_PATIENT_PROFILE           = URL_BASE+"/patient/profile"               ; //POST
    public static final String URL_GET_MY_PATIENT_PRESCRIPTIONS     = URL_BASE+"/patient/prescriptions"         ; //GET
    public static final String URL_GET_MY_PATIENT_SURGERY           = URL_BASE+"/patient/surgery"               ; //GET
    public static final String URL_GET_MY_PATIENT_MEDICATIONS       = URL_BASE+"/patient/medications"           ; //GET
    public static final String URL_CREATE_A_MEDICATION_FOR_MY_PATIENT = URL_BASE+"/patient/medication"             ; //POST


    public static final String URL_GET_MY_PATIENT_PHARMACY          = URL_BASE+"/patient/pharmacies"            ; //GET
    public static final String URL_SET_MY_PATIENT_PHARMACY_PROFILE  = URL_BASE+"/patient/pharmacy/{id}"            ; //POST
    public static final String URL_GET_MY_PATIENT_LETTERS           = URL_BASE+"/patient/letters"               ; //GET
    public static final String URL_GET_MY_PATIENT_ALLERGIES         = URL_BASE+"/patient/allergies"             ; //GET
    public static final String URL_CREATE_AN_ALLERGY_FOR_MY_PATIENT = URL_BASE+"/patient/allergy"             ; //POST
    public static final String URL_GET_MY_PATIENT_GP                = URL_BASE+"/patient/gp"                    ; //GET
    public static final String URL_UPDATE_MY_PATIENT_GP                = URL_BASE+"/patient/custom-gp"                    ; //GET
    public static final String URL_GET_MY_PATIENT_HISTORY           = URL_BASE+"/patient/history"               ; //GET
    public static final String URL_CREATE_A_HISTORY_FOR_MY_PATIENT = URL_BASE+"/patient/history"             ; //POST
    public static final String URL_GET_MY_PATIENT_CLINICAL_ALERTS   = URL_BASE+"/patient/clinical-alerts"       ; //GET
    // PRACTITIONER RELATED
    public static final String URL_PRACTIONER_LOGIN                 = URL_BASE+"/login"                         ; //POST
    public static final String URL_PRACTIONER_GET_PROFILE           = URL_BASE+"/practitioner/profile"          ; //GET
    public static final String URL_PRACTIONER_GET_REVIEWS           = URL_BASE+"/practitioner/reviews"          ; //GET
    public static final String URL_PRACTIONER_GET_QUALIFICATIONS    = URL_BASE+"/practitioner/qualifications"   ; //GET
    public static final String URL_PRACTIONER_GET_REGISTRATIONS     = URL_BASE+"/practitioner/registrations"    ; //GET
    public static final String URL_PRACTIONER_GET_SURGERIES         = URL_BASE+"/practitioner/surgeries"        ; //GET
    public static final String URL_PRACTIONER_GET_DOCUMENTS         = URL_BASE+"/practitioner/documents"        ; //GET
    public static final String URL_PRACTIONER_GET_ROTA              = URL_BASE+"/practitioner/rota"             ; //GET
    // ADMIN RELATED
    public static final String URL_ADMIN_LOGIN                      = URL_BASE+"/login"                         ; //POST
    public static final String URL_ADMIN_REGISTER                   = URL_BASE+"/admin/create"                  ; //POST
    public static final String URL_ADMIN_GET_PROFILE                = URL_BASE+"/admin/profile"                 ; //GET
    public static final String URL_ADMIN_GET_REPORTS                = URL_BASE+"/reports"                       ; //POST
    // SEARCH RELATED
    public static final String URL_SEARCH_PATIENTS                  = URL_BASE+"/search/patients"               ; //POST
    public static final String URL_SEARCH_DOCTORS                   = URL_BASE+"/search/doctors"                ; //POST
    public static final String URL_SEARCH_TICKETS                   = URL_BASE+"/search/ticket"                 ; //POST
    public static final String URL_SEARCH_SURGERIES                 = URL_BASE+"/search/surgeries"              ; //POST
    public static final String URL_SEARCH_CONSULTATIONS             = URL_BASE+"/search/consultation"           ; //POST

    // MISC
    public static final String URL_GET_LOCAL_SURGERIES                = URL_BASE+"/surgeries"                 ; //GET
    public static final String URL_GET_GPLIST_FOR_SURGERY                = URL_BASE+"/patient/gp"                    ; //GET
    public static final String URL_SET_MY_PATIENT_SURGERY_AND_GP                = URL_BASE+"/patient/profile/gp-surgery"                    ; //POST

    //NETWORK STATES
    // LOGIN STUFF
    public static final int NETWORK_STATE_REGISTER                          =   1001;
    public static final int NETWORK_STATE_LOGIN                             =   1002;
    public static final int NETWORK_STATE_PASSWORD_RESET_FORGOT             =   1003;
    public static final int NETWORK_STATE_PASSWORD_RESET_RESET              =   1004;
    public static final int NETWORK_STATE_PASSWORD_RESET_CHANGE             =   1005;
    // MISC
    public static final int NETWORK_STATE_GET_SURGERIES                     =   1006;
    public static final int NETWORK_STATE_GET_ENQUIRIES                     =   1007;
    public static final int NETWORK_STATE_GET_NOTIFICATIONS                 =   1008;
    public static final int NETWORK_STATE_GET_S3_ACCESS                     =   1009;
    // APPOINTMENT RELATED
    public static final int NETWORK_STATE_GET_APPOINTMENTS_SCHEDULED        =   1010;
    public static final int NETWORK_STATE_GET_APPOINTMENTS_PREVIOUS         =   1011;
    public static final int NETWORK_STATE_GET_APPOINTMENTS_AVAILABLE        =   1012;
    public static final int NETWORK_STATE_BOOK_AN_APPOINTMENT               =   1013;
    public static final int NETWORK_STATE_GET_MY_NEXT_APPOINTMENT           =   1014;
    public static final int NETWORK_STATE_CANCEL_AN_APPOINTMENT             =   1015;
    public static final int NETWORK_STATE_START_CALL_TO_BEGIN_APPOINTMENT   =   1016;
    public static final int NETWORK_STATE_END_CALL_TO_END_APPOINTMENT       =   1017;
    // PATIENT RELATED
    public static final int NETWORK_STATE_GET_MY_PATIENT_DETAILS            =   1018;
    public static final int NETWORK_STATE_GET_MY_PATIENT_PROFILE            =   1019;
    public static final int NETWORK_STATE_SET_MY_PATIENT_PROFILE            =   10190;
    public static final int NETWORK_STATE_GET_MY_PATIENT_PRESCRIPTIONS      =   1020;
    public static final int NETWORK_STATE_GET_MY_PATIENT_SURGERY            =   1021;
    public static final int NETWORK_STATE_GET_MY_PATIENT_MEDICATIONS        =   1022;
    public static final int NETWORK_STATE_CREATE_A_MEDICATION_FOR_MY_PATIENT  = 102500;

    public static final int NETWORK_STATE_GET_MY_PATIENT_PHARMACY           =   1023;
    public static final int NETWORK_STATE_MY_PATIENT_PHARMACY_PROFILE       =   10230;
    public static final int NETWORK_STATE_GET_MY_PATIENT_LETTERS            =   1024;
    public static final int NETWORK_STATE_GET_MY_PATIENT_ALLERGIES          =   1025;
    public static final int NETWORK_STATE_CREATE_AN_ALLERGY_FOR_MY_PATIENT  =   10250;

    public static final int NETWORK_STATE_GET_MY_PATIENT_GP                 =   1026;
    public static final int NETWORK_STATE_UPDATE_MY_PATIENT_GP                        =   10260;

    public static final int NETWORK_STATE_GET_MY_PATIENT_HISTORY            =   1027;
    public static final int NETWORK_STATE_CREATE_A_HISTORY_FOR_MY_PATIENT   =   102510;
    public static final int NETWORK_STATE_GET_MY_PATIENT_CLINICAL_ALERTS    =   1028;
    // PRACTITIONER RELATED
    public static final int NETWORK_STATE_PRACTIONER_LOGIN                  =   1029;
    public static final int NETWORK_STATE_PRACTIONER_GET_PROFILE            =   1030;
    public static final int NETWORK_STATE_PRACTIONER_GET_REVIEWS            =   1031;
    public static final int NETWORK_STATE_PRACTIONER_GET_QUALIFICATIONS     =   1032;
    public static final int NETWORK_STATE_PRACTIONER_GET_REGISTRATIONS      =   1033;
    public static final int NETWORK_STATE_PRACTIONER_GET_SURGERIES          =   1034;
    public static final int NETWORK_STATE_PRACTIONER_GET_DOCUMENTS          =   1035;
    public static final int NETWORK_STATE_PRACTIONER_GET_ROTA               =   1036;
    // ADMIN RELATED
    public static final int NETWORK_STATE_ADMIN_LOGIN                       =   1037;
    public static final int NETWORK_STATE_ADMIN_REGISTER                    =   1038;
    public static final int NETWORK_STATE_ADMIN_GET_PROFILE                 =   1039;
    public static final int NETWORK_STATE_ADMIN_GET_REPORTS                 =   1040;
    // SEARCH RELATED
    public static final int NETWORK_STATE_SEARCH_PATIENTS                   =   1041;
    public static final int NETWORK_STATE_SEARCH_DOCTORS                    =   1042;
    public static final int NETWORK_STATE_SEARCH_TICKETS                    =   1043;
    public static final int NETWORK_STATE_SEARCH_SURGERIES                  =   1044;
    public static final int NETWORK_STATE_SEARCH_CONSULTATIONS              =   1045;

    //MISC
    public static final int NETWORK_STATE_GET_LOCAL_SURGERIES              =   1046;
    public static final int NETWORK_STATE_GET_GPLIST_FOR_SURGERY              =   1047;
    public static final int NETWORK_STATE_SET_MY_PATIENT_SURGERY_AND_GP              =   1048;

    // Error codes /////////////////////////////
    public static final String ERROR_CODE_BAD_REQUEST               = "400"; // Bad Request – Something about your request is wrong (i.e. missing parameter)
    public static final String ERROR_CODE_UNAUTHORIZED              = "401"; // Unauthorized – Your username / password for login is incorrect
    public static final String ERROR_CODE_FORBIDDEN                 = "403"; // Forbidden – Your API key is incorrect or your Auth Token is incorrect or invalid
    public static final String ERROR_CODE_NOT_FOUND                 = "404"; // Not Found – Either the endpoint or the object for the endpoint does not exist (i.e. /abc doesn’t exist and /appointments/99999 may not exist)
    public static final String ERROR_CODE_INTERNAL_SERVER_ERROR     = "500"; // Internal Server Error – Server problem. Try again later.
    public static final String ERROR_CODE_SERVICE_UNAVAILABLE       = "503"; // Service Unavailable – Server is in maintenance mode. Try again later.

    boolean REQUIRES_AUTHORISATION_KEY_IN_HEADER;
    boolean REQUIRES_AUTHORISATION__TOKEN__IN_HEADER;//Made this stand out since i frequently get it confused with the var above
    String CONTENT_TYPE_FORM_ENCODED    =   "application/x-www-form-urlencoded";
    String CONTENT_TYPE_JSON            =   "application/json";

    boolean needsAndroidProgressBar;
    String progressMessage;
    Context c;

    public static boolean USER_JUST_REGISTERED=false; //so once they hit main menu after registering we know to show them first time stuff




    public Networking(String progressMessage_, Context c_, boolean needsAndroidProgressBar_)
    {
        _("Networking made.");
        progressMessage=progressMessage_;
        c=c_;
        needsAndroidProgressBar=needsAndroidProgressBar_;
    }

    @Override
    protected Object doInBackground(Object[] params)
    {
        //_("doInBackground. ");
        //so tools is ready for loading and saving stuff
        if (t==null)
        {
            t = new Tools(c);
            t.setupPrefs(c);
        }
        getJson((String) params[0], // URL
                (Integer) params[1] // NETWORK STATE
        );
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        //_("onPreExecute------------------------------------------------------------------------------------------------------");
        if (needsAndroidProgressBar)
        {
            //_("==========make progress bar========");
            showProgressBar(c, progressMessage);
        }
        else
        {
            //_("NO PROGRESS BAR WILL BE USED...........................");
        }
    }

    //DOES THE ACTUAL REQUEST//
    private void getJson(String url, int state)
    {
        HttpClient httpClient = new DefaultHttpClient();
        _("------------------------------------------> doing request to url now "+url);
        HttpPost request = new HttpPost(url);
        REQUIRES_AUTHORISATION_KEY_IN_HEADER =true; // they all require the API key
        boolean isPost = true; // post or get.. we flag this in the cases below
        //NOTE: POSTS use params added to the request, GETs use params tacked onto the url (as you know :))
        //this means that we need to add the X_CLINIX_API_KEY_ANDROID explicitly on GETs, so we pass that
        //right through to the networking call for GET in Tools. Also note, all GETs require X_CLINIX_AUTH_TOKEN_ANDROID
        //which we also do in the same method.

        ////////

       /*if (    state!=NETWORK_STATE_REGISTER                      &&
               state!=NETWORK_STATE_LOGIN
                )
        {
            REQUIRES_AUTHORISATION_KEY_IN_HEADER=true;
        }
        else
        {
            REQUIRES_AUTHORISATION_KEY_IN_HEADER=false;
        }*/

        if (REQUIRES_AUTHORISATION_KEY_IN_HEADER)
        {
            _("REQUIRES_AUTHORISATION_KEY_IN_HEADER");
            request.addHeader("X-Clinix-API-Key", X_CLINIX_API_KEY_ANDROID);    _("X-Clinix-API-Key:" + Networking.X_CLINIX_API_KEY_ANDROID);


            // request.setHeader("Accept"      , CONTENT_TYPE_JSON);
            //  request.setHeader("Content-type", CONTENT_TYPE_FORM_ENCODED);
            //request.setHeader("Accept-Encoding", "gzip"); // only set this parameter if you would like to use gzip compression
        }
        else
        {
            _("NO AUTHORISATION HEADER REQUIRED.");
        }


        List<NameValuePair> postParameters = new ArrayList<NameValuePair>();
        boolean valid=false;

        switch(state) {
            case NETWORK_STATE_REGISTER:
                _("NETWORK_STATE_REGISTER");
                postParameters.add(new BasicNameValuePair("forename"    , forename));       _("forename:    " + forename);
                postParameters.add(new BasicNameValuePair("surname"     , surname));        _("surname:     " + surname);
                postParameters.add(new BasicNameValuePair("email"       , email));          _("email:       " + email);
                postParameters.add(new BasicNameValuePair("password"    , password));       _("password:    " + password);
                postParameters.add(new BasicNameValuePair("dob"         , dob));            _("dob:         " + dob);
                postParameters.add(new BasicNameValuePair("nhs_number " , nhs_number));     _("nhs_number:  " + nhs_number);
                postParameters.add(new BasicNameValuePair("surgery_id"  , surgery_id));     _("surgery_id:  " + surgery_id);
                valid=true;
                break;

            case NETWORK_STATE_PASSWORD_RESET_FORGOT:
                _("NETWORK_STATE_PASSWORD_RESET_FORGOT");
                postParameters.add(new BasicNameValuePair("email"       , email));          _("email:"   + email);
                valid=true;
                break;

            case NETWORK_STATE_PASSWORD_RESET_RESET:
                _("NETWORK_STATE_PASSWORD_RESET_RESET");
                isPost = false;
                valid  = true;
                break;
            case NETWORK_STATE_PASSWORD_RESET_CHANGE:
                _("NETWORK_STATE_PASSWORD_RESET_CHANGE");
                valid=true;
                REQUIRES_AUTHORISATION__TOKEN__IN_HEADER =true;
                break;
            case NETWORK_STATE_GET_SURGERIES:
                _("NETWORK_STATE_GET_SURGERIES");
                isPost = false;
                valid  = true;
                break;
            case NETWORK_STATE_GET_ENQUIRIES:
                _("NETWORK_STATE_GET_ENQUIRIES");
                isPost = false;
                valid  = true;
                break;
            case NETWORK_STATE_GET_NOTIFICATIONS:
                _("NETWORK_STATE_GET_NOTIFICATIONS");
                isPost = false;
                valid  = true;
                url += "?type="+type+"&id="+id;
                break;
            case NETWORK_STATE_GET_S3_ACCESS:
                _("NETWORK_STATE_GET_S3_ACCESS");
                isPost = false;
                valid  = true;
                break;
            case NETWORK_STATE_GET_APPOINTMENTS_SCHEDULED:
                _("NETWORK_GET_APPOINTMENTS_SCHEDULED");
                isPost = false;
                valid  = true;
                break;
            case NETWORK_STATE_GET_APPOINTMENTS_PREVIOUS:
                _("NETWORK_STATE_GET_APPOINTMENTS_PREVIOUS");
                isPost = false;
                valid  = true;
                break;
            case NETWORK_STATE_GET_APPOINTMENTS_AVAILABLE:
                _("NETWORK_STATE_GET_APPOINTMENTS_AVAILABLE");
                _("time we are sending is ( "+dateofAppointmentRequired+" ) "+new Date(Long.parseLong(dateofAppointmentRequired)).toString());
                //to get it back to UTC we divide by 1000!
                long utcTime = (Long.parseLong(dateofAppointmentRequired)/1000);///1000L;//
                _("Converted to UTC this is now: "+utcTime);
                dateofAppointmentRequired= utcTime+"";
                url += "?time="+dateofAppointmentRequired+""+"&practitioner="+typeOfSpecialistRequired;
                isPost = false;
                valid  = true;
                break;
            case NETWORK_STATE_BOOK_AN_APPOINTMENT:
                _("NETWORK_STATE_BOOK_AN_APPOINTMENT");
                postParameters.add(new BasicNameValuePair("time", mySelectedAppointmentTime));      _("time:" + mySelectedAppointmentTime);
                postParameters.add(new BasicNameValuePair("practitioner",typeOfSpecialistRequired));_("practitioner:" + typeOfSpecialistRequired);
                postParameters.add(new BasicNameValuePair("mode", preferredCallType));              _("mode:" + preferredCallType);
                postParameters.add(new BasicNameValuePair("problem", detailsOfTheProblem));         _("problem:" + detailsOfTheProblem);
                postParameters.add(new BasicNameValuePair("image", "..."));         _("image:" + "...");
                ////NEED TO ADD PHOTO HERE. BUT HOW!?
                REQUIRES_AUTHORISATION__TOKEN__IN_HEADER =true;
                valid=true;
                break;
            case NETWORK_STATE_GET_MY_NEXT_APPOINTMENT:
                _("NETWORK_STATE_GET_MY_NEXT_APPOINTMENT");
                isPost = false;
                REQUIRES_AUTHORISATION__TOKEN__IN_HEADER =true;
                valid  = true;
                break;
            case NETWORK_STATE_CANCEL_AN_APPOINTMENT:
                _("NETWORK_STATE_CANCEL_AN_APPOINTMENT");
                valid=true;
                break;
            case NETWORK_STATE_START_CALL_TO_BEGIN_APPOINTMENT:
                _("NETWORK_STATE_START_CALL_TO_BEGIN_APPOINTMENT");
                postParameters.add(new BasicNameValuePair("id", myProfile.id));      _("id:" + myProfile.id);
                postParameters.add(new BasicNameValuePair("type", "patient "));      _("type:" + "patient ");
                REQUIRES_AUTHORISATION__TOKEN__IN_HEADER =true;
                valid=true;
                break;
            case NETWORK_STATE_END_CALL_TO_END_APPOINTMENT:
                _("NETWORK_STATE_END_CALL_TO_END_APPOINTMENT");
                valid=true;
                break;
            case NETWORK_STATE_GET_MY_PATIENT_DETAILS:
                _("NETWORK_STATE_GET_MY_PATIENT_DETAILS");
                valid=true;
                break;
            case NETWORK_STATE_GET_MY_PATIENT_PROFILE:
                _("NETWORK_STATE_GET_MY_PATIENT_PROFILE");
                isPost = false;
                valid=true;
                break;
            case NETWORK_STATE_SET_MY_PATIENT_PROFILE:
                _("NETWORK_STATE_SET_MY_PATIENT_PROFILE");
                postParameters.add(new BasicNameValuePair("id", myProfile.id));                         _("id:" + myProfile.id);
                postParameters.add(new BasicNameValuePair("title", myProfile.title));                   _("title:" + myProfile.title);
                postParameters.add(new BasicNameValuePair("forename", myProfile.forename));             _("forename:" + myProfile.forename);
                postParameters.add(new BasicNameValuePair("surname", myProfile.surname));               _("surname:" + myProfile.surname);
                postParameters.add(new BasicNameValuePair("suffix", myProfile.suffix));                 _("suffix:" + myProfile.suffix);
                postParameters.add(new BasicNameValuePair("email", myProfile.email));                   _("email:" + myProfile.email);
                postParameters.add(new BasicNameValuePair("register_date", myProfile.register_date));   _("register_date:" + myProfile.register_date);
                postParameters.add(new BasicNameValuePair("dob", myProfile.dob));                       _("dob:" + myProfile.dob);
                postParameters.add(new BasicNameValuePair("nhs_number", myProfile.nhs_number));         _("nhs_number:" + myProfile.nhs_number);
                postParameters.add(new BasicNameValuePair("practioner_id", myProfile.practitioner_id)); _("practioner_id:" + myProfile.practitioner_id);
                postParameters.add(new BasicNameValuePair("surgery_id", myProfile.surgery_id));         _("surgery_id:" + myProfile.surgery_id);
                postParameters.add(new BasicNameValuePair("telephone", myProfile.telephone));           _("telephone:" + myProfile.telephone);
                postParameters.add(new BasicNameValuePair("street", myProfile.street));                 _("street:" + myProfile.street);
                postParameters.add(new BasicNameValuePair("address_2", myProfile.address_2));           _("address_2:" + myProfile.address_2);
                postParameters.add(new BasicNameValuePair("town", myProfile.town));                     _("town:" + myProfile.town);
                postParameters.add(new BasicNameValuePair("county", myProfile.county));                 _("county:" + myProfile.county);
                postParameters.add(new BasicNameValuePair("postcode", myProfile.postcode));             _("postcode:" + myProfile.postcode);
                postParameters.add(new BasicNameValuePair("country", myProfile.country));               _("country:" + myProfile.country);
                postParameters.add(new BasicNameValuePair("gender", myProfile.gender));                 _("gender:" + myProfile.gender);
                postParameters.add(new BasicNameValuePair("height_type", myProfile.height_type));       _("height_type:" + myProfile.height_type);
                postParameters.add(new BasicNameValuePair("height", myProfile.height));                 _("height:" + myProfile.height);
                postParameters.add(new BasicNameValuePair("weight_type", myProfile.weight_type));       _("weight_type:" + myProfile.weight_type);
                postParameters.add(new BasicNameValuePair("weight", myProfile.weight));                 _("weight:" + myProfile.weight);
                postParameters.add(new BasicNameValuePair("smoker", myProfile.smoker));                 _("smoker:" + myProfile.smoker);
                postParameters.add(new BasicNameValuePair("image", myProfile.image));                   _("image:" + myProfile.image);
                REQUIRES_AUTHORISATION__TOKEN__IN_HEADER =true;
                valid=true;
                break;
            case NETWORK_STATE_GET_MY_PATIENT_PRESCRIPTIONS:
                _("NETWORK_STATE_GET_MY_PATIENT_PRESCRIPTIONS");
                isPost = false;
                valid  = true;
                break;
            case NETWORK_STATE_GET_MY_PATIENT_SURGERY:
                _("NETWORK_STATE_GET_MY_PATIENT_SURGERY");
                isPost = false;
                valid  = true;
                break;
            case NETWORK_STATE_GET_MY_PATIENT_MEDICATIONS:
                _("NETWORK_STATE_GET_MY_PATIENT_MEDICATIONS");
                isPost = false;
                valid  = true;
                break;
            case NETWORK_STATE_CREATE_A_MEDICATION_FOR_MY_PATIENT:
                _("NETWORK_STATE_CREATE_A_MEDICATION_FOR_MY_PATIENT");
                postParameters.add(new BasicNameValuePair("user_id", myMedication.user_id));        _("user_id:" + myMedication.user_id);
                postParameters.add(new BasicNameValuePair("date", myMedication.date));              _("date:" + myMedication.date);
                postParameters.add(new BasicNameValuePair("drug_name", myMedication.drug_name));    _("drug_name:" + myMedication.drug_name);
                postParameters.add(new BasicNameValuePair("quantity", myMedication.quantity));      _("quantity:" + myMedication.quantity);
                postParameters.add(new BasicNameValuePair("course", myMedication.course));          _("course:" + myMedication.course);
                postParameters.add(new BasicNameValuePair("id", myMedication.id));                  _("id:" + myMedication.id);
                REQUIRES_AUTHORISATION__TOKEN__IN_HEADER =true;
                valid  = true;
                break;
            case NETWORK_STATE_GET_MY_PATIENT_PHARMACY:
                _("NETWORK_STATE_GET_MY_PATIENT_PHARMACY");
                isPost = false;
                valid  = true;
                break;
            case NETWORK_STATE_MY_PATIENT_PHARMACY_PROFILE:
                _("NETWORK_STATE_MY_PATIENT_PHARMACY_PROFILE");
                postParameters.add(new BasicNameValuePair("id", myPharmacy.id));                _("id:" + myPharmacy.id);
                postParameters.add(new BasicNameValuePair("name", myPharmacy.name));            _("name:" + myPharmacy.name);
                postParameters.add(new BasicNameValuePair("address", myPharmacy.address_2));    _("address:" + myPharmacy.address_2);
                postParameters.add(new BasicNameValuePair("postcode", myPharmacy.postcode));    _("postcode:" + myPharmacy.postcode);
                postParameters.add(new BasicNameValuePair("email", myPharmacy.email));          _("email:" + myPharmacy.email);
                postParameters.add(new BasicNameValuePair("website", myPharmacy.website));      _("website:" + myPharmacy.website);
                postParameters.add(new BasicNameValuePair("telephone", myPharmacy.telephone));  _("telephone:" + myPharmacy.telephone);
                postParameters.add(new BasicNameValuePair("fax", myPharmacy.fax));              _("fax:" + myPharmacy.fax);
                REQUIRES_AUTHORISATION__TOKEN__IN_HEADER =true;
                valid  = true;
                break;
            case NETWORK_STATE_GET_MY_PATIENT_LETTERS:
                _("NETWORK_STATE_GET_MY_PATIENT_LETTERS");
                isPost = false;
                valid  = true;
                break;
            case NETWORK_STATE_GET_MY_PATIENT_ALLERGIES:
                _("NETWORK_STATE_GET_MY_PATIENT_ALLERGIES");
                isPost = false;
                valid  = true;
                /////url += "?type="+type+"&id="+id;
                break;
            case NETWORK_STATE_CREATE_AN_ALLERGY_FOR_MY_PATIENT:
                _("NETWORK_STATE_CREATE_AN_ALLERGY_FOR_MY_PATIENT");
                postParameters.add(new BasicNameValuePair("user_id", myAllergy.user_id));   _("user_id:" + myAllergy.user_id);
                postParameters.add(new BasicNameValuePair("date", myAllergy.date));         _("date:" + myAllergy.date);
                postParameters.add(new BasicNameValuePair("allergy", myAllergy.allergy));   _("allergy:" + myAllergy.allergy);
                postParameters.add(new BasicNameValuePair("reaction", myAllergy.reaction)); _("reaction:" + myAllergy.reaction);
                postParameters.add(new BasicNameValuePair("type", myAllergy.type));         _("type:" + myAllergy.type);
                postParameters.add(new BasicNameValuePair("id", myAllergy.id));             _("id:" + myAllergy.id);
                REQUIRES_AUTHORISATION__TOKEN__IN_HEADER =true;
                valid  = true;
                break;
            case NETWORK_STATE_GET_MY_PATIENT_GP:
                _("NETWORK_STATE_GET_MY_PATIENT_GP");
                isPost = false;
                valid  = true;
                break;
            case NETWORK_STATE_UPDATE_MY_PATIENT_GP:
                _("NETWORK_STATE_UPDATE_MY_PATIENT_GP");
                //FIX THE EMPTY ONES HERE!!! BASICALLY THE API DOESNT SEEM TO MATCH TRO WHAT IT USED TO, ADJUST TO FIX TO NEW API




             //   postParameters.add(new BasicNameValuePair("title", myGPdetails.title));        _("title:" + myGPdetails.title);
              //  postParameters.add(new BasicNameValuePair("forename", myGPdetails.forename));        _("forename:" + myGPdetails.forename);
               // postParameters.add(new BasicNameValuePair("surname", myGPdetails.surname));        _("surname:" + myGPdetails.surname);
               // postParameters.add(new BasicNameValuePair("suffix", myGPdetails.suffix));        _("suffix:" + myGPdetails.suffix);
                postParameters.add(new BasicNameValuePair("email", myGPdetails.email));        _("email:" + myGPdetails.email);
               // postParameters.add(new BasicNameValuePair("telephone", myGPdetails.telephone));        _("telephone:" + myGPdetails.telephone);
               // postParameters.add(new BasicNameValuePair("street", myGPdetails.street));        _("street:" + myGPdetails.street);
               // postParameters.add(new BasicNameValuePair("address_2", myGPdetails.address_2));        _("title:" + myGPdetails.address_2);
               // postParameters.add(new BasicNameValuePair("town", myGPdetails.town));        _("town:" + myGPdetails.town);
               // postParameters.add(new BasicNameValuePair("county", myGPdetails.county));        _("county:" + myGPdetails.county);
               // postParameters.add(new BasicNameValuePair("postcode", myGPdetails.postcode));        _("postcode:" + myGPdetails.postcode);
               // postParameters.add(new BasicNameValuePair("country", myGPdetails.country));        _("country:" + myGPdetails.country);
                isPost = false;
                valid  = true;
                break;
            case NETWORK_STATE_GET_MY_PATIENT_HISTORY:
                _("NETWORK_STATE_GET_MY_PATIENT_HISTORY");
                isPost = false;
                valid  = true;
                break;
            case NETWORK_STATE_CREATE_A_HISTORY_FOR_MY_PATIENT:
                _("NETWORK_STATE_CREATE_A_HISTORY_FOR_MY_PATIENT");
                postParameters.add(new BasicNameValuePair("user_id", myHistory.user_id));        _("user_id:" + myHistory.user_id);
                postParameters.add(new BasicNameValuePair("date", myHistory.date));              _("date:" + myHistory.date);
                postParameters.add(new BasicNameValuePair("diagnosis", myHistory.diagnosis));    _("diagnosis:" + myHistory.diagnosis);
                postParameters.add(new BasicNameValuePair("notes", myHistory.notes));      _("notes:" + myHistory.notes);
                postParameters.add(new BasicNameValuePair("id", myHistory.id));                  _("id:" + myHistory.id);
                REQUIRES_AUTHORISATION__TOKEN__IN_HEADER =true;
                valid  = true;
                break;
            case NETWORK_STATE_GET_MY_PATIENT_CLINICAL_ALERTS:
                _("NETWORK_STATE_GET_MY_PATIENT_CLINICAL_ALERTS");
                isPost = false;
                valid  = true;
                break;
            case NETWORK_STATE_PRACTIONER_LOGIN:
                _("NETWORK_STATE_PRACTIONER_LOGIN");
                break;
            case NETWORK_STATE_PRACTIONER_GET_PROFILE:
                _("NETWORK_STATE_PRACTIONER_GET_PROFILE");
                isPost = false;
                valid  = true;
                break;
            case NETWORK_STATE_PRACTIONER_GET_REVIEWS:
                _("NETWORK_STATE_PRACTIONER_GET_REVIEWS");
                isPost = false;
                valid  = true;
                break;
            case NETWORK_STATE_PRACTIONER_GET_QUALIFICATIONS:
                _("NETWORK_STATE_PRACTIONER_GET_QUALIFICATIONS");
                isPost = false;
                valid  = true;
                break;
            case NETWORK_STATE_PRACTIONER_GET_REGISTRATIONS:
                _("NETWORK_STATE_PRACTIONER_GET_REGISTRATIONS");
                isPost = false;
                valid  = true;
                break;
            case NETWORK_STATE_PRACTIONER_GET_SURGERIES:
                _("NETWORK_STATE_PRACTIONER_GET_SURGERIES");
                isPost = false;
                valid  = true;
                break;
            case NETWORK_STATE_PRACTIONER_GET_DOCUMENTS:
                _("NETWORK_STATE_PRACTIONER_GET_DOCUMENTS");
                isPost = false;
                valid  = true;
                break;
            case NETWORK_STATE_PRACTIONER_GET_ROTA:
                _("NETWORK_STATE_PRACTIONER_GET_ROTA");
                isPost = false;
                valid  = true;
                break;
            case NETWORK_STATE_ADMIN_LOGIN:
                _("NETWORK_STATE_ADMIN_LOGIN");
                isPost = false;
                valid  = true;
                break;
            case NETWORK_STATE_ADMIN_REGISTER:
                _("NETWORK_STATE_ADMIN_REGISTER");
                isPost = false;
                valid  = true;
                break;
            case NETWORK_STATE_ADMIN_GET_PROFILE:
                _("NETWORK_STATE_ADMIN_GET_PROFILE");
                isPost = false;
                valid  = true;
                break;
            case NETWORK_STATE_ADMIN_GET_REPORTS:
                _("NETWORK_STATE_ADMIN_GET_REPORTS");
                valid=true;
                break;
            case NETWORK_STATE_SEARCH_DOCTORS:
                _("NETWORK_STATE_SEARCH_DOCTORS");
                valid=true;
                break;
            case NETWORK_STATE_SEARCH_PATIENTS:
                _("NETWORK_STATE_SEARCH_PATIENTS");
                valid=true;
                break;
            case NETWORK_STATE_SEARCH_TICKETS:
                _("NETWORK_STATE_SEARCH_TICKETS");
                valid=true;
                break;
            case NETWORK_STATE_SEARCH_SURGERIES:
                _("NETWORK_STATE_SEARCH_SURGERIES");
                valid=true;
                break;
            case NETWORK_STATE_SEARCH_CONSULTATIONS:
                _("NETWORK_STATE_SEARCH_CONSULTATIONS");
                break;

            case NETWORK_STATE_LOGIN:
                _("NETWORK_STATE_LOGIN");
                postParameters.add(new BasicNameValuePair("email", email));_("email:" + email);
                postParameters.add(new BasicNameValuePair("password", password));_("password:" + password);

                valid=true;
                break;
            case NETWORK_STATE_GET_LOCAL_SURGERIES:
                _("NETWORK_STATE_GET_LOCAL_SURGERIES");

                url += "?lat="+myLat+"&lng="+myLong;
                isPost = false;
                REQUIRES_AUTHORISATION_KEY_IN_HEADER=true;
                valid=true;
                break;
            //case NETWORK_STATE_RETRIEVE_PASSWORD:
                //  _("NETWORK_STATE_RETRIEVE_PASSWORD "+emailAddress);
                //  postParameters.add(new BasicNameValuePair("email", emailAddress));
                //postParameters.add(new BasicNameValuePair("userName",username));
               // valid=true;
               // break;
            case NETWORK_STATE_GET_GPLIST_FOR_SURGERY:
                _("NETWORK_STATE_GET_GPLIST_FOR_SURGERY");
                url += "?id="+ surgeryID;
                isPost = false;
                REQUIRES_AUTHORISATION_KEY_IN_HEADER=true;
                valid=true;
                break;
            case NETWORK_STATE_SET_MY_PATIENT_SURGERY_AND_GP:
                _("NETWORK_STATE_SET_MY_PATIENT_SURGERY_AND_GP");
                postParameters.add(new BasicNameValuePair("surgery_id", newSurgeryID));_("surgery_id:" + newSurgeryID);
                postParameters.add(new BasicNameValuePair("practitioner_id", newGPid));_("practitioner_id:" + newGPid);
                REQUIRES_AUTHORISATION__TOKEN__IN_HEADER =true;
                REQUIRES_AUTHORISATION_KEY_IN_HEADER=true;
                valid=true;
                break;

            default:
                _("//////////////Warning unknown state!////////////////");
        }

        if (valid)
        {
            if (REQUIRES_AUTHORISATION__TOKEN__IN_HEADER)
            {
                _("REQUIRES_AUTHORISATION__TOKEN__IN_HEADER");
                request.addHeader("X-Clinix-Auth-Token", X_CLINIX_AUTH_TOKEN_ANDROID);  _("X-Clinix-Auth-Token:" + Networking.X_CLINIX_AUTH_TOKEN_ANDROID);

            }
            else
            {
                _("x does not need auth token in header.");
            }

            _("Valid!");
            StringBuffer stringBuffer = new StringBuffer();
            if (isPost)
            {
                _("This call will be POST");
                stringBuffer = Tools.executeNetworking_HTTPPOST(request, postParameters, httpClient);
            }
            else
            {
                _("This call will be GET");
                stringBuffer = Tools.executeNetworking_HTTPGET(url);
            }

            //_("result:"+stringBuffer);
            decodeResultIntoJson(stringBuffer.toString(), state);
        }
        else
        {
            _("/////////////////WARNING//// Valid was not true! Will do nothing");
        }
    }


    /// HERE WE MAKE THE STRING INTO A JSON OBJECT THEN BREAK IT DOWN INTO ITS COMPONENTS ///
    private void decodeResultIntoJson(String response, int state)
    {
        _("########################################################");
        _("########################################################");
        _("############## DECODE RESPONSE FROM SERVER #############");
        _("########################################################");
        _("########################################################");
        hideProgressBar();

        if (response.contains("error"))
        {
            _("WARNING///////////////////errors are detected!!! [Did you set GET/POST correctly?] --> "+response);
            try {
                JSONObject jo = new JSONObject(response);
                String error = jo.getString("error");
                _("Error from server is: "+error);
                if (error.indexOf("Invalid token")!=-1)
                {
                    error = error+". You need to log back in.";
                    //send them to login screen
                    Intent intent = new Intent(c, LoginActivity.class);
                    c.startActivity(intent);
                }

                Tools.toast(/*"Server response: "+*/error, c);


            } catch (JSONException e) {
                _("WARNING PROBLEM DECODING THE -ERROR- ITS NOT JSON!!! "+e.getMessage());

                Tools.toast(/*"Server response: "+*/"WARNING! Data isn't Json? Server bug? -> "+e.getMessage(), c);
                e.printStackTrace();
            }
            return;
        }
        else
        {
            //_("No error detected");
        }

        switch (state) {
            case NETWORK_STATE_LOGIN:
                _("NETWORK_STATE_LOGIN");
                if (response==null || response.length()==0)
                {
                    _("No JSON from server, error on server?");
                    Tools.toast("No JSON from server - server error?",c);
                    return;
                }
                //{"user":{"id":3,"forename":"Gareth","surname":"Murfin","nhs_number":null},"token":"3-323464e5-acff-48ca-aafa-4cec42f9b642"}
                try {
                    JSONObject jo = new JSONObject(response);
                    JSONObject user = jo.getJSONObject("user");
                    String id = user.getString("id");
                    String forename = user.getString("forename");
                    String surname = user.getString("surname");
                    String nhs_number = user.getString("nhs_number");
                    String token = jo.getString("token");
                    /*String venue_type_id    = jo.getString("venue_type_id");
                    String slug             = jo.getString("slug");
                    String updated_at       = jo.getString("updated_at");
                    String created_at       = jo.getString("created_at");
                    String id               = jo.getString("id");
                    String partySession     = jo.getString("partySession");
                    String linkUrl          = jo.getString("linkUrl");
*/
                    _("JSON CONTENTS_________________________________________:");
                    _("id............:" + id);
                    _("forename......:" + forename);
                    _("surname.......:" + surname);
                    _("nhs_number....:" + nhs_number);
                    _("token.........:" + token);

  /*                  _("venue_type_id...:"+venue_type_id);
                    _("slug............:"+slug);
                    _("updated_at......:"+updated_at);
                    _("created_at......:"+created_at);
                    _("id..............:"+id);
                    _("partySession....:"+partySession);
                    _("linkUrl.........:"+linkUrl);
*/
                    _("SUCCESSFULLY LOGGED IN!!!!");
                    if (Tools.SHOW_DEBUG_TOASTS) {
                        Tools.toast("Login successful!", c);
                    }

                    //save to memory here-------------------------
                    //-1 just means we arent using an int, i will write all as strings
                    t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, "id", -1, id);
                    t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, "forename", -1, forename);
                    t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, "surname", -1, surname);
                    t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, "nhs_number", -1, nhs_number);
                    t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, "token", -1, token);
                    X_CLINIX_AUTH_TOKEN_ANDROID = token; //update this on each login


                    ////////////////////////

                    //////now we have id lets subscribe to parse channels for push notifications//
                    String myUserId = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, "id", "-1", c);
                    if (myUserId.equals("-1"))
                    {
                        Tools.toast("warning: cant subscribe to parse with this id of -1.",c);
                    }
                    else
                    {
                        //we need to subscribe to a specific channel for this user, in order to receive the push messages from parse, here is how dom describes the push:
                        //
                        //[4:20:26 PM] Dominic Talbot: Ok so, Push with Parse. When certain endpoints are hit on the server i.e. /appointments/book it triggers a few events:
                        // email; web notification and push notification. The push notification is then sent to parse as a simple cURL request.
                        // Parse then distributes it down to the devices which the user has logged in on
                        //[4:20:48 PM] Dominic Talbot: so the channel works like “user-{id}”
                        //[4:21:09 PM] Dominic Talbot: So upon sign in, you want to register onto the parse servers on that channel
                        //[4:21:15 PM] Dominic Talbot: the device will then receive that users push notifications
                        ///
                        _("now subscribing on parse channel: " + "user-" + myUserId);
                        //ParsePush.unsubscribeInBackground("user-");//dont subscribe to this one
                        //ParsePush.subscribeInBackground("user-" + myUserId);
                        //ParsePush.subscribeInBackground("broadcast");

                        ParsePush.subscribeInBackground("user-" + myUserId, new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                if (e == null) {
                                    _("successfully subscribed to the broadcast channel.");
                                } else {
                                    _("failed to subscribe for push "+ e.getMessage());
                                }
                            }
                        });
                    }

                    //we also need the generic broadcast channel we can receive messages about updates etc on this
                    ParsePush.subscribeInBackground("broadcast", new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                _("successfully subscribed to the broadcast channel.");
                            } else {
                                _("failed to subscribe for push" + e.getMessage());
                            }
                        }
                    });

                    ParseInstallation.getCurrentInstallation().saveInBackground();

                    _("my channels : ");
                    List<String> subscribedChannels = ParseInstallation.getCurrentInstallation().getList("channels");
                    for (int i=0; i<subscribedChannels.size(); i++)
                    {
                        _(i+"// "+subscribedChannels.get(i));
                    }
                    ///////////////////////////////////////////////////////////////////////

                    Intent intent = new Intent(c, MainMenuActivity.class);
                    c.startActivity(intent);
                } catch (JSONException e) {
                    _("WARNING PROBLEM DECODING THE JSON!!! " + e.getMessage());
                    e.printStackTrace();
                }
                break;
            case NETWORK_STATE_REGISTER:
                _("NETWORK_STATE_REGISTER");
                /*
                A successful response looks like this:

               {
                   "type":"patient",
                   "user":{
                      "id":"23",
                      "title":"",
                      "forename":"Gaz",
                      "surname":"Murfin",
                      "suffix":null,
                      "email":"w@w.com",
                      "register_date":"2016-04-11 09:04:26",
                      "dob":"1984-04-10",
                      "nhs_number":"",
                      "practitioner_id":null,
                      "surgery_id":"0",
                      "custom_practitioner_id":null,
                      "custom_practitioner":"0",
                      "custom_surgery_id":null,
                      "custom_surgery":"0",
                      "telephone":"",
                      "street":"",
                      "address_2":"",
                      "town":"",
                      "county":"",
                      "postcode":"",
                      "country":"",
                      "gender":"unknown",
                      "height_type":"feet",
                      "height":"",
                      "weight_type":"stone",
                      "weight":"",
                      "smoker":"unknown",
                      "image":"",
                      "bmi":"",
                      "blood":"",
                      "type":"nhs",
                      "status":"active",
                      "account_status":"active",
                      "login_modal":"1"
                   },
                   "surgery":[

                   ],
                   "token":"23-3333ae92-2635-4a8c-8bc8-e4fcac99ed2b"
                }
                */

                try {
                    JSONObject jo = new JSONObject(response);
                    String user_type                = jo.getString("type");
                    ProfileActivity.myProfile       = parseUserObject(jo, "user");           // now we need to get the user object!

                    _("////////////////////////////SUCCESSFULLY REGISTERED!!!!");

                    if (Tools.SHOW_DEBUG_TOASTS) {
                        Tools.toast("Registration successful!", c);
                    }

                    //save to memory here-------------------------
                    Tools t = new Tools(c);
                    t.setupPrefs(c);

//PROB SHOULD SAVE THIS TO MEM HERE BUT USE STACIALLY FOR NOW!!!!!!!!!!!!!
                    USER_JUST_REGISTERED=true;
                    //now move to main menu
                    Intent intent = new Intent(c, MainMenuActivity.class);
                    c.startActivity(intent);
                    //Tools.moveOnToNextActivity(state,c);

                } catch (JSONException e) {
                    _("WARNING PROBLEM DECODING THE JSON!!! " + e.getMessage());
                    e.printStackTrace();
                }
                break;
            case NETWORK_STATE_GET_APPOINTMENTS_SCHEDULED:
            case NETWORK_STATE_GET_APPOINTMENTS_PREVIOUS:

                /*
                {
                   "total":1,
                   "per_page":20,
                   "current_page":1,
                   "last_page":1,
                   "next_page_url":null,
                   "prev_page_url":null,
                   "from":1,
                   "to":1,
                   "data":[
                      {
                         "id":1,
                         "practitioner_id":2,
                         "user_id":1,
                         "child_id":null,
                         "start":1447143000,
                         "mode":"video",
                         "problem":"sdf",
                         "image":"",
                         "status":"booked",
                         "session_id":"",
                         "user_token":"",
                         "practitioner_token":"",
                         "user":{
                            "id":1,
                            "title":"Mr",
                            "forename":"Dominic",
                            "surname":"Talbot",
                            "suffix":null,
                            "email":"dominic@royand.co",
                            "register_date":"2015-08-25 11:00:00",
                            "dob":"1993-07-16",
                            "nhs_number":1234567891,
                            "practitioner_id":1,
                            "surgery_id":1,
                            "custom_practitioner_id":null,
                            "custom_practitioner":0,
                            "custom_surgery_id":null,
                            "custom_surgery":0,
                            "telephone":7791193905,
                            "street":"8 Rufford Avenue, Retford, Notts",
                            "address_2":"",
                            "town":"",
                            "county":"",
                            "postcode":"DN22 7RY",
                            "country":"",
                            "gender":"male",
                            "height_type":"cm",
                            "height":188,
                            "weight_type":"kg",
                            "weight":"90kg",
                            "smoker":"non-smoker",
                            "image":"https:\/\/s3-eu-west-1.amazonaws.com\/clinix\/patients\/avatars\/woman.jpg",
                            "bmi":"",
                            "blood":"",
                            "type":"nhs",
                            "status":"active",
                            "practitioner":{
                               "id":1,
                               "title":"Dr",
                               "forename":"John",
                               "surname":"Smith",
                               "dob":"0000-00-00",
                               "suffix":null,
                               "email":"john@johnsmith.com",
                               "register_date":"2015-08-25 11:00:00",
                               "telephone":7791193905,
                               "street":"8 Rufford Avenue",
                               "address_2":"",
                               "town":"",
                               "county":"",
                               "postcode":"DN22 7RY",
                               "country":"",
                               "type":"doctor",
                               "surgery_ids":"1,3",
                               "account_status":"active",
                               "profession":"Genius Doctor",
                               "specialty":"Brain Surgery",
                               "registered_date":"1994-08-25 11:00:00",
                               "information":"Lorem Ipsum",
                               "information_date":"2015-08-25 11:00:00",
                               "signature":"",
                               "signature_date":"0000-00-00 00:00:00",
                               "initial":100,
                               "follow_up":100,
                               "image":"https:\/\/s3-eu-west-1.amazonaws.com\/clinix\/practitioner\/avatars\/doc.jpg",
                               "status":"disabled",
                               "category":"Medical Practitioner",
                               "service_type":"NHS",
                               "insurer":"",
                               "policy_no":"",
                               "gmc_no":""
                            }
                         },
                         "practitioner":{
                            "id":2,
                            "title":"Dr",
                            "forename":"John",
                            "surname":"Smith",
                            "dob":"0000-00-00",
                            "suffix":null,
                            "email":"john@johndoe.com",
                            "register_date":"2015-08-25 11:00:00",
                            "telephone":"",
                            "street":"",
                            "address_2":"",
                            "town":"",
                            "county":"",
                            "postcode":"",
                            "country":"",
                            "type":"doctor",
                            "surgery_ids":"1,3",
                            "account_status":"active",
                            "profession":"Genius Doctor",
                            "specialty":"Brain Surgery",
                            "registered_date":"1994-08-25 11:00:00",
                            "information":"",
                            "information_date":"0000-00-00 00:00:00",
                            "signature":"",
                            "signature_date":"0000-00-00 00:00:00",
                            "initial":"",
                            "follow_up":"",
                            "image":"",
                            "status":"disabled",
                            "category":"",
                            "service_type":"",
                            "insurer":"",
                            "policy_no":"",
                            "gmc_no":""
                         }
                      }
                   ]
                }
                * */

                String whatState = "";// just so we can print it
                switch (state) {
                    case NETWORK_STATE_GET_APPOINTMENTS_SCHEDULED:
                        whatState = "NETWORK_STATE_GET_APPOINTMENTS_SCHEDULED";
                        AppointmentsActivity.itemListScheduled = new ArrayList<Appointment>();
                        break;
                    case NETWORK_STATE_GET_APPOINTMENTS_PREVIOUS:
                        whatState = "NETWORK_STATE_GET_APPOINTMENTS_PREVIOUS";
                        AppointmentsActivity.itemListPrevious = new ArrayList<Appointment>();
                        break;
                }

                try {
                    String friendlyDateString   = "";
                    String friendlyDateStringAlternative   = "";
                    String practionerName       = "";
                    String data_mode            = "";

                    _("JSON:___________________________________" + whatState);
                    JSONObject jo           = new JSONObject(response);
                    String total            = jo.getString("total");
                    String per_page         = jo.getString("per_page");
                    String current_page     = jo.getString("current_page");
                    String last_page        = jo.getString("last_page");
                    String next_page_url    = jo.getString("next_page_url");
                    String prev_page_url    = jo.getString("prev_page_url");
                    String from             = jo.getString("from");
                    String to               = jo.getString("to");

                    _("total        :" + total);
                    _("per_page     :" + per_page);
                    _("current_page :" + current_page);
                    _("last_page    :" + last_page);
                    _("next_page_url:" + next_page_url);
                    _("prev_page_url:" + prev_page_url);
                    _("from         :" + from);
                    _("to           :" + to);


                    //the json contains a main array called dataArray which contains a lot of stuff about EACH appointment
                    JSONArray dataArray = jo.getJSONArray("data");
                    if (dataArray.length()==0)
                    {
                        _("dataArray length is 0");
                    }
                    else
                    {
                        _("dataArray array (length:"+dataArray.length()+"):");
                    }

                    for (int i = 0; i < dataArray.length(); i++)
                    {
                        JSONObject datajo               = dataArray.getJSONObject(i);
                        String data_id                  = datajo.getString("id");
                        String data_practitioner_id     = datajo.getString("practitioner_id");
                        String data_user_id             = datajo.getString("user_id");
                        String data_child_id            = datajo.getString("child_id");
                        String data_start               = datajo.getString("start");
                        data_mode                       = datajo.getString("mode");
                        String data_problem             = datajo.getString("problem");
                        String data_image               = datajo.getString("image");
                        String data_status              = datajo.getString("status");
                        String data_session_id          = datajo.getString("session_id");
                        String data_user_token          = datajo.getString("user_token");
                        String data_practitioner_token  = datajo.getString("practitioner_token");

                        _("#" + i + "================================");
                        _("  id                 :" + data_id);
                        _("  practitioner_id    :" + data_practitioner_id);
                        _("  user_id            :" + data_user_id);
                        _("  child_id           :" + data_child_id);
                        _("  start              :" + data_start);
                        _("  mode               :" + data_mode);
                        _("  problem            :" + data_problem);
                        _("  image              :" + data_image);
                        _("  status             :" + data_status);
                        _("  session_id         :" + data_session_id);
                        _("  user_token         :" + data_user_token);
                        _("  practitioner_token :" + data_practitioner_token);

                        friendlyDateString = Tools.convertStringLongIntoFriendlyDateString(data_start);//convert date to right format WHICH IS 10/11/15 16:10
                        _("friendlyDateString ->"+friendlyDateString);
                        //and theres another friendly Date string we need which is Tues 10 November 2015
                        friendlyDateStringAlternative = Tools.convertStringLongIntoFriendlyDateStringALT(data_start);
                        //now we need the user object:
                        //user doesnt always exist so we need to get it only if its there, or we end up in the catch
                      /*  JSONObject userjo = tryGetJsonObject(datajo, "user");
                        if (userjo != null)
                        {
                            _("user object:");
                            String user_id                      = userjo.getString("id");
                            String user_title                   = userjo.getString("title");
                            String user_forename                = userjo.getString("forename");
                            String user_surname                 = userjo.getString("surname");
                            String user_suffix                  = userjo.getString("suffix");
                            String user_email                   = userjo.getString("email");
                            String user_register_date           = userjo.getString("register_date");
                            String user_dob                     = userjo.getString("dob");
                            String user_nhs_number              = userjo.getString("nhs_number");
                            String user_practitioner_id         = userjo.getString("practitioner_id");
                            String user_surgery_id              = userjo.getString("surgery_id");
                            String user_custom_practitioner_id  = userjo.getString("custom_practitioner_id");
                            String user_custom_practitioner     = userjo.getString("custom_practitioner");
                            String user_custom_surgery_id       = userjo.getString("custom_surgery_id");
                            String user_custom_surgery          = userjo.getString("custom_surgery");
                            String user_telephone               = userjo.getString("telephone");
                            String user_street                  = userjo.getString("street");
                            String user_address_2               = userjo.getString("address_2");
                            String user_town                    = userjo.getString("town");
                            String user_county                  = userjo.getString("county");
                            String user_postcode                = userjo.getString("postcode");
                            String user_country                 = userjo.getString("country");
                            String user_gender                  = userjo.getString("gender");
                            String user_height_type             = userjo.getString("height_type");
                            String user_height                  = userjo.getString("height");
                            String user_weight_type             = userjo.getString("weight_type");
                            String user_weight                  = userjo.getString("weight");
                            String user_smoker                  = userjo.getString("smoker");
                            String user_image                   = userjo.getString("image");
                            String user_bmi                     = userjo.getString("bmi");
                            String user_blood                   = userjo.getString("blood");
                            String user_type                    = userjo.getString("type");
                            String user_status                  = userjo.getString("status");

                            _("         user_id                     :" + user_id);
                            _("         user_title                  :" + user_title);
                            _("         user_forename               :" + user_forename);
                            _("         user_surname                :" + user_surname);
                            _("         user_suffix                 :" + user_suffix);
                            _("         user_email                  :" + user_email);
                            _("         user_register_date          :" + user_register_date);
                            _("         user_dob                    :" + user_dob);
                            _("         user_nhs_number             :" + user_nhs_number);
                            _("         user_practitioner_id        :" + user_practitioner_id);
                            _("         user_surgery_id             :" + user_surgery_id);
                            _("         user_custom_practitioner_id :" + user_custom_practitioner_id);
                            _("         user_custom_practitioner    :" + user_custom_practitioner);
                            _("         user_custom_surgery_id      :" + user_custom_surgery_id);
                            _("         user_custom_surgery         :" + user_custom_surgery);
                            _("         user_telephone              :" + user_telephone);
                            _("         user_street                 :" + user_street);
                            _("         user_address_2              :" + user_address_2);
                            _("         user_town                   :" + user_town);
                            _("         user_county                 :" + user_county);
                            _("         user_postcode               :" + user_postcode);
                            _("         user_country                :" + user_country);
                            _("         user_gender                 :" + user_gender);
                            _("         user_height_type            :" + user_height_type);
                            _("         user_height                 :" + user_height);
                            _("         user_weight_type            :" + user_weight_type);
                            _("         user_weight                 :" + user_weight);
                            _("         user_smoker                 :" + user_smoker);
                            _("         user_image                  :" + user_image);
                            _("         user_bmi                    :" + user_bmi);
                            _("         user_blood                  :" + user_blood);
                            _("         user_type                   :" + user_type);
                            _("         user_status                 :" + user_status);
*/
                            /////
                            //now we need the practioner object associated with this appointment
                            //practitioner doesnt always exist so we need to get it only if its there, or we end up in the catch
                            JSONObject practitionerObject_ForUser = tryGetJsonObject(datajo, "practitioner");
                            if (practitionerObject_ForUser != null) {
                                _("//practitioner object for USER://");

                                String practitioner_id              = practitionerObject_ForUser.getString("id");
                                String practitioner_title           = practitionerObject_ForUser.getString("title");
                                String practitioner_forename        = practitionerObject_ForUser.getString("forename");
                                String practitioner_surname         = practitionerObject_ForUser.getString("surname");
                                String practitioner_dob             = practitionerObject_ForUser.getString("dob");
                                String practitioner_suffix          = practitionerObject_ForUser.getString("suffix");
                                String practitioner_email           = practitionerObject_ForUser.getString("email");
                                String practitioner_register_date   = practitionerObject_ForUser.getString("register_date");
                                String practitioner_telephone       = practitionerObject_ForUser.getString("telephone");
                                String practitioner_street          = practitionerObject_ForUser.getString("street");
                                String practitioner_address_2       = practitionerObject_ForUser.getString("address_2");
                                String practitioner_town            = practitionerObject_ForUser.getString("town");
                                String practitioner_county          = practitionerObject_ForUser.getString("county");
                                String practitioner_postcode        = practitionerObject_ForUser.getString("postcode");
                                String practitioner_country         = practitionerObject_ForUser.getString("country");
                                String practitioner_type            = practitionerObject_ForUser.getString("type");
                                String practitioner_surgery_ids     = practitionerObject_ForUser.getString("surgery_ids");
                                String practitioner_account_status  = practitionerObject_ForUser.getString("account_status");
                                String practitioner_profession      = practitionerObject_ForUser.getString("profession");
                                String practitioner_specialty       = practitionerObject_ForUser.getString("specialty");
                                String practitioner_registered_date = practitionerObject_ForUser.getString("registered_date");
                                String practitioner_information     = practitionerObject_ForUser.getString("information");
                                String practitioner_information_date= practitionerObject_ForUser.getString("information_date");
                                String practitioner_signature       = practitionerObject_ForUser.getString("signature");
                                String practitioner_signature_date  = practitionerObject_ForUser.getString("signature_date");
                                String practitioner_initial         = practitionerObject_ForUser.getString("initial");
                                String practitioner_follow_up       = practitionerObject_ForUser.getString("follow_up");
                                String practitioner_image           = practitionerObject_ForUser.getString("image");
                                String practitioner_status          = practitionerObject_ForUser.getString("status");
                                String practitioner_category        = practitionerObject_ForUser.getString("category");
                                String practitioner_service_type    = practitionerObject_ForUser.getString("service_type");
                                String practitioner_insurer         = practitionerObject_ForUser.getString("insurer");
                                String practitioner_policy_no       = practitionerObject_ForUser.getString("policy_no");
                                String practitioner_gmc_no          = tryGetString(practitionerObject_ForUser,"gmc_no") ;// practitionerObject_ForUser.getString("gmc_no");

                                _("                 practitioner_id                     :" + practitioner_id);
                                _("                 practitioner_title                  :" + practitioner_title);
                                _("                 practitioner_forename               :" + practitioner_forename);
                                _("                 practitioner_surname                :" + practitioner_surname);
                                _("                 practitioner_dob                    :" + practitioner_dob);
                                _("                 practitioner_suffix                 :" + practitioner_suffix);
                                _("                 practitioner_email                  :" + practitioner_email);
                                _("                 practitioner_register_date          :" + practitioner_register_date);
                                _("                 practitioner_telephone              :" + practitioner_telephone);
                                _("                 practitioner_street                 :" + practitioner_street);
                                _("                 practitioner_address_2              :" + practitioner_address_2);
                                _("                 practitioner_town                   :" + practitioner_town);
                                _("                 practitioner_county                 :" + practitioner_county);
                                _("                 practitioner_postcode               :" + practitioner_postcode);
                                _("                 practitioner_country                :" + practitioner_country);
                                _("                 practitioner_type                   :" + practitioner_type);
                                _("                 practitioner_surgery_ids            :" + practitioner_surgery_ids);
                                _("                 practitioner_account_status         :" + practitioner_account_status);
                                _("                 practitioner_profession             :" + practitioner_profession);
                                _("                 practitioner_specialty              :" + practitioner_specialty);
                                _("                 practitioner_registered_date        :" + practitioner_registered_date);
                                _("                 practitioner_information            :" + practitioner_information);
                                _("                 practitioner_information_date       :" + practitioner_information_date);
                                _("                 practitioner_signature              :" + practitioner_signature);
                                _("                 practitioner_signature_date         :" + practitioner_signature_date);
                                _("                 practitioner_initial                :" + practitioner_initial);
                                _("                 practitioner_follow_up              :" + practitioner_follow_up);
                                _("                 practitioner_image                  :" + practitioner_image);
                                _("                 practitioner_status                 :" + practitioner_status);
                                _("                 practitioner_category               :" + practitioner_category);
                                _("                 practitioner_service_type           :" + practitioner_service_type);
                                _("                 practitioner_insurer                :" + practitioner_insurer);
                                _("                 practitioner_policy_no              :" + practitioner_policy_no);
                                _("                 practitioner_gmc_no                 :" + practitioner_gmc_no);

                                practionerName = practitioner_title + " " + practitioner_forename+" "+practitioner_surname;
                                _("practionerName:"+practionerName);
                            }

                  /*          _("//Now the practitioner for DATA// ");
                            //now we need the practioner object associated with this user
                            //practitioner doesnt always exist so we need to get it only if its there, or we end up in the catch
                            JSONObject practitionerObject_ForWhat = tryGetJsonObject(userjo, "practitioner");
                            if (practitionerObject_ForWhat != null) {
                                //practitionerObject_ForWhat = datajo.getJSONObject("practitioner");

                                String practitioner_id              = practitionerObject_ForWhat.getString("id");
                                String practitioner_title           = practitionerObject_ForWhat.getString("title");
                                String practitioner_forename        = practitionerObject_ForWhat.getString("forename");
                                String practitioner_surname         = practitionerObject_ForWhat.getString("surname");
                                String practitioner_dob             = practitionerObject_ForWhat.getString("dob");
                                String practitioner_suffix          = practitionerObject_ForWhat.getString("suffix");
                                String practitioner_email           = practitionerObject_ForWhat.getString("email");
                                String practitioner_register_date   = practitionerObject_ForWhat.getString("register_date");
                                String practitioner_telephone       = practitionerObject_ForWhat.getString("telephone");
                                String practitioner_street          = practitionerObject_ForWhat.getString("street");
                                String practitioner_address_2       = practitionerObject_ForWhat.getString("address_2");
                                String practitioner_town            = practitionerObject_ForWhat.getString("town");
                                String practitioner_county          = practitionerObject_ForWhat.getString("county");
                                String practitioner_postcode        = practitionerObject_ForWhat.getString("postcode");
                                String practitioner_country         = practitionerObject_ForWhat.getString("country");
                                String practitioner_type            = practitionerObject_ForWhat.getString("type");
                                String practitioner_surgery_ids     = practitionerObject_ForWhat.getString("surgery_ids");
                                String practitioner_account_status  = practitionerObject_ForWhat.getString("account_status");
                                String practitioner_profession      = practitionerObject_ForWhat.getString("profession");
                                String practitioner_specialty       = practitionerObject_ForWhat.getString("specialty");
                                String practitioner_registered_date = practitionerObject_ForWhat.getString("registered_date");
                                String practitioner_information     = practitionerObject_ForWhat.getString("information");
                                String practitioner_information_date= practitionerObject_ForWhat.getString("information_date");
                                String practitioner_signature       = practitionerObject_ForWhat.getString("signature");
                                String practitioner_signature_date  = practitionerObject_ForWhat.getString("signature_date");
                                String practitioner_initial         = practitionerObject_ForWhat.getString("initial");
                                String practitioner_follow_up       = practitionerObject_ForWhat.getString("follow_up");
                                String practitioner_image           = practitionerObject_ForWhat.getString("image");
                                String practitioner_status          = practitionerObject_ForWhat.getString("status");
                                String practitioner_category        = practitionerObject_ForWhat.getString("category");
                                String practitioner_service_type    = practitionerObject_ForWhat.getString("service_type");
                                String practitioner_insurer         = practitionerObject_ForWhat.getString("insurer");
                                String practitioner_policy_no       = practitionerObject_ForWhat.getString("policy_no");
                                String practitioner_gmc_no          = practitionerObject_ForWhat.getString("gmc_no");

                                _(" practitioner_id                     :" + practitioner_id);
                                _(" practitioner_title                  :" + practitioner_title);
                                _(" practitioner_forename               :" + practitioner_forename);
                                _(" practitioner_surname                :" + practitioner_surname);
                                _(" practitioner_dob                    :" + practitioner_dob);
                                _("	practitioner_suffix                 :" + practitioner_suffix);
                                _("	practitioner_email                  :" + practitioner_email);
                                _("	practitioner_register_date          :" + practitioner_register_date);
                                _("	practitioner_telephone              :" + practitioner_telephone);
                                _("	practitioner_street                 :" + practitioner_street);
                                _("	practitioner_address_2              :" + practitioner_address_2);
                                _("	practitioner_town                   :" + practitioner_town);
                                _("	practitioner_county                 :" + practitioner_county);
                                _("	practitioner_postcode               :" + practitioner_postcode);
                                _("	practitioner_country                :" + practitioner_country);
                                _("	practitioner_type                   :" + practitioner_type);
                                _("	practitioner_surgery_ids            :" + practitioner_surgery_ids);
                                _("	practitioner_account_status         :" + practitioner_account_status);
                                _("	practitioner_profession             :" + practitioner_profession);
                                _("	practitioner_specialty              :" + practitioner_specialty);
                                _("	practitioner_registered_date        :" + practitioner_registered_date);
                                _("	practitioner_information            :" + practitioner_information);
                                _("	practitioner_information_date       :" + practitioner_information_date);
                                _("	practitioner_signature              :" + practitioner_signature);
                                _("	practitioner_signature_date         :" + practitioner_signature_date);
                                _("	practitioner_initial                :" + practitioner_initial);
                                _("	practitioner_follow_up              :" + practitioner_follow_up);
                                _("	practitioner_image                  :" + practitioner_image);
                                _("	practitioner_status                 :" + practitioner_status);
                                _("	practitioner_category               :" + practitioner_category);
                                _("	practitioner_service_type           :" + practitioner_service_type);
                                _("	practitioner_insurer                :" + practitioner_insurer);
                                _("	practitioner_policy_no              :" + practitioner_policy_no);
                                _("	practitioner_gmc_no                 :" + practitioner_gmc_no);

                                practionerName = practitioner_title + " " + practitioner_forename+" "+practitioner_surname;
                                _("practionerName:"+practionerName);

                            } else {
                                _("////////////////////////User object does not exist to get./////////////////////////////// ");
                            }*/
                       //////////////////////// }

                       // Appointment appointment = new Appointment(friendlyDateString,data_mode,practionerName );

                        Appointment appointment = new Appointment(
                                //customs ones to make life easy
                                friendlyDateString,friendlyDateStringAlternative,data_mode,practionerName,
                                //practitioner specific
                                data_id,data_practitioner_id,data_user_id,data_child_id,data_start,data_mode,data_problem,data_image,data_status,data_session_id,data_user_token,data_practitioner_token);//friendlyDateString,data_mode,practionerName );

                        switch (state)
                        {
                            case NETWORK_STATE_GET_APPOINTMENTS_SCHEDULED:
                                _("ADDING SCHEDULED APPOINTMENT => "+appointment.personWith);
                                AppointmentsActivity.itemListScheduled.add(appointment);
                                break;
                            case NETWORK_STATE_GET_APPOINTMENTS_PREVIOUS:
                                _("ADDING PREVIOUS APPOINTMENT => "+appointment.personWith);
                                AppointmentsActivity.itemListPrevious.add(appointment);
                                break;
                        }

                    }


                    //get the practioner based on id.
                   // Practitioner p = new Practitioner();

                    //pass in nice date string,
                    //data_mode ie, video
                    //name of practioner


                }catch(JSONException e){
                    _("WARNING PROBLEM DECODING THE JSON!!! " + e.getMessage());
                    e.printStackTrace();
                }





                //now we fill the list in the activity
                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(
                        new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                ((AppointmentsActivity)c).fillUpList();
                            }
                        }
                );
                switch (state)
                {
                    case NETWORK_STATE_GET_APPOINTMENTS_SCHEDULED:
                        _("SUCCESSFULLY GOT APPOINTMENTS SCHEDULED!!!!");
                        if (Tools.SHOW_DEBUG_TOASTS)
                        {
                            Tools.toast("Getting scheduled appointments successful!", c);
                        }
                        break;
                    case NETWORK_STATE_GET_APPOINTMENTS_PREVIOUS:
                        _("SUCCESSFULLY GOT APPOINTMENTS PREVIOUS!!!!");
                        if (Tools.SHOW_DEBUG_TOASTS)
                        {
                            Tools.toast("Getting previous appointments successful!", c);
                        }
                        break;
                }



                //     _("_______________________________________________________________________");
     //   }



                break;
            case NETWORK_STATE_GET_MY_NEXT_APPOINTMENT:
                _("NETWORK_STATE_GET_MY_NEXT_APPOINTMENT");
                /*
                * {
                   "id":21,
                   "practitioner_id":1,
                   "user_id":1,
                   "child_id":null,
                   "start":1513956000,
                   "mode":"video",
                   "problem":"noneEntered.",
                   "image":"...",
                   "status":"booked",
                   "session_id":"",
                   "user_token":"",
                   "practitioner_token":"",
                   "reminder_sent":0,
                   "user":{
                      "id":1,
                      "title":"Mr",
                      "forename":"Dominic",
                      "surname":"Talbot",
                      "suffix":"null",
                      "email":"dominic@royand.co",
                      "register_date":"2015-08-25 11:00:00",
                      "dob":"1990-07-16",
                      "nhs_number":1234567891,
                      "practitioner_id":1,
                      "surgery_id":1,
                      "custom_practitioner_id":null,
                      "custom_practitioner":0,
                      "custom_surgery_id":null,
                      "custom_surgery":0,
                      "telephone":7791193905,
                      "street":"8s Rufford Avenue, Retford, Notts",
                      "address_2":"",
                      "town":"",
                      "county":"",
                      "postcode":"DN22 7RY",
                      "country":"",
                      "gender":"",
                      "height_type":"cm",
                      "height":"188 cm",
                      "weight_type":"kg",
                      "weight":"0 kgs",
                      "smoker":"",
                      "image":"https:\/\/s3-eu-west-1.amazonaws.com\/clinix\/patients\/avatars\/woman.jpg",
                      "bmi":"",
                      "blood":"",
                      "type":"nhs",
                      "status":"active",
                      "account_status":"active",
                      "login_modal":0
                   },
                   "practitioner":{
                      "id":1,
                      "title":"Dr",
                      "forename":"John",
                      "surname":"Smith",
                      "dob":"0000-00-00",
                      "suffix":null,
                      "email":"john@johnsmith.com",
                      "register_date":"2015-08-25 11:00:00",
                      "telephone":7791193905,
                      "street":"8 Rufford Avenue",
                      "address_2":"",
                      "town":"",
                      "county":"",
                      "postcode":"DN22 7RY",
                      "country":"",
                      "type":"doctor",
                      "surgery_ids":"1,3",
                      "account_status":"active",
                      "profession":"Genius Doctor",
                      "specialty":"General Practitioner",
                      "registered_date":"1994-08-25 00:00:00",
                      "information":"Dr Smith qualified from UCL in 1999 and undertook his training in general practice in the Frimley area, before joining Camberley Health Centre in 2004. He is also a GP trainer and is employed by the Deanery as a programme director for the GP training scheme in Frimley. \nHe has a clinical interest in mens health.",
                      "information_date":"2015-12-09 17:40:35",
                      "signature":"",
                      "signature_date":"0000-00-00 00:00:00",
                      "initial":100,
                      "follow_up":100,
                      "image":"https:\/\/s3-eu-west-1.amazonaws.com\/clinix\/practitioner\/avatars\/doc.jpg",
                      "status":"disabled",
                      "category":"Medical Practitioner",
                      "service_type":"NHS",
                      "insurer":"MPS",
                      "policy_no":57898086,
                      "professional_body":"GMC",
                      "temp_password":0,
                      "registration_number":255778697,
                      "dbs_no":56689809,
                      "dbs_date":"0000-00-00 00:00:00"
                   }
                }
                * */

                if (response==null || response.length()==2) //ie {}
                {
                    _("EMPTY RESPONSE - USER HAS NO APPOINTMENTS TO GET YET. bailing.");
                    return;
                }


                try
                {
                    JSONObject jo           = new JSONObject(response);
                    //get the header stuff
                    String id                   = jo.getString("id");
                    String practitioner_id      = jo.getString("practitioner_id");
                    String user_id              = jo.getString("user_id");
                    String child_id             = jo.getString("child_id");
                    String start                = jo.getString("start");
                    String mode                 = jo.getString("mode");
                    String problem              = jo.getString("problem");
                    String image                = jo.getString("image");
                    String status               = jo.getString("status");
                    String session_id           = jo.getString("session_id");
                    String user_token           = jo.getString("user_token");
                    String practitioner_token   = jo.getString("practitioner_token");
                    String reminder_sent        = jo.getString("reminder_sent");

                    //so we get a readable date to print
                    String friendlyDateString = "";
                    final Date d = new Date( Long.parseLong(start)* 1000L );// /1000)* 1000L);//to get it out of 1970 you need to multiply the Unix timestamp by 1000;
                    friendlyDateString = d.toString();

                    _("id...................:"+id);
                    _("practitioner_id......:"+practitioner_id);
                    _("user_id..............:"+user_id);
                    _("child_id.............:"+child_id);
                    _("///////////////////NEXT APPOINTMENT IS/////////////////");
                    _("start................"+start+" which is: "+friendlyDateString);
                    _("mode.................:"+mode);
                    _("problem..............:"+problem);
                    _("image................:"+image);
                    _("status...............:"+status);
                    _("session_id...........:"+session_id);
                    _("user_token...........:"+user_token);
                    _("practitioner_token...:"+practitioner_token);
                    _("reminder_sent........:"+reminder_sent);

                    //is it today?
                    boolean weHaveAnAppointmentToday=false;
                    if (DateUtils.isToday(d.getTime()))
                    {
                        weHaveAnAppointmentToday=true;
                        _("YOU HAVE AN APPOINTMENT TODAY");
                        _("It starts -> " + DateUtils.getRelativeTimeSpanString(d.getTime()));
                        handler = new Handler(Looper.getMainLooper());
                        handler.post(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        MainMenuActivity.tvDebug.setText("DEBUG: Appointment starts " + DateUtils.getRelativeTimeSpanString(d.getTime())+". tap me to test call.");
                                    }
                                }
                        );

                    }
                    else
                    {
                        handler = new Handler(Looper.getMainLooper());
                        handler.post(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        MainMenuActivity.tvDebug.setText("DEBUG: NO APPOINTMENTS TODAY, tap me to test anyway");
                                    }
                                }
                        );
                        _("NO APPOINTMENTS TODAY.");
                    }



                    User_fromNotification userNf                = parseUserObject(jo,"user");
                    Practitioner_fromNotification practNf       = parsePractitionerObject(jo, "practitioner");


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case NETWORK_STATE_GET_MY_PATIENT_ALLERGIES:
                _("NETWORK_STATE_GET_MY_PATIENT_ALLERGIES");
                /*
                * {
                   "allergies":[
                      {
                         "id":"1",
                         "user_id":"1",
                         "consultation_id":"1",
                         "allergy":"Pollen",
                         "reaction":"Lorem Ipsum...",
                         "type":"Allergy Type Here",
                         "image":"test.png",
                         "date":"2015-08-25 11:00:00"
                      },
                      {
                         "id":"2",
                         "user_id":"1",
                         "consultation_id":"1",
                         "allergy":"Peanut",
                         "reaction":"Lorem Ipsum...",
                         "type":"Allergy Type Here",
                         "image":"test.png",
                         "date":"2015-08-25 11:00:00"
                      }
                   ]
                }
                * */
                try {

                    AllergiesActivity.itemListFood      = new ArrayList<Allergy>();
                    AllergiesActivity.itemListDrugs     = new ArrayList<Allergy>();
                    AllergiesActivity.itemListOthers    = new ArrayList<Allergy>();


                    JSONObject jo               = new JSONObject(response);
                    JSONArray allergiesArray    = jo.getJSONArray("allergies");

                    for (int i=0; i<allergiesArray.length(); i++)
                    {
                        JSONObject allergyobj   = (JSONObject) allergiesArray.get(i);
                        String id               = allergyobj.getString("id");
                        String user_id          = allergyobj.getString("user_id");
                        String consultation_id  = allergyobj.getString("consultation_id");
                        String allergy          = allergyobj.getString("allergy");
                        String reaction         = allergyobj.getString("reaction");
                        String type             = allergyobj.getString("type");
                        String image            = allergyobj.getString("image");
                        String date             = allergyobj.getString("date");

                        _("_______________________________________________________");
                        _("id..............."+id);
                        _("user_id........."+user_id);
                        _("consultation_id."+consultation_id);
                        _("allergy........."+allergy);
                        _("reaction........"+reaction);
                        _("type............."+type);
                        _("image............"+image);
                        _("date............."+date);

                        Allergy allergyObject  = new Allergy(id,user_id,consultation_id,allergy,reaction,type,image,date);
                        if (type.equals("food"))
                        {
                            _("Allergy added to FOOD list.");
                            AllergiesActivity.itemListFood.add(allergyObject);
                        }
                        else if (type.equals("drugs"))
                        {
                            _("Allergy added to DRUGS list.");
                            AllergiesActivity.itemListDrugs.add(allergyObject);
                        } else //if (type.equals("food"))
                        {
                            _("Allergy added to OTHER list.");
                            AllergiesActivity.itemListOthers.add(allergyObject);
                        }
                    }
                  //  JSONObject allergyObject =


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                break;
            case NETWORK_STATE_PASSWORD_RESET_FORGOT:
                _("NETWORK_STATE_PASSWORD_RESET_FORGOT");

                try {
                    JSONObject jo           = new JSONObject(response);
                    String success          = jo.getString("success");
                    _("success:"+success);
                    Tools.toast(success, c);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case NETWORK_STATE_GET_NOTIFICATIONS:
                _("NETWORK_STATE_GET_NOTIFICATIONS");
                /*
                {
                   "total":14,
                   "per_page":20,
                   "current_page":1,
                   "last_page":1,
                   "next_page_url":null,
                   "prev_page_url":null,
                   "from":1,
                   "to":14,
                   "data":[
                      {
                         "id":"1",
                         "user_id":"1",
                         "practitioner_id":"1",
                         "admin_id":"100",
                         "date":"2015-09-01 00:00:00",
                         "message":"Lorem Ipsum....",
                         "read":"0",
                         "user":{
                            "id":"1",
                            "title":"Mr",
                            "forename":"Dominic",
                            "surname":"Talbot",
                            "suffix":null,
                            "email":"dominic@royand.co",
                            "register_date":"2015-08-25 11:00:00",
                            "dob":"1993-07-16",
                            "nhs_number":"1234567891",
                            "practitioner_id":"1",
                            "surgery_id":"1",
                            "custom_practitioner_id":null,
                            "custom_practitioner":"0",
                            "custom_surgery_id":null,
                            "custom_surgery":"0",
                            "telephone":"07791193905",
                            "street":"8 Rufford Avenue, Retford, Notts",
                            "address_2":"",
                            "town":"",
                            "county":"",
                            "postcode":"DN22 7RY",
                            "country":"",
                            "gender":"male",
                            "height_type":"cm",
                            "height":"188",
                            "weight_type":"kg",
                            "weight":"90kg",
                            "smoker":"non-smoker",
                            "image":"https:\/\/s3-eu-west-1.amazonaws.com\/clinix\/patients\/avatars\/woman.jpg",
                            "bmi":"",
                            "blood":"",
                            "type":"nhs",
                            "status":"active"
                         },
                         "admin":{
                            "id":"100",
                            "title":"Mr",
                            "forename":"Roy & Co",
                            "surname":"Admin",
                            "suffix":null,
                            "email":"admin@royand.co",
                            "register_date":"2015-08-25 11:00:00",
                            "surgery_ids":null,
                            "dob":"1993-07-16",
                            "telephone":"01302 965110",
                            "street":"Gresley House, Ten Pound Walk, Doncaster",
                            "address_2":"",
                            "town":"",
                            "county":"",
                            "postcode":"DN4 5HX",
                            "country":"",
                            "gender":"male",
                            "image":"https:\/\/s3-eu-west-1.amazonaws.com\/clinix\/gen-admin.jpg",
                            "type":"super"
                         },
                         "practitioner":{
                            "id":"1",
                            "title":"Dr",
                            "forename":"John",
                            "surname":"Smith",
                            "dob":"0000-00-00",
                            "suffix":null,
                            "email":"john@johnsmith.com",
                            "register_date":"2015-08-25 11:00:00",
                            "telephone":"07791193905",
                            "street":"8 Rufford Avenue",
                            "address_2":"",
                            "town":"",
                            "county":"",
                            "postcode":"DN22 7RY",
                            "country":"",
                            "type":"doctor",
                            "surgery_ids":"1,3",
                            "account_status":"active",
                            "profession":"Genius Doctor",
                            "specialty":"Brain Surgery",
                            "registered_date":"1994-08-25 11:00:00",
                            "information":"Lorem Ipsum",
                            "information_date":"2015-08-25 11:00:00",
                            "signature":"",
                            "signature_date":"0000-00-00 00:00:00",
                            "initial":"100",
                            "follow_up":"100",
                            "image":"https:\/\/s3-eu-west-1.amazonaws.com\/clinix\/practitioner\/avatars\/doc.jpg",
                            "status":"disabled",
                            "category":"Medical Practitioner",
                            "service_type":"NHS",
                            "insurer":"",
                            "policy_no":"",
                            "gmc_no":""
                         }
                      },
                      {
                         "id":"2",
                         "user_id":"1",
                         "practitioner_id":"1",
                             etc etc etc
                             etc etc etc
                * */
                try
                {
                    JSONObject jo           = new JSONObject(response);
                    //get the header stuff
                    String total            = jo.getString("total");
                    String per_page         = jo.getString("per_page");
                    String current_page     = jo.getString("current_page");
                    String last_page        = jo.getString("last_page");
                    String next_page_url    = jo.getString("next_page_url");
                    String prev_page_url    = jo.getString("prev_page_url");
                    String from             = jo.getString("from");
                    String to               = jo.getString("to");
                    _("total................:"+total);
                    _("per_page.............:"+per_page);
                    _("current_page.........:"+current_page);
                    _("last_page............:"+last_page);
                    _("next_page_url........:"+next_page_url);
                    _("prev_page_url........:"+prev_page_url);
                    _("from.................:"+from);
                    _("to...................:"+to);

                    NotificationActivity.notificationList = new ArrayList<Notification_>();

                    ///now get the data array//
                    //this is a large array containing each notification and sub objects etc!..
                    JSONArray dataArray = jo.getJSONArray("data");
                    if (dataArray.length()==0)
                    {
                        _("Warning.. dataArray length is 0");
                    }
                    else
                    {
                        _("dataArray array: (length:"+dataArray.length()+"):");
                    }
                    for (int i = 0; i < dataArray.length(); i++)
                    {
                        JSONObject datajo           = dataArray.getJSONObject(i);
                        String data_id              = datajo.getString("id");
                        String data_user_id         = datajo.getString("user_id");
                        String data_practitioner_id = datajo.getString("practitioner_id");
                        String data_admin_id        = datajo.getString("admin_id");
                        String date                 = datajo.getString("date");
                        String message              = datajo.getString("message");
                        String read                 = datajo.getString("read");
                        _(i+"// NOTIFICATION_____________________");
                        _("data_id.............:"+data_id);
                        _("data_user_id........:"+data_user_id);
                        _("data_practitioner_id:"+data_practitioner_id);
                        _("data_admin_id.......:"+data_admin_id);
                        _("date................:"+date);
                        _("message.............:"+message);
                        _("read................:"+read);

                        //use methods to do this since theres so much code, also it seems the API returns different versions of things,
                        //so I had to introduce a notification version of a "user", "admin" and "practitioner"
                        //if these all turn out to be compatible later then I wont need specific ones, but im assuming it wont change now,
                        //so thats why I have things like User_fromNotification, Admin_fromNotification, Practitioner_fromNotification here.
                        _("   GET THE USER OBJECT_______________________________________:");
                        User_fromNotification userNf                = parseUserObject(datajo,"user");           // now we need to get the user object!
                        _("   GET THE ADMIN OBJECT_______________________________________:");
                        Admin_fromNotification adminNf              = parseAdminObject(datajo);          // now we need to get the admin object!
                        _("   GET THE PRACTITIONER OBJECT_______________________________________:");
                        Practitioner_fromNotification practNf       = parsePractitionerObject(datajo,"user");   // now we need to get the practitioner object!

                        ///long dateL = Long.parseLong(date);//date isnt a long
                        boolean readB = Boolean.parseBoolean(read);

                        Notification_ n = new Notification_("subject here",message,date,readB);
                        NotificationActivity.notificationList.add(n);

                    }
                    _("END OF NOTIFICATION PARSING!//////////////////////////////////////////////");
                    _("There are:"+NotificationActivity.notificationList.size()+" notifications.");

                    //now we fill the list in the activity
                    handler = new Handler(Looper.getMainLooper());
                    handler.post(
                            new Runnable() {
                                @Override
                                public void run() {
                                    ((NotificationActivity) c).fillUpList();
                                }
                            }
                    );


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case NETWORK_STATE_GET_MY_PATIENT_HISTORY:
                _("NETWORK_STATE_GET_MY_PATIENT_HISTORY");
                /*
                {
                   "history":[
                      {
                         "id":"1",
                         "user_id":"1",
                         "diagnosis":"Patient diagnosis here",
                         "notes":"Persistent aches and pains Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua.",
                         "practitioner_id":"1",
                         "custom_practitioner":"",
                         "image":"",
                         "date":"2015-10-16 00:00:00",
                         "practitioner":{
                            "id":"1",
                            "title":"Dr",
                            "forename":"John",
                            "surname":"Smith",
                            "dob":"0000-00-00",
                            "suffix":null,
                            "email":"john@johnsmith.com",
                            "register_date":"2015-08-25 11:00:00",
                            "telephone":"07791193905",
                            "street":"8 Rufford Avenue",
                            "address_2":"",
                            "town":"",
                            "county":"",
                            "postcode":"DN22 7RY",
                            "country":"",
                            "type":"doctor",
                            "surgery_ids":"1,3",
                            "account_status":"active",
                            "profession":"Genius Doctor",
                            "specialty":"Brain Surgery",
                            "registered_date":"1994-08-25 11:00:00",
                            "information":"Lorem Ipsum",
                            "information_date":"2015-08-25 11:00:00",
                            "signature":"",
                            "signature_date":"0000-00-00 00:00:00",
                            "initial":"100",
                            "follow_up":"100",
                            "image":"https:\/\/s3-eu-west-1.amazonaws.com\/clinix\/practitioner\/avatars\/doc.jpg",
                            "status":"disabled",
                            "category":"Medical Practitioner",
                            "service_type":"NHS",
                            "insurer":"",
                            "policy_no":"",
                            "gmc_no":""
                         }
                      },
                      {
                         "id":"2",
                         "user_id":"1",
                         "diagnosis":"Patient diagnosis here",
                         "notes":"Persistent aches and pains Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua.",
                         "practitioner_id":"1",
                         "custom_practitioner":"",
                         "image":"",
                         "date":"2015-10-16 00:00:00",
                         "practitioner":{
                            "id":"1",
                            "title":"Dr",
                            "forename":"John",
                            "surname":"Smith",
                            "dob":"0000-00-00",
                            "suffix":null,
                            "email":"john@johnsmith.com",
                            "register_date":"2015-08-25 11:00:00",
                            "telephone":"07791193905",
                            "street":"8 Rufford Avenue",
                            "address_2":"",
                            "town":"",
                            "county":"",
                            "postcode":"DN22 7RY",
                            "country":"",
                            "type":"doctor",
                            "surgery_ids":"1,3",
                            "account_status":"active",
                            "profession":"Genius Doctor",
                            "specialty":"Brain Surgery",
                            "registered_date":"1994-08-25 11:00:00",
                            "information":"Lorem Ipsum",
                            "information_date":"2015-08-25 11:00:00",
                            "signature":"",
                            "signature_date":"0000-00-00 00:00:00",
                            "initial":"100",
                            "follow_up":"100",
                            "image":"https:\/\/s3-eu-west-1.amazonaws.com\/clinix\/practitioner\/avatars\/doc.jpg",
                            "status":"disabled",
                            "category":"Medical Practitioner",
                            "service_type":"NHS",
                            "insurer":"",
                            "policy_no":"",
                            "gmc_no":""
                         }
                      }
                   ]
                }
                * */
                try {

                    HistoryActivity.itemListHistory = new ArrayList<History>();

                    JSONObject jo               = new JSONObject(response);
                    JSONArray historyArray  = jo.getJSONArray("history");
                    for (int i=0; i<historyArray.length(); i++) {
                        JSONObject historyjo = historyArray.getJSONObject(i);


                        /// JSONObject jo           = new JSONObject(response);
                        String id = historyjo.getString("id");
                        String user_id = historyjo.getString("user_id");
                        String diagnosis = historyjo.getString("diagnosis");
                        String notes = historyjo.getString("notes");
                        String practitioner_id = historyjo.getString("practitioner_id");
                        String image = historyjo.getString("image");
                        String date = historyjo.getString("date");
                        _("_______________________________________");
                        _("id..............:" + id);
                        _("user_id.........:" + user_id);
                        _("diagnosis.......:" + diagnosis);
                        _("notes...........:" + notes);
                        _("practitioner_id.:" + practitioner_id);
                        _("image...........:" + image);
                        _("date............:" + date);

                        HistoryActivity.itemListHistory.add(new History(id,user_id,date,diagnosis, notes));
                    }

                    //now we fill the list in the activity
                    handler = new Handler(Looper.getMainLooper());
                    handler.post(
                            new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    ((HistoryActivity)c).fillUpList();
                                }
                            }
                    );

                } catch (JSONException e) {
                    e.printStackTrace();
                }


                break;
            case NETWORK_STATE_GET_MY_PATIENT_MEDICATIONS:
                _("NETWORK_STATE_GET_MY_PATIENT_MEDICATIONS");
                /*
                {
                   "medications":[
                      {
                         "id":"1",
                         "user_id":"1",
                         "child_id":"0",
                         "practitioner_id":"1",
                         "drug_name":"Metamorfin 1g Tablets",
                         "quantity":"60",
                         "course":"30 Days",
                         "instructions":"Take 2 tablets daily with evening meal",
                         "date":"2015-08-25 11:00:00",
                         "practitioner":{
                            "id":"1",
                            "title":"Dr",
                            "forename":"John",
                            "surname":"Smith",
                            "dob":"0000-00-00",
                            "suffix":null,
                            "email":"john@johnsmith.com",
                            "register_date":"2015-08-25 11:00:00",
                            "telephone":"07791193905",
                            "street":"8 Rufford Avenue",
                            "address_2":"",
                            "town":"",
                            "county":"",
                            "postcode":"DN22 7RY",
                            "country":"",
                            "type":"doctor",
                            "surgery_ids":"1,3",
                            "account_status":"active",
                            "profession":"Genius Doctor",
                            "specialty":"Brain Surgery",
                            "registered_date":"1994-08-25 11:00:00",
                            "information":"Lorem Ipsum",
                            "information_date":"2015-08-25 11:00:00",
                            "signature":"",
                            "signature_date":"0000-00-00 00:00:00",
                            "initial":"100",
                            "follow_up":"100",
                            "image":"https:\/\/s3-eu-west-1.amazonaws.com\/clinix\/practitioner\/avatars\/doc.jpg",
                            "status":"disabled",
                            "category":"Medical Practitioner",
                            "service_type":"NHS",
                            "insurer":"",
                            "policy_no":"",
                            "gmc_no":""
                         }
                      },
                      {
                         "id":"2",
                         "user_id":"1",
                         "child_id":"0",
                         "practitioner_id":"1",
                         "drug_name":"Metamorfin 1g Tablets",
                         "quantity":"60",
                         "course":"30 Days",
                         "instructions":"Take 2 tablets daily with evening meal",
                         "date":"2015-08-25 11:00:00",
                         "practitioner":{
                            "id":"1",
                            "title":"Dr",
                            "forename":"John",
                            "surname":"Smith",
                            "dob":"0000-00-00",
                            "suffix":null,
                            "email":"john@johnsmith.com",
                            "register_date":"2015-08-25 11:00:00",
                            "telephone":"07791193905",
                            "street":"8 Rufford Avenue",
                            "address_2":"",
                            "town":"",
                            "county":"",
                            "postcode":"DN22 7RY",
                            "country":"",
                            "type":"doctor",
                            "surgery_ids":"1,3",
                            "account_status":"active",
                            "profession":"Genius Doctor",
                            "specialty":"Brain Surgery",
                            "registered_date":"1994-08-25 11:00:00",
                            "information":"Lorem Ipsum",
                            "information_date":"2015-08-25 11:00:00",
                            "signature":"",
                            "signature_date":"0000-00-00 00:00:00",
                            "initial":"100",
                            "follow_up":"100",
                            "image":"https:\/\/s3-eu-west-1.amazonaws.com\/clinix\/practitioner\/avatars\/doc.jpg",
                            "status":"disabled",
                            "category":"Medical Practitioner",
                            "service_type":"NHS",
                            "insurer":"",
                            "policy_no":"",
                            "gmc_no":""
                         }
                      }
                   ]
                }
                * */
                MedicationsActivity.itemListMedication      = new ArrayList<Medication>();

                try {
                    JSONObject jo               = new JSONObject(response);
                    JSONArray medicationsArray  = jo.getJSONArray("medications");
                    for (int i=0; i<medicationsArray.length(); i++)
                    {
                        JSONObject medicationjo = medicationsArray.getJSONObject(i);
                        String id               = medicationjo.getString("id");
                        String user_id          = medicationjo.getString("user_id");
                        String practitioner_id  = medicationjo.getString("practitioner_id");
                        String drug_name        = medicationjo.getString("drug_name");
                        String quantity         = medicationjo.getString("quantity");
                        String course           = medicationjo.getString("course");
                        String instructions     = medicationjo.getString("instructions");
                        String date             = medicationjo.getString("date");
                        ////GET PRACTITION HERE   String practitioner             = jo.getString("practitioner");

                        _(i+"___________________________________________");
                        _("id................:"+id);
                        _("user_id...........:"+user_id);
                        _("practitioner_id...:"+practitioner_id);
                        _("drug_name.........:"+drug_name);
                        _("quantity..........:"+quantity);
                        _("course............:"+course);
                        _("instructions......:"+instructions);
                        _("date..............:"+date);

                        MedicationsActivity.itemListMedication.add(new Medication(id, user_id, practitioner_id, drug_name, quantity, course, instructions, date));
                    }

                    //////PLEASE NOTE://///
                    /////this gets called from 2 activities, so work out which to call
                    //wow you can do this on contexts!
                    if (c instanceof  MedicationsActivity)
                    {
                        _("c is a MedicationsActivity");
                        //now we fill the list in the activity
                        handler = new Handler(Looper.getMainLooper());
                        handler.post(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        ((MedicationsActivity) c).fillUpList();
                                    }
                                }
                        );
                    }
                    if (c instanceof  MedicationsDetailsActivity)
                    {
                        _("c is a MedicationsDetailsActivity");
                        //now we fill the GUI in the activity
                        handler = new Handler(Looper.getMainLooper());
                        handler.post(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        ((MedicationsDetailsActivity) c).assignMyMedicationObject_AndPopulateGui();
                                    }
                                }
                        );
                    }

                    //////////////////////////////



                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case NETWORK_STATE_GET_MY_PATIENT_SURGERY:
                _("NETWORK_STATE_GET_MY_PATIENT_SURGERY");
                try
                {
                    JSONObject jo       = new JSONObject(response);
                    JSONObject joe      = jo.getJSONObject("surgery");
                    //get the header stuff
                    String id           = joe.getString("id");
                    String name           = joe.getString("name");
                    String type           = joe.getString("type");
                    String street           = joe.getString("street");
                    String town           = joe.getString("town");
                    String county           = joe.getString("county");
                    String postcode           = joe.getString("postcode");
                    String lat           = joe.getString("lat");
                    String lng           = joe.getString("lng");
                    String email           = joe.getString("email");
                    String telephone           = joe.getString("telephone");

                    _("id............:"+id);
                    _("name.......:"+name);
                    _("type.......:"+type);
                    _("street.......:"+street);
                    _("town.......:"+town);
                    _("county.......:"+county);
                    _("postcode.......:"+postcode);
                    _("lat.......:"+lat);
                    _("lng.......:"+lng);
                    _("email.......:"+email);
                    _("telephone.......:"+telephone);

                    String address_2 = "";//do we need this?";
                    String country = "UK";
                    String website = "unknown www";

                    //get the active practioners//
                    JSONArray active_practitioners           = joe.getJSONArray("active_practitioners");
                    for (int x=0; x>active_practitioners.length();x++)
                    {
                        _("Active practitioner:"+active_practitioners.getString(x));
                    }

                    SurgeryDetailsActivity.mySurgery = new Surgery(
                            id ,
                            name                 ,
                            type                 ,
                            street               ,
                            address_2            ,
                            town                 ,
                            postcode             ,
                            county               ,
                            country              ,
                            lat                  ,
                            lng                  ,
                            email                ,
                            telephone            ,
                            website              );
                  /*  PharmacyDetailsActivity.myPharmacy = new Pharmacy
                            (
                                    id ,
                                    user_id ,
                                    name ,
                                    street ,
                                    address_2 ,
                                    town ,
                                    county ,
                                    postcode ,
                                    country ,
                                    email ,
                                    website ,
                                    telephone ,
                                    fax
                            );
*/
                    //now we fill the GUI in the activity
                    handler = new Handler(Looper.getMainLooper());
                    handler.post(
                            new Runnable() {
                                @Override
                                public void run() {
                                    ((SurgeryDetailsActivity) c).populateGUI();
                                }
                            }
                    );
                }
                catch(Exception e)
                {
                    _("WARNING!!!!!!!!!!!!! Problem parsing the pharamacy info !!"+e.getMessage());
                }
            break;

         //   case NETWORK_STATE_RETRIEVE_PASSWORD:
        //        _("NETWORK_STATE_RETRIEVE_PASSWORD");
           ////     Tools.toast("Your password will be sent to your email.", c);
          //      break;
            case NETWORK_STATE_GET_MY_PATIENT_GP:
                _("NETWORK_STATE_GET_MY_PATIENT_GP - I THINK THIS IS DEFUNCT??????????");
                /*
                * {
                   "practitioner":{
                      "id":"1",
                      "title":"Dr",
                      "forename":"John",
                      "surname":"Smith",
                      "dob":"0000-00-00",
                      "suffix":null,
                      "email":"john@johnsmith.com",
                      "register_date":"2015-08-25 11:00:00",
                      "telephone":"07791193905",
                      "street":"8 Rufford Avenue",
                      "address_2":"",
                      "town":"",
                      "county":"",
                      "postcode":"DN22 7RY",
                      "country":"",
                      "type":"doctor",
                      "surgery_ids":"1,3",
                      "account_status":"active",
                      "profession":"Genius Doctor",
                      "specialty":"Brain Surgery",
                      "registered_date":"1994-08-25 11:00:00",
                      "information":"Lorem Ipsum",
                      "information_date":"2015-08-25 11:00:00",
                      "signature":"",
                      "signature_date":"0000-00-00 00:00:00",
                      "initial":"100",
                      "follow_up":"100",
                      "image":"https:\/\/s3-eu-west-1.amazonaws.com\/clinix\/practitioner\/avatars\/doc.jpg",
                      "status":"disabled",
                      "category":"Medical Practitioner",
                      "service_type":"NHS",
                      "insurer":"",
                      "policy_no":"",
                      "gmc_no":""
                   }
                }
                * */
              /*  try
                {
                    JSONObject jo   = new JSONObject(response);
                    SurgeryDetailsActivity.myGP = parsePractitionerObject(jo,"practitioner");   // now we need to get the practitioner object!

                    //update the gui
                    handler = new Handler(Looper.getMainLooper());
                    handler.post(
                            new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    ((SurgeryDetailsActivity)c).populateGUI();
                                }
                            }
                    );
                }
                catch(Exception e)
                {
                    _("Error pasing JSON.. "+e.getMessage());
                }*/
            break;
            case NETWORK_STATE_GET_MY_PATIENT_PHARMACY://CPONSIDER CHANGIGN THIS IT ONLY GETS THEIR OWN SINGLE PHARMACY AS FAR AS IM AWARE
                _("NETWORK_STATE_GET_MY_PATIENT_PHARMACY");
                /*
                * {
                   "pharmacy":{
                      "id":"1",
                      "user_id":"1",
                      "name":"Riverside",
                      "street":"Riverside, Retford, Notts",
                      "address_2":"",
                      "town":"",
                      "county":"",
                      "postcode":"DN22",
                      "country":"",
                      "email":"info@riversidehealthcare.co.uk",
                      "website":"http:\/\/riversidehealthcare.co.uk",
                      "telephone":"01777 700000",
                      "fax":""
                   }
                }
                * */
                try
                {
                    JSONObject jo       = new JSONObject(response);
                    JSONObject joe      = jo.getJSONObject("pharmacy");
                    //get the header stuff
                    String id           = joe.getString("id");
                    String user_id      = joe.getString("user_id");
                    String name         = joe.getString("name");
                    String street       = joe.getString("street");
                    String address_2    = joe.getString("address_2");
                    String town         = joe.getString("town");
                    String county       = joe.getString("county");
                    String postcode     = joe.getString("postcode");
                    String country      = joe.getString("country");
                    String email        = joe.getString("email");
                    String website      = joe.getString("website");
                    String telephone    = joe.getString("telephone");
                    String fax          = joe.getString("fax");
                    _("id............:"+id);
                    _("user_id.......:"+user_id);
                    _("name..........:"+name);
                    _("street........:"+street);
                    _("address_2.....:"+address_2);
                    _("town..........:"+town);
                    _("county........:"+county);
                    _("postcode......:"+postcode);
                    _("country.......:"+country);
                    _("email.........:"+email);
                    _("website.......:"+website);
                    _("telephone.....:"+telephone);
                    _("fax...........:"+fax);

                    PharmacyDetailsActivity.myPharmacy = new Pharmacy
                            (
                                    id ,
                                    user_id ,
                                    name ,
                                    street ,
                                    address_2 ,
                                    town ,
                                    county ,
                                    postcode ,
                                    country ,
                                    email ,
                                    website ,
                                    telephone ,
                                    fax
                            );

                    //now we fill the GUI in the activity
                    handler = new Handler(Looper.getMainLooper());
                    handler.post(
                            new Runnable() {
                                @Override
                                public void run() {
                                    ((PharmacyDetailsActivity) c).populateGUI();
                                }
                            }
                    );
                }
                catch(Exception e)
                {
                    _("WARNING!!!!!!!!!!!!! Problem parsing the pharamacy info !!"+e.getMessage());
                }
                break;
            case NETWORK_STATE_GET_MY_PATIENT_PROFILE:
                _("NETWORK_STATE_GET_MY_PATIENT_PROFILE");
                /*
                {
                   "user":{
                      "id":"27",
                      "title":"",
                      "forename":"Gaz",
                      "surname":"Murfin",
                      "suffix":null,
                      "email":"wddaxddsdxdsfsfsddddmxxv@wam.com",
                      "register_date":"2016-04-12 08:04:33",
                      "dob":"1984-04-10",
                      "nhs_number":"",
                      "practitioner_id":null,
                      "surgery_id":"0",
                      "custom_practitioner_id":null,
                      "custom_practitioner":"0",
                      "custom_surgery_id":null,
                      "custom_surgery":"0",
                      "telephone":"",
                      "street":"",
                      "address_2":"",
                      "town":"",
                      "county":"",
                      "postcode":"",
                      "country":"",
                      "gender":"unknown",
                      "height_type":"feet",
                      "height":"",
                      "weight_type":"stone",
                      "weight":"",
                      "smoker":"unknown",
                      "image":"",
                      "bmi":"",
                      "blood":"",
                      "type":"nhs",
                      "status":"active",
                      "account_status":"active",
                      "login_modal":"1"
                   }
                }
                * */
                try
                {
                    JSONObject jo   = new JSONObject(response);
                    ProfileActivity.myProfile = parseUserObject(jo, "user");   // now we need to get the practitioner object!

                    //update the gui
                    if (c instanceof  ProfileActivity)
                    {
                        handler = new Handler(Looper.getMainLooper());
                        handler.post(
                                new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        ((ProfileActivity)c).loadUserProfile();
                                    }
                                }
                        );
                    }
                    if (c instanceof  MainMenuActivity)
                    {
                        handler = new Handler(Looper.getMainLooper());
                        handler.post(
                                new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        ((MainMenuActivity)c).updateProfilePic();
                                    }
                                }
                        );
                    }

                }
                catch(Exception e)
                {
                    _("Error pasing JSON.. "+e.getMessage());
                }
                break;
            case NETWORK_STATE_GET_APPOINTMENTS_AVAILABLE:
                _("NETWORK_STATE_GET_APPOINTMENTS_AVAILABLE");
                /*
                {
                    "appointments": [
                        {
                            "start": 1437652200,
                            "available": true
                        },
                        {
                            "start": 1437652800,
                            "available": true
                        },
                        {
                            "start": 1437653400,
                            "available": false
                        },
                        {
                            "start": 1437654000,
                            "available": false
                        },
                        {
                            "start": 1437654600,
                            "available": false
                        },
                        {
                            "start": 1437655200,
                            "available": false
                        },
                        {
                            "start": 1437655800,
                            "available": false
                        },
                        {
                            "start": 1437656400,
                            "available": true
                        },
                        {
                            "start": 1437657000,
                            "available": true
                        }
                    ]
                }
                * */
                try
                {
                    BookAppointmentActivity_Page2.itemListAvailableAppointments = new ArrayList<AvailableAppointment>();
                    JSONObject jo = new JSONObject(response);
                    JSONArray availableAppointmentsArray    = jo.getJSONArray("appointments");

                    for (int i=0; i<availableAppointmentsArray.length(); i++)
                    {
                        JSONObject availableApp = (JSONObject) availableAppointmentsArray.get(i);
                        String start               = availableApp.getString("start");
                        String available           = availableApp.getString("available");

                        _("_______________________________________________________");
                        String friendlyDateString = "";
                        Date d = new Date( Long.parseLong(start)* 1000L );// /1000)* 1000L);//to get it out of 1970 you need to multiply the Unix timestamp by 1000;
                        friendlyDateString = d.toString();
                        _("start..............."+start+" which is: "+friendlyDateString);
                        _("available..........."+available);

                        AvailableAppointment availableAppointmentObject  = new AvailableAppointment(start,available);
                        BookAppointmentActivity_Page2.itemListAvailableAppointments.add(availableAppointmentObject);
                    }


                    //now we fill the GUI in the activity
                    handler = new Handler(Looper.getMainLooper());
                    handler.post(
                            new Runnable() {
                                @Override
                                public void run() {
                                    ((BookAppointmentActivity_Page2) c).updateAvailableAppointmentsButtons();
                                }
                            }
                    );

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case NETWORK_STATE_BOOK_AN_APPOINTMENT:
                _("NETWORK_STATE_BOOK_AN_APPOINTMENT");
                /*
                * {
                   "id":"13",
                   "practitioner_id":"1",
                   "user_id":"1",
                   "child_id":null,
                   "start":1449130800,
                   "mode":"video",
                   "problem":"noneEntered.",
                   "image":"...",
                   "status":"booked",
                   "session_id":"",
                   "user_token":"",
                   "practitioner_token":"",
                   "practitioner":{
                      "id":"1",
                      "title":"Dr",
                      "forename":"John",
                      "surname":"Smith",
                      "dob":"0000-00-00",
                      "suffix":null,
                      "email":"john@johnsmith.com",
                      "register_date":"2015-08-25 11:00:00",
                      "telephone":"07791193905",
                      "street":"8 Rufford Avenue",
                      "address_2":"",
                      "town":"",
                      "county":"",
                      "postcode":"DN22 7RY",
                      "country":"",
                      "type":"doctor",
                      "surgery_ids":"1,3",
                      "account_status":"suspended",
                      "profession":"Genius Doctor",
                      "specialty":"Brain Surgery",
                      "registered_date":"1994-08-25 11:00:00",
                      "information":"Lorem Ipsum",
                      "information_date":"2015-08-25 11:00:00",
                      "signature":"",
                      "signature_date":"0000-00-00 00:00:00",
                      "initial":"100",
                      "follow_up":"100",
                      "image":"https:\/\/s3-eu-west-1.amazonaws.com\/clinix\/practitioner\/avatars\/doc.jpg",
                      "status":"disabled",
                      "category":"Medical Practitioner",
                      "service_type":"NHS",
                      "insurer":"",
                      "policy_no":"",
                      "gmc_no":"",
                      "temp_password":"0"
                   }
                }
                * */

                ////MedicationsActivity.itemListMedication      = new ArrayList<Medication>();
                if (response==null || response.length()==0)
                {
                    _("Warning: Can't book appointment! No JSON from server, error on server? ");
                    Tools.toast("No JSON from server - server error?",c);
                    return;
                }
                try {
                    JSONObject jo               = new JSONObject(response);

                        String id                   = jo.getString("id");
                        String practitioner_id      = jo.getString("practitioner_id");
                        String user_id              = jo.getString("user_id");
                        String child_id             = jo.getString("child_id");
                        final String start                = jo.getString("start");
                        String mode                 = jo.getString("mode");
                        final String problem              = jo.getString("problem");
                        String image                = jo.getString("image");
                        String status               = jo.getString("status");
                        String session_id           = jo.getString("session_id");
                        String user_token           = jo.getString("user_token");
                        String practitioner_token   = jo.getString("practitioner_token");
                        final Practitioner_fromNotification practitioner = parsePractitionerObject(jo, "practitioner");

                        _("id................:" + id);
                        _("practitioner_id...:" + practitioner_id);
                        _("user_id...........:" + user_id);
                        _("child_id..........:" + child_id);
                        _("start.............:" + start);
                        _("mode..............:" + mode);
                        _("problem...........:" + problem);
                        _("image.............:" + image);
                        _("status............:" + status);
                        _("session_id........:" + session_id);
                        _("user_token........:" + user_token);
                        _("practitioner_token:" + practitioner_token);


                   //     MedicationsActivity.itemListMedication.add(new Medication(id, user_id, practitioner_id, drug_name, quantity, course, instructions, date));


                    //////PLEASE NOTE://///
                    /////this gets called from 2 activities, so work out which to call
                    //wow you can do this on contexts!
                    if (c instanceof BookAppointmentActivity_Page3)
                    {
                        _("c is a BookAppointmentActivity_Page3");
                        //now we fill the list in the activity
                        handler = new Handler(Looper.getMainLooper());
                        handler.post(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        ((BookAppointmentActivity_Page3) c).populateGUIForBookedAppointment(start,practitioner,problem);
                                    }
                                }
                        );
                    }

                    //////////////////////////////



                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case NETWORK_STATE_START_CALL_TO_BEGIN_APPOINTMENT:
                _("NETWORK_STATE_START_CALL_TO_BEGIN_APPOINTMENT");
                try {
                    JSONObject jo               = new JSONObject(response);

                    String id                   = jo.getString("id");
                    String start                   = jo.getString("start");
                    String mode                   = jo.getString("mode");
                    String problem                   = jo.getString("problem");
                    String image                   = jo.getString("image");
                    String status                   = jo.getString("status");
                    String session_id                   = jo.getString("session_id");
                    String token                   = tryGetString(jo,"token");//if no call is ready this will be empty.

                    _("id................:" + id);
                    _("start.............:" + start);
                    _("mode..............:" + mode);
                    _("problem...........:" + problem);
                    _("image.............:" + image);
                    _("status............:" + status);
                    _("session_id........:" + session_id);
                    _("token.............:" + token);

                    //set up the right credentials for this call,
                    OpenTokConfig.SESSION_ID = session_id;
                    OpenTokConfig.TOKEN = token;

                    // IF there is a session id and token (which will only exist if a genuine appointment is ready)
                    // then it sends the app to the tokbox call screen and call should magically begin with practitioner.
                    // THERE SHOULD be a nicer way to test this really! Right now need to wait for a genuine appointment
                    // to happen because token will be empty is the appointment id is... !
                    // NOTE THE END POINT MUST BE THE LIVE ONE WITH httpS or no token will ever generate ever.
                    if (token.equals(""))
                    {
                        Tools.toast("Can't set up call. 'Token' was empty, no call ready to set up. Is there an actual appointment?",c);
                    }
                    else
                    {
                        Tools.toast("Call setting up...please wait",c);
                        //now we start the call!...
                        Intent intent = new Intent(c, OpenTokSamples.class);
                        c.startActivity(intent);
                        //////////////////////////////

                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case NETWORK_STATE_GET_LOCAL_SURGERIES:
                _("NETWORK_STATE_GET_LOCAL_SURGERIES");
                        /*{
                   "total":5,
                   "per_page":20,
                   "current_page":1,
                   "last_page":1,
                   "next_page_url":null,
                   "prev_page_url":null,
                   "from":1,
                   "to":5,
                   "data":[
                      {
                         "id":5,
                         "name":"Queens Medical Centre",
                         "type":"nhs",
                         "street":"245 Cheetham Hill Road",
                         "address_2":"",
                         "town":"Manchester",
                         "county":"",
                         "postcode":"M8 8UP",
                         "country":"",
                         "lat":52.944079,
                         "lng":-1.186186,
                         "email":"surgery@royand.co",
                         "telephone":"0162 277 6939",
                         "website":"queensmedicalcentre.co.uk"
                      },
                      {
                         "id":2,
                         "name":"The Limes Medical Centre",
                         "type":"nhs",
                         "street":"65 Leicester Road",
                         "address_2":"",
                         "town":"Narborough",
                         "county":"Leicestershire",
                         "postcode":"LE19 2DU",
                         "country":"",
                         "lat":52.57396785,
                         "lng":-1.19999239,
                         "email":"surgery@royand.co",
                         "telephone":"01302 000000",
                         "website":"royand.co"
                      },
                      {
                         "id":1,
                         "name":"Dr A L Jeffries-Beckley and Partners",
                         "type":"nhs",
                         "street":"The Orchard Medical Practice, Orchard Road",
                         "address_2":"",
                         "town":"Broughton Astley",
                         "county":"Leicestershire",
                         "postcode":"LE9 6RG",
                         "country":"",
                         "lat":52.53397414,
                         "lng":-1.23167848,
                         "email":"surgery@royand.co",
                         "telephone":"01302 000000",
                         "website":"royand.co"
                      },
                      {
                         "id":6,
                         "name":"Royton and Crompton Family Practice",
                         "type":"nhs",
                         "street":"Park Street",
                         "address_2":"",
                         "town":"Oldham",
                         "county":"",
                         "postcode":"OL2 6QW",
                         "country":"",
                         "lat":53.56612,
                         "lng":-2.121672,
                         "email":"surgery@royand.co",
                         "telephone":"0161 621 7635",
                         "website":"royton.co.uk"
                      },
                      {
                         "id":3,
                         "name":"Dr G E Aram & Partners",
                         "type":"nhs",
                         "street":"Central Street",
                         "address_2":"",
                         "town":"Countesthorpe",
                         "county":"Leicestershire",
                         "postcode":"LE8 5QJ",
                         "country":"",
                         "lat":52.55445551,
                         "lng":-1.13671824,
                         "email":"surgery@royand.co",
                         "telephone":"01302 000000",
                         "website":"royand.co"
                      }
                   ]
                }*/
                try
                {
                    JSONObject jo           = new JSONObject(response);
                    //get the header stuff
                    String total            = jo.getString("total");
                    String per_page         = jo.getString("per_page");
                    String current_page     = jo.getString("current_page");
                    String last_page        = jo.getString("last_page");
                    String next_page_url    = jo.getString("next_page_url");
                    String prev_page_url    = jo.getString("prev_page_url");
                    String from             = jo.getString("from");
                    String to               = jo.getString("to");
                    _("total................:"+total);
                    _("per_page.............:"+per_page);
                    _("current_page.........:"+current_page);
                    _("last_page............:"+last_page);
                    _("next_page_url........:"+next_page_url);
                    _("prev_page_url........:"+prev_page_url);
                    _("from.................:"+from);
                    _("to...................:"+to);

                    NearbyNHSSurgeriesActivity.itemListSurgeries = new ArrayList<Surgery>();

                    ///now get the data array//
                    //this is a large array containing each notification and sub objects etc!..
                    JSONArray dataArray = jo.getJSONArray("data");
                    if (dataArray.length()==0)
                    {
                        _("Warning.. dataArray length is 0");
                    }
                    else
                    {
                        _("dataArray array: (length:"+dataArray.length()+"):");
                    }
                    for (int i = 0; i < dataArray.length(); i++)
                    {
                        JSONObject datajo           = dataArray.getJSONObject(i);
                        String id              = datajo.getString("id");
                        String name              = datajo.getString("name");
                        String type              = datajo.getString("type");
                        String street              = datajo.getString("street");
                        String address_2              = datajo.getString("address_2");
                        String town              = datajo.getString("town");
                        String postcode              = datajo.getString("postcode");
                        String county =  datajo.getString("county");
                        String country              = tryGetString(datajo,"country");
                        String lat              = datajo.getString("lat");
                        String lng              = datajo.getString("lng");
                        String email              = datajo.getString("email");
                        String telephone              = datajo.getString("telephone");
                        String website              = datajo.getString("website");

                        _(i+"// SURGERY_____________________");
                        _("id..........:"+id);
                        _("name........:"+name);
                        _("type........:"+type);
                        _("street......:"+street);
                        _("address_2...:"+address_2);
                        _("town........:"+town);
                        _("postcode....:"+postcode);
                        _("county....:"+county);
                        _("country.....:"+country);
                        _("lat.........:"+lat);
                        _("lng.........:"+lng);
                        _("email.......:"+email);
                        _("telephone...:"+telephone);
                        _("website.....:" + website);

                        Surgery s = new Surgery(id,name,type,street,address_2,town,postcode,county,country,lat,lng,email,telephone,website);
                        NearbyNHSSurgeriesActivity.itemListSurgeries.add(s);

                    }
                    _("END OF NOTIFICATION PARSING!//////////////////////////////////////////////");
                    _("There are:"+NearbyNHSSurgeriesActivity.itemListSurgeries.size()+" Surgeries.");

                    //now we fill the list in the activity
                    handler = new Handler(Looper.getMainLooper());
                    handler.post(
                            new Runnable() {
                                @Override
                                public void run() {
                                    ((NearbyNHSSurgeriesActivity) c).fillUpList();
                                }
                            }
                    );


                } catch (JSONException e) {
                    e.printStackTrace();
                }


                break;
            case NETWORK_STATE_GET_GPLIST_FOR_SURGERY:
                _("NETWORK_STATE_GET_GPLIST_FOR_SURGERY");
                /*
                {
                   "practitioner":{
                      "id":"1",
                      "title":"Dr",
                      "forename":"John",
                      "surname":"Smith",
                      "dob":"0000-00-00",
                      "suffix":null,
                      "email":"john@johnsmith.com",
                      "register_date":"2015-08-25 11:00:00",
                      "telephone":"07791193905",
                      "street":"8 Rufford Avenue",
                      "address_2":"",
                      "town":"",
                      "county":"",
                      "postcode":"DN22 7RY",
                      "country":"",
                      "type":"doctor",
                      "surgery_ids":"1,3",
                      "account_status":"active",
                      "profession":"Genius Doctor",
                      "specialty":"General Practitioner",
                      "registered_date":"1994-08-25 00:00:00",
                      "information":"Dr Smith qualified from UCL in 1999 and undertook his training in general practice in the Frimley area, before joining Camberley Health Centre in 2004. He is also a GP trainer and is employed by the Deanery as a programme director for the GP training scheme in Frimley. \nHe has a clinical interest in mens health.",
                      "information_date":"2015-12-09 17:40:35",
                      "signature":"",
                      "signature_date":"0000-00-00 00:00:00",
                      "initial":"100",
                      "follow_up":"100",
                      "image":"https:\/\/s3-eu-west-1.amazonaws.com\/clinix\/practitioner\/avatars\/doc.jpg",
                      "status":"disabled",
                      "category":"Medical Practitioner",
                      "service_type":"NHS",
                      "insurer":"MPS",
                      "policy_no":"57898086",
                      "professional_body":"GMC",
                      "temp_password":"0",
                      "registration_number":"255778697",
                      "dbs_no":"56689809",
                      "dbs_date":"0000-00-00 00:00:00"
                   }
                }
                * */
                try
                {
                    JSONObject jo   = new JSONObject(response);
                    //ListOfGPsAtLocalSurgeryActivity.itemListGPs

                    ListOfGPsAtLocalSurgeryActivity.itemListGPs = new ArrayList<Practitioner_fromNotification>();
                    //is this a list or just one???
                    Practitioner_fromNotification p = parsePractitionerObject(jo,"practitioner");   // now we need to get the practitioner object!

                    _("now set first in list");
                    //looks likee just oine for now, change later if more
                    ListOfGPsAtLocalSurgeryActivity.itemListGPs.add(0,p);

                    //update the gui
                    handler = new Handler(Looper.getMainLooper());
                    handler.post(
                            new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    ((ListOfGPsAtLocalSurgeryActivity)c).fillUpList();
                                }
                            }
                    );
                }
                catch(Exception e)
                {
                    _("Error pasing JSON.. "+e.getMessage());
                    e.printStackTrace();
                }
                break;
            case NETWORK_STATE_SET_MY_PATIENT_SURGERY_AND_GP:
                _("NETWORK_STATE_SET_MY_PATIENT_SURGERY_AND_GP");

                final String message[] = new String[]{""};

                //all we need to know if if it was a success or not.
                if (response.contains("surgery_id"))
                {
                    _("New surgery and gp set fine.");
                    message[0] = "You can now book video consultations with your GP";
                }
                else
                {
                    _("there was an error setting the new surgery and gp!");
                    message[0] = "There was an error setting your new surgery and GP!";
                }
                //update the gui
                handler = new Handler(Looper.getMainLooper());
                handler.post(
                        new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                ((ListOfGPsAtLocalSurgeryActivity)c).popUpResponseFromSettingNewSurgeryAndGP(message[0]);
                            }
                        }
                );
            break;

            default:
                _("//////////////Warning unknown state!////////////////");
        }

    }
    public static boolean TRACK_WAS_JUST_REMOVED;

    public void makeLivePlayListFromResponse(String response)
    {

    }

    public void decodeInitialSync(String response)
    {
        _("decodeInitialSync,");// log interactions to card? "+Tools.LOG_WHAT_SERVER_AND_CLIENT_SAY_TO_EACH_OTHER);



    }

    private String tryGetString(JSONObject jo, String key)
    {
        try {
            return jo.getString(key);
        } catch (JSONException e) {
            _("Warning couldnt get this string, it doesnt exist in this object : "+key+" ["+e.getMessage()+"]");
        }
        return "";
    }

    private JSONObject tryGetJsonObject(JSONObject datajo, String name)
    {
        if (datajo==null)
        {
            _("WARNING//THIS SHOULD NOT HAPPEN!!!!!!.////////////////////datajo is null, when trying to get "+name);
        }
        try {
            JSONObject returnMe = datajo.getJSONObject(name);
            return returnMe;
        } catch (JSONException e) {
            _("PLEASE NOTE: could not get the object ["+name+"], it musnt be there. " + e.getMessage());
            //e.printStackTrace();
            return null;
        }
    }


    ProgressDialog mProgressDialog;
    public void showProgressBar(final Context c, final String message)
    {
        //_("MAKING PROGRESS BAR============================="+message);
        //so we dont get thread nonsense from android
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                //FOR DOWNLOADING A FILE WITH A PROGRESS BAR
               /*
               // instantiate it within the onCreate method
               mProgressDialog = new ProgressDialog(GenericDetailsFragment.myActivity);
               mProgressDialog.setMessage("A message");
               mProgressDialog.setIndeterminate(false);
               mProgressDialog.setMax(100);
               mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                */

                try
                {
                    //FOR DOWNLOADING WITHOUT PROGRESS BAR

                    //avoid ui thread nonsense
                    Handler handler = new Handler(Looper.getMainLooper());
                    handler.post(
                            new Runnable() {
                                @Override
                                public void run() {
                                    mProgressDialog = new ProgressDialog(c);
                                    mProgressDialog.setMessage(message);
                                    mProgressDialog.show();
                                }
                            }
                    );

                }
                catch(Exception e)
                {
                    _("WARNING PROBLEM SHOWING THIS DIALOG! "+e.getMessage());
                    e.printStackTrace();
                }
            }
        });
    }

    private void hideProgressBar()
    {
        //so we dont get thread nonsense from android
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                if (mProgressDialog!=null && mProgressDialog.isShowing())
                {
                    _("Hide progress bar.");
                    mProgressDialog.hide();
                }
            }
        });
    }

    // @Override
    // protected void onPostExecute() {
    //  super.onPostExecute();
    //    _("onPostExecute=====================================================================");
    //
    // }


    String forename, surname,  email, password, dob, nhs_number, surgery_id;
    // methods we must call to make sure specific info is set up for each network call as we do them,
    //simply means passing over username and password before we need them
    //REGISTER
    public void setRegisterInfo(String forename_, String surname_, String email_, String password_, String dob_, String nhs_number_, String surgery_id_ )
    {
        forename    =   forename_;
        surname     =   surname_;
        email       =   email_;
        password    =   password_;
        dob         =   dob_;
        nhs_number  =   nhs_number_;
        surgery_id  =   surgery_id_;
        needsAndroidProgressBar=true;
    }
    //LOGIN
    public void setLoginInfo(String email_,String password_)
    {
        email = email_;
        password = password_;
        needsAndroidProgressBar=true;
    }

    //FORGOT PASSWORD
    public void setEmailInfo(String emailAddress_)
    {
        email = emailAddress_;
        needsAndroidProgressBar=true;
    }

    //CHANGE PASSWORD
    String oldPassword      ;
    String newPassword      ;
    public void setChangePasswordInfo(String oldPassword_,String newPassword_)
    {
        oldPassword = oldPassword_;
        newPassword = newPassword_;
        needsAndroidProgressBar=true;
    }

    //NOTIFICATIONS
    String type, id;
    public void setNotificationInfo(String type_, String id_)
    {
        type = type_;
        id = id_;
        needsAndroidProgressBar=true;
    }

    //HISTORY
    public void setHistoryInfo(String id_)
    {
        id = id_;
        needsAndroidProgressBar=true;
    }

    //AVAILABLE APPOINTMENTS
    public String dateofAppointmentRequired;
    public String typeOfSpecialistRequired;
    public void setAvailableAppointmentInfo( String dateofAppointmentRequired_,String typeOfSpecialistRequired_)
    {
        dateofAppointmentRequired=dateofAppointmentRequired_;
        typeOfSpecialistRequired=typeOfSpecialistRequired_;
    }

    //BOOK APPOINTMENT
    String mySelectedAppointmentTime;
    String detailsOfTheProblem;
    String preferredCallType;
    //String typeOfSpecialistRequired;
    public void setBookAppointmentInfo(String mySelectedAppointmentTime_,String detailsOfTheProblem_, String preferredCallType_, String typeOfSpecialistRequired_)
    {
        mySelectedAppointmentTime=mySelectedAppointmentTime_;
        detailsOfTheProblem=detailsOfTheProblem_;
        preferredCallType=preferredCallType_;
        typeOfSpecialistRequired=typeOfSpecialistRequired_;
    }

    //SET PROFILE DETAILS           //CONSIDER REFACTORING THIS User_fromNotification THINGY
    User_fromNotification myProfile;
    public void setUserProfileInfo(User_fromNotification myProfile_)
    {
        myProfile = myProfile_;
    }

    //SET PHARMACY DETAILS
    Pharmacy myPharmacy;
    public void setUserPharmacyInfo(Pharmacy myPharmacy_)
    {
        myPharmacy = myPharmacy_;
    }

    //SET ALLERGY DETAILS (FOR ADDING ONE)
    Allergy myAllergy;
    public void setNewAllergyDetails(Allergy myAllergy_)
    {
        myAllergy = myAllergy_;
    }

    //SET MEDICATION DETAILS (FOR ADDING ONE)
    Medication myMedication;
    public void setNewMedicationDetails(Medication myMedication_)
    {
        myMedication = myMedication_;
    }

    //SET HISTORY DETAILS (FOR ADDING ONE)
    History myHistory;
    public void setNewHistoryDetails(History myHistory_)
    {
        myHistory = myHistory_;
    }

    //SET LOCATION INFO
    String myLong, myLat;
    public void setMyLocationInfo( String long_, String lat_)
    {
        myLong=long_;
        myLat = lat_;
    }

    //SET GP DETAILS
    GPDetails myGPdetails;
    public void setMyGPDetails(GPDetails myGPdetails_)
    {
        myGPdetails = myGPdetails_;
    }

    //SET SURGERY ID (for looking for a surgery, so we can then find its GPs)
    String surgeryID;
    public void setSurgery_IDtoFind(String gpID_)
    {
        surgeryID =gpID_;
    }

    String newSurgeryID, newGPid;
    public void setNewSurgeryAndGPInfo(String newSurgeryID_, String newGPid_)
    {
        newSurgeryID = newSurgeryID_;
        newGPid = newGPid_;
    }

    ////JSON METHODS OF CONVENIENCE since we need to get the same thing over and over////

    public User_fromNotification parseUserObject(JSONObject datajo, String objectName) {
        _("parseUserObject");

        boolean showOutput = true;
        try
        {
            JSONObject user                 = datajo.getJSONObject(objectName);

            String id                       = user.getString("id");
            String title                    = user.getString("title");
            String forename                 = user.getString("forename");
            String surname                  = user.getString("surname");
            String suffix                   = user.getString("suffix");
            String email                    = user.getString("email");
            String register_date            = user.getString("register_date");
            String dob                      = user.getString("dob");
            String nhs_number               = user.getString("nhs_number");
            String practitioner_id          = user.getString("practitioner_id");
            String surgery_id               = user.getString("surgery_id");
            String custom_practitioner_id   = user.getString("custom_practitioner_id");
            String custom_practitioner      = user.getString("custom_practitioner");
            String custom_surgery_id        = user.getString("custom_surgery_id");
            String custom_surgery           = user.getString("custom_surgery");
            String telephone                = user.getString("telephone");
            String street                   = user.getString("street");
            String address_2                = user.getString("address_2");
            String town                     = user.getString("town");
            String county                   = user.getString("county");
            String postcode                 = user.getString("postcode");
            String country                  = user.getString("country");
            String gender                   = user.getString("gender");
            String height_type              = user.getString("height_type");
            String height                   = user.getString("height");
            String weight_type              = user.getString("weight_type");
            String weight                   = user.getString("weight");
            String smoker                   = user.getString("smoker");
            String image                    = user.getString("image");
            String bmi                      = user.getString("bmi");
            String blood                    = user.getString("blood");
            String type                     = user.getString("type");
            String status                   = user.getString("status");

            //0.4.4 stick this in the token variable, its not always there so no worries if its not
            String token                    = tryGetString(datajo,"token");
            if (token.length()>0)
            {
                Networking.X_CLINIX_AUTH_TOKEN_ANDROID = token;
                _("X_CLINIX_AUTH_TOKEN_ANDROID was assigned successfully.");
            }
            else
            {
                _("no token to assign, but thats no problem");
            }


            if (showOutput)
            {
                _("     id.....................:"+id);
                _("     title..................:"+title);
                _("     forename...............:"+forename);
                _("     surname................:"+surname);
                _("     suffix.................:"+suffix);
                _("     email..................:"+email);
                _("     register_date..........:"+register_date);
                _("     dob....................:"+dob);
                _("     nhs_number.............:"+nhs_number);
                _("     practitioner_id........:"+practitioner_id);
                _("     surgery_id.............:"+surgery_id);
                _("     custom_practitioner_id.:"+custom_practitioner_id);
                _("     custom_practitioner....:"+custom_practitioner);
                _("     custom_surgery_id......:"+custom_surgery_id);
                _("     custom_surgery.........:"+custom_surgery);
                _("     telephone..............:"+telephone);
                _("     street.................:"+street);
                _("     address_2..............:"+address_2);
                _("     town...................:"+town);
                _("     county.................:"+county);
                _("     postcode...............:"+postcode);
                _("     country................:"+country);
                _("     gender.................:"+gender);
                _("     height_type............:"+height_type);
                _("     height.................:"+height);
                _("     weight_type............:"+weight_type);
                _("     weight.................:"+weight);
                _("     smoker.................:"+smoker);
                _("     image..................:"+image);
                _("     bmi....................:"+bmi);
                _("     blood..................:"+blood);
                _("     type...................:"+type);
                _("     status.................:"+status);
                _("     token..................:"+token);
            }

            //make an object from all of this, ive split it up so its clearer if anything needs changing.
            User_fromNotification userFn = new User_fromNotification(
                    id                       ,
                    title                    ,
                    forename                 ,
                    surname                  ,
                    suffix                   ,
                    email                    ,
                    register_date            ,
                    dob                      ,
                    nhs_number               ,
                    practitioner_id          ,
                    surgery_id               ,
                    custom_practitioner_id   ,
                    custom_practitioner      ,
                    custom_surgery_id        ,
                    custom_surgery           ,
                    telephone                ,
                    street                   ,
                    address_2                ,
                    town                     ,
                    county                   ,
                    postcode                 ,
                    country                  ,
                    gender                   ,
                    height_type              ,
                    height                   ,
                    weight_type              ,
                    weight                   ,
                    smoker                   ,
                    image                    ,
                    bmi                      ,
                    blood                    ,
                    type                     ,
                    status
            );
            _("OK.");
            return userFn;
        } catch (Exception e)
        {
            _("WARNING!!!!!!!!");
            _("EXCEPTION PARSING USER OBJECT IN parseUserObject!!! "+e.getMessage());
        }
        return null;
    }

    public Admin_fromNotification parseAdminObject(JSONObject datajo) {
        _("parseAdminbject");

        boolean showOutput = false;
        try
        {
            JSONObject user                 = datajo.getJSONObject("user");

            String id                       = user.getString("id");
            String title                    = user.getString("title");
            String forename                 = user.getString("forename");
            String surname                  = user.getString("surname");
            String suffix                   = user.getString("suffix");
            String email                    = user.getString("email");
            String register_date            = user.getString("register_date");
            String surgery_ids              = tryToGetString(user, "surgery_ids");// this can be null which calls catch, so we use a method instead of: user.getString("surgery_ids");
            String dob                      = user.getString("dob");
            String telephone                = user.getString("telephone");
            String street                   = user.getString("street");
            String address_2                = user.getString("address_2");
            String town                     = user.getString("town");
            String county                   = user.getString("county");
            String postcode                 = user.getString("postcode");
            String country                  = user.getString("country");
            String gender                   = user.getString("gender");
            String image                    = user.getString("image");
            String type                     = user.getString("type");

            if (showOutput)
            {
                _("     id.....................:"+id);
                _("     title..................:"+title);
                _("     forename...............:"+forename);
                _("     surname................:"+surname);
                _("     suffix.................:"+suffix);
                _("     email..................:"+email);
                _("     register_date..........:"+register_date);
                _("     surgery_ids............:"+surgery_ids);
                _("     dob....................:"+dob);
                _("     telephone..............:"+telephone);
                _("     street.................:"+street);
                _("     address_2..............:"+address_2);
                _("     town...................:"+town);
                _("     county.................:"+county);
                _("     postcode...............:"+postcode);
                _("     country................:"+country);
                _("     gender.................:"+gender);
                _("     image..................:"+image);
                _("     type...................:"+type);
            }


            //make an object from all of this, ive split it up so its clearer if anything needs changing.
            Admin_fromNotification adminFn = new Admin_fromNotification(
                    id,
                    title,
                    forename,
                    surname,
                    suffix,
                    email,
                    register_date,
                    surgery_ids,
                    dob,
                    telephone,
                    street,
                    address_2,
                    town,
                    county,
                    postcode,
                    country,
                    gender,
                    image,
                    type
            );
            _("OK");
            return adminFn;
        } catch (Exception e)
        {
            _("EXCEPTION PARSING USER OBJECT IN parseAdminbject!!! "+e.getMessage());
        }
        return null;
    }


    //refactor this if they end up the same!? ie Practitioner_fromNotification
    public Practitioner_fromNotification parsePractitionerObject(JSONObject datajo, String objectName)
    {
        _("parsePractitionerObject");
        boolean showOutput = true;
        try
        {
            JSONObject user                 = datajo.getJSONObject(objectName);

            String id                       = tryToGetString(user, "id");//user.getString("id");
            String title                    = tryToGetString(user, "title");//user.getString("title");
            String forename                 = tryToGetString(user, "forename");//user.getString("forename");
            String surname                  = tryToGetString(user, "surname");//user.getString("surname");
            String dob                      = tryToGetString(user, "dob");//user.getString("dob");
            String suffix                   = tryToGetString(user, "suffix");//user.getString("suffix");
            String email                    = tryToGetString(user, "email");//user.getString("email");
            String register_date            = tryToGetString(user, "register_date");//user.getString("register_date");
            String telephone                = tryToGetString(user, "telephone");//user.getString("telephone");
            String street                   = tryToGetString(user, "street");//user.getString("street");
            String address_2                = tryToGetString(user, "address_2");//user.getString("address_2");
            String town                     = tryToGetString(user, "town");//user.getString("town");
            String county                   = tryToGetString(user, "county");//user.getString("county");
            String postcode                 = tryToGetString(user, "postcode");//user.getString("postcode");
            String country                  = tryToGetString(user, "country");//user.getString("country");
            String type                     = tryToGetString(user, "type");//user.getString("type");
            String surgery_ids              = tryToGetString(user, "surgery_ids");      // this can be null which calls catch, so we use a method instead of: user.getString("surgery_ids");
            String account_status           = tryToGetString(user, "account_status");   // this can be null which calls catch, so we use a method instead of: user.getString("account_status");
            String profession               = tryToGetString(user, "profession");       // this can be null which calls catch, so we use a method instead of: user.getString("profession");
            String specialty                = tryToGetString(user, "specialty");        // this can be null which calls catch, so we use a method instead of: user.getString("specialty");
            String registered_date          = tryToGetString(user, "registered_date");  // this can be null which calls catch, so we use a method instead of: user.getString("registered_date");
            String information              = tryToGetString(user, "information");//user.getString("information");
            String information_date         = tryToGetString(user, "information_date");//user.getString("information_date");
            String signature                = tryToGetString(user, "signature");//user.getString("signature");
            String signature_date           = tryToGetString(user, "signature_date");//user.getString("signature_date");
            String initial                  = tryToGetString(user, "initial");//user.getString("initial");
            String follow_up                = tryToGetString(user, "follow_up");//user.getString("follow_up");
            String image                    = tryToGetString(user, "image");//user.getString("image");
            String status                   = tryToGetString(user, "status");//user.getString("status");
            String category                 = tryToGetString(user, "category");//user.getString("category");
            String service_type             = tryToGetString(user, "service_type");//user.getString("service_type");
            String insurer                  = tryToGetString(user, "insurer");//user.getString("insurer");
            String policy_no                = tryToGetString(user, "policy_no");//user.getString("policy_no");
            String gmc_no                   = tryToGetString(user, "gmc_no");//user.getString("gmc_no");

            if (showOutput)
            {
                _("     id.....................:"+id);
                _("     title..................:"+title);
                _("     forename...............:"+forename);
                _("     surname................:"+surname);
                _("     dob....................:"+dob);
                _("     suffix.................:"+suffix);
                _("     email..................:"+email);
                _("     register_date..........:"+register_date);
                _("     telephone..............:"+telephone);
                _("     street.................:"+street);
                _("     address_2..............:"+address_2);
                _("     town...................:"+town);
                _("     county.................:"+county);
                _("     postcode...............:"+postcode);
                _("     country................:"+country);
                _("     type...................:"+type);
                _("     surgery_ids............:"+surgery_ids);
                _("     account_status.........:"+account_status);
                _("     profession.............:"+profession);
                _("     specialty..............:"+specialty);
                _("     registered_date........:"+registered_date);
                _("     information............:"+information);
                _("     information_date.......:"+information_date);
                _("     signature..............:"+signature);
                _("     signature_date.........:"+signature_date);
                _("     initial................:"+initial);
                _("     follow_up..............:"+follow_up);
                _("     image..................:"+image);
                _("     status.................:"+status);
                _("     category...............:"+category);
                _("     service_type...........:"+service_type);
                _("     insurer................:"+insurer);
                _("     policy_no..............:"+policy_no);
                _("     gmc_no.................:"+gmc_no);
            }


            //make an object from all of this, ive split it up so its clearer if anything needs changing.
            Practitioner_fromNotification adminFn = new Practitioner_fromNotification(
                    id,
                    title,
                    forename,
                    surname,
                    dob,
                    suffix,
                    email,
                    register_date,
                    telephone,
                    street,
                    address_2,
                    town,
                    county,
                    postcode,
                    country,
                    type,
                    surgery_ids,
                    account_status,
                    profession,
                    specialty,
                    registered_date,
                    information,
                    information_date,
                    signature,
                    signature_date,
                    initial,
                    follow_up,
                    image,
                    status,
                    category,
                    service_type,
                    insurer,
                    policy_no,
                    gmc_no
            );
            return adminFn;
        } catch (Exception e)
        {
            _("EXCEPTION PARSING USER OBJECT IN parsePractitionerObject!!! "+e.getMessage());
        }
        _("Warning this is returning null!!!!!");
        return null;
    }

    //unfortunately if one val is null the catch is called so we need to use this menthod instead
    public String tryToGetString(JSONObject jo,String nameToGet)
    {
        try
        {
            String value = jo.getString(nameToGet);
            if (value==null)
            {
                _("warning value was null, making it empty string");
                value="";
            }
            return value;
        }
        catch(Exception e)
        {
            _("Warning tryToGetString() could not get "+nameToGet);
        }
        return null;

    }

    //a wrapper so we dont have to type this every time
    private void _(String s)
    {
        if (!Tools.DEBUG)
        {
            return;
        }
        if (s.length()>3000) {
            _("BIG STRING COMING UP");
            Tools.logLargeString(s);

        }
        else
        {
            Log.d("MyApp", "Networking" + "#######" + s);
        }

    }

}
