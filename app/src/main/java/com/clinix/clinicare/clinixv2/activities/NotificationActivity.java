/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.adapters.NotificationAdapter;
import com.clinix.clinicare.clinixv2.common.Networking;
import com.clinix.clinicare.clinixv2.common.Tools;
import com.clinix.clinicare.clinixv2.dataobjs.Notification_;

import java.util.ArrayList;

public class NotificationActivity extends FragmentActivity {

    Context c;
    public static ArrayList<Notification_> notificationList;
    private ListView listView;
    NotificationAdapter adapter;
    Activity a;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.notifications);
        c = this;
        a = this;

        doNetworkingToGetNotifications();
        //fillUpList();

        Tools.toast("Tip: You can long press items to delete them",c);
    }

    public void fillUpList()
    {
        listView = (ListView)findViewById(R.id.mylist);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,int pos, long id) {
                _("long clicked pos: " + pos);
                final CharSequence[] items = {"Yes","No"};
                AlertDialog.Builder builder = new AlertDialog.Builder(c);
                builder.setTitle("Delete notification?");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        _("onClick " + item);
                        if (item==0)//YES
                        {
                            _("User wants to delete.");
                            Notification_ n = notificationList.get(item);
                            //remove
                            notificationList.remove(item);
                            listView.invalidateViews();
                            new AlertDialog.Builder(c)
                                    .setTitle("Deleted")
                                    .setMessage("Notification was deleted.")
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            _("CLICKED OK");
                                        }
                                    })
                                    .show();
                        }
                        else
                        {
                            _("user cancelled.");
                        }
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
                return true;
            }
            });

        _("fillUpList----");
      /*  notificationList = new ArrayList<Notification_>();

        Notification_ n = new Notification_();
        n.subject="A new prescription is available";
        n.message="";
        n.dateArrived = System.currentTimeMillis()-(Notification_.ONE_MINUTE*15);
        notificationList.add(n);

        n = new Notification_();
        n.subject="Dr Smith has updated then otes on your account";
        n.message="";
        n.beenRead=true;
        n.dateArrived = System.currentTimeMillis()-(Notification_.ONE_HOUR);
        notificationList.add(n);

        n = new Notification_();
        n.subject="An archive of your appointment with Dr Smith is ready for viewing";
        n.message="";
        n.dateArrived = System.currentTimeMillis()-(Notification_.ONE_HOUR*2);
        notificationList.add(n);

        n = new Notification_();
        n.subject="Your appointment with Dr Smith will begin in 15 minutes";
        n.message="";
        n.beenRead=true;
        n.dateArrived = System.currentTimeMillis()-(Notification_.ONE_HOUR*2);
        notificationList.add(n);

        n = new Notification_();
        n.subject="Your appointment with Dr Smith is 1 day away";
        n.message="";
        n.beenRead=true;
        n.dateArrived = System.currentTimeMillis()-(Notification_.ONE_DAY);
        notificationList.add(n);*/
/*
        notificationList.add(p);

        p = new Pharmacy();
        p.name="Boots the Chemist";
        p.address="23 Church Lane, Another Town, XYZ, 321";
        notificationList.add(p);

        p = new Pharmacy();
        p.name="Family Pharmacy";
        p.address="Example Street, Fictiontopia, SW1. 3PE";
        notificationList.add(p);*/

        //this will get a list of songs all over the device, but when they select then it will get only ones at that uri
        ///////////   getSongList();

        //sort them by distance
      /*  Collections.sort(notificationList, new Comparator<Notification_>() {
            public int compare(Notification_ a, Notification_ b) {
                return a.subject.compareTo(b.subject);//    return a.getRealpath().compareTo(b.getRealpath()); // to order by filename!
            }
        });
*/
        //make adapter and set it, this updates the view
        adapter = new NotificationAdapter(this, notificationList);
        listView.setAdapter(adapter);

    }

    private void doNetworkingToGetNotifications()
    {
        _("doNetworkingToGetNotifications=========================================");
        if (!Tools.isNetworkAvailable(this) )
        {
            Tools.toast("This app needs an active Internet connection to work.",c);
            return;
        }

        //Do networking!
        String messageForProgressBar = "Getting notifications";
        String url = Networking.URL_GET_NOTIFICATIONS;
        int nState = Networking.NETWORK_STATE_GET_NOTIFICATIONS;
        Networking n = new Networking(messageForProgressBar, c, true);

        Tools t = new Tools(c);
        t.setupPrefs(c);
        String type = "patient"; // this can be admin / practitioner / patient
        String id = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, "id",null,c);
        n.setNotificationInfo(type,id);// we call this specific method for this state so right vars are set up.
        n.execute(url, nState);
    }

    //the checking of these files to make sure there is no duplicates adds a large delay right now, so we use a dialog to indicate work being done
    ProgressDialog mProgressDialog;

    public void itemPicked(View v)
    {
        _("itemPicked");
        //nothing needs to happen in here as far as im aware
        /*
        if (v ==null)
        {
            _("V is null must be being played from main button.");

        }
        else
        {
            int pos = Integer.parseInt(v.getTag().toString());
            _("List clicked."+pos);
            Song s = notificationList.get(pos);
        }*/
    }

    public void itemPickedLongClick(View v)
    {
        _("itemPickedLongClick");
    }

    @Override
    public void onResume()
    {
        super.onResume();
        _("///////////////onResume/////////////");
        //so keyboard doesnt come up automatically here, it will wait until they tap the search te
        //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

    }

    private Runnable returnRes = new Runnable() {

        public void run() {
            _("RUN()");
            if (notificationList ==null)
            {
                _("listItems was null!!!");
            }
            else
            {
                _("listItems size: "+ notificationList.size());
            }

            if (notificationList == null ){/////////// || listItems.size() == 1) { //1 Since we have an empty row at bottom
                _("notificationList is null JUMP OUT! This shouldnt happen,");
                Tools.toast("notificationList is null!", c);
                _("Dismiss dialog");
                //finish();
                return;
            }
        }
    };
/*
    private Notification_ getNotificationByName(String name)
    {
        int nullSongs=0;
        for (int i=0; i< notificationList.size(); i++)
        {
            Notification_ s = notificationList.get(i);

            //error checks
            if (s==null)
            {
                //_("WARNING S IS NULL!!!!!");
                nullSongs++;
            }
            else
            {
                if (s.subject.equals(name))
                {
                    return s;
                }
            }
        }
        _("Warning This should not happen - getNotificationByName has not found the song you wanted: "+name+" WE SEARCHED: "+ notificationList.size()+" items ("+nullSongs+" were null).");
        return null;
    }*/

    private static ArrayList<String> arr_sort= new ArrayList<String>();
    static
    {
        Log.d("MyApp", "PharmacyActivity####### SORTING WITH Arrays.Sort");
        ///////////////Arrays.sort(names);
    }
    /////////////////////////////////////////////////////////////////////////////////////////


    //a wrapper so we dont have to type this every time
    private void _(String s) {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", "NotificationActivity" + "#######" + s);
    }
}
