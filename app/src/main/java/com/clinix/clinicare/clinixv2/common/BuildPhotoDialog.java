package com.clinix.clinicare.clinixv2.common;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.clinix.clinicare.clinixv2.activities.AllergyDetailsActivity;
import com.clinix.clinicare.clinixv2.activities.BookAppointment_NotesActivity;
import com.clinix.clinicare.clinixv2.activities.ProfileActivity;
import com.clinix.clinicare.clinixv2.common.cameratools.AlbumStorageDirFactory;
import com.clinix.clinicare.clinixv2.common.cameratools.BaseAlbumDirFactory;
import com.clinix.clinicare.clinixv2.common.cameratools.FroyoAlbumDirFactory;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by USER on 12/23/2015.
 */
public class BuildPhotoDialog {//extends Activity {

    //this class makes it a reusable affair selecting a photo

    ImageView ibMyPic;
    Activity a;
    Context c;
    Tools t;
    public BuildPhotoDialog(ImageView ibMyPic_, Activity a_, Context c_, Tools t_, int mode_)
    {
        _("BuildPhotoDialog made");
        ibMyPic=ibMyPic_;
        a=a_;
        c=c_;
        t=t_;
        mode=mode_;
        buildPhotoDialog( c, a);
    }


    public void dealWithResultFromActivity(int requestCode, int resultCode, Intent data)
    {
        _("////////dealWithResultFromActivity////////////");
        if (BuildPhotoDialog.USE_EXTREMELY_LOW_RAM)
        {
            Tools.reportMem("onActivityResult");
        }
        switch (requestCode) {
            case BuildPhotoDialog.ACTION_PHOTO_FROM_CAMERA:
            {
                _("ACTION_PHOTO_FROM_CAMERA");
                if (resultCode == Activity.RESULT_OK) {
                    _("RESULT OK.");
                    handleBigCameraPhoto();
                }
                else
                {
                    _("RESULT NOT OK!");
                }
                break;
            }
            case BuildPhotoDialog.ACTION_PHOTO_FROM_GALLERY:
            {
                _("ACTION_PHOTO_FROM_GALLERY");
                if (resultCode == Activity.RESULT_OK) {

                    _("PHOTO WAS TAKEN FROM GALLERY");
                    Uri photoUri = data.getData();
                    if (photoUri != null) {
                        try
                        {
                            //we can hit out of memory here so lets try lower the ram usage a bit
                            String photoPath = Tools.getFileNameByUri(c, photoUri);
                            //huge optimisation in terms of memory and file size, to avoid any issues on older phones
                            photoPath = makeSmallerImage(photoPath);
                            photoUri = Uri.fromFile(new File(photoPath));

                            bitmap = MediaStore.Images.Media.getBitmap(a.getContentResolver(), photoUri);
                            _("Got image from gallery");
                            if (BuildPhotoDialog.USE_EXTREMELY_LOW_RAM)
                            {
                                Tools.reportMem("IMAGE GRABBED FROM GALLERY");
                            }

                            mCurrentPhotoPath=null;
                            //write it
                            File f = setUpPhotoFile();
                            mCurrentPhotoPath = f.getAbsolutePath();

                            try {
                                _("Writing to " + mCurrentPhotoPath);
                                FileOutputStream out = new FileOutputStream(mCurrentPhotoPath);
                                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                                out.flush();
                                out.close();
                                _("written fine!");
                            } catch (Exception e) {
                                _("problem writing image "+e.getMessage());
                                e.printStackTrace();
                            }
                            ///////////
                            saveUserPicture(mCurrentPhotoPath);
                            t.loadPictureAndSetImage(ibMyPic,mCurrentPhotoPath);
                            //////////////
                            mCurrentPhotoPath = null;

                            //set it now//
                            //ibMyPic.setImageBitmap( bitmap );
                            //////////////
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                else
                {
                    _("RESULT NOT OK!");
                }
                break;
            }
            default:
                _("UNKNOWN STATE HERE "+requestCode);
        }
    }
   /*@Override
   protected void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);
       _("BuildPhotoDialog Born!////////////////////////////////");
       c = this;
       a = this;
       setContentView(R.layout.backgroundforphotodialog);
   }
*/
    /////////////////////////////////////////////////////////
    ////SELECT PHOTO CODE////////////////////////////////////
    /////////////////////////////////////////////////////////
    //this stuff has been used in various projects over the years, so it is tuned and has some funny comments etc, but its all good :)

    AlbumStorageDirFactory mAlbumStorageDirFactory = null;
    AlertDialog dialog;
    public Bitmap bitmap;

    public static final int ACTION_PHOTO_FROM_CAMERA   = 1;
    public static final int ACTION_PHOTO_FROM_GALLERY  = 2;

    //for the old devices that are killing me right now
    public static boolean USE_EXTREMELY_LOW_RAM=true;
    public String mCurrentPhotoPath;

    public void buildPhotoDialog(Context c, final Activity a)
    {
        _("buildPhotoDialog()");
        final String[] items = new String[] { "Take from camera","Select from gallery" };
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(c, android.R.layout.select_dialog_item, items);
        AlertDialog.Builder builder = new AlertDialog.Builder(c);
        builder.setTitle("Select Image");
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                _("//////onClick " + item);
                if (item == 0) {
                    _("-FROM CAMERA");
                    dispatchTakePictureIntent(ACTION_PHOTO_FROM_CAMERA);
                } else {
                    _("-FROM FILE");
                    // pick from file
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    a.startActivityForResult(Intent.createChooser(intent, "Choose a Photo"), ACTION_PHOTO_FROM_GALLERY);
                }
            }
        });

        dialog = builder.create();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
            mAlbumStorageDirFactory = new FroyoAlbumDirFactory();
        } else {
            mAlbumStorageDirFactory = new BaseAlbumDirFactory();
        }
        dialog.show();
    }

    public File setUpPhotoFile() throws IOException {
        _("setUpPhotoFile");
        File f = createImageFile();
        mCurrentPhotoPath = f.getAbsolutePath();
        _("mCurrentPhotoPath becomes " + mCurrentPhotoPath);
        return f;
    }
    private void dispatchTakePictureIntent(int actionCode) {
        _("dispatchTakePictureIntent");
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        switch(actionCode) {
            case ACTION_PHOTO_FROM_CAMERA:
                _("ACTION_PHOTO_FROM_CAMERA");
                File f = null;
                try {
                    f = setUpPhotoFile();
                    mCurrentPhotoPath = f.getAbsolutePath();
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                } catch (IOException e) {
                    e.printStackTrace();
                    f = null;
                    mCurrentPhotoPath = null;
                }
                break;
            default:
                Tools._S("uknown action code "+actionCode);
                break;
        }
        a.startActivityForResult(takePictureIntent, actionCode);
    }

    private static final String JPEG_FILE_PREFIX = "Clinix_";
    private static final String JPEG_FILE_SUFFIX = ".jpg";

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = JPEG_FILE_PREFIX + timeStamp + "_";
        File albumF = getAlbumDir();
        File imageF = File.createTempFile(imageFileName, JPEG_FILE_SUFFIX, albumF);
        return imageF;
    }

    private String getAlbumName()
    {
        return ("clinix");
    }

    private File getAlbumDir() {
        File storageDir = null;
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            storageDir = mAlbumStorageDirFactory.getAlbumStorageDir(getAlbumName());
            _("storageDir: "+storageDir);
            if (storageDir != null) {
                if (! storageDir.mkdirs()) {
                    if (! storageDir.exists()){
                        _("failed to create directory");
                        return null;
                    }
                }
            }
        } else {
            _("External storage is not mounted READ/WRITE.");
        }
        return storageDir;
    }


    public void handleBigCameraPhoto() {
        _("handleBigCameraPhoto");
        if (mCurrentPhotoPath != null) {
            setPic();
            mCurrentPhotoPath = null;
        }
        else
        {
            _("Warning "+mCurrentPhotoPath+" was null!");
        }

    }

    private void setPic() {

        _("setPic");
        if (USE_EXTREMELY_LOW_RAM)
        {
            Tools.reportMem("setpic START - before");
        }
		/* There isn't enough memory to open up more than a couple camera photos */
		/* So pre-scale the target bitmap into which the file is decoded */

		/* Get the size of the ImageView */
        int targetW =  ibMyPic.getWidth();
        int targetH =  ibMyPic.getHeight();

        _("scaling down the pic @ "+mCurrentPhotoPath);
		/* Get the size of the image */
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

		/* Figure out which way needs to be reduced less */
        int scaleFactor = 1;
        if ((targetW > 0) || (targetH > 0)) {
            scaleFactor = Math.min(photoW/targetW, photoH/targetH);
        }

		/* Set bitmap options to scale the image decode target */
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = 4; //4 TO LOWER MEMORY scaleFactor;
        bmOptions.inPurgeable = true;

		/* Decode the JPEG file into a Bitmap */
        //RFR//_("decoding pic to set it, from  "+mCurrentPhotoPath);
        //0.6.1 Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);

        //0.6.0 it seems that some android cams allow portrait phototaking fine, but ones that dont force landscape
        //and thus the img is rotated wrongly, we need to detect this and rotate to fix if needed
        //0.6.1 trying new solution to this camera nonsense!!!
        bitmap = rotateIfNeeded(mCurrentPhotoPath);

        //Save this path to memory for later
        saveUserPicture(mCurrentPhotoPath);

        if (bitmap==null)
        {
            _("warning bitmap still null.");
        }

        if (USE_EXTREMELY_LOW_RAM)
        {
            Tools.reportMem("set pic END");
        }
		/* Associate the Bitmap to the ImageView */
        _("Now setting the pic in the imageview.");
        ibMyPic.setImageBitmap(bitmap);
    }

    private Bitmap rotateIfNeeded(String imagePath )
    {
        try {
            _("rotateIfNeeded ---> "+imagePath);
            File f = new File(imagePath);
            ExifInterface exif = new ExifInterface(f.getPath());
            int orientation = exif.getAttributeInt(    ExifInterface.TAG_ORIENTATION,  ExifInterface.ORIENTATION_NORMAL);

            int angle = 0;
            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                angle = 90;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                angle = 180;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                angle = 270;
            }

            if (Tools.DEBUG)
            {
                Tools.toast("angle: "+angle, c);
            }

            Matrix mat = new Matrix();
            mat.postRotate(angle);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 4;////2;  4 uses less memory [ for really low mem devices like nexus one!]

            _("decode bitmap");
            Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(f), null, options);
            _("bitmap decided");
            if (bmp==null)
            {
                _("warning bmp is still null");
            }
            if (angle==0)
            {
                //RFR//_("NO NEED TO ROTATE");
                return bmp;//THIS IS RETURNING THE GLOBAL BITMAP so we dont waste memory making another instance etc
            }

            bitmap = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), mat, true);
            ByteArrayOutputStream outstudentstreamOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outstudentstreamOutputStream);
            //imageView.setImageBitmap(bitmap);
            return bitmap;

        } catch (IOException e) {
            //RFR//_("-- Error in setting image "+e.getMessage());
            e.printStackTrace();
        } catch (OutOfMemoryError oom) {
            //RFR//_("-- OOM Error in setting image "+oom.getMessage());
            Tools.toast("Out of memory", c);
            oom.printStackTrace();
        }
        return null;
    }

    ///0.6.7 t optimise memory usages
    //loads up the image much smaller, saves it out as JPEG then passes back the path to this jpeg
    //kind is either headAndShoulders, or Signature
    public String makeSmallerImage(/*ImageButton ib,*/ String path)//, String kind)
    {
        //RFR//_("////////////////////////////////////////////////////");
        _("//////////////////////////////////makeSmallerImage");
        //RFR//_("////////////////////////////////////////////////////");
        String pathTmp = "";
        //we need to make a low quality version so we aren't dealing with huge images going over the air etc

        int w = 640;//fix choose this
        int h = 480;
        Bitmap lowQualityImage = Tools.decodeSampledBitmapFromFile(path,w,h);//will get an image the size of the screen (or thereabouts, which is far less than a pic from gallery usually)
        _("lowQualityImage : "+lowQualityImage.getWidth()+" x "+lowQualityImage.getHeight());

        //Deal with rotation! (will this slow things down doing yet more work on image?)
        //it seems that some android cams allow portrait phototaking fine, others force landscape so
        //the pic ends up the wrong way around, we need to detect this and rotate to fix if needed
        Bitmap rotated = Tools.rotateIfNeeded(path,c);
        if (rotated!=null)
        {
            lowQualityImage = rotated;
        }
        /////ib.setImageBitmap(lowQualityImage);

        //now we save the low quality version somewhere and refer to that instead of the massive one
        String filename = "image_PIC_"+System.currentTimeMillis(); //keep these unique, not sure why yet :)
        String fileNameExtension = ".jpg";
        File sdCard = Environment.getExternalStorageDirectory();
        String imageStorageFolder = File.separator+"ClinixTmp"+File.separator;

        File destinationDIR = new File(sdCard, imageStorageFolder);
        destinationDIR.mkdirs();
        File destinationFile = new File(sdCard, imageStorageFolder + filename + fileNameExtension);
        try
        {
            _("Attempting to save image to "+destinationFile.getAbsolutePath());
            FileOutputStream out = new FileOutputStream(destinationFile);
            lowQualityImage.compress(Bitmap.CompressFormat.JPEG, 70, out);
            out.flush();
            out.close();
            pathTmp = destinationFile.getAbsolutePath();
            _("Image saved. photoPath is now :"+pathTmp);
        }
        catch (Exception e)
        {
            _("ERROR saving image! " + e.toString());
        }
        return pathTmp;
    }

    public static final int MODE_SELECT_PROFILE_PIC=1;
    public static final int MODE_ADD_PIC_TO_APPOINTMENT=2;
    public static final int MODE_ADD_PIC_TO_ALLERGY=3;
int mode;
    //saves the user picture path so we can load it later
    public void saveUserPicture(String pathToUserPic)
    {
        _("//////////////saveUserPicture to -> " + pathToUserPic);

        //depending on the mode we do different things//
        switch (mode)
        {
            case MODE_SELECT_PROFILE_PIC:
                _("MODE_SELECT_PROFILE_PIC");
                ProfileActivity.myProfile.image = pathToUserPic;
            break;
            case MODE_ADD_PIC_TO_APPOINTMENT:
                _("MODE_ADD_PIC_TO_APPOINTMENT");
                BookAppointment_NotesActivity.image = pathToUserPic;
                break;
            case MODE_ADD_PIC_TO_ALLERGY:
                _("MODE_ADD_PIC_TO_ALLERGY");
                AllergyDetailsActivity.image = pathToUserPic;
                break;
            default:
                _("WARNING UNKNOWN MODE FIX ME!");
        }

        // t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SaveKeys.SAVE_KEY_PICTURE, -1, pathToUserPic); //write name
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////

    //a wrapper so we dont have to type this every time
    private void _(String s)
    {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", "BuildPhotoDialog" + "#######" + s);
    }
}
