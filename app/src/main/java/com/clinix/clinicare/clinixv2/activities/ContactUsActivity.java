/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.common.Tools;

import java.util.ArrayList;
import java.util.List;

public class ContactUsActivity extends Activity {

    Context c;
    Activity a;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _("ContactUsActivity Born!////////////////////////////////");
        c = this;
        a = this;
        setContentView(R.layout.contactus);

        //title spinner
        Spinner spEnquiryType  = (Spinner)    findViewById(R.id.spEnquiryType);
        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add("General");
        spinnerArray.add("Technical Support");
        spinnerArray.add("Account and Billing");
        spinnerArray.add("Medical Complaint");
        spinnerArray.add("Other");
        Tools.populateSpinner(spEnquiryType, spinnerArray);


        final TextView etQuery = (TextView)findViewById( R.id.etQuery );

        String hint = "Please provide as much detail as possible to help us resolve your query. If your query relates to a specfic consultation, please include the " +
                "Consultation Number (accessible via Previous Consultations > Summary > Consultation Details " +
                "\\n" +
                "We will then forward your query on to yout surgey and they will respond to you directly and deal with your query as necessary." +
                "\\n" +
                "Alternatively you may contact your surgery or practitioner directly.";
        etQuery.setHint("" + hint);

        Button btSendMessage = (Button)findViewById( R.id.btSendMessage );
        btSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("btSendMessage pressed.");
                Tools.openEmailClientToSendUsFeedback("sendto@who.com",etQuery.getText()+"");
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        _("///////////////onResume/////////////");
       // Tools.sendAnalytics_ScreenView(this, "RegisterActivity");//do a screenview for analytics
    }

    //a wrapper so we dont have to type this every time
    private void _(String s)
    {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", "ContactUsActivity" + "#######" + s);
    }
}
