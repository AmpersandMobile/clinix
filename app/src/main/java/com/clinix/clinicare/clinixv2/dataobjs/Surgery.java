package com.clinix.clinicare.clinixv2.dataobjs;

import android.location.Location;
import android.util.Log;

import com.clinix.clinicare.clinixv2.activities.NearbyNHSSurgeriesActivity;
import com.clinix.clinicare.clinixv2.common.Tools;

/**
 * Created by USER on 1/25/2016.
 */
public class Surgery {

    public String id                   ;
    public String name                 ;
    public String type                 ;
    public String street               ;
    public String address_2            ;
    public String town                 ;
    public String postcode             ;
    public String county                       ;
    public String country              ;
    public String lat                  ;
    public String lng                  ;
    public String email                ;
    public String telephone            ;
    public String website              ;

    public String distance             ;//we calc this in constructor


    boolean isDummyRepresentingNoSurgeriesAreNear;
    public Surgery(String name_)
    {
        name=name_;
        isDummyRepresentingNoSurgeriesAreNear=true;
    }

    public Surgery(String id_,String name_,String type_,String street_,String address_2_,String town_,String postcode_,String county_, String country_,String lat_,String lng_,String email_,String telephone_,String website_)
    {
        id                   = id_;
        name                 = name_;
        type                 = type_;
        street               = street_;
        address_2            = address_2_;
        town                 = town_;
        postcode             = postcode_;
        county               = county_;
        country              = country_;
        lat                  = lat_;
        lng                  = lng_;
        email                = email_;
        telephone            = telephone_;
        website              = website_;

        calcDistance();
    }

    private void calcDistance()
    {
        double startlatL = Double.parseDouble(lat);
        double startlngL = Double.parseDouble(lng);

        double endlatL = Double.parseDouble(NearbyNHSSurgeriesActivity.latS);
        double endlngL = Double.parseDouble(NearbyNHSSurgeriesActivity.lngS);

        float[] results = new float[1];
        Location.distanceBetween(startlatL, startlngL, endlatL, endlngL, results);

        float miles = results[0]*(float)0.000621371;
        _("miles: "+miles);
        distance = miles+"";
        if (distance.contains("."))
        {
            distance = distance.substring(0,distance.indexOf("."));
        }
        else
        {
            _("no .");
        }

        distance = distance+"mi";
        _("distance calculated : "+distance+" MILES. length of results is "+results.length);

    }


    //a wrapper so we dont have to type this every time
    private void _(String s)
    {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", "Surgery" + "#######" + s);
    }
}
