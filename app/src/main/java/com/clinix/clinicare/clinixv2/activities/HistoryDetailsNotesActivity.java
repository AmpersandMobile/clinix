/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.adapters.HistoryAdapter;
import com.clinix.clinicare.clinixv2.common.Networking;
import com.clinix.clinicare.clinixv2.common.Tools;
import com.clinix.clinicare.clinixv2.dataobjs.History;
import com.clinix.clinicare.clinixv2.dataobjs.Medication;

import java.util.ArrayList;

public class HistoryDetailsNotesActivity extends FragmentActivity {

    Context c;
    public static ArrayList<History> itemListHistory;
    public ListView listView;
    HistoryAdapter adapter;
    Activity a;

    History myHistory;
    String idOfMyHistory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.history_notes_screen);
        c = this;
        a = this;

        idOfMyHistory = getIntent().getStringExtra("id");
        _("id passed into activity: " + idOfMyHistory);
        if (idOfMyHistory==null)
        {
            _("since idOfMyHistory is null you must be adding a new history");
            TextView tvTitle = (TextView)    findViewById(R.id.tvTitle);
            tvTitle.setText("Add History");
        }


       ////// doNetworkingToGetHistory();
       /// setupFakeActionBar();
    }


    @Override
    public void onResume()
    {
        super.onResume();
        _("///////////////onResume/////////////");
        assignMyHistoryObject();
        populateGUI();
        setupFakeActionBar();
    }

    private void assignMyHistoryObject()
    {
        for (int i=0; i<HistoryActivity.itemListHistory.size(); i++)
        {
            History h = HistoryActivity.itemListHistory.get(i);
            _("Looking for ID "+idOfMyHistory+". IS THIS IT? "+h.id);
            if (h.id.equals(idOfMyHistory))
            {
                _("History object assigned");
                myHistory = h;
            }
        }

        if (myHistory==null)
        {
            _("WARNING myHistory was not assigned, could not find id: "+idOfMyHistory);
        }
    }

    //we use our own actionbar to avoid the crap real one
    private void setupFakeActionBar()
    {
        //fake actionbar menu button
        //we use this for:
        //-change password
        //-save
        //
        //This is basically a fake menu that pops up
        final LinearLayout fakeActionBarButtonHolder = (LinearLayout) findViewById(R.id.fakeActionBarButtonHolder);
        //hide it right away
        fakeActionBarButtonHolder.setVisibility(View.GONE);

        ImageButton ibMenu = (ImageButton)    findViewById(R.id.ibMenu);
        ibMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ibMenu hit.");
                Tools.toggleActionBarMenu(fakeActionBarButtonHolder);
            }
        });
        //and its buttons:
        Button btA = (Button)    findViewById(R.id.btA);
        btA.setText(" Save ");
        btA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("Save btA hit.");
                //ADD/SAVE HISTORY ON SERVER
                doNetworkingToAddHistory();
                Tools.toggleActionBarMenu(fakeActionBarButtonHolder);
            }
        });
        Button btB = (Button)    findViewById(R.id.btB);
        btB.setVisibility(View.GONE);
    }

    EditText tvNotes ;
    EditText tvDiagnosis ;
    TextView tvDate ;
    private void populateGUI()
    {
        tvNotes     = (EditText) findViewById(R.id.tvNotes);
        tvDiagnosis = (EditText) findViewById(R.id.tvDiagnosis);
        tvDate      = (TextView) findViewById(R.id.tvDate);
        if (myHistory==null)
        {
            _("warning myHistory is still null! bailing");
            return;
        }

        tvNotes.setText(""+myHistory.notes);
        tvDiagnosis.setText(""+myHistory.diagnosis);
        tvDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tools.popupDateDialog(tvDate, tvDiagnosis, a, false);
            }
        });
    }

    private void doNetworkingToAddHistory()
    {
        _("doNetworkingToAddHistory=========================================");
        if (!Tools.isNetworkAvailable(this) )
        {
            Tools.toast("This app needs an active Internet connection to work.",c);
            return;
        }

        //Do networking!
        String messageForProgressBar = "Adding History";
        String url = Networking.URL_CREATE_A_HISTORY_FOR_MY_PATIENT;
        int nState = Networking.NETWORK_STATE_CREATE_A_HISTORY_FOR_MY_PATIENT;
        Networking n = new Networking(messageForProgressBar, c, true);

        Tools t = new Tools(c);
        t.setupPrefs(c);

        //make a new one with whatever info we know, leave stuff we dont know blank.
        String user_id    = ProfileActivity.myProfile.id;//YEH?
        String date_      = tvDate.toString();
        String diagnosis_ = tvDiagnosis.toString();
        String notes_     = tvNotes.toString();
        String id_        = "";

        History h = new History(id_,user_id,date_,diagnosis_,notes_);
        n.setNewHistoryDetails(h);// we call this specific method for this state so right vars are set up.
        n.execute(url, nState);
    }

    //a wrapper so we dont have to type this every time
    private void _(String s) {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", "HistoryActivity" + "#######" + s);
    }
}
