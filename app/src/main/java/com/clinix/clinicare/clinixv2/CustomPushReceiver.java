package com.clinix.clinicare.clinixv2;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.clinix.clinicare.clinixv2.activities.MainMenuActivity;
import com.clinix.clinicare.clinixv2.common.NotificationUtils;
import com.clinix.clinicare.clinixv2.common.Tools;
import com.parse.ParsePushBroadcastReceiver;

import org.json.JSONException;
import org.json.JSONObject;

//import info.androidhive.parsenotifications.activity.MainActivity;
//import info.androidhive.parsenotifications.helper.NotificationUtils;
/**
 * Created by USER on 1/5/2016.
 */

//this class receives the push notifications from PARSE.
public class CustomPushReceiver  extends ParsePushBroadcastReceiver {
        private final String TAG = CustomPushReceiver.class.getSimpleName();

        private NotificationUtils notificationUtils;

        private Intent parseIntent;

        public CustomPushReceiver() {
            super();
        }

        @Override
        protected void onPushReceive(Context context, Intent intent) {
            super.onPushReceive(context, intent);
            _("onPushReceive");


            ////HACK ASSUME IT IS TO SET UP A CALL....

            ////

            if (intent == null)
                return;

            try {
                JSONObject json = new JSONObject(intent.getExtras().getString("com.parse.Data"));
                _("Push received: " + json);
                parseIntent = intent;
                parsePushJson(context, json);
            } catch (JSONException e) {
                _("Push message json exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPushDismiss(Context context, Intent intent) {
            super.onPushDismiss(context, intent);
            _("onPushDismiss");
        }

        @Override
        protected void onPushOpen(Context context, Intent intent) {
            super.onPushOpen(context, intent);
            _("onPushOpen");
        }

        /**
         * Parses the push notification json
         *
         * @param context
         * @param json
         */
        private void parsePushJson(Context context, JSONObject json) {
            _("parsePushJson");
            try {
                boolean isBackground = json.getBoolean("is_background");
                JSONObject data = json.getJSONObject("data");
                String title = data.getString("title");
                String message = data.getString("message");

                if (!isBackground) {
                    Intent resultIntent = new Intent(context, MainMenuActivity.class);
                    showNotificationMessage(context, title, message, resultIntent);
                }

            } catch (JSONException e) {
                Log.e(TAG, "Push message json exception: " + e.getMessage());
            }
        }


        /**
         * Shows the notification message in the notification bar
         * If the app is in background, launches the app
         *
         * @param context
         * @param title
         * @param message
         * @param intent
         */
        private void showNotificationMessage(Context context, String title, String message, Intent intent) {
            _("onPushReceive");
            notificationUtils = new NotificationUtils(context);

            intent.putExtras(parseIntent.getExtras());

            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            notificationUtils.showNotificationMessage(title, message, intent);
        }



        //a wrapper so we dont have to type this every time
        private void _(String s)
        {
            if (!Tools.DEBUG)
            {
                return;
            }
            Log.d("MyApp", "CustomPushReceiver" + "#######" + s);
        }
    }