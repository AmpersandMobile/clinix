/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.common.Tools;
import com.clinix.clinicare.clinixv2.dataobjs.Payment;

public class PaymentActivity extends Activity {

    Context     c;
    Activity    a;
    Tools t;

    private EditText etYourName;
    private EditText etCardNumber;
    private EditText etAddressA;
    private EditText etAddressB;
    private EditText etAddressC;
    private EditText etAddressD;
    private TextView tvItem;
    private TextView tvPaymentDue;
    private TextView tvExpiryDate;
    private EditText etEmail;
    private Button btSave;

    //For loading and saving.
    public static final String SAVE_KEY_PAYMENT_NAME        =   "SAVE_KEY_PAYMENT_NAME";
    public static final String SAVE_KEY_PAYMENT_CARDNUMBER  =   "SAVE_KEY_PAYMENT_CARDNUMBER";
    public static final String SAVE_KEY_PAYMENT_EXPIRYDATE  =   "SAVE_KEY_PAYMENT_EXPIRYDATE";
    public static final String SAVE_KEY_PAYMENT_CVV         =   "SAVE_KEY_PAYMENT_CVV";
    public static final String SAVE_KEY_PAYMENT_ADDRESS1    =   "SAVE_KEY_PAYMENT_ADDRESS1";
    public static final String SAVE_KEY_PAYMENT_ADDRESS2    =   "SAVE_KEY_PAYMENT_ADDRESS2";
    public static final String SAVE_KEY_PAYMENT_ADDRESS3    =   "SAVE_KEY_PAYMENT_ADDRESS3";
    public static final String SAVE_KEY_PAYMENT_ADDRESS4    =   "SAVE_KEY_PAYMENT_ADDRESS4";
    public static final String SAVE_KEY_PAYMENT_ITEM        =   "SAVE_KEY_PAYMENT_ITEM";
    public static final String SAVE_KEY_PAYMENT_PAYMENT_DUE =   "SAVE_KEY_PAYMENT_PAYMENT_DUE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _("PaymentActivity Born!////////////////////////////////");
        c = this;
        a = this;
        setContentView(R.layout.payment);
    }

    @Override
    public void onResume() {
        super.onResume();
        _("///////////////onResume/////////////");

        t = new Tools(c);
        t.setupPrefs(c);

        setupGUI();
        loadPaymentDetails();
        setupFakeActionBar();
    }


    //we use our own actionbar to avoid the crap real one
    private void setupFakeActionBar()
    {
        //fake actionbar menu button
        //we use this for:
        //-change password
        //-save
        //
        final LinearLayout fakeActionBarButtonHolder = (LinearLayout) findViewById(R.id.fakeActionBarButtonHolder);
        //hide it right away
        fakeActionBarButtonHolder.setVisibility(View.GONE);

        ImageButton ibMenu = (ImageButton)    findViewById(R.id.ibMenu);
        ibMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ibMenu hit.");
                Tools.toggleActionBarMenu(fakeActionBarButtonHolder);
            }
        });
        //and its buttons:
        Button btA = (Button)    findViewById(R.id.btA);
        btA.setText(" Save ");
        btA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("SAVE btA hit.");
                savePaymentDetails();
                Tools.toggleActionBarMenu(fakeActionBarButtonHolder);
            }
        });
        Button btB = (Button)    findViewById(R.id.btB);
        btB.setVisibility(View.GONE);
    }

    private void setupGUI()
    {
        etYourName      = (EditText)findViewById( R.id.etYourName );
        etCardNumber    = (EditText)findViewById( R.id.etCardNumber );
        etAddressA      = (EditText)findViewById( R.id.etAddressA );
        etAddressB      = (EditText)findViewById( R.id.etAddressB );
        etAddressC      = (EditText)findViewById( R.id.etAddressC );
        etAddressD      = (EditText)findViewById( R.id.etAddressD );
        tvItem          = (TextView)findViewById( R.id.tvItem );
        tvPaymentDue    = (TextView)findViewById( R.id.tvPaymentDue );
        tvExpiryDate    = (TextView)findViewById( R.id.tvExpiryDate );
        etEmail         = (EditText)findViewById( R.id.etEmail );

        //Save hit
        Button btSave = (Button) findViewById(R.id.btSave);
        btSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("btSave HIT");
                savePaymentDetails();
            }
        });
    }

    /////////////////////////////////////////////////////////////////////////////////////
    ///LOADING AND SAVING CODE, FOR GP DETAILS//////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////

    private void savePaymentDetails()
    {
        Payment.yourName = etYourName.getText()+"";
        t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_PAYMENT_NAME, -1, Payment.yourName);

        Payment.cardNumber = etCardNumber.getText()+"";
        t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_PAYMENT_CARDNUMBER, -1, Payment.cardNumber);

        Payment.expiryDate = tvExpiryDate.getText()+"";
        t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_PAYMENT_EXPIRYDATE, -1, Payment.expiryDate);

        Payment.cvv = etYourName.getText()+"";
        t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_PAYMENT_CVV, -1, Payment.cvv);

        Payment.Address_1 = etAddressA.getText()+"";
        t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_PAYMENT_ADDRESS1, -1, Payment.Address_1);

        Payment.Address_2 = etAddressB.getText()+"";
        t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_PAYMENT_ADDRESS2, -1, Payment.Address_2);

        Payment.Address_3 = etAddressC.getText()+"";
        t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_PAYMENT_ADDRESS3, -1, Payment.Address_3);

        Payment.Address_4 = etAddressD.getText()+"";
        t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_PAYMENT_ADDRESS4, -1, Payment.Address_4);

        Payment.item = tvItem.getText()+"";
        t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_PAYMENT_ITEM, -1, Payment.item);

        Payment.paymentDue = tvPaymentDue.getText()+"";
        t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_PAYMENT_PAYMENT_DUE, -1, Payment.paymentDue);

        Tools.toast("Saved.", c);
    }

    private void loadPaymentDetails()
    {
        _("LOADING USER DATA____________________");

        Payment.yourName = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_PAYMENT_NAME, "", c);
        etYourName.setText(Payment.yourName);

        Payment.cardNumber = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_PAYMENT_CARDNUMBER, "", c);
        etCardNumber.setText(Payment.cardNumber);

        Payment.expiryDate = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_PAYMENT_EXPIRYDATE, "", c);
        tvExpiryDate.setText(Payment.expiryDate);

        Payment.cvv = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_PAYMENT_CVV, "", c);
        etYourName.setText(Payment.cvv);

        Payment.Address_1 = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_PAYMENT_ADDRESS1, "", c);
        etAddressA.setText(Payment.Address_1);

        Payment.Address_2 = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_PAYMENT_ADDRESS2, "", c);
        etAddressB.setText(Payment.Address_2);

        Payment.Address_3 = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_PAYMENT_ADDRESS3, "", c);
        etAddressC.setText(Payment.Address_3);

        Payment.Address_4 = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_PAYMENT_ADDRESS4, "", c);
        etAddressD.setText(Payment.Address_4);

        Payment.item = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_PAYMENT_ITEM, "", c);
        etYourName.setText(Payment.item);

        Payment.paymentDue = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_PAYMENT_PAYMENT_DUE, "", c);
        etYourName.setText(Payment.paymentDue);
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////

    //a wrapper so we dont have to type this every time
    private void _(String s)
    {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", "PaymentActivity" + "#######" + s);
    }
}
