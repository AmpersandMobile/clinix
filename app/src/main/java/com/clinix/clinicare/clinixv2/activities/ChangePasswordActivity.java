/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.common.Networking;
import com.clinix.clinicare.clinixv2.common.Tools;

import java.util.ArrayList;
import java.util.List;

public class ChangePasswordActivity extends Activity {

    Context c;
    Activity a;
    String oldPassword      ;
    String newPassword      ;
    String confirmPassword  ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _("ChangePasswordActivity Born!////////////////////////////////");
        c = this;
        a = this;
        setContentView(R.layout.changepassword);

        final EditText etoldpassword       = (EditText)    findViewById(R.id.etoldpassword);
        final EditText etnewpassword       = (EditText)    findViewById(R.id.etnewpassword);
        final EditText etconfirmpassword   = (EditText)    findViewById(R.id.etconfirmpassword);

        Button btChangePassword = (Button)    findViewById(R.id.btChangePassword);
        btChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("btChangePassword hit.");

                oldPassword      = ""+etoldpassword.getText();
                newPassword      = ""+etnewpassword.getText();
                confirmPassword  = ""+etconfirmpassword.getText();

                if (oldPassword.length()==0 || newPassword.length()==0 || confirmPassword.length()==0  )
                {
                    Tools.toast("Please enter your passwords",c);
                    return;
                }
                if (!newPassword.equals(confirmPassword))
                {
                    Tools.toast("Passwords don't match!",c);
                    return;
                }
                doNetworkingToChangePassword();
            }
        });

    }


    private void doNetworkingToChangePassword()
    {
        _("doNetworkingToChangePassword=========================================");
        if (!Tools.isNetworkAvailable(this) )
        {
            Tools.toast("This app needs an active Internet connection to work.",c);
            return;
        }

        //Do networking!
        String messageForProgressBar = "Change password";
        String url = Networking.URL_PASSWORD_RESET_CHANGE;
        int nState = Networking.NETWORK_STATE_PASSWORD_RESET_CHANGE;
        Networking n = new Networking(messageForProgressBar, c, true);

        Tools t = new Tools(c);
        t.setupPrefs(c);
        String id = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, "id",null,c);
        n.setChangePasswordInfo(oldPassword,newPassword);// we call this specific method for this state so right vars are set up.
        n.execute(url, nState);
    }
    @Override
    public void onResume() {
        super.onResume();
        _("///////////////onResume/////////////");
    }

    //a wrapper so we dont have to type this every time
    private void _(String s)
    {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", "ChangePasswordActivity" + "#######" + s);
    }
}
