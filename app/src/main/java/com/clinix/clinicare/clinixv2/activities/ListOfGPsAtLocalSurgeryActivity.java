/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.adapters.PractitionersAtLocalGPAdapter;
import com.clinix.clinicare.clinixv2.common.Networking;
import com.clinix.clinicare.clinixv2.common.Tools;
import com.clinix.clinicare.clinixv2.dataobjs.Insurance;
import com.clinix.clinicare.clinixv2.dataobjs.Practitioner_fromNotification;

import java.util.ArrayList;

public class ListOfGPsAtLocalSurgeryActivity extends FragmentActivity {

    Context c;
    public static ArrayList<Practitioner_fromNotification> itemListGPs;
    public ListView listView;
    PractitionersAtLocalGPAdapter adapter;
    Activity a;
    String idOfMySurgery, addressOfMySurgery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.gps_at_localsurgery);
        c = this;
        a = this;

        idOfMySurgery = getIntent().getStringExtra("id");
        addressOfMySurgery = getIntent().getStringExtra("address");

        doNetworkingToFindGPsAtThisSurgery();
        setupFakeActionBar();

        //set address of the surgey they click on.
        TextView tvSurgeryAddress      = (TextView)    findViewById(R.id.tvSurgeryAddress);
        tvSurgeryAddress.setText(addressOfMySurgery);
    }


    public void fillUpList()
    {
        listView = (ListView)findViewById(R.id.mylist);
        _("fillUpList----");

        //make adapter and set it, this updates the view
        adapter = new PractitionersAtLocalGPAdapter(this, itemListGPs);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                _("////////////////////////clicked on list/////////////////////");
                //since only one can be the default we need to make sure all others are flagged as false now
                ///// adapter.flagThisItemAsDefault(position,a);


                Practitioner_fromNotification s = itemListGPs.get(position);
                _("" + s.forename + " Id:" + s.id);

                String surgeryid = idOfMySurgery;
                String gpId = s.id;

                doNetworkingToSetMyNewSurgeryAndGP(idOfMySurgery,gpId);

                //Intent intent = new Intent(c, ListOfGPsAtLocalSurgeryActivity.class);
               // intent.putExtra("id", s.id);//pass over the id so we can get it from the list
               // c.startActivity(intent);
                //   listView.invalidateViews();//refresh it
            }
        });

        //set the button colours
        //shcedule starts selected, so we want it to be white with green font
      //  btScheduled.setBackgroundResource(R.drawable.buttonborder_whitebackground);
      //  btScheduled.setTextColor(Tools.CLINIX_GREEN);
        //and the previous button, green with white text
      //  btPrevious.setBackgroundResource(R.drawable.buttonborder_greenbackground);
      //  btPrevious.setTextColor(Tools.CLINIX_WHITE);

      ////  doNetworkingToGetAppointments();
    }


    private void doNetworkingToFindGPsAtThisSurgery()
    {
        _("doNetworkingToFindGPsAtThisSurgery=========================================");
        if (!Tools.isNetworkAvailable(this) )
        {
            Tools.toast("This app needs an active Internet connection to work.",c);
            return;
        }

        //Do networking!
        String messageForProgressBar = "Getting list of GPs";
        String url = Networking.URL_GET_GPLIST_FOR_SURGERY;
        int nState = Networking.NETWORK_STATE_GET_GPLIST_FOR_SURGERY;
        Networking n = new Networking(messageForProgressBar, c, true);

        Tools t = new Tools(c);
        t.setupPrefs(c);

        n.setSurgery_IDtoFind(idOfMySurgery);//
        n.execute(url, nState);
    }

    private void doNetworkingToSetMyNewSurgeryAndGP(String idOfMySurgery, String gpId)
    {
        _("doNetworkingToSetMyNewSurgeryAndGP=========================================");
        if (!Tools.isNetworkAvailable(this) )
        {
            Tools.toast("This app needs an active Internet connection to work.",c);
            return;
        }

        //Do networking!
        String messageForProgressBar = "Setting new surgery and GP";
        String url = Networking.URL_SET_MY_PATIENT_SURGERY_AND_GP;
        int nState = Networking.NETWORK_STATE_SET_MY_PATIENT_SURGERY_AND_GP;
        Networking n = new Networking(messageForProgressBar, c, true);

        Tools t = new Tools(c);
        t.setupPrefs(c);

        n.setNewSurgeryAndGPInfo(idOfMySurgery, gpId);//
        n.execute(url, nState);
    }

    public static final String USER_IS_NHS_PATIENT="USER_IS_NHS_PATIENT";
    //this gets called from the networking thrad once the result comes in
    public void popUpResponseFromSettingNewSurgeryAndGP(String text)
    {
        _("popUpResponseFromSettingNewSurgeryAndGP");
        Tools.showDialog(a,
                "Consultations",
                text,
                "OK",
                null,
                Tools.DIALOG_SET_NEW_SURGERY_AND_GP);

        //at this point they have auto added a NHS surgery, so we remember this because if they wish to manually enter a surgery later
        //not specified by our api then they arent actually an NHS patient anymore and the app wont let them make calls etc, so we warn them
        Tools t = new Tools(c);
        t.setupPrefs(c);
        t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, USER_IS_NHS_PATIENT, -1, "TRUE");
    }

    //we use our own actionbar to avoid the crap real one
    private void setupFakeActionBar()
    {
        //fake actionbar menu button
        //we use this for:
        //-change password
        //-save
        //
        //This is basically a fake menu that pops up
        final LinearLayout fakeActionBarButtonHolder = (LinearLayout) findViewById(R.id.fakeActionBarButtonHolder);
        //hide it right away
        fakeActionBarButtonHolder.setVisibility(View.GONE);

        ImageButton ibMenu = (ImageButton)    findViewById(R.id.ibMenu);
        ibMenu.setVisibility(View.GONE);
        //and its buttons:
        Button btA = (Button)    findViewById(R.id.btA);
        btA.setVisibility(View.GONE);
        Button btB = (Button)    findViewById(R.id.btB);
        btB.setVisibility(View.GONE);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        _("///////////////onResume/////////////");
    }

    //a wrapper so we dont have to type this every time
    private void _(String s) {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", "NearbyNHSSurgeriesActivity" + "#######" + s);
    }
}
