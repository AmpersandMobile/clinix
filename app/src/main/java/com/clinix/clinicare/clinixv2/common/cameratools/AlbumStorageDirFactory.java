package com.clinix.clinicare.clinixv2.common.cameratools;

import java.io.File;

public abstract class AlbumStorageDirFactory {
	public abstract File getAlbumStorageDir(String albumName);
}
