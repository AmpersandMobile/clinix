package com.clinix.clinicare.clinixv2.dataobjs;

/**
 * Created by USER on 28/10/2015.
 */
public class Medication
{
    public String id;
    public String user_id;
    public String practitioner_id;
    public String drug_name;
    public String quantity;
    public String course;
    public String instructions;
    public String date;
    public Practitioner practitioner;

    public Medication(String id_,String user_id_,String practitioner_id_, String drug_name_, String quantity_, String course_, String instructions_, String  date_)
    {
        id=id_;
        user_id=user_id_;
        practitioner_id=practitioner_id_;
        drug_name=drug_name_;
        quantity=quantity_;
        course=course_;
        instructions=instructions_;
        date=date_;

    }


}
