/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.common.Networking;
import com.clinix.clinicare.clinixv2.common.Tools;
import com.clinix.clinicare.clinixv2.dataobjs.SaveKeys;

// a simple register activity class
public class ForgotPasswordActivity extends Activity {

    Context c;
    Activity a;
    EditText etEmailAddress, etPassword;
    Button btLogin;
    TextView tvIAccept;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        _("ForgotPasswordActivity Born!////////////////////////////////");
        c = this;
        a = this;
        final Tools t = new Tools(c);
        t.setupPrefs(c);

        setContentView(R.layout.forgotpassword);

        etEmailAddress      = (EditText)    findViewById(R.id.etEmailAddress);
        //populate it if we have it
        String emailAddress     = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SaveKeys.STORAGE_KEY_EMAIL_ADDRESS,null,this);
        if (emailAddress.length()>0)
        {
            etEmailAddress.setText(emailAddress);
        }

        Button btResetPassword  = (Button) findViewById(R.id.btResetPassword);
        btResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("btResetPassword hit!");

                String emailAddress = etEmailAddress.getText() + "";
                _("emailAddress........:" + emailAddress);

                if (    emailAddress.length()   == 0 ) {
                    Tools.toast("Please fill in the info", c);
                    return;
                }

                if (Tools.USE_ANALYTICS) {
                    //Tools.sendAnalytics_ButtonPress(a, "ibSignUp");
                }

                if (!Tools.isNetworkAvailable(c) )
                {
                    Tools.toast("This app needs an active Internet connection to work.",c);
                    return;
                }


                ///////////////////////////////////////////NETWORK_STATE_PASSWORD_RESET_FORGOT//////////////
                //Do networking!
                String messageForProgressBar = "Resetting Password...";
                String url = Networking.URL_PASSWORD_RESET_FORGOT;
                int nState = Networking.NETWORK_STATE_PASSWORD_RESET_FORGOT;
                Networking n = new Networking(messageForProgressBar, c, true);
                n.setEmailInfo(emailAddress);// we call this specific method for this state so right vars are set up.
                n.execute(url, nState);
            }
        });
   }

    @Override
    public void onResume() {
        super.onResume();
        _("///////////////onResume/////////////");
       // Tools.sendAnalytics_ScreenView(this, "RegisterActivity");//do a screenview for analytics
    }





    //a wrapper so we dont have to type this every time
    private void _(String s)
    {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", "ForgotPasswordActivity" + "#######" + s);
    }
}
