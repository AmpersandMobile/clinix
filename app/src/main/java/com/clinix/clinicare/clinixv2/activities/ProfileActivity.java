/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.common.BuildPhotoDialog;
import com.clinix.clinicare.clinixv2.common.Networking;
import com.clinix.clinicare.clinixv2.common.Tools;
import com.clinix.clinicare.clinixv2.dataobjs.SaveKeys;
import com.clinix.clinicare.clinixv2.dataobjs.User_fromNotification;
import com.pkmmte.view.CircularImageView;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;

public class ProfileActivity extends Activity {

    public static User_fromNotification myProfile;
    Context c;
    Activity a;
    CircularImageView ibMyPic;
    Tools t;
    LinearLayout fakeActionBarButtonHolder;//this holds the buttons in the fake action bar

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _("ProfileActivity Born!////////////////////////////////");
        c = this;
        a = this;
        setContentView(R.layout.profilepage);
    }

    @Override
    public void onResume() {
        super.onResume();
        _("///////////////onResume/////////////");

        t = new Tools(c);
        t.setupPrefs(c);

        //sets up gui
        setupGUI();
        doNetworkingToGetMyProfile();
        //loadUserProfile();
    }


    private void doNetworkingToGetMyProfile()
    {
        _("doNetworkingToGetMyProfile=========================================");
        if (!Tools.isNetworkAvailable(this) )
        {
            Tools.toast("This app needs an active Internet connection to work.",c);
            return;
        }

        //Do networking!
        String messageForProgressBar = "Getting Profile details";
        String url = Networking.URL_GET_MY_PATIENT_PROFILE;
        int nState = Networking.NETWORK_STATE_GET_MY_PATIENT_PROFILE;
        Networking n = new Networking(messageForProgressBar, c, true);

        Tools t = new Tools(c);
        t.setupPrefs(c);
        ///String type = "patient"; // this can be admin / practitioner / patient
        // String id = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, "id",null,c);
        // n.setHistoryInfo(id);// we call this specific method for this state so right vars are set up.
        n.execute(url, nState);
    }
    BuildPhotoDialog buildPhotoObject;
    private void setupGUI()
    {
        //set circular image
        ibMyPic = (CircularImageView)findViewById(R.id.ibMyPic);      //we use a special circular imageview
        //load their profile pic with picasso, start with default if theres non and real one if its theres
        Picasso.with(c).load(R.drawable.defaultavatar).into(ibMyPic);

        //Profile picture - we need user to be able to tap this and select a photo
        ibMyPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("user tapped profile pic");
               //start activity
                buildPhotoObject = new BuildPhotoDialog(ibMyPic,a, c, t,BuildPhotoDialog.MODE_SELECT_PROFILE_PIC);
           //     Intent intent = new Intent(c, BuildPhotoDialog.class);
                //c.startActivity(intent);
               // BuildPhotoDialog bpd = new BuildPhotoDialog();
               // bpd.buildPhotoDialog();
            }
        });

        //title spinner
        Spinner spTitle  = (Spinner)    findViewById(R.id.spTitle);
        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add("Master");
        spinnerArray.add("Miss");
        spinnerArray.add("Mr");
        spinnerArray.add("Mrs");
        spinnerArray.add("Ms");
        spinnerArray.add("Other");
        Tools.populateSpinner(spTitle, spinnerArray);

        //date of birth popup
        final TextView tvDOB = (TextView)    findViewById(R.id.tvDOB);
        final TextView tvHeight = (TextView)    findViewById(R.id.tvHeight);//what to focus on after you hit ok, ie next thing
        //special tap on date to pop up date picker
        tvDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tools.popupDateDialog(tvDOB, tvHeight, a,false);
            }
        });

        //height popup
        //special tap on date to pop up date picker
        tvHeight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tools.popupBiometricDialog(c, tvHeight, Tools.BODY_HEIGHT, Tools.HEIGHT_FEET_AND_INCHES);
            }
        });

        //Weight popup
        //special tap on date to pop up date picker
        final TextView tvWeight = (TextView)    findViewById(R.id.tvWeight);
        tvWeight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tools.popupBiometricDialog(c, tvWeight, Tools.BODY_WEIGHT, Tools.WEIGHT_STONES_AND_POUNDS);
            }
        });

        //gender buttons
        final ImageButton ibMale    = (ImageButton)     findViewById(R.id.ibMale);
        final TextView tvMale       = (TextView)        findViewById(R.id.tvMale);
        final ImageButton ibFemale  = (ImageButton)     findViewById(R.id.ibFemale);
        final TextView tvFemale     = (TextView)        findViewById(R.id.tvFemale);

        ibMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ibMale hit");
                //make female greyed out and male normal
                ibFemale.setImageResource(R.drawable.circleiconfemaleoff);
                tvFemale.setTextColor(0xff1194A1);
                ibMale.setImageResource(R.drawable.circleiconmale2x);
                tvMale.setTextColor(0xffffffff);
            }
        });

        ibFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ibFemale hit");
                //make female normal and male greyed out
                ibFemale.setImageResource(R.drawable.circleiconfemale2x);
                tvFemale.setTextColor(0xffffffff);
                ibMale.setImageResource(R.drawable.circleiconmaleoff);
                tvMale.setTextColor(0xff1194A1);
            }
        });

        //smoker
        RadioGroup rgSmoker             = (RadioGroup)      findViewById(R.id.rgSmoker);
        final RadioButton rbNonsmoker   = (RadioButton)     findViewById(R.id.rbNonsmoker);
        final RadioButton rbSmoker      = (RadioButton)     findViewById(R.id.rbSmoker);
        rbSmoker.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                _("rbSmoker hit");
            }
        });
        rbNonsmoker.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                _("rbNonsmoker hit");
            }
        });

        setupFakeActionBar();

    }

    //we use our own actionbar to avoid the crap real one
    private void setupFakeActionBar()
    {
        //fake actionbar menu button
        //we use this for:
        //-change password
        //-save
        //
        fakeActionBarButtonHolder = (LinearLayout) findViewById(R.id.fakeActionBarButtonHolder);
        //hide it right away
        fakeActionBarButtonHolder.setVisibility(View.GONE);

        ImageButton ibMenu = (ImageButton)    findViewById(R.id.ibMenu);
        ibMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ibMenu hit.");
                toggleActionBarMenu();
            }
        });
        //and its buttons:
        Button btA = (Button)    findViewById(R.id.btA);
        btA.setText(" Change Password ");
        btA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("CHANGE PASSWORD btA hit.");
                ///changePasswordTapped();
                Intent intent = new Intent(c, ChangePasswordActivity.class);
                c.startActivity(intent);

                toggleActionBarMenu();
            }
        });
        Button btB = (Button)    findViewById(R.id.btB);
        btB.setText(" Save ");

        btB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("SAVE btB hit.");
                saveUserProfile();
                doNetworkingToSaveProfile();
                toggleActionBarMenu();
                finish();
            }
        });
    }

    private void doNetworkingToSaveProfile()
    {
        _("doNetworkingToSaveProfile=========================================");
        if (!Tools.isNetworkAvailable(this) )
        {
            Tools.toast("This app needs an active Internet connection to work.",c);
            return;
        }

        //Do networking!
        String messageForProgressBar = "Saving profile";
        String url = Networking.URL_SET_MY_PATIENT_PROFILE;
        int nState = Networking.NETWORK_STATE_SET_MY_PATIENT_PROFILE;
        Networking n = new Networking(messageForProgressBar, c, true);

        Tools t = new Tools(c);
        t.setupPrefs(c);
        n.setUserProfileInfo(myProfile);// we call this specific method for this state so right vars are set up.
        n.execute(url, nState);
    }

    private void changePasswordTapped()
    {
        final Dialog dialog = new Dialog(c);
        dialog.setTitle("Change Password");
        dialog.setContentView(R.layout.dialog_changepassword_picker);

        final EditText etPassA = (EditText) dialog.findViewById(R.id.etPassA);
        final EditText etPassB = (EditText) dialog.findViewById(R.id.etPassB);

        Button btYES = (Button) dialog.findViewById(R.id.btYes);
        btYES.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("OK HIT");

                String passA = etPassA.getText() + "";
                String passB = etPassB.getText() + "";
                _("passA-> " + passA);
                _("passB-> " + passB);

                if (passA.equals(passB)) {
                    if (passA.length() > 5) {
                        _("Writing password ->" + passA);
                        myProfile.password = passA;
                        t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SaveKeys.SAVE_KEY_FIRSTNAME, -1, ProfileActivity.myProfile.password);
                    } else {
                        Tools.toast("Passwords should be 5 characters or more", c);
                    }
                    dialog.dismiss();
                } else {
                    Tools.toast("Passwords do not match", c);
                }
            }
        });
        Button btNo = (Button) dialog.findViewById(R.id.btNo);
        btNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("CANCEL HIT");
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    private void toggleActionBarMenu()
    {
        if (fakeActionBarButtonHolder.getVisibility()==View.VISIBLE)
        {
            fakeActionBarButtonHolder.setVisibility(View.GONE);
        }
        else
        {
            fakeActionBarButtonHolder.setVisibility(View.VISIBLE);
        }
    }



    /////////////////////////////////////////////////////////////////////////////////////
    ///LOADING AND SAVING CODE, FOR PROFILE DETAILS//////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////

    private void saveUserProfile()
    {
        TextView etFirstName = (TextView)    findViewById(R.id.etFirstName);
        myProfile.forename = etFirstName.getText()+"";
       // t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SaveKeys.SAVE_KEY_FIRSTNAME, -1, UserProfile.firstName);

        TextView etLastName = (TextView)    findViewById(R.id.etLastName);
        myProfile.surname = etLastName.getText()+"";
       // t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SaveKeys.SAVE_KEY_LASTNAME, -1, UserProfile.lastName );

        TextView etNhsNumber = (TextView)    findViewById(R.id.etNhsNumber);
        myProfile.nhs_number = etNhsNumber.getText()+"";
      //  t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SaveKeys.SAVE_KEY_NHSNUMBER, -1, UserProfile.nhsNumber );

        TextView etEmail = (TextView)    findViewById(R.id.etEmail);
        myProfile.email = etEmail.getText()+"";
       // t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SaveKeys.SAVE_KEY_EMAIL, -1, UserProfile.email );

        TextView etMobile = (TextView)    findViewById(R.id.etMobile);
        myProfile.telephone = etMobile.getText()+"";
       // t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SaveKeys.SAVE_KEY_MOBILE, -1, UserProfile.mobile );

        TextView etAddressA = (TextView)    findViewById(R.id.etAddressA);
        myProfile.street = etAddressA.getText()+"";
      //  t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SaveKeys.SAVE_KEY_ADDRESS1, -1, UserProfile.Address_1 );

        TextView etAddressB = (TextView)    findViewById(R.id.etAddressB);
        myProfile.address_2 = etAddressB.getText()+"";
       // t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SaveKeys.SAVE_KEY_ADDRESS2, -1, UserProfile.Address_2 );

        TextView etAddressC = (TextView)    findViewById(R.id.etAddressC);
        myProfile.town = etAddressC.getText()+"";
       // t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SaveKeys.SAVE_KEY_ADDRESS3, -1, UserProfile.Address_3 );

        TextView etAddressD = (TextView)    findViewById(R.id.etAddressD);
        myProfile.county = etAddressD.getText()+"";
//        t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SaveKeys.SAVE_KEY_ADDRESS4, -1, UserProfile.Address_4 );

        //gender
        TextView tvMale       = (TextView)        findViewById(R.id.tvMale);
        //we check the colour to know what it is
        int genderColour = tvMale.getCurrentTextColor();
        _("genderColour:"+genderColour);
        if ( genderColour == 1 )
        {
            myProfile.gender="M"; //its male
        }
        else
        {
            myProfile.gender="F"; //its female
        }
      //  t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SaveKeys.SAVE_KEY_GENDER, -1, UserProfile.gender );

        TextView etDOB = (TextView)    findViewById(R.id.tvDOB);
        myProfile.dob = etDOB.getText()+"";
       // t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SaveKeys.SAVE_KEY_DOB, -1, UserProfile.dob );

        TextView tvHeight = (TextView)    findViewById(R.id.tvHeight);
        myProfile.height = tvHeight.getText()+"";
       // t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SaveKeys.SAVE_KEY_HEIGHT, -1, UserProfile.height );

        TextView tvWeight = (TextView)    findViewById(R.id.tvWeight);
        myProfile.weight = tvWeight.getText()+"";
       // t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SaveKeys.SAVE_KEY_WEIGHT, -1, UserProfile.weight );

        //smokers
        RadioButton rbSmoker = (RadioButton)    findViewById(R.id.rbSmoker);
        if (rbSmoker.isChecked())
        {
            myProfile.smoker = "Y";
        }
        else
        {
            myProfile.smoker = "N";
        }
       // t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SaveKeys.SAVE_KEY_SMOKER, -1, UserProfile.smoker );

        //Dont need to save password here it has its own button
        /*
        TextView etPassword = (TextView)    findViewById(R.id.etPassword);
        UserProfile.password = etPassword.getText()+"";
        t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_PASSWORD, -1, UserProfile.password );
        */

        Tools.toast("Saved.",c);
    }
//FIX THIS UP AND MAKE IT POPULATEGUI() REMOVE THE CRAP
    public void loadUserProfile()
    {
        _("LOADING USER DATA____________________");
        if (myProfile==null)
        {
            _("WARNING!!!! myProfile IS NULL!!!!.... FIX ME");
        }
        ibMyPic = (CircularImageView)findViewById(R.id.ibMyPic); //we use a special circular imageview
        myProfile.image = t.loadPictureAndSetImage(ibMyPic,ProfileActivity.myProfile.image);

        TextView etFirstName = (TextView)    findViewById(R.id.etFirstName);
        etFirstName.setText(myProfile.forename);

        TextView etLastName = (TextView)    findViewById(R.id.etLastName);
        etLastName.setText(myProfile.surname);

        TextView etNhsNumber = (TextView)    findViewById(R.id.etNhsNumber);
        etNhsNumber.setText(myProfile.nhs_number);

        TextView etEmail = (TextView)    findViewById(R.id.etEmail);
        etEmail.setText(myProfile.email);

        TextView etMobile = (TextView)    findViewById(R.id.etMobile);
        etMobile.setText(myProfile.telephone);

        TextView etAddressA = (TextView)    findViewById(R.id.etAddressA);
        etAddressA.setText(myProfile.street);

        TextView etAddressB = (TextView)    findViewById(R.id.etAddressB);
        etAddressB.setText(myProfile.address_2);

        TextView etAddressC = (TextView)    findViewById(R.id.etAddressC);
        etAddressC.setText(myProfile.town);

        TextView etAddressD = (TextView)    findViewById(R.id.etAddressD);
        etAddressD.setText(myProfile.county);

        //gender
        TextView tvMale       = (TextView)        findViewById(R.id.tvMale);
        //we check the colour to know what it is
        int genderColour = tvMale.getCurrentTextColor();
        _("genderColour:" + genderColour);
        if ( genderColour == 1 )
        {
            myProfile.gender="M"; //its male
        }
        else
        {
            myProfile.gender="F"; //its female
        }

        TextView etDOB = (TextView)    findViewById(R.id.tvDOB);
        etDOB.setText(myProfile.dob);

        TextView tvHeight = (TextView)    findViewById(R.id.tvHeight);
        tvHeight.setText(myProfile.height+" cm");

        TextView tvWeight = (TextView)    findViewById(R.id.tvWeight);
        tvWeight.setText(myProfile.weight+" kgs");

        //smokers
        RadioButton rbSmoker    = (RadioButton)    findViewById(R.id.rbSmoker);
        RadioButton rbNonsmoker = (RadioButton)    findViewById(R.id.rbNonsmoker);
        RadioButton rbExsmoker  = (RadioButton)    findViewById(R.id.rbExsmoker);
        if (myProfile.smoker.equals("non-smoker"))
        {
            rbSmoker.setChecked(false);
            rbNonsmoker.setChecked(true);
            rbExsmoker.setChecked(false);
        }
        else if (myProfile.smoker.equals("smoker"))
        {
            rbSmoker.setChecked(true);
            rbNonsmoker.setChecked(false);
            rbExsmoker.setChecked(false);
        }
        else if (myProfile.smoker.equals("ex-smoker"))
        {
            rbSmoker.setChecked(false);
            rbNonsmoker.setChecked(false);
            rbExsmoker.setChecked(true);
        }

        //TODO: LOAD PASSWORD HERE?
        //Dont need to save password here it has its own button
        /*
        TextView etPassword = (TextView)    findViewById(R.id.etPassword);
        UserProfile.password = etPassword.getText()+"";
        t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_PASSWORD, -1, UserProfile.password );
        */

    }

    ////////////////SELECT PHOTO CODE///////////////
    //the work is done in BuildPhotoDialog object which wraps up all the implementation into a reusable form.
    //for when we get back the photo from the intent
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        _("onActivityResult///////////////////////////////////");
        buildPhotoObject.dealWithResultFromActivity( requestCode,  resultCode,  data);
    }



    //a wrapper so we dont have to type this every time
    private void _(String s)
    {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", "ProfileActivity" + "#######" + s);
    }
}
