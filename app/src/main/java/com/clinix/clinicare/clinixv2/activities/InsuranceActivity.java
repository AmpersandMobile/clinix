/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.common.Tools;
import com.clinix.clinicare.clinixv2.dataobjs.Insurance;

public class InsuranceActivity extends Activity {

    Context c;
    Activity a;
    Tools t;

    private EditText etCompanyName;
    private EditText etMembershipNo;
    private EditText etFirstName;
    private EditText etLastName;
    private EditText etPostcode;
    private Button btSave;
    TextView tvDOB;

    //For loading and saving.
    public static final String SAVE_KEY_INSURANCE_NAME         =   "SAVE_KEY_INSURANCE_NAME";
    public static final String SAVE_KEY_INSURANCE_MEMBERSHIPNO =   "SAVE_KEY_INSURANCE_MEMBERSHIPNO";
    public static final String SAVE_KEY_INSURANCE_FIRSTNAME    =   "SAVE_KEY_INSURANCE_FIRSTNAME";
    public static final String SAVE_KEY_INSURANCE_LASTNAME     =   "SAVE_KEY_INSURANCE_LASTNAME";
    public static final String SAVE_KEY_INSURANCE_DOB          =   "SAVE_KEY_INSURANCE_DOB";
    public static final String SAVE_KEY_INSURANCE_POSTCODE     =   "SAVE_KEY_INSURANCE_POSTCODE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _("InsuranceActivity Born!////////////////////////////////");
        c = this;
        a = this;
        setContentView(R.layout.insurance);
    }

    @Override
    public void onResume() {
        super.onResume();
        _("///////////////onResume/////////////");

        t = new Tools(c);
        t.setupPrefs(c);

        setupGUI();
        loadInsuranceDetails();
    }

    private void setupGUI()
    {
        etCompanyName = (EditText)findViewById( R.id.etCompanyName );
        etMembershipNo = (EditText)findViewById( R.id.etMembershipNo );
        etFirstName = (EditText)findViewById( R.id.etFirstName );
        etLastName = (EditText)findViewById( R.id.etLastName );
        etPostcode = (EditText)findViewById( R.id.etPostcode );

        //date of birth popup
        tvDOB = (TextView)    findViewById(R.id.tvDOB);
        //special tap on date to pop up date picker
        tvDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tools.popupDateDialog(tvDOB, tvDOB, a, false);
            }
        });

        //Save hit
        Button btSave = (Button) findViewById(R.id.btSave);
        btSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("btSave HIT");
                saveInsuranceDetails();
            }
        });
        setupFakeActionBar();
    }


    //we use our own actionbar to avoid the crap real one
    private void setupFakeActionBar()
    {
        //fake actionbar menu button
        //we use this for:
        //-change password
        //-save
        //
        final LinearLayout fakeActionBarButtonHolder = (LinearLayout) findViewById(R.id.fakeActionBarButtonHolder);
        //hide it right away
        fakeActionBarButtonHolder.setVisibility(View.GONE);

        ImageButton ibMenu = (ImageButton)    findViewById(R.id.ibMenu);
        ibMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ibMenu hit.");
                Tools.toggleActionBarMenu(fakeActionBarButtonHolder);
            }
        });
        //and its buttons:
        Button btA = (Button)    findViewById(R.id.btA);
        btA.setText(" Save ");
        btA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("SAVE btA hit.");
                saveInsuranceDetails();
                Tools.toggleActionBarMenu(fakeActionBarButtonHolder);
            }
        });
        Button btB = (Button)    findViewById(R.id.btB);
        btB.setVisibility(View.GONE);
    }

    /////////////////////////////////////////////////////////////////////////////////////
    ///LOADING AND SAVING CODE, FOR GP DETAILS//////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////

    private void saveInsuranceDetails()
    {
        Insurance.companyName = etCompanyName.getText()+"";
        t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_INSURANCE_NAME, -1, Insurance.companyName);

        Insurance.membershipNo = etMembershipNo.getText()+"";
        t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_INSURANCE_MEMBERSHIPNO, -1, Insurance.membershipNo);

        Insurance.firstName = etFirstName.getText()+"";
        t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_INSURANCE_FIRSTNAME, -1, Insurance.firstName);

        Insurance.lastName = etLastName.getText()+"";
        t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_INSURANCE_LASTNAME, -1, Insurance.lastName);

        Insurance.dob = tvDOB.getText()+"";
        t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_INSURANCE_DOB, -1, Insurance.dob);

        Insurance.postcode = etPostcode.getText()+"";
        t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_INSURANCE_POSTCODE, -1, Insurance.postcode);

        Tools.toast("Saved.", c);
    }

    private void loadInsuranceDetails()
    {
        _("LOADING USER DATA____________________");

        Insurance.companyName = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_INSURANCE_NAME, "", c);
        etCompanyName.setText(Insurance.companyName);

        Insurance.membershipNo = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_INSURANCE_MEMBERSHIPNO, "", c);
        etMembershipNo.setText(Insurance.membershipNo);

        Insurance.firstName = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_INSURANCE_FIRSTNAME, "", c);
        etFirstName.setText(Insurance.firstName);

        Insurance.lastName = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_INSURANCE_LASTNAME, "", c);
        etLastName.setText(Insurance.lastName);

        Insurance.dob = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_INSURANCE_DOB, "", c);
        tvDOB.setText(Insurance.dob);

        Insurance.postcode = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_INSURANCE_DOB, "", c);
        etPostcode.setText(Insurance.postcode);

    }

    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////

    //a wrapper so we dont have to type this every time
    private void _(String s)
    {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", "InsuranceActivity" + "#######" + s);
    }
}
