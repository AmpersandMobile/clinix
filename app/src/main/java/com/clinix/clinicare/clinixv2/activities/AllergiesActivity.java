/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.adapters.AllergiesAdapter;
import com.clinix.clinicare.clinixv2.adapters.AppointmentsAdapter;
import com.clinix.clinicare.clinixv2.common.Networking;
import com.clinix.clinicare.clinixv2.common.Tools;
import com.clinix.clinicare.clinixv2.dataobjs.Allergy;
import com.clinix.clinicare.clinixv2.dataobjs.Appointment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class AllergiesActivity extends FragmentActivity {

    int state;
    public static final int ALLERGIES_FOOD  = 1;
    public static final int ALLERGIES_DRUG  = 2;
    public static final int ALLERGIES_OTHER = 3;

    public static ArrayList<Allergy> itemListFood;
    public static ArrayList<Allergy> itemListDrugs;
    public static ArrayList<Allergy> itemListOthers;

    public ListView listView;
    AllergiesAdapter adapter;
    Context c;
    Activity a;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.allergies);
        c = this;
        a = this;

        doNetworkingToGetAllergies();
        setupGui();
        setupFakeActionBar();
    }

    Button btFood, btDrug, btOther;
    public void setupGui()
    {
        //buttons at top for filtering//
        btFood = (Button)findViewById(R.id.btFood);
        btFood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("//////onClick-btFood/////");
                state = ALLERGIES_FOOD;
                makeHeaderSelectedForThisButton(btFood, btDrug, btOther);
                fillUpList();
            }
        });
        btDrug = (Button)findViewById(R.id.btDrug);
        btDrug.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("//////onClick-btDrug/////");
                state = ALLERGIES_DRUG;
                makeHeaderSelectedForThisButton(btDrug, btFood, btOther);
                fillUpList();
            }
        });
        btOther = (Button)findViewById(R.id.btOther);
        btOther.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("//////onClick-btOther/////");
                state = ALLERGIES_OTHER;
                makeHeaderSelectedForThisButton(btOther, btFood, btDrug);
                fillUpList();
            }
        });
        //set the button colours
        makeHeaderSelectedForThisButton(btFood, btDrug, btOther);
    }

    private void makeHeaderSelectedForThisButton(Button btSelected, Button btNonSelectedA, Button btNonSelectedB)
    {
        //set the button colours
        //shcedule starts selected, so we want it to be white with green font
        btSelected.setBackgroundResource(R.drawable.buttonborder_whitebackground);
        btSelected.setTextColor(Tools.CLINIX_GREEN);
        //and the other buttons, green with white text
        btNonSelectedA.setBackgroundResource(R.drawable.buttonborder_greenbackground);
        btNonSelectedA.setTextColor(Tools.CLINIX_WHITE);

        btNonSelectedB.setBackgroundResource(R.drawable.buttonborder_greenbackground);
        btNonSelectedB.setTextColor(Tools.CLINIX_WHITE);
    }
    private void fillUpList()
    {
        _("fillUpList///////////////");
        listView = (ListView)findViewById(R.id.mylist);

        //make adapter and set it, this updates the view
        switch(state)
        {
            case ALLERGIES_FOOD:
                adapter = new AllergiesAdapter(this, itemListFood);
                _("list size: "+itemListFood.size());
                break;
            case ALLERGIES_DRUG:
                adapter = new AllergiesAdapter(this, itemListDrugs);
                _("list size: "+itemListDrugs.size());
                break;
            case ALLERGIES_OTHER:
                adapter = new AllergiesAdapter(this, itemListOthers);
                _("list size: "+itemListOthers.size());
                break;
        }

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                _("////////////////////////clicked on list/////////////////////");
                //since only one can be the default we need to make sure all others are flagged as false now
                ///// adapter.flagThisItemAsDefault(position,a);
                Allergy a = null;
                switch (state) {
                    case ALLERGIES_FOOD:
                        a = itemListFood.get(position);
                        break;
                    case ALLERGIES_DRUG:
                        a = itemListDrugs.get(position);
                        break;
                    case ALLERGIES_OTHER:
                        a = itemListOthers.get(position);
                        break;
                }
                _("" + a.allergy);

                //VIEW ALLERGY
                Intent intent = new Intent(c, AllergyDetailsActivity.class);
                _("id to pass over:" + a.id);
                intent.putExtra("id", a.id);
                c.startActivity(intent);
            }
        });
    }


    private void doNetworkingToGetAllergies()//String firstName, String lastName, String emailAddress, String password, String dob, String nhsNumber, String surgery_id)
    {
        _("doNetworkingToGetAllergies=========================================");
        if (!Tools.isNetworkAvailable(this) )
        {
            Tools.toast("This app needs an active Internet connection to work.",c);
            return;
        }

        //Do networking!
        String messageForProgressBar = "Getting allergies";
        String url = Networking.URL_GET_MY_PATIENT_ALLERGIES;//+"?all=1";
        int nState = Networking.NETWORK_STATE_GET_MY_PATIENT_ALLERGIES;

        Networking n = new Networking(messageForProgressBar, c, true);
       // n.setRegisterInfo(firstName,lastName, emailAddress,password,dob,nhsNumber,surgery_id);// we call this specific method for this state so right vars are set up.
        n.execute(url, nState);
    }

    //we use our own actionbar to avoid the crap real one
    private void setupFakeActionBar()
    {
        //fake actionbar menu button
        //we use this for:
        //-change password
        //-save
        //
        //This is basically a fake menu that pops up
        final LinearLayout fakeActionBarButtonHolder = (LinearLayout) findViewById(R.id.fakeActionBarButtonHolder);
        //hide it right away
        fakeActionBarButtonHolder.setVisibility(View.GONE);

        ImageButton ibMenu = (ImageButton)    findViewById(R.id.ibMenu);
        ibMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ibMenu hit.");
                Tools.toggleActionBarMenu(fakeActionBarButtonHolder);
            }
        });
        //and its buttons:
        Button btA = (Button)    findViewById(R.id.btA);
        btA.setText(" Add Allergy ");
        btA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("Add Allergy btA hit.");

                //ADD ALLERGY
                Intent intent = new Intent(c, AllergyDetailsActivity.class);
                c.startActivity(intent);
                Tools.toggleActionBarMenu(fakeActionBarButtonHolder);
            }
        });
        Button btB = (Button)    findViewById(R.id.btB);
        btB.setVisibility(View.GONE);
    }



    @Override
    public void onResume()
    {
        super.onResume();
        _("///////////////onResume/////////////");
    }

    //a wrapper so we dont have to type this every time
    private void _(String s) {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", "AllergiesActivity" + "#######" + s);
    }
}
