/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.adapters.AppointmentsAdapter;
import com.clinix.clinicare.clinixv2.common.Networking;
import com.clinix.clinicare.clinixv2.common.Tools;
import com.clinix.clinicare.clinixv2.dataobjs.Appointment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class AppointmentsActivity extends FragmentActivity {

    int state;
    public static final int APPOINTMENTS_SCHEDULED = 1;
    public static final int APPOINTMENTS_PREVIOUS  = 2;

    Context c;
    //since every time we go into a screen it downloads the data again, it seems ok to keep these as static vectors in memory and add to them from the networking
    public static ArrayList<Appointment> itemListScheduled;
    public static ArrayList<Appointment> itemListPrevious;

    public ListView listView;
    AppointmentsAdapter adapter;
    Activity a;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.appointments);
        c = this;
        a = this;

        setupGui();
        doNetworkingToGetAppointments();
        setupFakeActionBar();
    }

    Button btPrevious, btScheduled;
    public void setupGui()
    {
        //buttons at top for filtering

        btPrevious = (Button)findViewById(R.id.btPrevious);
        btPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("//////onClick-btPrevious/////");
                state = APPOINTMENTS_PREVIOUS;
                makeHeaderSelectedForPrevious();
                fillUpList();
            }
        });

        btScheduled = (Button)findViewById(R.id.btScheduled);
        btScheduled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("//////onClick-btScheduled/////");
                state = APPOINTMENTS_SCHEDULED;
                makeHeaderSelectedForScheduled();
                fillUpList();
            }
        });

        //set the button colours
        //shcedule starts selected, so we want it to be white with green font
        btScheduled.setBackgroundResource(R.drawable.buttonborder_whitebackground);
        btScheduled.setTextColor(Tools.CLINIX_GREEN);
        //and the previous button, green with white text
        btPrevious.setBackgroundResource(R.drawable.buttonborder_greenbackground);
        btPrevious.setTextColor(Tools.CLINIX_WHITE);
    }

    private void makeHeaderSelectedForPrevious()
    {
        //set the button colours
        //shcedule starts selected, so we want it to be white with green font
        btPrevious.setBackgroundResource(R.drawable.buttonborder_whitebackground);
        btPrevious.setTextColor(Tools.CLINIX_GREEN);
        //and the previous button, green with white text
        btScheduled.setBackgroundResource(R.drawable.buttonborder_greenbackground);
        btScheduled.setTextColor(Tools.CLINIX_WHITE);
    }

    private void makeHeaderSelectedForScheduled()
    {
        //set the button colours
        //shcedule starts selected, so we want it to be white with green font
        btScheduled.setBackgroundResource(R.drawable.buttonborder_whitebackground);
        btScheduled.setTextColor(Tools.CLINIX_GREEN);
        //and the previous button, green with white text
        btPrevious.setBackgroundResource(R.drawable.buttonborder_greenbackground);
        btPrevious.setTextColor(Tools.CLINIX_WHITE);
    }

    //this is called from the networking once the lists are ready and will immediately fill up
    public void fillUpList()
    {
        _("fillUpList///////////////");
        listView = (ListView)findViewById(R.id.mylist);

        //sort them by date
        if (itemListScheduled!=null)
        {
            Collections.sort(itemListScheduled, new Comparator<Appointment>() {
                public int compare(Appointment a, Appointment b) {
                    return a.dateFriendly.compareTo(b.dateFriendly);
                }
            });
        }

        //sort them by date
        if (itemListPrevious!=null)
        {
            Collections.sort(itemListPrevious, new Comparator<Appointment>() {
                public int compare(Appointment a, Appointment b) {
                    return a.dateFriendly.compareTo(b.dateFriendly);
                }
            });
        }

        //make adapter and set it, this updates the view
        switch(state)
        {
            case APPOINTMENTS_SCHEDULED:
                adapter = new AppointmentsAdapter(this, itemListScheduled);
                break;
            case APPOINTMENTS_PREVIOUS:
                adapter = new AppointmentsAdapter(this, itemListPrevious);
                break;
        }

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                _("////////////////////////clicked on list/////////////////////");
                //since only one can be the default we need to make sure all others are flagged as false now
                ///// adapter.flagThisItemAsDefault(position,a);
                Appointment a = null;
                switch (state) {
                    case APPOINTMENTS_SCHEDULED:
                        a = itemListScheduled.get(position);
                        break;
                    case APPOINTMENTS_PREVIOUS:
                        a = itemListPrevious.get(position);
                        break;
                }
                _("" + a.personWith);
                Intent intent = new Intent(c, AppointmentsSubMenuActivity.class);
                _("id to pass over:" + a.data_id);
                intent.putExtra("id",a.data_id);
                c.startActivity(intent);


            }
        });
    }

    private void doNetworkingToGetAppointments()
    {
        _("doNetworkingToGetAppointments=========================================");
        if (!Tools.isNetworkAvailable(this) )
        {
            Tools.toast("This app needs an active Internet connection to work.",c);
            return;
        }

        //Do networking!
        String messageForProgressBar = "Getting scheduled appointments";
        String url = Networking.URL_GET_APPOINTMENTS_SCHEDULED;
        int nState = Networking.NETWORK_STATE_GET_APPOINTMENTS_SCHEDULED;

        Networking n = new Networking(messageForProgressBar, c, true);
        n.execute(url, nState);

        messageForProgressBar = "Getting previous appointments";
        url = Networking.URL_GET_APPOINTMENTS_PREVIOUS;
        nState = Networking.NETWORK_STATE_GET_APPOINTMENTS_PREVIOUS;

        n = new Networking(messageForProgressBar, c, true);
        n.execute(url, nState);
    }

    //we use our own actionbar to avoid the crap real one
    private void setupFakeActionBar()
    {
        //fake actionbar menu button
        //we use this for:
        //-change password
        //-save
        //
        //This is basically a fake menu that pops up
        final LinearLayout fakeActionBarButtonHolder = (LinearLayout) findViewById(R.id.fakeActionBarButtonHolder);
        //hide it right away
        fakeActionBarButtonHolder.setVisibility(View.GONE);

        ImageButton ibMenu = (ImageButton)    findViewById(R.id.ibMenu);
        ibMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ibMenu hit.");
                Tools.toggleActionBarMenu(fakeActionBarButtonHolder);
            }
        });
        //and its buttons:
        Button btA = (Button)    findViewById(R.id.btA);
        btA.setText(" Add Appointment ");
        btA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("Add Appointment btA hit.");
              //  Tools.toast("Add Appointment. [NOT IMPLEMENTED]", c);
                Intent intent = new Intent(c, BookAppointmentActivity_Page1.class);
                c.startActivity(intent);
                Tools.toggleActionBarMenu(fakeActionBarButtonHolder);
            }
        });
        Button btB = (Button)    findViewById(R.id.btB);
        btB.setVisibility(View.GONE);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        _("///////////////onResume/////////////");
    }

    //a wrapper so we dont have to type this every time
    private void _(String s) {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", "AppointmentsActivity" + "#######" + s);
    }
}
