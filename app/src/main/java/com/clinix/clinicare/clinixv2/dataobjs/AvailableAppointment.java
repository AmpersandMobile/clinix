package com.clinix.clinicare.clinixv2.dataobjs;

import java.util.Date;

/**
 * Created by USER on 11/26/2015.
 */
public class AvailableAppointment {

    public String start;
    public String available;

    public AvailableAppointment(String start_, String available_)
    {
        start=start_;
        available=available_;
    }

    public String getFriendlyDateString()
    {
        String friendlyDateString = "";
        Date d = new Date( Long.parseLong(start)* 1000L );//to get it out of 1970 you need to multiply the Unix timestamp by 1000;
        friendlyDateString = d.toString();
        //this gets us WED JAN 06 07:30:00 GMT
        //we want just 07:30
        friendlyDateString = friendlyDateString.substring(friendlyDateString.indexOf(":")-2, friendlyDateString.indexOf(":")+3);
        return friendlyDateString;
    }
}
