/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.common.Networking;
import com.clinix.clinicare.clinixv2.common.Tools;

public class GenericTextScreenActivity extends Activity {

    Context c;
    Activity a;

    public static int textState;
    public static final int INTRO_SCREEN_A=1;
    public static final int INTRO_SCREEN_B=2;
    public static final int INTRO_SCREEN_C=3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _("GenericTextScreenActivity Born!////////////////////////////////");
        c = this;
        a = this;
        setContentView(R.layout.generic_text_details_screen);

        TextView tvTitle= (TextView)    findViewById(R.id.tvTitle);
        TextView tvText= (TextView)    findViewById(R.id.tvText);

        String text ="";
        switch(textState)
        {
            case INTRO_SCREEN_A:
                _("INTRO_SCREEN_A");
                text = "Clinix will use your location to list local NHS Surgeries that partner with us.\n\n" +
                        "Just select your Surgery and then your GP from the lists shown.\n\n" +
                        "If your Surgery is not listed you can skip the set-up process and manually enter your Surgery and GP details in the Settings section.\n\n" +
                        "Free GP consulttions are only available to NHS patients registered with one of our partner NHS Surgeries.\n\n";
                tvText.setText(text);
                tvTitle.setText("NHS Surgery");
                break;
            case INTRO_SCREEN_B:
                _("INTRO_SCREEN_B");
                text = "Please complete all the information in your Profile and Settings.\n\n" +
                        "If you manually enter your surgery and GP information, you will be registered as a private patient.\n\n" +
                        "There is no charge for private patients to use this app.\n\n" +
                        "As and when your NHS Surgery partners with Clinix, you can charge your registration yto a NHS patient simply by following the 'Select NHS Surgery' process on your GP page\n\n";
                tvText.setText(text);
                tvTitle.setText("Profile & Settings");
                break;
            case INTRO_SCREEN_C:
                _("INTRO_SCREEN_C");
                text = "You can book a video or audio consultation with your GP directly via Clinix on your mobile phone.\n\n" +
                        "We'll confirm your appointment by email and also let you know how to cancel it, if necessary\n\n" +
                        "This may not always be  your own GP but may be another GP at your surgery and with access to your medical records.\n\n" +
                        "You will be able to see the details of the GP you're booked to see.\n\n";
                tvText.setText(text);
                tvTitle.setText("Video Consultation");
                break;
        }



        //this was removed
        /*
        ImageButton idAboutClinix = (ImageButton)    findViewById(R.id.idAboutClinix);
        idAboutClinix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("idAboutClinix hit.");
                Intent intent = new Intent(c, AboutUsActivity.class);
                c.startActivity(intent);
            }
        });
*/
    }


    @Override
    public void onResume() {
        super.onResume();
        _("///////////////onResume/////////////");
       // Tools.sendAnalytics_ScreenView(this, "RegisterActivity");//do a screenview for analytics
    }

    //a wrapper so we dont have to type this every time
    private void _(String s)
    {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", "GenericTextScreenActivity" + "#######" + s);
    }
}
