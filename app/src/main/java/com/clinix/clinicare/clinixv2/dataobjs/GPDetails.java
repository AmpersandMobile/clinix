package com.clinix.clinicare.clinixv2.dataobjs;

/**
 * Created by USER on 07/07/2015.
 */
public class GPDetails {

    public static String gpName;
    public static String surgeryName;
    public static String Address_1;
    public static String Address_2;
    public static String Address_3;
    public static String Address_4;
    public static String tel;
    public static String email;


    public GPDetails(   String gpName_,
                        String surgeryName_,
                        String Address_1_,
                        String Address_2_,
                        String Address_3_,
                        String Address_4_,
                        String tel_,
                        String email_)
    {
        gpName=gpName_;
        surgeryName=surgeryName_;
        Address_1=Address_1_;
        Address_2=Address_2_;
        Address_3=Address_3_;
        Address_4=Address_4_;
        tel=tel_;
        email=email_;
    }
}
