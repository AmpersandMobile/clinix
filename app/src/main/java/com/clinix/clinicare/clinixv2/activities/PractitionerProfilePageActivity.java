/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.common.Networking;
import com.clinix.clinicare.clinixv2.common.Tools;
import com.clinix.clinicare.clinixv2.dataobjs.Practitioner_fromNotification;
import com.clinix.clinicare.clinixv2.dataobjs.SaveKeys;

// a simple register activity class
public class PractitionerProfilePageActivity extends Activity {

    Context c;
    Activity a;
    TextView etName, etSpeciality,etGMCNumber,etProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        _("PractitionerProfilePageActivity Born!////////////////////////////////");
        c = this;
        a = this;
        final Tools t = new Tools(c);
        t.setupPrefs(c);

        setContentView(R.layout.practitionerdetailsscreen);

        etName      = (TextView)    findViewById(R.id.etName);
        etSpeciality      = (TextView)    findViewById(R.id.etSpeciality);
        etGMCNumber      = (TextView)    findViewById(R.id.etGMCNumber);
        etProfile      = (TextView)    findViewById(R.id.etProfile);

        Practitioner_fromNotification p = ListOfGPsAtLocalSurgeryActivity.itemListGPs.get(0);
        etName.setText(p.forename+" "+p.surname);
        etSpeciality.setText(p.specialty);
        etGMCNumber.setText(p.gmc_no);
        etProfile.setText("");


   }

    @Override
    public void onResume() {
        super.onResume();
        _("///////////////onResume/////////////");
       // Tools.sendAnalytics_ScreenView(this, "RegisterActivity");//do a screenview for analytics
    }





    //a wrapper so we dont have to type this every time
    private void _(String s)
    {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", "PractitionerProfilePageActivity" + "#######" + s);
    }
}
