/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ScrollView;

import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.adapters.AppointmentsAdapter;
import com.clinix.clinicare.clinixv2.common.Tools;
import com.clinix.clinicare.clinixv2.dataobjs.Appointment;

import java.util.ArrayList;
import java.util.Date;

public class BookAppointmentActivity_Page1 extends FragmentActivity {

    Context c;
    public ListView listView;
    AppointmentsAdapter adapter;
    Activity a;

    public Date dateofAppointmentRequired;
    public String typeOfSpecialistRequired;

    ImageButton ibNurse       ;
    ImageButton ibDoctor      ;
    ImageButton ibSpecialist  ;

    public static final String SPECIALIST_TYPE_NURSE        = "nurse";
    public static final String SPECIALIST_TYPE_DOCTOR       = "doctor";
    public static final String SPECIALIST_TYPE_SPECIALIST   = "specialist";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.bookappointment1);
        c = this;
        a = this;

        //type of practitioner
        ibNurse       = (ImageButton)    findViewById(R.id.ibNurse);
        ibDoctor      = (ImageButton)    findViewById(R.id.ibDoctor);
        ibSpecialist  = (ImageButton)    findViewById(R.id.ibSpecialist);

        ibNurse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nurseClicked();
            }
        });
        ibDoctor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doctorClicked();
            }
        });
        ibSpecialist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //tell them we add soon//
                Tools.showDialog(a,
                        "Coming soon",
                        "This service will be added soon. We'll email you when it's launched.",
                        "OK",
                        null,
                        Tools.DIALOG_TERMS_AND_CONDITIONS);
            }
        });
        //select the doctor by default
        doctorClicked();

        final Button btSetDate      = (Button)    findViewById(R.id.btSetDate);
        final Button btNextButton   = (Button)    findViewById(R.id.btNextButton);
        btSetDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("btSetDate clicked.");
                Tools.popupDateDialog(btSetDate, btNextButton, a,true);//true means we also get time picker
            }
        });
        btNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("btNextButton clicked.");

                dateofAppointmentRequired = Tools.dateJustPicked;
                //Only let them go if they have set a date
                if (dateofAppointmentRequired==null)
                {
                    Tools.toast("Please set a date for your appointment",c);
                }
                else
                {
                    //the iOS one seems to get appointments JUST before mine, so to make it the same I subtract 5 mins from the date
                    long fiveMins = (1000*60)*5;
                    dateofAppointmentRequired.setTime(dateofAppointmentRequired.getTime()-fiveMins);

                    _("Picked:");
                    _("Type of practitioner: "+typeOfSpecialistRequired);
                    _("Date selected: "+dateofAppointmentRequired.getTime());

                    Date checkDate = new Date(dateofAppointmentRequired.getTime());
                    _("which is "+checkDate.toString());

                    Intent intent = new Intent(c, BookAppointmentActivity_Page2.class);

                    _("typeOfSpecialistRequired to pass over:"+typeOfSpecialistRequired);
                    intent.putExtra("typeOfSpecialistRequired", typeOfSpecialistRequired);

                    _("dateofAppointmentRequired to pass over:"+dateofAppointmentRequired.getTime()+"");
                    intent.putExtra("dateofAppointmentRequired",dateofAppointmentRequired.getTime()+"");

                    c.startActivity(intent);
                }

            }
        });

        //not using embedded date picker now
        /*
        //for some reason having a date picker makes the scrollview move down to it, we need it to stay at top and
        //let the user scroll down so we use:
        final ScrollView scrollView2      = (ScrollView)    findViewById(R.id.scrollView2);

        (new Thread(new Runnable(){
            public void run(){
                scrollView2.pageScroll(View.FOCUS_UP);
            }
        })).start();
        */
    }

    private void nurseClicked()
    {
        _("ibNurse clicked.");
        ibNurse.setImageResource(R.drawable.circleicongp2xticked);
        ibDoctor.setImageResource(R.drawable.circleicongp2x);
        ibSpecialist.setImageResource(R.drawable.circleicongp2x);
        typeOfSpecialistRequired = SPECIALIST_TYPE_NURSE;
    }

    private void doctorClicked()
    {
        _("ibDoctor clicked.");
        ibNurse.setImageResource(R.drawable.circleicongp2x);
        ibDoctor.setImageResource(R.drawable.circleicongp2xticked);
        ibSpecialist.setImageResource(R.drawable.circleicongp2x);
        typeOfSpecialistRequired = SPECIALIST_TYPE_DOCTOR;
    }

    private void specialistClicked()
    {
        _("ibSpecialist clicked.");
        ibNurse.setImageResource(R.drawable.circleicongp2x);
        ibDoctor.setImageResource(R.drawable.circleicongp2x);
        ibSpecialist.setImageResource(R.drawable.circleicongp2xticked);
        typeOfSpecialistRequired = SPECIALIST_TYPE_SPECIALIST;
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        final ScrollView scrollView2      = (ScrollView)    findViewById(R.id.scrollView2);
        if (hasFocus) {
            scrollView2.fullScroll(View.FOCUS_UP);
        }
    }
    @Override
    public void onResume()
    {
        super.onResume();
        _("///////////////onResume/////////////");
    }

    //a wrapper so we dont have to type this every time
    private void _(String s) {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", "BookAppointmentActivity_Page1" + "#######" + s);
    }
}
