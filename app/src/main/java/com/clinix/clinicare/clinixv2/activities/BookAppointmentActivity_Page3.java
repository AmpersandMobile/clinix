/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.adapters.AppointmentsAdapter;
import com.clinix.clinicare.clinixv2.common.Networking;
import com.clinix.clinicare.clinixv2.common.Tools;
import com.clinix.clinicare.clinixv2.dataobjs.Appointment;
import com.clinix.clinicare.clinixv2.dataobjs.Practitioner_fromNotification;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class BookAppointmentActivity_Page3 extends FragmentActivity {

    Context c;
    private ArrayList<Appointment> itemList;
    public ListView listView;
    AppointmentsAdapter adapter;
    Activity a;

    String mySelectedAppointmentTime    ;
    String detailsOfTheProblem          ;
    String preferredCallType            ;
    String typeOfSpecialistRequired     ;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.appointmentdetails);//we use the same xml and tweak itn slightly for this and view appointment details
        c = this;
        a = this;

        TextView tvTitle   = (TextView)    findViewById(R.id.tvTitle);
        tvTitle.setText("Confirmation");

        //receive all appointment details so we can book it on the server
        mySelectedAppointmentTime    = getIntent().getStringExtra("mySelectedAppointmentTime");
        detailsOfTheProblem          = getIntent().getStringExtra("detailsOfTheProblem");
        preferredCallType            = getIntent().getStringExtra("preferredCallType");
        typeOfSpecialistRequired     = getIntent().getStringExtra("typeOfSpecialistRequired");
        _("Appointment details:");
        _("mySelectedAppointmentTime:"+mySelectedAppointmentTime);
        _("detailsOfTheProblem:"+detailsOfTheProblem);
        _("preferredCallType:" + preferredCallType);
        _("typeOfSpecialistRequired:" + typeOfSpecialistRequired);
        //

        //home button
        ImageButton ibHomeButton = (ImageButton)    findViewById(R.id.ibHomeButton);
        ibHomeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ibHomeButton hit.");
                Intent intent = new Intent(c, MainMenuActivity.class);
                c.startActivity(intent);
            }
        });

        //this button simply takes them to their list of appointments
        Button btViewDetails = (Button)    findViewById(R.id.btViewDetails);
        btViewDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("btViewDetails hit.");
                Intent intent = new Intent(c, AppointmentsActivity.class);
                c.startActivity(intent);
            }
        });

        Button btAddToCalendar = (Button)    findViewById(R.id.btAddToCalendar);
        btAddToCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("btAddToCalendar hit.");
                addCalendarEvent();
            }
        });

        doNetworkingToBookAppointment();
    }

    public void addCalendarEvent()
    {
        _("addCalendarEvent");
        long startTime = Long.parseLong(calendarStartTime);
        Date startDate = new Date(startTime*1000);
        _("date ot add to calendar is " + startDate.toString());

        //add it to the calendar
        Calendar cal = Calendar.getInstance();
        Intent intent = new Intent(Intent.ACTION_EDIT);
        intent.setType("vnd.android.cursor.item/event");
        intent.putExtra("beginTime", cal.getTimeInMillis());
        //intent.putExtra("allDay", true);
        //intent.putExtra("rrule", "FREQ=YEARLY");
        //intent.putExtra("endTime", cal.getTimeInMillis()+60*60*1000);
        intent.putExtra("title", "Appointment with "+tvPractitioner.getText());
        intent.putExtra("description", "Doctor appointment booked with Clinix app.");
        startActivity(intent);


        //tell them we added//
        Tools.showDialog(a,
                "Appointment Saved",
                "Your appointment has been added to your calendar and we'll give you a reminder 10 minutes prior to the call.",
                "OK",
                null,
                Tools.DIALOG_TERMS_AND_CONDITIONS);

    }

    private void doNetworkingToBookAppointment()
    {
        _("doNetworkingToBookAppointment=========================================");
        if (!Tools.isNetworkAvailable(this) )
        {
            Tools.toast("This app needs an active Internet connection to work.",c);
            return;
        }

        //Do networking!
        String messageForProgressBar = "Booking appointment";
        String url = Networking.URL_BOOK_AN_APPOINTMENT;
        int nState = Networking.NETWORK_STATE_BOOK_AN_APPOINTMENT;
        Networking n = new Networking(messageForProgressBar, c, true);

        Tools t = new Tools(c);
        t.setupPrefs(c);
        String id = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, "id",null,c);
        n.setBookAppointmentInfo(mySelectedAppointmentTime, detailsOfTheProblem, preferredCallType, typeOfSpecialistRequired);// we call this specific method for this state so right vars are set up.
        n.execute(url, nState);
    }


    String calendarStartTime; //for adding it to the calendar we store start time here
    TextView tvDate             ;
    TextView tvTime             ;
    TextView tvPractitioner     ;
    public void populateGUIForBookedAppointment(String start, Practitioner_fromNotification practitioner,String problem)
    {
        _("populateGUIForBookedAppointment");
        calendarStartTime=start;
        if (practitioner==null)
        {
            _("practitioner is null!");
        }
        tvDate             = (TextView)    findViewById(R.id.tvDate);
        tvTime             = (TextView)    findViewById(R.id.tvTime);
        tvPractitioner     = (TextView)    findViewById(R.id.tvPractitioner);

       long startTime = Long.parseLong(start);
       Date startDate = new Date(startTime);

        //this needs to be in the format "Wed 23 December 2015"
        tvDate.setText( Tools.convertStringLongIntoFriendlyDateStringALT(start) );

        //and then the time like this: "11:00"
        Calendar c = Calendar.getInstance();
        c.setTime(startDate);
        String hourString = c.get(Calendar.HOUR_OF_DAY)+"";
        tvTime.setText(hourString);

        tvPractitioner.setText(practitioner.forename + " " + practitioner.surname);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        _("///////////////onResume/////////////");
    }

    //a wrapper so we dont have to type this every time
    private void _(String s) {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", "BookAppointmentActivity_Page3" + "#######" + s);
    }
}
