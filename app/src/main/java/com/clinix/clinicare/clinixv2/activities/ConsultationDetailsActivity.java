/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.TextView;

import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.common.Tools;
import com.clinix.clinicare.clinixv2.dataobjs.Allergy;
import com.clinix.clinicare.clinixv2.dataobjs.Appointment;

public class ConsultationDetailsActivity extends FragmentActivity {

    Context c;
    Activity a;

    Appointment myAppointment;
    String idOfMyAppointment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.consultation_details);
        c = this;
        a = this;
        idOfMyAppointment = getIntent().getStringExtra("id");
        _("id passed into activity: " + idOfMyAppointment);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        _("///////////////onResume/////////////");
        assignMyAppoinjtmentObject();
        populateGUI();
    }

    private void assignMyAppoinjtmentObject()
    {
        //need to go through each list
        for (int i=0; i<AppointmentsActivity.itemListPrevious.size(); i++)
        {
            Appointment a = AppointmentsActivity.itemListPrevious.get(i);
            _("Looking for ID "+idOfMyAppointment+". IS THIS IT? "+a.data_id);
            if (a.data_id.equals(idOfMyAppointment))
            {
                _("Appointment object assigned");
                myAppointment = a;
            }
        }
        for (int i=0; i<AppointmentsActivity.itemListScheduled.size(); i++)
        {
            Appointment a = AppointmentsActivity.itemListScheduled.get(i);
            _("Looking for ID "+idOfMyAppointment+". IS THIS IT? "+a.data_id);
            if (a.data_id.equals(idOfMyAppointment))
            {
                _("Appointment object assigned");
                myAppointment = a;
            }
        }

        if (myAppointment==null)
        {
            _("WARNING myAllergy was not assigned, could not find id: "+idOfMyAppointment);
        }
    }

    private void populateGUI()
    {
        if (myAppointment==null)
        {
            _("warning myHistory is still null!");
        }

        TextView tvConsultationNumber = (TextView)    findViewById(R.id.tvConsultationNumber);
        tvConsultationNumber.setText(""+myAppointment.data_id);//.data_child_id);

        TextView tvDate = (TextView)    findViewById(R.id.tvDate);
        tvDate.setText(""+myAppointment.dateFriendlyAlt);

        TextView tvTime = (TextView)    findViewById(R.id.tvTime);
        tvTime.setText(""+Tools.convertStringLongIntoFriendlyTimeString(myAppointment.data_start));

        TextView tvPractitioner = (TextView)    findViewById(R.id.tvPractitioner);
        tvPractitioner.setText(""+myAppointment.personWith);//.data_practitioner_id);


        TextView tvProblem = (TextView)    findViewById(R.id.tvProblem);
        tvProblem.setText(""+myAppointment.data_problem);
        TextView tvGpInformed = (TextView)    findViewById(R.id.tvGpInformed);
        tvGpInformed.setText(""+myAppointment);
        TextView tvRating = (TextView)    findViewById(R.id.tvRating);
        tvRating.setText(""+myAppointment);
        TextView tvPatientFeedback = (TextView)    findViewById(R.id.tvPatientFeedback);
        tvPatientFeedback.setText(""+myAppointment);

    }

    //a wrapper so we dont have to type this every time
    private void _(String s) {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", "AllergyDetailsActivity" + "#######" + s);
    }
}
