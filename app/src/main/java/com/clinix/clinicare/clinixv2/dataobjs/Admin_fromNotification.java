package com.clinix.clinicare.clinixv2.dataobjs;

import android.util.Log;

import com.clinix.clinicare.clinixv2.common.Tools;

/**
 * Created by USER on 11/16/2015.
 */
public class Admin_fromNotification
{

    String id;
    String title;
    String forename;
    String surname;
    String suffix;
    String email;
    String register_date;
    String surgery_ids;
    String dob;
    String telephone;
    String street;
    String address_2;
    String town;
    String county;
    String postcode;
    String country;
    String gender;
    String image;
    String type;

    public Admin_fromNotification(
            String id_,
            String title_,
            String forename_,
            String surname_,
            String suffix_,
            String email_,
            String register_date_,
            String surgery_ids_,
            String dob_,
            String telephone_,
            String street_,
            String address_2_,
            String town_,
            String county_,
            String postcode_,
            String country_,
            String gender_,
            String image_,
            String type_
    )
    {
        id              = id_;
        title           = title_;
        forename        = forename_;
        surname         = surname_;
        suffix          = suffix_;
        email           = email_;
        register_date   = register_date_;
        surgery_ids     = surgery_ids_;
        dob             = dob_;
        telephone       = telephone_;
        street          = street_;
        address_2       = address_2_;
        town            = town_;
        county          = county_;
        postcode        = postcode_;
        country         = country_;
        gender          = gender_;
        image           = image_;
        type            = type_;

        _("Admin_fromNotification made!");
    }

    //a wrapper so we dont have to type this every time
    private void _(String s)
    {
        if (!Tools.DEBUG){return;}
        Log.d("MyApp", "Admin_fromNotification" + "#######" + s);
    }

}
