/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.adapters.HistoryAdapter;
import com.clinix.clinicare.clinixv2.common.BuildPhotoDialog;
import com.clinix.clinicare.clinixv2.common.Tools;
import com.clinix.clinicare.clinixv2.dataobjs.History;

import java.util.ArrayList;

public class BookAppointment_NotesActivity extends FragmentActivity {

    Context c;
    public static ArrayList<History> itemListHistory;
    public ListView listView;
    HistoryAdapter adapter;
    Activity a;
    ImageView ivMyPic;
    public static String image; // this is the path to any image they specify

    History myHistory;
    String idOfMyHistory;
    EditText etDetails;

    BuildPhotoDialog buildPhotoObject;
    /////public static String details;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.book_appointments_notes_screen);
        c = this;
        a = this;

        idOfMyHistory = getIntent().getStringExtra("id");
        _("id passed into activity: " + idOfMyHistory);

        etDetails = (EditText)    findViewById(R.id.etDetails);
      //  tvNotes.setText("" + myHistory.notes);
     ivMyPic = (ImageView)    findViewById(R.id.ivMyPic);
       ////// doNetworkingToGetHistory();
       /// setupFakeActionBar();



        Button ibAttachPhoto = (Button)    findViewById(R.id.ibAttachPhoto);
        //Profile picture - we need user to be able to tap this and select a photo
        ibAttachPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("user tapped attach pic");
                //start activity
                buildPhotoObject = new BuildPhotoDialog(ivMyPic,a, c, new Tools(c),BuildPhotoDialog.MODE_ADD_PIC_TO_APPOINTMENT);

            }
        });
    }

    ////////////////SELECT PHOTO CODE///////////////
    //the work is done in BuildPhotoDialog object which wraps up all the implementation into a reusable form.
    //for when we get back the photo from the intent
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        _("onActivityResult///////////////////////");
        buildPhotoObject.dealWithResultFromActivity(requestCode, resultCode, data);
    }
    @Override
    public void onPause()
    {
        super.onPause();
        _("///////////////onPause/////////////");
        BookAppointmentActivity_Page2.detailsOfTheProblem = etDetails.getText()+"";

    }


    @Override
    public void onResume()
    {
        super.onResume();
        _("///////////////onResume/////////////");

        if (image!=null && image.length()>0)
        {
            _("Picture for this appointment is "+image);
            ivMyPic = (ImageView)    findViewById(R.id.ivMyPic);
            image = new Tools(c).loadPictureAndSetImage(ivMyPic,image);

        }
        else
        {
            _("this appointment has no picture yet");

        }

     /////   assignMyHistoryObject();
     /////   populateGUI();

    }



    //a wrapper so we dont have to type this every time
    private void _(String s) {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", "BookAppointment_NotesActivity" + "#######" + s);
    }
}
