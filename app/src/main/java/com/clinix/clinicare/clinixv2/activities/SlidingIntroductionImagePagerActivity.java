/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Interpolator;
import android.widget.Button;
import android.widget.Scroller;
import com.clinix.clinicare.clinixv2.adapters.Features_ImagePager_Adapter;
import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.common.Tools;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import java.lang.reflect.Field;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

//this class shows the sliding pager thing that introduces them about the app.
public class SlidingIntroductionImagePagerActivity extends FragmentActivity {

    Activity a;
	private static final String STATE_POSITION = "STATE_POSITION";
	DisplayImageOptions options;
	ViewPager pager;
    com.viewpagerindicator.CirclePageIndicator mIndicator;

	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
        a = this;
        setContentView(R.layout.features_image_pager_holder_for_content);
        _("////////////////////////////////////////////////////////////////////");
        _("////////////////////////////////////////////////////////////////////");
        _("////////////////////////////////////////////////////////////////////");
        _("////////////////////////////////////////////////////////////////////");
        _("////////////////////////////////////////////////////////////////////");
        _("////////////////////////////////////////////////////////////////////");
        _("///////APP BORN//////CLINIX "+ Tools.VERSION);
        _("////////////////////////////////////////////////////////////////////");
        _("////////////////////////////////////////////////////////////////////");
        _("////////////////////////////////////////////////////////////////////");
        _("////////////////////////////////////////////////////////////////////");
        _("////////////////////////////////////////////////////////////////////");
        _("////////////////////////////////////////////////////////////////////");
		_("SlidingIntroductionImagePagerActivity Born!/////////////////////////");

        //if they have already registered, then we auto log them in ?
   ///////     autoLoginIfApplicable();

        //
        Button btRegister  = (Button) findViewById(R.id.btRegister);
        btRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("btRegister hit");
                Intent intent = new Intent(a, RegisterActivity.class);
                a.startActivity(intent);
            }
        });

        Button btLogin  = (Button) findViewById(R.id.btLogin);
        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("btLogin hit");
                Intent intent = new Intent(a, LoginActivity.class);
                a.startActivity(intent);
            }
        });
        setupSlidingPager(savedInstanceState);
	}

    long whenUserTouchedPager;
    private void setupSlidingPager(Bundle savedInstanceState)
    {
        _("setupSlidingPager");
        String[] imageUrls = null;	//bundle.getStringArray(Extra.IMAGES);
        int pagerPosition = 0;		//bundle.getInt(Extra.IMAGE_POSITION, 0);
        if (savedInstanceState != null)
        {
            pagerPosition = savedInstanceState.getInt(STATE_POSITION);
        }
        options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.ic_empty)
                .showImageOnFail(R.drawable.ic_error)
                .resetViewBeforeLoading(true)
                .cacheOnDisc(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .displayer(new FadeInBitmapDisplayer(300))
                .build();
        _(".........");
        pager = (ViewPager) findViewById(R.id.pager);
        _("making NEW Features_ImagePager_Adapter");

        //Tools.getRealPathFromURI ( getApplicationContext(),Tools.resIdToUri(getApplicationContext(),R.drawable.intro_bg_1) )
        //"intro_bg_1",//We can even use live urls here: "http://www.online-image-editor.com//styles/2014/images/example_image.png",

        imageUrls = new String[]{
        //here I am passing paths to the drawables we want as backgrounds.
                //but avctually the real images ar especified in the xml now..
                "drawable://" + R.drawable.screendummy,
                "drawable://" + R.drawable.screendummy,
                "drawable://" + R.drawable.screendummy,
                "drawable://" + R.drawable.screendummy,
                "drawable://" + R.drawable.screendummy,
              //  "drawable://" + R.drawable.screendummy, //last 2 pages are now turned off.
              //  "drawable://" + R.drawable.screendummy,
        };

        pager.setAdapter(new Features_ImagePager_Adapter(imageUrls, options, this, getSupportFragmentManager(), Features_ImagePager_Adapter.PAGER_ADAPTER__FEATURES));//,getSupportFragmentManager()));
        _("adapter made.");

        //ADD THE CIRCLE INDICATORS
        mIndicator = (com.viewpagerindicator.CirclePageIndicator) findViewById(R.id.indicator);
        mIndicator.setViewPager(pager);
        mIndicator.setCurrentItem(0);//pagerAdapter.getCount() - 1);
        pager.setCurrentItem(pagerPosition);

        //We need to detect user input, as soon as we do then the pager shouldnt auto move unless we dont touch it for 4 seconds
        pager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //_("pager touched.");
                //when the user touches the pager we make sure the auto timer doesnt do anything for X seconds
                whenUserTouchedPager = System.currentTimeMillis();
                return false; //return false here so the OS still consume the touch normally letting us scroll
            }
        });

        //custom hack to make the scroll speed slow when we programatically change it
        try {
            Field mScroller = null;
            mScroller = ViewPager.class.getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            ViewPagerScroller scroller = new ViewPagerScroller(pager.getContext());
            mScroller.set(pager, scroller);
        } catch (Exception e) {
            _("error of change scroller "+e.getMessage());
        }
        /////
        if (Tools.SHOW_DEBUG_TOASTS || Tools.DEBUG)
        {
            Tools.toast("CLINIX V"+Tools.VERSION+" DEBUG.",this);
        }
    }

    private ScheduledExecutorService scheduleTaskExecutor;
    int page=0;
    long timeWhenPageLastTurned;

    private void kickOffMovePageTimer()
    {
        final int secondsBetweenRuns = 1;
        final int secondsBetweenSwipes = 4;
        final int initialDelay = 4;
        //This schedule a task to run every X seconds:
        //we actually run it quicker than we need, because we want to detect if a user has touched, and if so not move for another 4 seconds,
        //but we wont get exactly 4 seconds if we simply bail out on every 4 second check,
        scheduleTaskExecutor.scheduleAtFixedRate(new Runnable() {
            public void run() {
                //DO WORK HERE TO MOVE TO NEXT PAGE//
                //If you need update UI, simply do this:
                runOnUiThread(new Runnable() {
                    public void run() {
                        //we do not automate if 4 seconds hasnt passed..
                        if (System.currentTimeMillis()-timeWhenPageLastTurned>=(secondsBetweenSwipes*1000)-200)//200ms leeway makes it feel right stays within 4 sec
                        {
                            ////_("4 SECS HAS PASSED");
                        }
                        else
                        {
                            ////_("4 SECS HAS NOT PASSED ONLY: "+(System.currentTimeMillis()-timeWhenPageLastTurned));
                            return;
                        }

                        //we do not automate it if the user touched the pager within the last 4 sec
                        long timeSinceTouched = System.currentTimeMillis()-whenUserTouchedPager;
                        if ( timeSinceTouched >= secondsBetweenSwipes*1000 )
                        {
                            ////_("Will auto scroll since time since touched is "+timeSinceTouched);
                        }
                        else
                        {
                            ////_("Has not been four seconds since they touched, its been "+timeSinceTouched);
                            return;
                        }

                        page = pager.getCurrentItem()+1;
                        ////_("Move to page "+page);
                        if (page==5)//7)
                        {
                            ////_("loop");
                            page=0;
                        }
                        timeWhenPageLastTurned = System.currentTimeMillis();
                        pager.setCurrentItem(page,true);
                        //if considered too fast implement this http://stackoverflow.com/questions/8155257/slowing-speed-of-viewpager-controller-in-android/9731345#9731345

                    }
                });
            }
        }, initialDelay, secondsBetweenRuns, TimeUnit.SECONDS);// TimeUnit.MINUTES);
        //THIS HAPPENS EVERY X SECONDS
    }

    public class ViewPagerScroller extends Scroller {

        private int mScrollDuration = 600;// delay
        public ViewPagerScroller(Context context) {
            super(context);
        }
        public ViewPagerScroller(Context context, Interpolator interpolator) {
            super(context, interpolator);
        }
        @Override
        public void startScroll(int startX, int startY, int dx, int dy, int duration) {
            super.startScroll(startX, startY, dx, dy, mScrollDuration);
        }
        @Override
        public void startScroll(int startX, int startY, int dx, int dy) {
            super.startScroll(startX, startY, dx, dy, mScrollDuration);
        }

    }

/*
    private void autoLoginIfApplicable()
    {
        //check by seeing if their name id stored.
        Tools t = new Tools();
        t.setupPrefs(this);

        String userid       = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, "id",null,this);
        String rememberme   = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, "rememberme",null,this);
        if ( userid == Tools.ERRORs)
        {
            _("Nothing was stored about this user, let them continue to register or login");
        }
        else
        {
            //0.2.0 turned this off now, so they have to login properly, we use this for AUTO LOGIN (if we want that feature later)
            boolean AUTO_LOGIN=false;
            //0.2.8 if no net then auto by pass
            if (!Tools.isNetworkAvailable() )
            {
                if (Tools.APP_CAN_FUNCTION_IN_OFFLINE_MODE)
                {
                    AUTO_LOGIN=true;
                    Tools.toast("No Internet detected, skipped login",this);
                }
                else
                {
                    //Tools.toast("Currently this app needs an active Internet connection to work.",this);
                }

            }
            _("AUTO_LOGIN.....:"+AUTO_LOGIN);
            if (AUTO_LOGIN)
            {
                _("We have the details of someone already stored in here (userid: "+userid+"), lets go right into app ");
                Tools.moveOnToNextActivity(Networking.NETWORK_STATE_LOGIN,this);
                return;
            }
        }
    }
*/
	@Override
	public void onSaveInstanceState(Bundle outState)
    {
        _("onSaveInstanceState");
        if (pager==null)
        {
            _("pager was still null cant save position (did you auto login?)");
        }
        else
        {
            outState.putInt(STATE_POSITION, pager.getCurrentItem());
        }
	}

    //these are called from xml in the adapter
    public void signUpHit(View view)
    {
        _("Sign up hit!");

        if (Tools.USE_ANALYTICS)
        {
          //  Tools.sendAnalytics_ButtonPress(a, "signUpHit");
        }

      //  Intent intent = new Intent(this, RegisterActivity.class);
      //  startActivity(intent);
    }
    public void loginHit(View view)
    {
        _("Login hit!");

        if (Tools.USE_ANALYTICS)
        {
        //    Tools.sendAnalytics_ButtonPress(a, "loginHit");
        }

       // Intent intent = new Intent(this, LoginActivity.class);
       // startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        _("///////////////onResume/////////////");
        //Tools.sendAnalytics_ScreenView(this, "SlidingIntroductionImagePagerActivity");//do a screenview for analytics
        //for this to auto scroll we ned a timer which changes the current item
        scheduleTaskExecutor = Executors.newScheduledThreadPool(0);//5);
        kickOffMovePageTimer();
    }
    @Override
    public void onStop() {
        super.onStop();
        _("///////////////onStop/////////////");
        scheduleTaskExecutor.shutdown();
        //Tools.sendAnalytics_ScreenView(this, "SlidingIntroductionImagePagerActivity");//do a screenview for analytics
    }

	//wrapper for system outs
    public static final String HASHES           = "###### "      ; // for system.out stuff: we want them all to line up nicely so these must differ between classes
	public static final String TAG              = "SlidingIntroductionImagePagerActivity"        ; // tag for system.out stuff

	//this is simply a wrapper around Log (easier to type _ than Log etc) 
	public static void _(String s)
   {
       if (!Tools.DEBUG)
       {
           return;
       }
           Log.d("MyApp",TAG+HASHES+s);
   }

}