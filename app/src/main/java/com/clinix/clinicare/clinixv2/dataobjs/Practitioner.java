package com.clinix.clinicare.clinixv2.dataobjs;

/**
 * Created by USER on 28/10/2015.
 */
public class Practitioner
{
    String id;
    String title;
    String forename;
    String surname;
    String suffix;
    String email;
    String register_date;
    String telephone;
    String address;
    String postcode;
    String type;
    String surgery_ids;
    String account_status;
    String profession;
    String specialty;
    String registered_date;
    String information;
    String information_date;
    String signature;
    String signature_date;
    String initial;
    String follow_up;

}
