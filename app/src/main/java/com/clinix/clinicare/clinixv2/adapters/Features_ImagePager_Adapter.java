/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.common.Tools;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

/*
import yodj.za.co.urbian.yodj.R;
import yodj.za.co.urbian.yodj.activities.PharmacyActivity;
import yodj.za.co.urbian.yodj.activities.RegisterActivity;
import yodj.za.co.urbian.yodj.common.Tools;
import yodj.za.co.urbian.yodj.dataobjs.DataWrapper;
import yodj.za.co.urbian.yodj.music.MusicPlayerActivity;
*/
/**
 * YODJ App Developed for URBIAN by GARETH MURFIN www.garethmurfin.co.uk
 */
//this class is the adapter for either of our sliding pagerviews, depending on the state its set to
//theres one pre login when they load up, and theres one on the music selection screen the first time they load up
public  class Features_ImagePager_Adapter extends PagerAdapter
{
    public static final int PAGER_ADAPTER__FEATURES         = 1; //5 pages explaining what yodj can do
    //as of 0.2.4 theres 2 substates for PAGER_ADAPTER__INSTRUCTIONS
    public static final int PAGER_ADAPTER__INSTRUCTIONS_music_selection_screen      = 2; //2 pages telling them to how to use the app - tick the songs etc
    public static final int PAGER_ADAPTER__INSTRUCTIONS_music_player_screen         = 3; //2 pages telling them to how to use the app - tick the songs etc
    int state = PAGER_ADAPTER__FEATURES;

	private String[] images;
	private LayoutInflater inflater;
	DisplayImageOptions options;
	Activity a;
	public FragmentManager fm;

	public Features_ImagePager_Adapter(String[] images, DisplayImageOptions options_, Activity a_, FragmentManager fm_, int state_) {
		//super(fm_);
		_("Features_ImagePager_Adapter made");
        fm=fm_;
		this.images = images;
		inflater = a_.getLayoutInflater();
		a=a_;
		options=options_;
        state = state_;

        initImageLoader(a);
	}

	@Override
	public int getItemPosition(Object object) {
	    return POSITION_NONE;
	}
	
	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		((ViewPager) container).removeView((View) object);
		//_("destroyItem");
	}

	@Override
	public void finishUpdate(View container)
    {
		//_("finishUpdate");
	}

	@Override
	public int getCount() 
	{
        if (images==null)
        {
            _("WARNING VIEW-PAGER-IMAGES ARE NULL!!!");
        }
		return images.length; 
	}

	@Override
	public Object instantiateItem(ViewGroup view, final int position) 
	{
		//_("%%%%% instantiateItem MAKING A PAGE FOR THE VIEWPAGER "+position);

        if (inflater==null)
        {
            _("Warning inflater is null this will crash!");
        }
		//seems we can run out of memory here...ive set caches accordingly (search for Gaz)

        //this view is different depending on what state we are in
        ImageView imageView = null;
        //spec changed and caused some weirdness here, just using arrays to get around final.
        final ProgressBar[] spinner = new ProgressBar[1];   //only an array to get around Java nonsense
        final View[] imageLayout    = new View[1];          // only an array to get around Java nonsense
        switch (state)
        {
            case PAGER_ADAPTER__FEATURES:
                //_("PAGER_ADAPTER__FEATURES");

                imageLayout[0] = inflater.inflate(   R.layout.features_individual_item_pager_screen,     view, false);
                if (imageLayout==null)
                {
                    _("Warning imageLayout is null! This will crash! ");
                }

                imageView = (ImageView) imageLayout[0].findViewById(R.id.icon);
                //_("OK we will set image now, the IV is "+imageView.getWidth()+" X "+imageView.getHeight()+"");
                spinner[0] = (ProgressBar) imageLayout[0].findViewById(R.id.progressBar1);

                //depending on what page we are on, we turn on and off the linears that hold the UI
                LinearLayout page1lin = (LinearLayout) imageLayout[0].findViewById(R.id.page1);
                LinearLayout page2lin = (LinearLayout) imageLayout[0].findViewById(R.id.page2);
                LinearLayout page3lin = (LinearLayout) imageLayout[0].findViewById(R.id.page3);
                LinearLayout page4lin = (LinearLayout) imageLayout[0].findViewById(R.id.page4);
                LinearLayout page5lin = (LinearLayout) imageLayout[0].findViewById(R.id.page5);
                LinearLayout page6lin = (LinearLayout) imageLayout[0].findViewById(R.id.page6);
                LinearLayout page7lin = (LinearLayout) imageLayout[0].findViewById(R.id.page7);

                switch(position)
                {
                    case 0:
                        page1lin.setVisibility(View.VISIBLE);
                        page2lin.setVisibility(View.INVISIBLE);
                        page3lin.setVisibility(View.INVISIBLE);
                        page4lin.setVisibility(View.INVISIBLE);
                        page5lin.setVisibility(View.INVISIBLE);
                        page6lin.setVisibility(View.INVISIBLE);
                        page7lin.setVisibility(View.INVISIBLE);
                        break;
                    case 1:
                        page1lin.setVisibility(View.INVISIBLE);
                        page2lin.setVisibility(View.VISIBLE);
                        page3lin.setVisibility(View.INVISIBLE);
                        page4lin.setVisibility(View.INVISIBLE);
                        page5lin.setVisibility(View.INVISIBLE);
                        page6lin.setVisibility(View.INVISIBLE);
                        page7lin.setVisibility(View.INVISIBLE);
                        break;
                    case 2:
                        page1lin.setVisibility(View.INVISIBLE);
                        page2lin.setVisibility(View.INVISIBLE);
                        page3lin.setVisibility(View.VISIBLE);
                        page4lin.setVisibility(View.INVISIBLE);
                        page5lin.setVisibility(View.INVISIBLE);
                        page6lin.setVisibility(View.INVISIBLE);
                        page7lin.setVisibility(View.INVISIBLE);
                        break;
                    case 3:
                        page1lin.setVisibility(View.INVISIBLE);
                        page2lin.setVisibility(View.INVISIBLE);
                        page3lin.setVisibility(View.INVISIBLE);
                        page4lin.setVisibility(View.VISIBLE);
                        page5lin.setVisibility(View.INVISIBLE);
                        page6lin.setVisibility(View.INVISIBLE);
                        page7lin.setVisibility(View.INVISIBLE);
                        break;
                    case 4:
                        page1lin.setVisibility(View.INVISIBLE);
                        page2lin.setVisibility(View.INVISIBLE);
                        page3lin.setVisibility(View.INVISIBLE);
                        page4lin.setVisibility(View.INVISIBLE);
                        page5lin.setVisibility(View.VISIBLE);
                        page6lin.setVisibility(View.INVISIBLE);
                        page7lin.setVisibility(View.INVISIBLE);
                        break;
                    case 5:
                        page1lin.setVisibility(View.INVISIBLE);
                        page2lin.setVisibility(View.INVISIBLE);
                        page3lin.setVisibility(View.INVISIBLE);
                        page4lin.setVisibility(View.INVISIBLE);
                        page5lin.setVisibility(View.INVISIBLE);
                        page6lin.setVisibility(View.VISIBLE);
                        page7lin.setVisibility(View.INVISIBLE);
                        break;
                    case 6:
                        page1lin.setVisibility(View.INVISIBLE);
                        page2lin.setVisibility(View.INVISIBLE);
                        page3lin.setVisibility(View.INVISIBLE);
                        page4lin.setVisibility(View.INVISIBLE);
                        page5lin.setVisibility(View.INVISIBLE);
                        page6lin.setVisibility(View.INVISIBLE);
                        page7lin.setVisibility(View.VISIBLE);
                        break;
                    default:
                        _("////////////////////////////////////////////// UNKNOWN PAGE ON VIEW PAGER "+position);
                }
                break;
         /*   case PAGER_ADAPTER__INSTRUCTIONS_music_selection_screen:
            case PAGER_ADAPTER__INSTRUCTIONS_music_player_screen:

                _("PAGER_ADAPTER__INSTRUCTIONS");
                imageLayout[0] = inflater.inflate(   R.layout.instructions_individual_item_pager_screen, view, false);
                if (imageLayout[0]==null)
                {
                    _("Warning imageLayout is null! This will crash! ");
                }

                imageView = (ImageView) imageLayout[0].findViewById(R.id.icon);
                _("OK we will set image now, the IV is "+imageView.getWidth()+" X "+imageView.getHeight()+"");
                spinner[0] = (ProgressBar) imageLayout[0].findViewById(R.id.progressBar1);

                //in order to let them close the instructions when they hit the X, I have simply overlaid a large
                //button at the top of the screen that takes up about 1/3 of screen, we keep this on but hide it
                Button btCloseInstructions = (Button) imageLayout[0].findViewById(R.id.btCloseInstructions);
                btCloseInstructions.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        _("btCloseInstructions hit!");

                        //just send them back to the same activity, since its not their same run anymore (this was) the instructions wont
                        //show it will show the music selection activity
                        switch (state)
                        {
                            case PAGER_ADAPTER__INSTRUCTIONS_music_selection_screen:
                                _("PAGER_ADAPTER__INSTRUCTIONS_music_selection_screen");
                                Intent intent = new Intent(a, PharmacyActivity.class);
                                a.startActivity(intent);
                                break;
                            case PAGER_ADAPTER__INSTRUCTIONS_music_player_screen:
                                _("PAGER_ADAPTER__INSTRUCTIONS_music_player_screen");


                                intent = new Intent(a, MusicPlayerActivity.class);
                                //0.2.5 since the pager was up and we received the songs, but now we are revisiting that activity after the pager
                                //we must pass the activity the songs it received already
                                _("passing back the datawrapper containing the songs, so we can start for real...SHOW_FIRST_TIME_PAGER:" + MusicPlayerActivity.SHOW_FIRST_TIME_PAGER);

                                DataWrapper dw =  ((MusicPlayerActivity)a).dw;
                                intent.putExtra("data", dw);
                                a.finish();//unfortunately this doesnt actually force it to end and call ondestroy, so instead in ondestory
                                // if playIntent is null we bail out early to avoid npe (since it must be this case [ie after instructions on first run],
                                // it doesnt actually rely on this flag we set due to ondestory not being called until a little later!

                                MusicPlayerActivity.SHOW_FIRST_TIME_PAGER=false; //so it wont display again (keep this below finish)
                                _("START ACTIVITY.");
                                a.startActivity(intent);

                                //this didnt work nothing was clickable, so instead i simply revisit the activity after the user
                                //closes the instructions, (see above lines)
                                //_("hide instructions");


                            break;
                        }

                    }
                });
                //btCloseInstructions.setVisibility(View.INVISIBLE);//so we dont see the ugly button, but we can still tap it :)
                //we cant hide it and still click it, we must make it transparent in xml

                //NO BOTTOM HOLDER NOW REMOVE THIS
                //turn off the bottom holder, which is for the other pager

                //// button for closing it///

                break;*/
            default:
                _("WARNING!! UNKNOWN STATE FOR THE ADAPTER!///////////////////////");
        }

	    ImageLoader imageLoader = ImageLoader.getInstance();
        //_("Image loading is: "+images[position]+" position:"+position);
		imageLoader.displayImage(images[position], imageView, options, new SimpleImageLoadingListener()
        {
			@Override
			public void onLoadingStarted(String imageUri, View view) {
                spinner[0].setVisibility(View.VISIBLE);
			}

			@Override
			public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
				String message = null;
                _("FAILED TO LOAD IMAGE!! "+imageUri.toString()+"  REASON: "+failReason.toString());
				switch (failReason.getType()) {

				 default:
						message = "Still downloading image...";//Unknown error";
						break;
				}

			}

			@Override
			public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
				//_("Loading completed ("+imageUri.toString()+"). hide spinner "+loadedImage);
                spinner[0].setVisibility(View.GONE);
			}
		});


		((ViewPager) view).addView(imageLayout[0], 0);
		return imageLayout[0];
	}

    public static void initImageLoader(Context context) {
        _("##################################initImageLoader###################################");

        // This configuration tuning is custom. You can tune every option, you may tune some of them,
        // or you can create default configuration by
        //  ImageLoaderConfiguration.createDefault(this);
        // method.
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                /////GAZ TURN ON CACHING
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                .memoryCacheSize(2 * 1024 * 1024)
                /////
                .denyCacheImageMultipleSizesInMemory()
                .discCacheFileNameGenerator(new Md5FileNameGenerator())
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                        ///////.writeDebugLogs() // Remove for release app
                .build();

        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config);
    }
	
	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view.equals(object);
	}

	@Override
	public void restoreState(Parcelable state, ClassLoader loader) {
	}

	@Override
	public Parcelable saveState() {
		return null;
	}

	@Override
	public void startUpdate(View container) {
		////RFR//_("startUpdate");
	}
	
	//wrapper for system outs
    public static final String HASHES           = "################ "      ; // for system.out stuff: we want them all to line up nicely so these must differ between classes
	public static final String TAG              = "Features_ImagePager_Adapter"        ; // tag for system.out stuff
	//this is simply a wrapper around Log (easier to type _ than Log etc)
    public static void _(String s) {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", TAG + HASHES + s);
    }
}