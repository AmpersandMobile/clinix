/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.common.Networking;
import com.clinix.clinicare.clinixv2.common.Tools;
import com.clinix.clinicare.clinixv2.dataobjs.Allergy;
import com.clinix.clinicare.clinixv2.dataobjs.GPDetails;
import com.clinix.clinicare.clinixv2.dataobjs.Practitioner;
import com.clinix.clinicare.clinixv2.dataobjs.Practitioner_fromNotification;
import com.clinix.clinicare.clinixv2.dataobjs.Surgery;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

public class SurgeryDetailsActivity extends Activity  implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    Context c;
    Activity a;
    Tools t;

    public static Surgery mySurgery;
    //public static Practitioner_fromNotification myGP;

    private EditText etGPName;
    private EditText etSurgeryName;
    private EditText etAddressA;
    private EditText etAddressB;
    private EditText etAddressC;
    private EditText etAddressD;
    private EditText etAddressE;
    private EditText etAddressF;
    private EditText etTelephone;
    private EditText etEmail;
    private Button btSave;

    //For loading and saving.
    public static final String SAVE_KEY_GPNAME         =   "SAVE_KEY_GPNAME";
    public static final String SAVE_KEY_SURGERYNAME    =   "SAVE_KEY_SURGERYNAME";
    public static final String SAVE_KEY_SURGERYADDRESS1=   "SAVE_KEY_SURGERYADDRESS1";
    public static final String SAVE_KEY_SURGERYADDRESS2=   "SAVE_KEY_SURGERYADDRESS2";
    public static final String SAVE_KEY_SURGERYADDRESS3=   "SAVE_KEY_SURGERYADDRESS3";
    public static final String SAVE_KEY_SURGERYADDRESS4=   "SAVE_KEY_SURGERYADDRESS4";
    public static final String SAVE_KEY_SURGERYTEL     =   "SAVE_KEY_SURGERYTEL";
    public static final String SAVE_KEY_SURGERYEMAIL   =   "SAVE_KEY_SURGERYEMAIL";

    GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _("SurgeryDetailsActivity Born!////////////////////////////////");
        c = this;
        a = this;
        setContentView(R.layout.surgerydetails);

        //for getting location if they wish to search for surgeries
        buildGoogleApiClient();
    }


    /**
     * Builds a GoogleApiClient. Uses the addApi() method to request the LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }



    @Override
    public void onResume() {
        super.onResume();
        _("///////////////onResume/////////////");

        t = new Tools(c);
        t.setupPrefs(c);

        setupGUI();
        doNetworkingToGetMyGP();
        //COME FROM NETWORK NOW I THINKL 0.3.1 loadGPDetails();
    }

    private void setupGUI()
    {
        etGPName = (EditText)findViewById( R.id.etGPName );
        etSurgeryName = (EditText)findViewById( R.id.etSurgeryName );
        etAddressA = (EditText)findViewById( R.id.etAddressA );
        etAddressB = (EditText)findViewById( R.id.etAddressB );
        etAddressC = (EditText)findViewById( R.id.etAddressC );
        etAddressD = (EditText)findViewById( R.id.etAddressD );
        etAddressE = (EditText)findViewById( R.id.etAddressE );
        etAddressF = (EditText)findViewById( R.id.etAddressF );
        etTelephone = (EditText)findViewById( R.id.etTelephone );
        etEmail = (EditText)findViewById( R.id.etEmail );
        btSave = (Button)findViewById( R.id.btSave );

        //Save hit
       /* Button btSave = (Button) findViewById(R.id.btSave);
        btSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("btSave HIT");
                saveGPDetails();
            }
        });
*/
        // i put a button here because we get sent back gere after settinga new surgery and get, and pressing back would go back through
        // that process, so it makes more sense to have a home key, I could turn this off until that exact case happens, but for now I will leave it here
        // and see what happens
        //home button
        ImageButton ibHomeButton = (ImageButton)    findViewById(R.id.ibHomeButton);
        ibHomeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ibHomeButton hit.");
                Intent intent = new Intent(c, MainMenuActivity.class);
                c.startActivity(intent);
            }
        });

        Button  ibSelectNHSsurgery= (Button) findViewById( R.id.ibSelectNHSsurgery );
        ibSelectNHSsurgery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ibSelectNHSsurgery");
                Intent intent = new Intent(c, NearbyNHSSurgeriesActivity.class);
                c.startActivity(intent);

            }
        });


        setupFakeActionBar();
    }

    public void populateGUI()
    {
        if (mySurgery==null)
        {
            _("WARNING mySurgery IS NULL SO WE CANNOT POPULATE THE GUI!..BAILING");
            return;
        }
        etGPName.setText("");etGPName.setVisibility(View.GONE);//removed
        etSurgeryName.setText(mySurgery.name);//myGP.suffix);
        etAddressA.setText(mySurgery.street);
        etAddressB.setText(mySurgery.address_2);
        etAddressC.setText(mySurgery.town);
        etAddressD.setText(mySurgery.county);
        etAddressE.setText(mySurgery.postcode);
        etAddressF.setText(mySurgery.country);
        etTelephone.setText(mySurgery.telephone);
        etEmail.setText(mySurgery.email);


    }

//DOES THIS COME FROM MEMORY OR FROM SERVER EACH TIME ????
    private void doNetworkingToGetMyGP()
    {
        _("doNetworkingToGetMyGP=========================================");
        if (!Tools.isNetworkAvailable(this) )
        {
            Tools.toast("This app needs an active Internet connection to work.",c);
            return;
        }

        //Do networking!
        // hmm this was wrong i think, the screen is now surgery details//
      //  String messageForProgressBar = "Getting GP details";
        //String url = Networking.URL_GET_MY_PATIENT_GP;
       // int nState = Networking.NETWORK_STATE_GET_MY_PATIENT_GP;
        String messageForProgressBar = "Getting surgery details";
        String url = Networking.URL_GET_MY_PATIENT_SURGERY;
        int nState = Networking.NETWORK_STATE_GET_MY_PATIENT_SURGERY;
        Networking n = new Networking(messageForProgressBar, c, true);

        Tools t = new Tools(c);
        t.setupPrefs(c);
        ///String type = "patient"; // this can be admin / practitioner / patient
       // String id = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, "id",null,c);
       // n.setHistoryInfo(id);// we call this specific method for this state so right vars are set up.
        n.execute(url, nState);
    }




    //we use our own actionbar to avoid the crap real one
    private void setupFakeActionBar()
    {
        //fake actionbar menu button
        //we use this for:
        //-change password
        //-save
        //
        final LinearLayout fakeActionBarButtonHolder = (LinearLayout) findViewById(R.id.fakeActionBarButtonHolder);
        if (fakeActionBarButtonHolder==null)
        {
            _("fakeActionBarButtonHolder is null bail.");
            return;
        }
        //hide it right away
        fakeActionBarButtonHolder.setVisibility(View.GONE);

        ImageButton ibMenu = (ImageButton)    findViewById(R.id.ibMenu);
        ibMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ibMenu hit.");
                Tools.toggleActionBarMenu(fakeActionBarButtonHolder);
            }
        });
        //and its buttons:
        Button btA = (Button)    findViewById(R.id.btA);
        btA.setText(" Save ");
        btA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("SAVE btA hit.");
                //   saveGPDetails();

                //my understanding now is that the user will only ever hit save if they are entering details manually,
                //if they do this they are not taking the NHS route, so we warn them

                Tools t = new Tools(c);
                t.setupPrefs(c);
                String USER_IS_NHS_PATIENT = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, ListOfGPsAtLocalSurgeryActivity.USER_IS_NHS_PATIENT, "",c);

                //how often do we show this? only if they have been through the auto process and we have this var saved
                if (USER_IS_NHS_PATIENT.equals("TRUE")) {

                    String text = "Manually adding or changing your GP details will register you as a private patient.\n\n" +
                            "To register as a NHS patient you must follow the auto-registration process to select your NHS Surgery and GP.";
                    Tools.showDialog(a,
                            "Alert",
                            text,
                            "Save",
                            "Cancel",
                            -1);

                    //and now im assuming we dont show this again so we set this var to false
                    t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, USER_IS_NHS_PATIENT, -1, "FALSE");
                }


                //now i dont think we set this on the server anymore? we must save it locally? for now lets save on server and see what happens.
                //MAYBE WE NEED TO SET UP A CUSTOM GP ON SERVER AND ADD DETAILS THERE I THINK WE DO, BUT WAIT FOR SYED TO NOTICE AND EXPLAIN.
                doNetworkingToSaveMyGp();
                Tools.toggleActionBarMenu(fakeActionBarButtonHolder);
            }
        });
        Button btB = (Button)    findViewById(R.id.btB);
        btB.setVisibility(View.GONE);
    }


    private void doNetworkingToSaveMyGp()
    {
        _("doNetworkingToSaveMyGp=========================================");
        if (!Tools.isNetworkAvailable(this) )
        {
            Tools.toast("This app needs an active Internet connection to work.",c);
            return;
        }

        //Do networking!
        String messageForProgressBar = "Updating GP";
        String url = Networking.URL_UPDATE_MY_PATIENT_GP;
        int nState = Networking.NETWORK_STATE_UPDATE_MY_PATIENT_GP;
        Networking n = new Networking(messageForProgressBar, c, true);

        Tools t = new Tools(c);
        t.setupPrefs(c);

        //make a GPDetails object
        String gpName       = etGPName.getText()+"";
        String surgeryName  = etSurgeryName.getText()+"";;
        String Address_1    = etAddressA.getText()+"";;
        String Address_2    = etAddressB.getText()+"";;
        String Address_3    = etAddressC.getText()+"";;
        String Address_4    = etAddressD.getText()+"";;
        String tel          = etTelephone.getText()+"";;
        String email        = etEmail.getText()+"";;

        GPDetails gpd = new GPDetails(gpName,surgeryName,Address_1,Address_2,Address_3,Address_4,tel,email );
        n.setMyGPDetails(gpd);
        n.execute(url, nState);

    }

    /////////////////////////////////////////////////////////////////////////////////////
    ///LOADING AND SAVING CODE, FOR GP DETAILS//////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////
/*
    private void saveGPDetails()
    {
        GPDetails.gpName = etGPName.getText()+"";
        t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_GPNAME, -1, GPDetails.gpName);

        GPDetails.surgeryName = etSurgeryName.getText()+"";
        t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_SURGERYNAME, -1, GPDetails.surgeryName );

        GPDetails.Address_1 = etAddressA.getText()+"";
        t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_SURGERYADDRESS1, -1, GPDetails.Address_1 );

        GPDetails.Address_2 = etAddressB.getText()+"";
        t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_SURGERYADDRESS2, -1, GPDetails.Address_2 );

        GPDetails.Address_3 = etAddressC.getText()+"";
        t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_SURGERYADDRESS3, -1, GPDetails.Address_3 );

        GPDetails.Address_4 = etAddressD.getText()+"";
        t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_SURGERYADDRESS4, -1, GPDetails.Address_4 );

        GPDetails.tel = etTelephone.getText()+"";
        t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_SURGERYTEL, -1, GPDetails.tel );

        GPDetails.email = etEmail.getText()+"";
        t.setValue(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_SURGERYEMAIL, -1, GPDetails.email );

        Tools.toast("Saved.", c);
    }

    private void loadGPDetails()
    {

        _("LOADING USER DATA____________________");

        GPDetails.gpName = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_GPNAME, "", c);
        etGPName.setText(GPDetails.gpName);

        GPDetails.surgeryName  = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_SURGERYNAME, "", c);
        etSurgeryName.setText(GPDetails.surgeryName);

        GPDetails.Address_1  = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_SURGERYADDRESS1, "", c);
        etAddressA.setText(GPDetails.Address_1);

        GPDetails.Address_2  = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_SURGERYADDRESS2, "", c);
        etAddressB.setText(GPDetails.Address_2);

        GPDetails.Address_3  = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_SURGERYADDRESS3, "", c);
        etAddressC.setText(GPDetails.Address_3);

        GPDetails.Address_4  = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_SURGERYADDRESS4, "", c);
        etAddressD.setText(GPDetails.Address_4);

        GPDetails.tel  = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_SURGERYTEL, "", c);
        etTelephone.setText(GPDetails.tel);

        GPDetails.email  = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, SAVE_KEY_SURGERYEMAIL, "", c);
        etEmail.setText(GPDetails.email);

    }
*/
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////

    //a wrapper so we dont have to type this every time
    private void _(String s)
    {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", "SurgeryDetailsActivity" + "#######" + s);
    }

    //get the users location on this screen so that its ready if they click change nhs surgery, and we dont have to wait
    /**
     * Represents a geographical location.
     */
    protected Location mLastLocation;
    /**
     * Runs when a GoogleApiClient object successfully connects.
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        // Provides a simple way of getting a device's location and is well suited for
        // applications that do not require a fine-grained location and that do not need location
        // updates. Gets the best and most recent location currently available, which may be null
        // in rare cases when a location is not available.
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            double lat = mLastLocation.getLatitude();
            double lng = mLastLocation.getLongitude();
            NearbyNHSSurgeriesActivity.latS=""+lat;
            NearbyNHSSurgeriesActivity.lngS=""+lng;
            _("Location found: LAT"+lat+", LNG"+lng);
        } else {
            Toast.makeText(this, "Can't detect location! Is your GPS turned on?", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
       _("Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }


    @Override
    public void onConnectionSuspended(int cause) {
        // The connection to Google Play services was lost for some reason. We call connect() to
        // attempt to re-establish the connection.
        _("Connection suspended");
        mGoogleApiClient.connect();
    }
}
