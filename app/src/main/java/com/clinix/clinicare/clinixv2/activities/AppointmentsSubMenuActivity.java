/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.common.Tools;
import com.clinix.clinicare.clinixv2.dataobjs.Appointment;

public class AppointmentsSubMenuActivity extends Activity {

    Context c;
    Activity a;
    String idOfMyAppointments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _("AppointmentsSubMenuActivity Born!////////////////////////////////");
        c = this;
        a = this;
        setContentView(R.layout.appointments_details_menu);

        idOfMyAppointments = getIntent().getStringExtra("id");
        _("id passed into activity: " + idOfMyAppointments);

        ImageButton ibConsultationDetails = (ImageButton)    findViewById(R.id.ibConsultationDetails);
        ibConsultationDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ibConsultationDetails hit.");
                Intent intent = new Intent(c, ConsultationDetailsActivity.class);

                _("id to pass over:" + idOfMyAppointments);
                intent.putExtra("id", idOfMyAppointments);
                c.startActivity(intent);
                //// c.startActivity(intent);
            }
        });

        ImageButton ibDoctorsNotes = (ImageButton)    findViewById(R.id.ibDoctorsNotes);
        ibDoctorsNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ibDoctorsNotes hit.");
                Intent intent = new Intent(c, AppointmentDoctorsNotesActivity.class);
                _("id to pass over:"+idOfMyAppointments);
                intent.putExtra("id",idOfMyAppointments);
                c.startActivity(intent);
            }
        });

        ImageButton ibPrescription = (ImageButton)    findViewById(R.id.ibPrescription);
        ibPrescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ibPrescription hit.");
                Intent intent = new Intent(c, MedicationsDetailsActivity.class);//we can reuse the existing class.

                _("id to pass over:"+idOfMyAppointments);
                intent.putExtra("id",idOfMyAppointments);
                c.startActivity(intent);
                //// c.startActivity(intent);
            }
        });

        ImageButton ibUploadedImage = (ImageButton)    findViewById(R.id.ibUploadedImage);
        //grey this button out
        Drawable icon = Tools.convertDrawableToGrayScale(ibUploadedImage.getDrawable());
        ibUploadedImage.setImageDrawable(icon);
        //
        ibUploadedImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ibUploadedImage hit.");
                /*Intent intent = new Intent(c, ConsultationDetailsActivity.class);

                _("id to pass over:"+idOfMyAppointments);
                intent.putExtra("id",idOfMyAppointments);
                c.startActivity(intent);
                //// c.startActivity(intent);*/
            }
        });

        ImageButton ibLetters = (ImageButton)    findViewById(R.id.ibLetters);
        //grey this button out
        icon = Tools.convertDrawableToGrayScale(ibLetters.getDrawable());
        ibLetters.setImageDrawable(icon);
        //
        ibLetters.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ibLetters hit.");
                /*Intent intent = new Intent(c, ConsultationDetailsActivity.class);

                _("id to pass over:"+idOfMyAppointments);
                intent.putExtra("id",idOfMyAppointments);
                c.startActivity(intent);*/
                //// c.startActivity(intent);
            }
        });

        /////   ////
        //
        ////
        TextView tvUploadedImage = (TextView)    findViewById(R.id.tvUploadedImage);
        TextView tvLetters = (TextView)    findViewById(R.id.tvLetters);
        //

        //this was removed
        /*
        ImageButton idAboutClinix = (ImageButton)    findViewById(R.id.idAboutClinix);
        idAboutClinix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("idAboutClinix hit.");
                Intent intent = new Intent(c, AboutUsActivity.class);
                c.startActivity(intent);
            }
        });
*/

    }

    @Override
    public void onResume() {
        super.onResume();
        _("///////////////onResume/////////////");
       // Tools.sendAnalytics_ScreenView(this, "RegisterActivity");//do a screenview for analytics
    }

    //a wrapper so we dont have to type this every time
    private void _(String s)
    {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", "AppointmentsSubMenuActivity" + "#######" + s);
    }
}
