/*
* Clinix App for CliniCare Group.
* Developed by Gareth Murfin (www.garethmurfin.co.uk)
*/
package com.clinix.clinicare.clinixv2.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clinix.clinicare.clinixv2.R;
import com.clinix.clinicare.clinixv2.common.Networking;
import com.clinix.clinicare.clinixv2.common.Tools;
import com.clinix.clinicare.clinixv2.dataobjs.Allergy;
import com.clinix.clinicare.clinixv2.dataobjs.Medication;

public class MedicationsDetailsActivity extends FragmentActivity {

    Context c;
 ////////   public static ArrayList<Medication> itemListMedication;
   //// public ListView listView;
   ///// HistoryAdapter adapter;
    Activity a;

    Medication myMedication;
    String idOfMyMedication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.medication_details);
        c = this;
        a = this;
        idOfMyMedication = getIntent().getStringExtra("id");
        _("id passed into activity: " + idOfMyMedication);
        if (idOfMyMedication==null)
        {
            _("since idOfMyAllergy is null you must be adding a new allergy");
            TextView tvTitle = (TextView)    findViewById(R.id.tvTitle);
            tvTitle.setText("Add a Medication");
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        _("///////////////onResume/////////////");
        if (MedicationsActivity.itemListMedication == null)
        {
            _("itemListMedication needs to be fetched before we can show this info, doing that now");
            doNetworkingToGetMedications();

            //we need to call the assign method in the else from the networking which we do, which populates this screen in this instance
        }
        else
        {
            assignMyMedicationObject_AndPopulateGui();
            //populateGUI(); gets called in the assign
        }
        setupFakeActionBar();
    }

    //stole this from medicationsActivity
    public void doNetworkingToGetMedications()
    {
        _("doNetworkingToGetMedications=========================================");
        if (!Tools.isNetworkAvailable(this) )
        {
            Tools.toast("This app needs an active Internet connection to work.",c);
            return;
        }

        //Do networking!
        String messageForProgressBar = "Getting medications";
        String url = Networking.URL_GET_MY_PATIENT_MEDICATIONS;
        int nState = Networking.NETWORK_STATE_GET_MY_PATIENT_MEDICATIONS;
        Networking n = new Networking(messageForProgressBar, c, true);

        Tools t = new Tools(c);
        t.setupPrefs(c);
        ///String type = "patient"; // this can be admin / practitioner / patient
        // String id = t.getValueS(Tools.INTERNAL_MEMORY_SAVE_TYPE_STRING, "id",null,c);
        // n.setHistoryInfo(id);// we call this specific method for this state so right vars are set up.
        n.execute(url, nState);
    }

    public void assignMyMedicationObject_AndPopulateGui()
    {
        if (MedicationsActivity.itemListMedication==null)
        {
            _("WARNING!!!!!!! MedicationsActivity.itemListMedication IS NULL THIS WILL CRASH!!!");
        }
        for (int i=0; i<MedicationsActivity.itemListMedication.size(); i++)
        {
            Medication m = MedicationsActivity.itemListMedication.get(i);
            _("Looking for ID "+idOfMyMedication+". IS THIS IT? "+m.id);
            if (m.id.equals(idOfMyMedication))
            {
                _("History object assigned");
                myMedication = m;
            }
        }
        if (myMedication==null)
        {
            _("WARNING myHistory was not assigned, could not find id: " + idOfMyMedication);
        }

        populateGUI();// NOTICE THAT THE GUI IS POPULATE HERE FOR EASE so when networking call it it auto populates in this case
    }

    TextView tvDate;
    EditText tvDrugName;
    EditText tvQuantity;
    EditText tvCourse;
    EditText tvInstructions;
    private void populateGUI()
    {
        tvDate          = (TextView) findViewById(R.id.tvDate);
        tvDrugName      = (EditText) findViewById(R.id.tvDrugName);
        tvQuantity      = (EditText) findViewById(R.id.tvQuantity);
        tvCourse        = (EditText) findViewById(R.id.tvCourse);
        tvInstructions  = (EditText) findViewById(R.id.tvInstructions);

        tvDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tools.popupDateDialog(tvDate, tvDrugName, a, false);
            }
        });

        if (myMedication==null)
        {
            _("warning myHistory is still null! bailing.");
            return;
        }

        tvDate.setText(""+myMedication.date);
        tvDrugName.setText("" + myMedication.drug_name);
        tvQuantity.setText("" + myMedication.quantity);
        tvCourse.setText("" + myMedication.course);
        tvInstructions.setText("" + myMedication.instructions);

        //make it so they can tap notes to see a new screen
        /*tvNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(c, HistoryDetailsNotesActivity.class);
                intent.putExtra("id", myMedication.id);//pass over the id so we can get it from the list
                c.startActivity(intent);
            }
        });*/
    }


    //we use our own actionbar to avoid the crap real one
    private void setupFakeActionBar()
    {
        //fake actionbar menu button
        //we use this for:
        //-change password
        //-save
        //
        //This is basically a fake menu that pops up
        final LinearLayout fakeActionBarButtonHolder = (LinearLayout) findViewById(R.id.fakeActionBarButtonHolder);
        //hide it right away
        fakeActionBarButtonHolder.setVisibility(View.GONE);

        ImageButton ibMenu = (ImageButton)    findViewById(R.id.ibMenu);
        ibMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("ibMenu hit.");
                Tools.toggleActionBarMenu(fakeActionBarButtonHolder);
            }
        });
        //and its buttons:
        Button btA = (Button)    findViewById(R.id.btA);
        btA.setText(" Save ");
        btA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _("Save Allergy btA hit.");
                //ADD/SAVE MEDICATION ON SERVER
                doNetworkingToAddMedication();
                Tools.toggleActionBarMenu(fakeActionBarButtonHolder);
            }
        });
        Button btB = (Button)    findViewById(R.id.btB);
        btB.setVisibility(View.GONE);
    }

    private void doNetworkingToAddMedication()
    {
        _("doNetworkingToAddAnAllergy=========================================");
        if (!Tools.isNetworkAvailable(this) )
        {
            Tools.toast("This app needs an active Internet connection to work.",c);
            return;
        }

        //Do networking!
        String messageForProgressBar = "Adding Medication";
        String url = Networking.URL_CREATE_A_MEDICATION_FOR_MY_PATIENT;
        int nState = Networking.NETWORK_STATE_CREATE_A_MEDICATION_FOR_MY_PATIENT;
        Networking n = new Networking(messageForProgressBar, c, true);

        Tools t = new Tools(c);
        t.setupPrefs(c);

        //make a new one with whatever info we know, leave stuff we dont know blank.
        String id_              = "";
        String user_id          = ProfileActivity.myProfile.id;//YEH?
        String practitioner_id_ = "";
        String drug_name_       = ""+tvDrugName.getText();
        String quantity_        = ""+tvQuantity.getText();
        String course_          = ""+tvCourse.getText();
        String instructions_    = "";
        String date_            = tvDate.toString();

        Medication m = new Medication(id_,user_id,practitioner_id_,drug_name_,quantity_,course_,instructions_,date_);
        n.setNewMedicationDetails(m);// we call this specific method for this state so right vars are set up.
        n.execute(url, nState);
    }

    //a wrapper so we dont have to type this every time
    private void _(String s) {
        if (!Tools.DEBUG)
        {
            return;
        }
        Log.d("MyApp", "MedicationsDetailsActivity" + "#######" + s);
    }
}
