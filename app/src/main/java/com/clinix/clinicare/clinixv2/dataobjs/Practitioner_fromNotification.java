package com.clinix.clinicare.clinixv2.dataobjs;

import android.util.Log;

import com.clinix.clinicare.clinixv2.common.Tools;

/**
 * Created by USER on 11/16/2015.
 */
public class Practitioner_fromNotification
{
    public String id;
    public String title;
    public String forename="";
    public String surname="";;
    public String dob;
    public String suffix;
    public String email;
    public String register_date;
    public String telephone;
    public String street;
    public String address_2;
    public String town;
    public String county;
    public String postcode;
    public String country;
    public String type;
    public String surgery_ids;
    public String account_status;
    public String profession;
    public String specialty;
    public String registered_date;
    public String information;
    public String information_date;
    public String signature;
    public String signature_date;
    public String initial;
    public String follow_up;
    public String image;
    public String status;
    public String category;
    public String service_type;
    public String insurer;
    public String policy_no;
    public String gmc_no;

    public Practitioner_fromNotification
            (
                    String id_,
                    String title_,
                    String forename_,
                    String surname_,
                    String dob_,
                    String suffix_,
                    String email_,
                    String register_date_,
                    String telephone_,
                    String street_,
                    String address_2_,
                    String town_,
                    String county_,
                    String postcode_,
                    String country_,
                    String type_,
                    String surgery_ids_,
                    String account_status_,
                    String profession_,
                    String specialty_,
                    String registered_date_,
                    String information_,
                    String information_date_,
                    String signature_,
                    String signature_date_,
                    String initial_,
                    String follow_up_,
                    String image_,
                    String status_,
                    String category_,
                    String service_type_,
                    String insurer_,
                    String policy_no_,
                    String gmc_no_
            )
    {
        id=id_;
        title=title_;
        forename=forename_;
        surname=surname_;
        dob=dob_;
        suffix=suffix_;
        email=email_;
        register_date=register_date_;
        telephone=telephone_;
        street=street_;
        address_2=address_2_;
        town=town_;
        county=county_;
        postcode=postcode_;
        country=country_;
        type=type_;
        surgery_ids=surgery_ids_;
        account_status=account_status_;
        profession=profession_;
        specialty=specialty_;
        registered_date=registered_date_;
        information=information_;
        information_date=information_date_;
        signature=signature_;
        signature_date=signature_date_;
        initial=initial_;
        follow_up=follow_up_;
        image=image_;
        status=status_;
        category=category_;
        service_type=service_type_;
        insurer=insurer_;
        policy_no=policy_no_;
        gmc_no=gmc_no_;
    }

    //a wrapper so we dont have to type this every time
    private void _(String s)
    {
        if (!Tools.DEBUG){return;}
        Log.d("MyApp", "Practitioner_fromNotification" + "#######" + s);
    }
}
